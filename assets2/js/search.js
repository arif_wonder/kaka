// function mySearchFunction()
// {
// 	$("#mySearch").text("searching...");
// 	$("#forms-search").submit();
// }

// ADDED BY FARHAN
// SEARCH FIELD

var IsplaceChange = false;
    $(document).ready(function () {            
        var input = document.getElementById("s");
        var options = {
         types: ['geocode'],
         componentRestrictions: {country: "in"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            IsplaceChange = true;
        });

        $("#s").keydown(function () {
            IsplaceChange = false;
        });

        $("#forms-search").submit(function () {

            var search = $("#s").val();
            if(search == '')
            {
                swal("Address field is required");
                return false;
            }

            var address = search.split(", ");
            var lastEl = address[address.length-1];
            if(lastEl == 'India')
            {   
                $("#mySearch").text("searching...");
                IsplaceChange = true;
            }
            if (IsplaceChange == false) {
                $("#s").val('');
                swal("Please select your location from dropdown list");
                return false;
            }
          

        });



       // CONTACT

        $("#myContactButton").click(function(){
            var name = $("#forms #name").val();
            var email = $("#forms #email").val();
            var phone = $("#forms #phone").val();
            var message = $("#forms #message").val();
            
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if(reg.test(email) != false && name != '' && email != '' && phone != '' && message != '')
            {
                    $("#myContactButton").hide();
                   // $("#contactBtn").html("<p>Your message was sent successfully. Thanks.</p>")

            }     
        });

       
        // POPUP
        //   check cookie
        var cookieTime = $("#cookieTime").val();
        var status = $("#farhanStatus").val();
        var visited = $.cookie("visited");
        
        if (visited == null && status == 1) {
            
            $("#myModal").modal();
            var date = new Date();

           switch (cookieTime) {
               case '0':
                    var minutes = 30;
                    date.setTime(date.getTime() + (minutes * 60 * 1000));
                    $.cookie('visited', 'yes', { expires: date});         
                   break;
               case '1':
                    var minutes = 60;
                    date.setTime(date.getTime() + (minutes * 60 * 1000));
                    $.cookie('visited', 'yes', { expires: date});
                   break;
               case '2':
                   
                    $.cookie('visited', 'yes', { expires: 1});
                   break;
               case '3':
                   $.cookie('visited', 'yes', { expires: 3});
                   break;
               case '4':
                   $.cookie('visited', 'yes', { expires: 7});
                   break;
           }
        }

 var options = {
    url: "https://www.speedypixelgame.com/kakatesting/store/MerchantJson",

    getValue: "restaurant_name",
    minCharNumber:3,
    list: {
        match: {
            enabled: true
        },
        onClickEvent: function() {
            $('#searchRestaurant').trigger('click');;
        }
    }
};

$("#restaurant_name").easyAutocomplete(options);      

// ADDED BY FARHAN 6 AUG 2018
  
$("#s").focus(function(){
    $("#s").val('');
});



$("#s").blur(function(){

    var s = $("#hidden_s").val();
    $("#s").val(s);

});

// END BY FARHAN



});

    /*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD (Register as an anonymous module)
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch(e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {},
            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling $.cookie().
            cookies = document.cookie ? document.cookie.split('; ') : [],
            i = 0,
            l = cookies.length;

        for (; i < l; i++) {
            var parts = cookies[i].split('='),
                name = decode(parts.shift()),
                cookie = parts.join('=');

            if (key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, { expires: -1 }));
        return !$.cookie(key);
    };

}));

$("#contact_phone").keyup(function() {
    $("#contact_phone").val(this.value.match(/[0-9]*/));
   
});
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

    var url = window.location.href;

    var pieces = url.split("/");
    str1 = pieces[5];
    str2 = 'searcharea';
    if(str1.indexOf(str2) != -1){
            

                $(document).scroll(function () {
                var y = $(this).scrollTop();
                if (y > 600) {
                    $('.topSearch').show();
                } else {
                    $('.topSearch').hide();
                }
             // some code..
            });
    }


}

// ADDED BY FARHAN 4 SEPT 2018

function cstm_taxes()
{
   $("#cstm_breakup").slideToggle();
}



function run()
{

var geocoder = new google.maps.Geocoder();
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
    } 


    //Get the latitude and the longitude;
function successFunction(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    codeLatLng(lat, lng)
}

function errorFunction(){
    alert("Geocoder failed");
}

  function initialize() {
    geocoder = new google.maps.Geocoder();



  }

  function codeLatLng(lat, lng) {

    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      $("#s").val(results[0]['formatted_address']);



      } else {
        alert("Geocoder failed due to: " + status);
      }
    });
  }
    
}


// $(document).ready(function () {

// $('#payment_opt').iCheck('check');

//    // For oncheck callback
// $('input').on('ifChecked', function () {

//     var val = this.value;
//     var baseUrl = $('#baseUrl').val();

//     $.post(baseUrl+'/store/ChangeDeliveryFee',{"val":val}, function(data){

//         location.reload();

//         // $(".summary-wrap .row:nth-child(2) div:nth-child(2)").html('₹ '+data);
//         // $(".cart")
//     });
    
// })
// });
// END BY FARHAN