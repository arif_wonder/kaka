<?php

/**
 * Default 404 Page
 */


get_header();

?>

<div class="main wrap cf">
	<div class="ts-row">
		<div class="page">
		
			<header class="post-heading">
				<h1 class="main-heading"><?php _e('Page Not Found!', 'bunyad'); ?></h1>
			</header>
		
			<div class="post-content error-page row">
				
				<div class="col-3 text-404 main-color">404</div>
				
				<div class="col-8 post-content text-font">
					<p>
					<?php _e("We're sorry, but we can't find the page you were looking for. It's probably some thing we've done wrong but now we know about it and we'll try to fix it. In the meantime, try one of these options:", 'bunyad'); ?>
					</p>
					<ul class="links fa-ul">
						<li><i class="fa fa-angle-double-right"></i> <a href="javascript: history.go(-1);"><?php _e('Go to Previous Page', 'bunyad'); ?></a></li>
						<li><i class="fa fa-angle-double-right"></i> <a href="<?php echo esc_url(site_url()); ?>"><?php _e('Go to Homepage', 'bunyad'); ?></a></li>
					</ul>
				</div>
				
			</div>

		</div>
		
	</div> <!-- .ts-row -->
</div> <!-- .main -->

<?php get_footer(); ?>