/**
 * Extend Visual Composer "Params" attributes
 *  
 * Ref: composer-atts.js
 */
jQuery(function($) {
	vc.atts.checkbox_ext = vc.atts.checkbox;
});