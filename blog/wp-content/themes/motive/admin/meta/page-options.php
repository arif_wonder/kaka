<?php 
/**
 * Options metabox for pages
 */

$options = array(
	array(
		'label' => __('Layout Style', 'bunyad'),
		'name'  => 'layout_style', // will be _bunyad_layout_style
		'type'  => 'radio',
		'options' => array(
			'' => __('Default', 'bunyad'),
			'right' => __('Right Sidebar', 'bunyad'),
			'full' => __('Full Width', 'bunyad')),
		'value' => '' // default
	),
	
	array(
		'label' => __('Show Page Title?', 'bunyad'),
		'name'  => 'page_title', 
		'type'  => 'select',
		'options' => array('yes' => 'Yes', 'no' => 'No'),
		'value' => 'yes' // default
	),
	
	array(
		'label' => __('Show Breadcrumbs?', 'bunyad'),
		'name'  => 'show_breadcrumbs', 
		'type'  => 'select',
		'options' => array('yes' => __('Yes', 'bunyad'), 'no' => __('No', 'bunyad')),
		'value' => 'yes' // default
	),
	
	array(
		'label' => __('Show Featured Slider?', 'bunyad'),
		'name'  => 'featured_slider',
		'type'  => 'select',
		'options' => array(
			''	=> __('None', 'bunyad'),
			'grid' => __('Featured Posts Grid', 'bunyad'),
			'main' => __('Full-width Classic Slider', 'bunyad'),
			'split' => __('Split Slider - Slider Left, 2 Posts Grid Right', 'bunyad'),
		),
		'value' => '' // default
	),
	
	array(
		'label' => __('Featured Slider Type', 'bunyad'),
		'name'  => 'slider_type',
		'type'  => 'select',
		'options' => array(
			'' => __('Use Posts Marked as "Featured Slider Post?"', 'bunyad'),
			'latest' => __('Use Latest Posts from Whole Site', 'bunyad'),
		),
		'value' => '' // default
	),
	
	array(
		'label' => __('Slider Limit by Tag', 'bunyad'),
		'name'  => 'slider_tags',
		'desc'  => __('Optional: To limit slider to certain tag or tags. If multiple, separate tag slugs by comma.', 'bunyad'),
		'type'  => 'text',
		'value' => '' // default
	),
	
	array(
		'label' => __('Slider Manual Post Ids', 'bunyad'),
		'name'  => 'slider_posts',
		'desc'  => __('Optional: ADVANCED! If you only want to show a set of selected pre-selected posts. Enter post ids separated by comma.', 'bunyad'),
		'type'  => 'text',
		'value' => '' // default
	),
	
	
);

if (Bunyad::options()->layout_style == 'boxed') {
	
	$options[] = array(
		'label' => __('Custom Background Image', 'bunyad'),
		'name'  => 'bg_image',
		'type' => 'upload',
		'options' => array(
				'type'  => 'image',
				'title' => __('Upload This Picture', 'bunyad'), 
				'button_label' => __('Upload',  'bunyad'),
				'insert_label' => __('Use as Background',  'bunyad')
		),	
		'value' => '', // default
		'bg_type' => array('value' => 'cover'),
	);
}

$options = $this->options(apply_filters('bunyad_metabox_page_options', $options));

?>

<div class="bunyad-meta cf">

<?php foreach ($options as $element): ?>
	
	<div class="option <?php echo esc_attr($element['name']); ?>">
		<span class="label"><?php echo esc_html($element['label']); ?></span>
		<span class="field">
			<?php echo $this->render($element); ?>
		
			<?php if (!empty($element['desc'])): ?>
			
			<p class="description"><?php echo esc_html($element['desc']); ?></p>
		
			<?php endif;?>
		
		</span>		
	</div>
	
<?php endforeach; ?>

</div>

<?php wp_enqueue_script('theme-options', get_template_directory_uri() . '/admin/js/options.js', array('jquery')); ?>

<script>
/**
 * Conditional show/hide 
 */
jQuery(function($) {
	$('._bunyad_featured_slider select').on('change', function() {

		var depend = '._bunyad_slider_type, ._bunyad_slider_tags, ._bunyad_slider_posts';
		
		($(this).val() != '' ? 	$(depend).show() : $(depend).hide());
	});

	// on-load
	$('._bunyad_featured_slider select').trigger('change');
		
});
</script>