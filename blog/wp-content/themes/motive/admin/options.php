<?php
/**
 * Theme Settings - All the relevant options!
 */

/**
 * Theme Options Array
 * 
 * Use filter bunyad_theme_options to change values, if needed.
 *  
 * @uses apply_filters()
 * @uses Bunyad_Admin_Options::render_options()
 */
$options = apply_filters('bunyad_theme_options', array(
	array(
		'title' => __('General Settings', 'bunyad'),
		'id'    => 'options-tab-global',
		'icon'  => 'dashicons-admin-generic',
		'sections' => array(
			array(
				'fields' => array(
			
					array(
						'name'   => 'layout_style',
						'value' => 'boxed',
						'label' => __('Layout Style', 'bunyad'),
						'desc'  => __('Select whether you want a boxed or a non-boxed container. It affects every page and the whole layout.', 'bunyad'),
						'type'  => 'select',
						'options' => array(
							'full' => __('Non-Boxed', 'bunyad'),
							'boxed' => __('Boxed', 'bunyad'),
						),
					),

					array(
						'name' => 'default_sidebar',
						'label'   => __('Default Sidebar', 'bunyad'),
						'value'   => 'right',
						'desc'    => __('Specify the sidebar to use by default. This can be overriden per-page or per-post basis when creating a page or post.', 'bunyad'),
						'type'    => 'radio',
						'options' =>  array('none' => __('No Sidebar', 'bunyad'), 'right' => __('Right Sidebar', 'bunyad'))
					),
					
					array(
						'name'   => 'no_responsive',
						'value' => 0,
						'label' => __('Disable Responsive Layout', 'bunyad'),
						'desc'  => __('Disabling responsive layout means mobile phones and tablets will no longer see a better optimized design. Do not disable this unless really necessary.', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'   => 'header_custom_code',
						'value' => '',
						'label' => __('Head Code', 'bunyad'),
						'desc'  => esc_html(__('This code will be placed before </head> tag in html. Useful if you have an external script that requires it.', 'bunyad')),
						'type'  => 'textarea',
						'options' => array('cols' => 75, 'rows' => 10),
						'strip' => 'none',
					),
				
					array(
						'name'   => 'footer_custom_code',
						'value' => '',
						'label' => __('Footer Code', 'bunyad'),
						'desc'  => esc_html(__('This code will be placed before </body> tag in html. Use for Google Analytics or similar external scripts.', 'bunyad')),
						'type'  => 'textarea',
						'options' => array('cols' => 75, 'rows' => 10),
						'strip' => 'none',
					),
					
					array(
						'name'   => 'search_posts_only',
						'value' => 1,
						'label' => __('Limit Search To Posts', 'bunyad'),
						'desc'  => __('WordPress, by default, uses pages and other posts types to display search results. Enabling this feature will limit it to posts only.', 'bunyad'),
						'type'  => 'checkbox'
					),
					
				),
			), // end section		
			
			array(
				'title'  => __('Favicons (optional)', 'bunyad'),
				'fields' => array(
					array(
						'name'  => 'favicon',
						'label' => __('Favicon', 'bunyad'),
						'desc'  => __('32x32px recommended. IMPORTANT: .ico file only!', 'bunyad'),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload Favicon (.ico file)', 'bunyad'), 
							'insert_label' => __('Use As Favicon', 'bunyad')
						),
					),
					
					array(
						'name'  => 'apple_icon',
						'label' => __('Mobile Icon (iOS, Android etc.)', 'bunyad'),
						'desc'  => __('152x152 recommended in PNG format. This icon will be used when users add your '
								. 'website as a shortcut on mobile devices like iPhone, iPad, Android etc.', 'bunyad'),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload Mobile Icon', 'bunyad'), 
							'insert_label' => __('Use As Mobile Icon',  'bunyad')
						),
					),
				)
			), // end section
						
		), // end sections
	),
	
	array(
		'title' => __('Header & Navigation', 'bunyad'),
		'id'    => 'options-header',
		'icon'  => 'dashicons-welcome-widgets-menus',
		'sections' => array(
	
			array(
				'fields' => array(
					array(
						'name'  => 'header_style',
						'value' => 'centered',
						'label' => __('Header Style', 'bunyad'),
						'desc'  => __('Select the header style you want to use.', 'bunyad'),
						'type'  => 'radio',
						'options' => array(
							'' => __('Left Logo + Right Ad', 'bunyad'),
							'centered'   => __('Centered Logo', 'bunyad'),
						),
						'events' => array('change' => array('value' => 'centered', 'actions' => array('show' => 'header_date')))
					),
					
					array(
						'name'  => 'header_date',
						'value' => 0,
						'label' => __('Show Date', 'bunyad'),
						'desc'  => __('Show date in header above logo?', 'bunyad'),
						'type'  => 'checkbox'
					),
				)
			),
			
			array(
				'title'  => __('Logo', 'bunyad'),
				'fields' => array(
			
					array(
						'name'    => 'text_logo',
						'label'   => __('Logo Text', 'bunyad'),
						'desc'    => __('It will be used if logo images are not provided below.', 'bunyad'),
						'value'   => get_bloginfo('name'),
						'type'    => 'text',
					),
					
					array(
						'name'    => 'slogan_logo',
						'label'   => __('Logo Slogan (For Text Logo)', 'bunyad'),
						'desc'    => __('Enter the slogan you wish to use for your logo. Note: It is different from Tagline.', 'bunyad'),
						'value'   => get_bloginfo('description'),
						'type'    => 'text',
					),
			
					array(
						'name'    => 'image_logo',
						'label'   => __('Logo Image', 'bunyad'),
						'desc'    => __('By default, a text-based logo is created using your site title. But you can upload an image-based logo here.', 'bunyad'),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload This Picture', 'bunyad'), 
							'insert_label' => __('Use As Logo', 'bunyad')
						),
					),
					
					array(
						'name'    => 'image_logo_retina',
						'label'   => __('Logo Image Retina (2x)', 'bunyad'),
						'desc'    => __('The retina version is 2x version of the same logo above. This will be used for higher resolution devices like iPhone. Requires WP Retina 2x plugin.', 'bunyad'),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload This Picture', 'bunyad'), 
							'insert_label' => __('Use As Logo', 'bunyad')
						),
					),
										
					array(
						'name'  => 'sticky_nav_logo',
						'value' => 0,
						'label' => __('Enable Sticky Nav Logo', 'bunyad'),
						'desc'  => __('Enabling this adds your logo to the sticky navigation. For image-based logos, you will have to upload a different logo due to color difference.', 'bunyad'),
						'type'  => 'checkbox',
						'events' => array('change' => array('value' => 'checked', 'actions' => array('show' => 'image_logo_nav')))
					),
					
					array(
						'name'    => 'image_logo_nav',
						'label'   => __('Sticky Nav Logo Image (Optional)', 'bunyad'),
						'desc'    => __('This logo image will be used in sticky navigation. Ignore this if you use a text-based logo. Recommended Size: 43x75 - PNG with transparent background.', 'bunyad'),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload This Picture', 'bunyad'), 
							'insert_label' => __('Use As Logo', 'bunyad')
						),
						// conditionally shown by sticky_nav_logo 
					),					
				)
				
			), // end section
			
									
			array(
				'title'  => __('Top Bar', 'bunyad'),
				'fields' => array(
			
					array(
						'name'  => 'topbar_style',
						'value' => 'dark',
						'label' => __('Style', 'bunyad'),
						'desc'  => __('Select the style you want to use.', 'bunyad'),
						'type'  => 'radio',
						'options' => array(
							'' => __('Light Style', 'bunyad'),
							'dark'   => __('Dark Style', 'bunyad'),
						),
					),
			
					array(
						'name'  => 'disable_topbar',
						'value' => 0,
						'label' => __('Disable Top Bar', 'bunyad'),
						'desc'  => __('Setting this to yes will disable the top bar element that appears above the logo area.', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'  => 'disable_topbar_ticker',
						'value' => 0,
						'label' => __('Disable Top News Ticker', 'bunyad'),
						'desc'  => __('Setting this to yes will disable the top bar news ticker', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'  => 'topbar_ticker_text',
						'value' => __('Breaking', 'bunyad'),
						'label' => __('Topbar Ticker Text', 'bunyad'),
						'desc'  => __('Enter the text you wish to display before the headlines in the ticker.', 'bunyad'),
						'type'  => 'text'
					),
				)
			), // end section 
			
												
			array(
				'title'  => __('Navigation', 'bunyad'),
				'fields' => array(
								
					array(
						'name'  => 'nav_style',
						'value' => 'alt',
						'label' => __('Style', 'bunyad'),
						'desc'  => __('Select the style you want to use.', 'bunyad'),
						'type'  => 'radio',
						'options' => array(
							'' => __('Dark Contrast', 'bunyad'),
							'alt'   => __('Light Style', 'bunyad'),
						),
					),
					
					array(
						'name'  => 'nav_align',
						'value' => 'nav-center',
						'label' => __('Alignment', 'bunyad'),
						'desc'  => __('You can center the top-level items or left-align them.', 'bunyad'),
						'type'  => 'radio',
						'options' => array(
							'' => __('Left', 'bunyad'),
							'nav-center'   => __('Centered', 'bunyad'),
						),
					),
					
					array(
						'name' => 'sticky_nav',
						'value' => 0,
						'label' => __('Sticky Navigation', 'bunyad'),
						'desc'  => __('This makes navigation float at the top when the user scrolls below the fold - essentially making navigation menu always visible.', 'bunyad'),
						'type'  => 'checkbox',
					),
					
					array(
						'name' => 'disable_breadcrumbs',
						'value' => 0,
						'label' => __('Disable Breadcrumbs', 'bunyad'),
						'desc'  => __('Breadcrumbs are a hierarchy of links displayed below the main navigation. They are displayed on all pages but the home-page.', 'bunyad'),
						'type'  => 'checkbox',
					),
					
					array(
						'name' => 'mobile_nav_search',
						'value' => 1,
						'label' => __('Enable Search on Mobile Menu', 'bunyad'),
						'desc'  => __('Disabling this will remove the search icon from the mobile navigation menu.', 'bunyad'),
						'type'  => 'checkbox',
					),
				)
				
			), // end section

		), // end sections
	),
	
	array(
		'title' => __('Footer Settings', 'bunyad'),
		'id'    => 'options-footer',
		'icon'  => 'dashicons-feedback',
		'sections' => array(
	
			array(
				'fields' => array(

					array(
						'name'  => 'footer_style',
						'label' => __('Footer Style', 'bunyad'),
						'desc'  => __('There are two types of footers available, light or dark.', 'bunyad'),
						'value' => 'dark',
						'type'  => 'select',
						'options' => array(
							'dark'   => __('Dark Style', 'bunyad'),
							'light' => __('Light Style', 'bunyad'),
						),
					),
			
					array(
						'name'  => 'disable_footer',
						'label' => __('Disable Large Footer', 'bunyad'),
						'desc'  => __('Setting this to yes will disable the large footer that appears above the lowest footer. Used to contain large widgets.', 'bunyad'),
						'type'  => 'checkbox'
					),
				
					array(
						'name'  => 'disable_middle_footer',
						'label' => __('Disable Middle Footer', 'bunyad'),
						'desc'  => __('Setting this to yes will disable the middle footer.', 'bunyad'),
						'type'  => 'checkbox' 
					),
					
					array(
						'name'  => 'disable_lower_footer',
						'label' => __('Disable Lower Footer', 'bunyad'),
						'desc'  => __('Setting this to yes will disable the smaller footer at bottom.', 'bunyad'),
						'type'  => 'checkbox' 
					),
			
					array(
						'name'  => 'footer_cols_upper',
						'value' => '1/3+1/3+1/3',
						'label' => __('Upper Footer Columns ', 'bunyad'),
						'desc'  => __('Sets the columns width and number of columns. Other examples: 1/2+1/2, 1/4+1/4+1/2', 'bunyad'),
						'type'  => 'text'
					),
					
					array(
						'name'  => 'footer_cols_middle',
						'value' => '1/2+1/2',
						'label' => __('Middle Footer Columns', 'bunyad'),
						'desc'  => __('Sets the columns width and number of columns. Other examples: 1/2+1/2, 1/4+1/4+1/2', 'bunyad'),
						'type'  => 'text'
					),
				)
			), // end section

		), // end sections
	),
			
	
	array(
		'title' => __('Listing Layouts', 'bunyad'),
		'id'    => 'options-listing-layouts',
		'icon'  => 'dashicons-list-view',
		'sections' => array(
	
			array(
				'fields' => array(
			
					array(
						'name' => 'default_cat_template',
						'label'   => __('Default Category Style', 'bunyad'),
						'value'   => 'grid-2',
						'desc'    => __('The style to use for listing while browsing categories. This can be overriden while creating or editing a category.', 'bunyad'),
						'type'    => 'select',
						'options' =>  array(
							'grid-2' => __('Grid Style - 2 Column', 'bunyad'),
							'grid-3' => __('Grid Style - 3 Column', 'bunyad'),
							'alt' => __('Blog List Style', 'bunyad'),
							'classic'  => __('Classic - Large Blog Style', 'bunyad'),
						)
					),

					array(
						'name' => 'author_loop_template',
						'label'   => __('Author Listing Style', 'bunyad'),
						'value'   => 'grid-2',
						'desc'    => __('This style is used while browsing author page.', 'bunyad'),
						'type'    => 'select',
						'options' =>  array(
							'grid-2' => __('Grid Style - 2 Column', 'bunyad'),
							'grid-3' => __('Grid Style - 3 Column', 'bunyad'),
							'alt' => __('Blog List Style', 'bunyad'),
							'classic'  => __('Classic - Large Blog Style', 'bunyad'),
						)
					),
				
					array(
						'name' => 'archive_loop_template',
						'label'   => __('Archive Listing Style', 'bunyad'),
						'value'   => 'grid-2',
						'desc'    => __('This style is used while browsing author page, searching, default blog format, date archives etc.', 'bunyad'),
						'type'    => 'select',
						'options' =>  array(
							'grid-2' => __('Grid Style - 2 Column', 'bunyad'),
							'grid-3' => __('Grid Style - 3 Column', 'bunyad'),
							'alt' => __('Blog List Style', 'bunyad'),
							'classic'  => __('Classic - Large Blog Style', 'bunyad'),
						)
					),
					
					array(
						'name'   => 'read_more',
						'value'  => 1,
						'label'  => __('Enable "Read More"', 'bunyad'),
						'desc'   => __('This is global setting for read more. If this is disabled, the individual settings below will not apply.', 'bunyad'),
						'type'   => 'checkbox'
					),
					
					array(
						'name' => 'listing_meta',
						'label'   => __('Meta In Listings?', 'bunyad'),
						'value'   => array('date' => 1, 'category' => 0, 'review' => 1),
						'desc'    => __('Select which meta to show in post listings.', 'bunyad'),
						'type'    => 'checkbox',
						'multiple' => array(
							'date' => __('Date', 'bunyad'),
							'category' => __('Category', 'bunyad'),
							'review'   => __('Rating (If Review)', 'bunyad'),
						),
					),
				)
			), // end section
	
			array(
				'title'  => __('Grid - 2 Column', 'bunyad'),
				'fields' => array(

					array(
						'name'   => 'excerpt_length_grid_2',
						'value'  => 15,
						'label'  => __('Excerpt Length', 'bunyad'),
						'desc'   => __('Set the excerpt length for this listing. By default, it is a word count length.', 'bunyad'),
						'type'   => 'number',
					),
					
				)
			), // end section
			
			array(
				'title'  => __('Grid - 3 Column', 'bunyad'),
				'fields' => array(

					array(
						'name'   => 'excerpt_length_grid_3',
						'value'  => 15,
						'label'  => __('Excerpt Length', 'bunyad'),
						'desc'   => __('Set the excerpt length for this listing. By default, it is a word count length.', 'bunyad'),
						'type'   => 'number',
					),
					
				)
			), // end section
			
			array(
				'title'  => __('Classic Large Blog', 'bunyad'),
				'fields' => array(

					array(
						'name'   => 'show_excerpts_classic',
						'value'  => 1,
						'label'  => __('Enable Excerpts', 'bunyad'),
						'desc'   => sprintf(__('By default whole post is displayed unless %s is used in posts. When excerpts are enabled, manual or automatic excerpt is used.', 'bunyad'), '&lt;!--more--&gt;'),
						'type'   => 'checkbox',
						'events' => array('change' => array('value' => 'checked', 'actions' => array('show' => 'excerpt_length_classic')))
					),
			
					array(
						'name'   => 'excerpt_length_classic',
						'value'  => 25,
						'label'  => __('Excerpt Length', 'bunyad'),
						'desc'   => __('Set the excerpt length for this listing. By default, it is a word count length.', 'bunyad'),
						'type'   => 'number'
					),					
					
					array(
						'name'   => 'social_icons_classic',
						'value'  => 0,
						'label'  => __('Show Social Icons', 'bunyad'),
						'desc'   => __('Enabling this will show social icons in this listing style. Requires: Social icons to be enabled for single pages.', 'bunyad'),
						'type'   => 'checkbox'
					),

					array(
						'name'   => 'show_tags_classic',
						'value'  => 1,
						'label'  => __('Show Tags', 'bunyad'),
						'desc'   => __('Enabling this will show tags in this listing style. Requires: Tags to be enabled for single pages.', 'bunyad'),
						'type'   => 'checkbox'
					),
					
				)
			), // end section
			
			array(
				'title'  => __('Blog List', 'bunyad'),
				'fields' => array(

					array(
						'name'   => 'excerpt_length_alt',
						'value'  => 20,
						'label'  => __('Excerpt Length', 'bunyad'),
						'desc'   => __('Set the excerpt length for this listing. By default, it is a word count length.', 'bunyad'),
						'type'   => 'number'
					),
										
					array(
						'name'   => 'read_more_alt',
						'value'  => 1,
						'label'  => __('Show "Read More"', 'bunyad'),
						'desc'   => __('Show read "More" links in listings of this type?', 'bunyad'),
						'type'   => 'checkbox'
					),
					
				)
			), // end section
									
		), // end sections
	),
	
	array(
		'title' => __('Homepage', 'bunyad'),
		'id'    => 'options-homepage-blocks',
		'icon'  => 'dashicons-admin-home',
		'sections' => array(
		
			array(
				'fields' => array(

					array(
						'name'   => 'no_home_duplicates',
						'value'  => 0,
						'label'  => __('No Duplicate Posts In Homepage Blocks?', 'bunyad'),
						'desc'   => __('If you have a lot of content or when you are using latest posts slider, you can see duplicate in featured area and homepage blocks. Setting this feature to Yes will remove duplicates.', 'bunyad'),
						'type'   => 'checkbox'
					),
					
					array(
						'label' => __('Current Homepage', 'bunyad'),
						'type'  => 'file',
						'desc'  => __('It is recommened to create a new page - using the page builder - and then set it as your Front Page.', 'bunyad'),
						'render'  => 'admin/options/field-home.php',
					),
					
					array(
						'label' => __('Homepage Slider', 'bunyad'),
						'type'  => 'file',
						'desc'  => '',
						'render'  => 'admin/options/field-slider.php',
					)
				)
			), // end section
									
		), // end sections
	),
	
	array(
		'title' => __('Posts & Reviews', 'bunyad'),
		'id'    => 'options-specific-pages',
		'icon'  => 'dashicons-admin-post',
		'sections' => array(
	
			array(
				'title'  => __('Single Post / Article Page', 'bunyad'),
				'fields' => array(

					array(
						'name'   => 'enable_lightbox',
						'value'  => 1,
						'label'  => __('Enable Lightbox', 'bunyad'),
						'desc'   => __('When enabled, lightbox will auto-bind to images such as featured images, WordPress galleries etc.', 'bunyad'),
						'type'   => 'checkbox'
					),
			
					array(
						'name'   => 'show_featured',
						'value'  => 1,
						'label'  => __('Show Featured', 'bunyad'),
						'desc'   => __('Disabling featured area will mean the featured image or video will no longer show at top of the article.', 'bunyad'),
						'type'   => 'checkbox'
					),
					
					array(
						'name'   => 'css_first_paragraph',
						'value'  => '',
						'label'  => __('First Paragraph', 'bunyad'),
						'desc'   => __('By default, the first paragraph is treated as a summary and thus emphasized with larger font.', 'bunyad'),
						'css'    => array('selectors' => 
							array('.post .post-content > p:first-child' => 'font-size: inherit; color: inherit; line-height: inherit;')
						),
						'type'   => 'radio',
						'options' => array(
							'' => __('Emphasized with larger font', 'bunyad'),
							'normal' => __('Normal - same as other paragraphs', 'bunyad'),
						)
					),
			
					array(
						'name'   => 'show_tags',
						'value'  => 1,
						'label'  => __('Show Tags', 'bunyad'),
						'desc'   => __('Show tags below posts? We recommend using categories instead of tags.', 'bunyad'),
						'type'   => 'checkbox'
					),
					
					array(
						'name'  => 'social_share',
						'value' => 1,
						'label' => __('Show Social Sharing', 'bunyad'),
						'desc'  => __('Show twitter, facebook, etc. share images beneath posts?', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'  => 'post_navigation',
						'value' => 1,
						'label' => __('Previous/Next Navigation?', 'bunyad'),
						'desc'  => __('Enabling this will add a Previous and Next post link in the single post page.', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'  => 'author_box',
						'value' => 1,
						'label' => __('Show Author Box', 'bunyad'),
						'desc'  => __('Setting to No will disable author box displayed below posts on post page.', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'  => 'related_posts',
						'value' => 1,
						'label' => __('Show Related Posts', 'bunyad'),
						'desc'  => __('Setting to No will disable the related posts that appear on the single post page.', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					
					array(
						'name'  => 'related_posts_by',
						'value' => 'cats',
						'label' => __('Related Posts By', 'bunyad'),
						'desc'  => __('By default, related posts will be displayed by finding posts based on the categories of post being viewed. You can change it to tags.', 'bunyad'),
						'type'  => 'select',
						'options' => array(
							'cats' => __('Categories', 'bunyad'), 'tags' => __('Tags', 'bunyad')
						)
					),
					
				)
			), // end section
			
			array(
				'title'  => __('Review Posts', 'bunyad'),
				'fields' => array(					
					
					array(
						'name'  => 'review_show',
						'value' => 1,
						'label' => __('Show Rating In Listings', 'bunyad'),
						'desc'  => __('Show the verdict rating points in category/home-page block?', 'bunyad'),
						'type'  => 'checkbox',
					),
					
					array(
						'name'  => 'review_show_widgets',
						'value' => 1,
						'label' => __('Show Rating In Widgets/Sidebar', 'bunyad'),
						'desc'  => __('On posts with reviews, show the verdict rating points in sidebar widgets?', 'bunyad'),
						'type'  => 'checkbox'
					),
					
					array(
						'name'  => 'review_style',
						'value' => 'bar',
						'label' => __('Widgets & Blocks Review Style', 'bunyad'),
						'desc'  => __('This setting affects review style in widgets & blocks.', 'bunyad'),
						'type'  => 'select',
						'options' => array(
							'bar' => __('Points Bar', 'bunyad'),
							'percent' => __('Percent', 'bunyad'),
							'stars' => __('Stars', 'bunyad')
						)
					),
					
					array(
						'name'  => 'user_rating',
						'value' => 1,
						'label' => __('Enable Users Ratings', 'bunyad'),
						'desc'  => __('This feature adds a user rating area below criterion to allow readers to click and vote.', 'bunyad'),
						'type'  => 'checkbox'
					),					
			
				)
			) // end section
						
		), // end sections
	),
	
	array(
		'title' => __('Typography', 'bunyad'),
		'id'    => 'options-typography',
		'icon'  => 'dashicons-editor-spellcheck',
		'sections' => array(
	
			array(
				'title'  => __('General', 'bunyad'),
				'desc'   => sprintf(__('Selecting a font will show a basic preview. Go to %s for more details. '
								. 'It is highly recommended that you choose fonts that have similar heights to '
								. 'the default fonts to maintain pleasing aesthetics.', 
								'bunyad'), '<a href="http://www.google.com/webfonts" target="_blank">google fonts directory</a>'),
								
				'fields' => array(
					array(
						'name'   => 'css_main_font',
						'value' => 'Lora',
						'label' => __('Main Font Family', 'bunyad'),
						'desc'  => __('This effects almost every element on the theme. Please use a family that has regular, and bold style.', 'bunyad'),
						'type'  => 'typography',
						'css'   => array(
							'selectors' => 'body'
						),
						'families' => true,
						'fallback_stack' => 'Georgia, serif',
					),
					
					array(
						'name'   => 'css_post_body_font',
						'value' => 'Open Sans',
						'label' => __('Contrast Font Family', 'bunyad'),
						'desc'  => __('Applies to elements like dates, breadcrumbs, special headings. Choose wisely.', 'bunyad'),
						'type'  => 'typography',
						'css'   => array(
							'selectors' => 
								'.breadcrumbs, .section-head, .section-head-small, .posts-grid time, .posts-list time, .review-meta .number, .tabbed .tabs-list a, 
								.read-more a, .comments-list .post-author, .comment-count, .main-footer .widget-title, .sc-accordion-title a, .sc-toggle-title a, 
								.sc-tabs a, .heading-view-all'
						),
						'families' => true,
						'fallback_stack' => 'Arial, sans-serif',
					),
					
					array(
						'name'   => 'css_navigation_font',
						'value' => 'Lora',
						'label' => __('Navigation Font Family', 'bunyad'),
						'desc'  => __('Change the font used in the navigation menu.', 'bunyad'),
						'type'  => 'typography',
						'families' => true,
						'css'   => array('selectors' => '.navigation .menu'),
						'size'  => array('value' => 14),
					),
					
					array(
						'name'   => 'css_listing_body_font',
						'value' => 'Open Sans:700',
						'label' => __('Blocks & Widget Headings', 'bunyad'),
						'desc'  => __('Change the heading displayed above home-page blocks or in sidebar widgets.', 'bunyad'),
						'type'  => 'typography',
						'css'   => array('selectors' => '.section-head'),
						'size'  => array('value' => 16)
					),
					
					
					array(
						'name'   => 'css_post_main_heading',
						'value' => 'Lora:regular',
						'label' => __('Post/Page Main Heading', 'bunyad'),
						'desc'  => __('Affects the main heading of a post or a page when viewing that page or post (single page).', 'bunyad'),
						'type'  => 'typography',
						'css'   => array(
							'selectors' => '.page .main-heading, .main-content > .main-heading, .post-header .post-title'
						),
						'size'  => array('value' => 24)
					),				
					
					array(
						'name'   => 'css_post_title_font',
						'value' => 'Volkhov:regular',
						'label' => __('Page/Post Content Headings', 'bunyad'),
						'desc'  => __('Changing this will affect the font used for pages content heading and heading h1-h6 used within posts or default template pages.', 'bunyad'),
						'type'  => 'typography',
						'css'   => array(
							'selectors' => '.post-content h1, .post-content h2, .post-content h3, .post-content h4, .post-content h5, .post-content h6'
						)
					),
					
					array(
						'name'   => 'css_blocks_title_font',
						'value' => 'Volkhov:regular',
						'label' => __('Blocks & Listings Post Titles', 'bunyad'),
						'desc'  => __('Affects emphasized post titles displayed in the blocks and listings.', 'bunyad'),
						'type'  => 'typography',
						'css'   => array(
							'selectors' => '.posts-list .post-link, .posts-grid .post-link, .latest-reviews .post-title, .navigation.alt .mega-menu.links > li > a, 
								.category-ext .heading, .main-slider .meta h3, .slider-split .blocks h3, .gallery-block .post-title, .featured-grid .meta h3, 
								.review-box .heading, .heading-text'
						)
					),

					array(
						'name'   => 'css_footer_headings',
						'value' => 'Open Sans:700',
						'label' => __('Footer Headings', 'bunyad'),
						'desc'  => __('Change the font used in footer headings.', 'bunyad'),
						'type'  => 'typography',
						'css'   => array('selectors' => '.main-footer .widget-title'),
						'size'  => array('value' => 15)
					),
					
				),
			), // end section
			
			array(
				'title'  => __('Content Heading Sizes', 'bunyad'),
				'desc'   => __('These sizes affects the heading of fonts used within posts.', 'bunyad'),
								
				'fields' => array(
			
					array(
						'name'   => 'css_post_h1',
						'value' =>  24,
						'label' => __('H1 Size', 'bunyad'),
						'desc'  => __('h1 size for in-post headings.', 'bunyad'),
						'type'  => 'number',
						'css'   => array(
							'selectors' => array('.post-content h1' => 'font-size: %spx;')
						),
					),
					
					array(
						'name'   => 'css_post_h2',
						'value' => 22,
						'label' => __('H2 Size', 'bunyad'),
						'desc'  => __('h2 size for in-post headings.', 'bunyad'),
						'type'  => 'number',
						'css'   => array(
							'selectors' => array('.post-content h2' => 'font-size: %spx;')
						),
					),
					
					array(
						'name'   => 'css_post_h3',
						'value' => 20,
						'label' => __('H3 Size', 'bunyad'),
						'desc'  => __('h3 size for in-post headings.', 'bunyad'),
						'type'  => 'number',
						'css'   => array(
							'selectors' => array('.post-content h3' => 'font-size: %spx;')
						),
					),
					
					array(
						'name'   => 'css_post_h4',
						'value' => 18,
						'label' => __('H4 Size', 'bunyad'),
						'desc'  => __('h4 size for in-post headings.', 'bunyad'),
						'type'  => 'number',
						'css'   => array(
							'selectors' => array('.post-content h4' => 'font-size: %spx;')
						),
					),
					
					array(
						'name'   => 'css_post_h5',
						'value' => 16,
						'label' => __('H5 Size', 'bunyad'),
						'desc'  => __('h5 size for in-post headings.', 'bunyad'),
						'type'  => 'number',
						'css'   => array(
							'selectors' => array('.post-content h5' => 'font-size: %spx;')
						),
					),
					
					array(
						'name'   => 'css_post_h6',
						'value' => 14,
						'label' => __('H6 Size', 'bunyad'),
						'desc'  => __('h6 size for in-post headings.', 'bunyad'),
						'type'  => 'number',
						'css'   => array(
							'selectors' => array('.post-content h6' => 'font-size: %spx;')
						),
					),					
				),
			), // end section
			
			array(
				'title' => __('Advanced', 'bunyad'),
				'fields' => array(
					array(
						'name' => 'font_charset',
						'label'   => __('Font Character Set', 'bunyad'),
						'value'   => '',
						'desc'    => __('For some languages, you will need an extended character set. Please note, not all fonts will have the subset. Check the google font to make sure.', 'bunyad'),
						'type'    => 'checkbox',
						'multiple' => array(
							'latin' => __('Latin', 'bunyad'),
							'latin-ext' => __('Latin Extended', 'bunyad'),
							'cyrillic'  => __('Cyrillic', 'bunyad'),
							'cyrillic-ext'  => __('Cyrillic Extended', 'bunyad'),
							'greek'  => __('Greek', 'bunyad'),
							'greek-ext' => __('Greek Extended', 'bunyad'),
							'vietnamese' => __('Vietnamese', 'bunyad'),
						),
					),
				),
			),
						
		), // end sections
	),
	
	array(
		'title' => __('Skin & Color', 'bunyad'),
		'id'    => 'options-style-color',
		'icon'  => 'dashicons-admin-appearance',
		'sections' => array(
	
			array(
				//'title'  => __('Defaults', 'bunyad'),
				'id' => 'defaults',
				'fields' => array(

					array(
						'name'   => 'predefined_style',
						'value' => 'news',
						'label' => __('Pre-defined Skin', 'bunyad'),
						'desc'  => __('WARNING: Changing the skin will affect some other settings like in Header & Navigation', 'bunyad'),
						'type'  => 'select',
						'options' => array(
							'magazine' => __('Classic Magazine', 'bunyad'),
							'news'  => __('Motive News', 'bunyad'),
							'creative' => __('Creative Magazine', 'bunyad'),
						),
					),
					
					array(
						'label' => __('Reset Colors', 'bunyad'),
						'desc'  => __('Clicking this button will reset all the color settings below to the default color settings.', 'bunyad'),
						'type'  => 'html',
						'html' => "<input type='submit' class='button' id='reset-colors' name='reset-colors' data-confirm='" 
								. __('Do you really wish to reset colors to defaults?', 'bunyad') . "' value='". __('Reset Colors', 'bunyad') ."' />",
					),
				)
			), // end section
			
			array(
				'title' => __('General', 'bunyad'),
				'fields' => array(		
					array(
						'name'  => 'css_main_color',
						'value' => '#b93434',
						'label' => __('Theme Color', 'bunyad'),
						'desc'  => __('It is the contrast color for the theme. It will be used for all links, menu, category overlays, main page and '
									. 'many contrasting elements.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
									'::selection' => 'background: %s',
									'::-moz-selection' => 'background: %s',

									'.main-color, .trending-ticker .heading, ul.styled li:before, .main-footer a, .post-content a, .tabbed .tabs-list .active a, 
									.skin-creative .navigation .mega-menu.links > li > a, .skin-creative .category-ext .heading'
											=> 'color: %s',

									'.section-head .title, .main-color-bg, .image-overlay, .read-more a, .main-pagination .current, .main-pagination a:hover, 
									.sc-button, .button, .drop-caps.square, .review-box .label, .review-box .rating-wrap, .verdict-box .overall .verdict, 
									.navigation .menu > li:hover > a, .navigation .menu > .current-menu-item > a, .navigation .menu > .current-menu-parent > a, 
									.navigation .menu > .current-menu-ancestor > a, ol.styled li:before, .comments-list .post-author, .main-footer.dark .tagcloud a:hover, 
									.tagcloud a:hover, .main-slider .owl-prev:hover, .main-slider .owl-next:hover' 
										=> 'background-color: %s',
									
									'.section-head.alt, .sc-tabs .active a, .sc-accordion-title.active, .sc-toggle-title.active, .verdict-box, 
									.navigation .menu > li > ul:after, .multimedia .carousel-nav-bar, .gallery-block .carousel-nav-bar, .post-content blockquote,
									.author-info, .review-box, .post-content .pullquote'
										=> 'border-top-color: %s',
										
									'.main-pagination .current, .main-pagination a:hover' => 'border-color: %s',
									
									'.sc-tabs-wrap.vertical .sc-tabs .active a, .navigation .menu > li > ul:after' => 'border-left-color: %s',
									
									'.navigation .menu > li > ul:after' => 'border-right-color: %s',
										
									'.section-head, .section-head-small, .navigation .mega-menu, .multimedia .first-title, .post-slideshow img.aligncenter, 
									.post-slideshow img.alignnone' 
											=> 'border-bottom-color: %s',

									'@media only screen and (max-width: 799px) { .navigation .mobile .fa' 
										=> 'background: %s',
							),
						)
					),
					
					
					array(
						'name'  => 'css_body_bg_color',
						'value' => '#f9f9f9',
						'label' => __('Body Background Color', 'bunyad'),
						'desc'  => __('Use light colors only in non-boxed layout. Setting a body background image below will override it.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'body, body.boxed' => 'background-color: %s;',
							),
						)
					),
					
					array(
						'name'  => 'css_body_bg',
						'value' => '',
						'label' => __('Body Background', 'bunyad'),
						'desc'  => __('Use light patterns in non-boxed layout. For patterns, use a repeating background. Use photo to fully cover the background with an image. Note that it will override the background color option.', 'bunyad'),
						'css' => array(
							'selectors' => array(
								'body' => 'background-image: url(%s);',
								'body.boxed' => 'background-image: url(%s);',
							),
						),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload This Picture', 'bunyad'), 
							'button_label' => __('Upload Image',  'bunyad'),
							'insert_label' => __('Use as Background',  'bunyad')
						),
						'bg_type' => array('value' => 'cover'),
					),
					
					array(
						'name'  => 'css_main_text_color',
						'value' => '#2e2e2e',
						'label' => __('Main Text Color', 'bunyad'),
						'desc'  => __('The body text color! It is used throughout where a color is not overwritten by other settings.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'body' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_main_links_color',
						'value' => '#2e2e2e',
						'label' => __('Main Links Color', 'bunyad'),
						'desc'  => __('The body links color! It is used throughout where a color is not overwritten by other settings.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'a, .posts-grid .post-link, .top-bar-content a, .post-content a' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_listing_text_color',
						'value' => '#444444',
						'label' => __('Blocks & Listings Excerpt Color', 'bunyad'),
						'desc'  => __('Text color applies to excerpt text displayed on homepage blocks and category listings.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.excerpt, .posts-list .excerpt, .posts-grid .excerpt' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_headings_text_color',
						'value' => '#2e2e2e',
						'label' => __('Headings Color', 'bunyad'),
						'desc'  => __('Applies to headings such as all the in-post headings.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'h1, h2, h3, h4, h5, h6' => 'color: %s',
								'.post-content h1, .post-content h2, .post-content h3, .post-content h4, .post-content h5, .post-content h6' => 'color: %s',
							),
						)
					),
				),
			), // end section
			
			array(
				'title' => __('Header', 'bunyad'),
				'fields' => array(
			
					array(
						'name'  => 'css_topbar_bg_color',
						'value' => '#161616',
						'label' => __('Top Bar Background Color', 'bunyad'),
						'desc'  => __('Only applies if top bar is enabled.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.top-bar' => 'background-color: %s;',
							),
						)
					),			
			
					array(
						'name'  => 'css_header_bg_color',
						'value' => '#ffffff',
						'label' => __('Header Background Color', 'bunyad'),
						'desc'  => __('Setting a header background pattern below will override it.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.main-head' => 'background-color: %s; background-image: none;',
							),
						)
					),

					array(
						'name'  => 'css_header_bg_pattern',
						'value' => '',
						'label' => __('Header Background', 'bunyad'),
						'desc'  => __('Please use a background pattern that can be repeated. Note that it will override the background color option.', 'bunyad'),
						'css' => array(
							'selectors' => array(
								'.main-head' => 'background-image: url(%s);',
							),
						),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload This Picture', 'bunyad'), 
							'button_label' => __('Upload Image',  'bunyad'),
							'insert_label' => __('Use as Background',  'bunyad')
						),
						'bg_type' => array('value' => 'repeat'),
					),
				),
			), // end section
			
			array(
				'title' => __('Navigation Menu', 'bunyad'),
				'fields' => array(
			
					array(
						'name'  => 'css_menu_bg_color',
						'value' => '#ffffff',
						'label' => __('Main Menu Background Color', 'bunyad'),
						'desc'  => __('Menu background affects the top-level background only.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation, .navigation.alt' => 'background-color: %s;',								
								'.navigation.sticky' => 'background: rgba(%s, 0.95);',
							),
						)
					),
					
					array(
						'name'  => 'css_menu_drop_bg',
						'value' => '#ffffff',
						'label' => __('Menu Dropdowns Background Color', 'bunyad'),
						'desc'  => __('Menu background color is only used when a background pattern is not specified below.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .menu ul, .mega-menu .sub-cats, .navigation.alt .menu ul, .navigation.alt .mega-menu .sub-cats' 
									=> 'background-color: %s;',
							),
						)
					),
					
					array(
						'name'  => 'css_menu_hover_bg_color',
						'value' => '#f5f5f5',
						'label' => __('Menu Hover/Current Background Color', 'bunyad'),
						'desc'  => __('This is the background color used when you hover a menu item.  It is not used for top-level active state.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .menu li li:hover, .navigation .menu li li.current-menu-item, .navigation.alt .menu li li:hover, 
								.navigation.alt .menu li li.current-menu-item' 
										=> 'background-color: %s;',
							),
						)
					),
					
					array(
						'name'  => 'css_menu_borders_color',
						'value' => '#e6e6e6',
						'label' => __('Menu Items Border Color', 'bunyad'),
						'desc'  => __('Menu items on drop down are separated by a border.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .menu > li li a, .navigation .mega-menu.links > li' 
										=> 'border-color: %s;',  
							),
						)
					),
					
					array(
						'name'  => 'css_mega_menu_bg',
						'value' => '#f7f7f7',
						'label' => __('Mega Menu Right Background', 'bunyad'),
						'desc'  => __('Mega Menu can have a different background than the regular drops.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .mega-menu, .navigation.alt .mega-menu' => 'background: %s;', 
							),
						)
					),
					
					array(
						'name'  => 'css_menu_main_text_color',
						'value' => '#ffffff',
						'label' => __('Main Text Color', 'bunyad'),
						'desc'  => __('Only affects the top-level links on the main navigation.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .menu > li > a' => 'color: %s;',
							),
						)
					),
					
					array(
						'name'  => 'css_menu_text_color',
						'value' => '#4f4f4f',
						'label' => __('Dropdown Text Color', 'bunyad'),
						'desc'  => __('Applies to top menu items on drop-downs. Does not apply to drop down.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .menu > li li a, .mega-menu .posts-grid .post-link, .mega-menu .posts-list a' => 'color: %s;',
							),
						)
					),
					
					array(
						'name'  => 'css_menu_headings_color',
						'value' => '#010101',
						'label' => __('Mega Menus Headings Color', 'bunyad'),
						'desc'  => __('Applies to headings in the mega menus.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .mega-menu.links > li > a, .category-ext .heading' => 'color: %s;',
							),
						)
					),
					
					array(
						'name'  => 'css_menu_hover_color',
						'value' => '#010101',
						'label' => __('Dropdown Links Hover & Active', 'bunyad'),
						'desc'  => __('Change hover color for links in menus. Also applies to active menu item - current page, post etc.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.navigation .menu li li:hover > a, .navigation .menu li li.current-menu-item > a' => 'color: %s;',
							),
						)
					),
					
				),
			), // end section
			
						
			array(
				'title' => __('Posts & Single Page', 'bunyad'),
				'fields' => array(
					
					array(
						'name'  => 'css_post_text_color',
						'value' => '#2e2e2e',
						'label' => __('Posts Main Text Color', 'bunyad'),
						'desc'  => __('Text color applies to body text of posts and pages.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.post-content' => 'color: %s',
							),
						)
					),
			
					array(
						'name'  => 'css_page_heading_color',
						'value' => '#000000',
						'label' => __('Page & Post Heading Color', 'bunyad'),
						'desc'  => __('Applies to the main heading on single post / page.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.main-heading, .post-header .post-title' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_links_color',
						'value' => '#b93434',
						'label' => __('Posts Link Color', 'bunyad'),
						'desc'  => __('Changes all the links color within posts and pages.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.post-content a' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_links_hover_color',
						'value' => '#b93434',
						'label' => __('Posts Link Hover Color', 'bunyad'),
						'desc'  => __('This color is applied when you mouse-over a certain link.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.post-content a:hover' => 'color: %s',
							),
						)
					),
				), 
						
			), // end section
						
			array(
				'title' => __('Footer', 'bunyad'),
				'fields' => array(
			
					array(
						'name'  => 'css_footer_bg_color',
						'value' => '#f7f7f7',
						'label' => __('Footer Background Color', 'bunyad'),
						'desc'  => __('Footer background color is only used when a background pattern is not specified below.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.main-footer' => 'background-color: %s; background-image: none;',
							),
						)
					),

					array(
						'name'  => 'css_footer_bg_pattern',
						'value' => '',
						'label' => __('Footer Background Pattern', 'bunyad'),
						'desc'  => __('Please use a background pattern that can be repeated. Note that it will override the background color option.', 'bunyad'),
						'css' => array(
							'selectors' => array(
								'.main-footer' => 'background-image: url(%s)',
							),
						),
						'type'    => 'upload',
						'options' => array(
							'type'  => 'image',
							'title' => __('Upload This Picture', 'bunyad'), 
							'button_label' => __('Upload Pattern', 'bunyad'),
							'insert_label' => __('Use as Background Pattern', 'bunyad')
						),
					),

					array(
						'name'  => 'css_footer_headings_color',
						'value' => '#2e2e2e',
						'label' => __('Footer Headings Color', 'bunyad'),
						'desc'  => __('Change color of headings in the footer.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.main-footer .widget-title' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_footer_text_color',
						'value' => '#2e2e2e',
						'label' => __('Footer Text Color', 'bunyad'),
						'desc'  => __('Affects color of text in the footer.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.main-footer, .main-footer .widget' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_footer_links_color',
						'value' => '#b93434',
						'label' => __('Footer Links Color', 'bunyad'),
						'desc'  => __('Affects color of links in the footer.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.main-footer .widget a' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_footer_lower_bg',
						'value' => '#2e2e2e',
						'label' => __('Lower Footer Background Color', 'bunyad'),
						'desc'  => __('Second footer uses this color in the background.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.lower-footer' => 'background-color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_footer_lower_text',
						'value' => '#b0b0b0',
						'label' => __('Lower Footer Text Color', 'bunyad'),
						'desc'  => __('Second footer uses this color for text.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.lower-footer' => 'color: %s',
							),
						)
					),
					
					array(
						'name'  => 'css_footer_lower_links',
						'value' => '#ffffff',
						'label' => __('Lower Footer Links Color', 'bunyad'),
						'desc'  => __('Affects color of links in the footer.', 'bunyad'),
						'type' => 'color',
						'css' => array(
							'selectors' => array(
								'.lower-footer a' => 'color: %s',
							),
						)
					),
					
				),
			), // end section
						
		), // end sections
	),
	
	array(
		'title' => __('Sliders Settings', 'bunyad'),
		'id'    => 'options-slider',
		'icon'  => 'dashicons-format-gallery',
		'sections' => array(
	
			array(
				'fields' => array(
					array(
						'label' => __('Homepage Slider', 'bunyad'),
						'type'  => 'file',
						'desc'  => '',
						'render'  => 'admin/options/field-slider.php',
					)
				),
			),
	
			array(
				'title'  => __('Slider Settings', 'bunyad'),
				'desc'   => __('These settings will be used on all sliders except for the grid slider (which is a static post grid).', 'bunyad'),
								
				'fields' => array(
					
					array(
						'name' => 'slider_animation',
						'label'   => __('Animation Type', 'bunyad'),
						'value'   => 'fade',
						'desc'    => __('Set the type of animation to use for the slider. Does not apply to default slider.', 'bunyad'),
						'type'    => 'select',
						'options' => array('fade' => __('Fade Animation', 'bunyad'), 'slide' => __('Slide Animation', 'bunyad')),
					),
					
					array(
						'name' => 'slider_autoplay',
						'label'   => __('Autoplay Slider?', 'bunyad'),
						'value'   => 1,
						'desc'    => __('Enable the slider to auto-slide after a set interval.', 'bunyad'),
						'type'    => 'checkbox',
					),
					
					array(
						'name' => 'slider_autoplay_interval',
						'label'   => __('Autoplay Interval', 'bunyad'),
						'value'   => 5000,
						'desc'    => __('Set the time a slide will be displayed for (in ms) before animating to the next one.', 'bunyad'),
						'type'    => 'text',
					),
				)
					
			), // end section
			
		), // end sections
	),
	
	array(
		'title' => __('Custom CSS', 'bunyad'),
		'id'    => 'options-custom-css',
		'icon'  => 'dashicons-editor-code',
		'sections' => array(
	
			array(
				//'title'  => __('General', 'bunyad'),
								
				'fields' => array(
					array(
						'name'   => 'css_custom',
						'value' => '',
						'label' => __('Custom CSS', 'bunyad'),
						'desc'  => __('Custom CSS will be added at end of all other customizations and thus can be used to overwrite rules. Less chances of specificity wars.', 'bunyad'),
						'type'  => 'textarea',
						'options' => array('cols' => 75, 'rows' => 15)
					),
					
					array(
						'name'   => 'css_custom_output',
						'value' => 'inline',
						'label' => __('Output Method', 'bunyad'),
						'desc'  => __('On-page is better for performance unless your Custom CSS is too large. If you are experiencing problem with a cache plugin or your Custom CSS is large, use external method.', 'bunyad'),
						'type'  => 'select',
						'options' => array(
							'inline' => __('On-page (For few lines of Custom CSS)', 'bunyad'),
							'external' => __('External (For a lot of Custom CSS)', 'bunyad'),
						)
					),
					
				)
					
			), // end section
						
		), // end sections
	),
	
	array(
		'title' => __('Backup & Restore', 'bunyad'),
		'id'    => 'options-backup-restore',
		'icon'  => 'dashicons-backup',
		'sections' => array(

			array(
				'fields' => array(
			
					array(
						'label'  => __('Backup / Export', 'bunyad'),
						'desc'   => __('This allows you to create a backup of your options and settings. Please note, it will not backup anything else.', 'bunyad'),
						'type'   => 'html',
						'html'   => "<input type='button' class='button' id='options-backup' value='". __('Download Backup', 'bunyad') ."' />",
					),
					
					array(
						'label'  => __('Restore / Import', 'bunyad'),
						'desc'   => __('<strong>It will override your current settings!</strong> Please make sure to select a valid backup file.', 'bunyad'),
						'type'   => 'html',
						'html'   => "<input type='file' name='import_backup' id='options-restore' />",
					)
					
				),
			
			),
	
		),
	),
	

	array(
		'title' => __('Sample Import', 'bunyad'),
		'id'    => 'options-sample-import',
		'icon'  => 'dashicons-download',
		'sections' => array(

			array(
				'title' => __('Import Sample Content', 'bunyad'),
				'desc'  => __('Import sample content from Motive official demo site. It needs a powerful webhost and may fail on a weaker one. It may take <strong>2-4</strong> minutes to complete. <strong>WARNING: Only use on an empty site and make sure you have enabled recommended plugins first! Existing widgets will NOT be deleted but it is a good idea to remove them.</strong>', 'bunyad'),
				'fields' => array(

					array(
						'name'  => 'import_media',
						'label' => __('Images & Media', 'bunyad'),
						'value' => 1,
						'type' => 'radio',
						'desc' => '',
						'options' => array(0 => __('Skip images & media', 'bunyad'), 1 => __('Import random images?', 'bunyad')),
					),
					
					array(
						'name'  => 'import_image_gen',
						'label' => __('Generate all image sizes?', 'bunyad'),
						'value' => 1,
						'type' => 'checkbox',
						'desc' => '<strong>Important:</strong> Only select Yes for powerful webhosts! If you selected No, you will have to install and run "Regenerate Thumbnails" plugin after import is done.',
					),

					array(
						'label'  => __('Start Import', 'bunyad'),
						'desc'   => '',
						'type'   => 'html',
						'html'   => "
							<input type='hidden' name='import_demo' value='1' />
							<p><input type='button' class='button-primary' id='options-demo-import' value='". __('Import Sample Data', 'bunyad') ."' data-confirm='"
							. __('WARNING: Do not use this on site with existing content. Enable Bunyad plugins before importing. Do you really wish to import sample data?', 'bunyad') . "'/></p>
						",
					),	
				),
			
			),
	
		),  // end sections
	),

));

return $options;