<?php
/**
 * Render "Current Home" options field
 */
	
	$homepage = get_option('page_on_front');
	$change   = '<a href="' . admin_url('options-reading.php') . '" class="inline button" target="_blank">'. __('Change', 'bunyad') .'</a> ';
	
	if (!$homepage) {
		$home_title = '<a href="' . admin_url('post-new.php?post_type=page') .'" class="inline button" target="_blank">' . __('Create', 'bunyad') . '</a>' . $change . __('Set to show latest posts.', 'bunyad'); 	
	}
	else {
		$homepage = get_post($homepage);
		$home_title = $change . __('Currently set to:', 'bunyad') . ' <a href="'. get_edit_post_link($homepage->ID) . '">' . $homepage->post_title . '</a>';
	}
	
	return '<p>' . $home_title . '</p>';