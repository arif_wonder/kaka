<?php

/**
 * Render "Current Home" options field
 */
	
	$homepage = get_option('page_on_front');
	
	$change   = '<a href="' . admin_url('options-reading.php') . '" class="inline button" target="_blank">'. __('Change', 'bunyad') .'</a> ';
	
	if (!$homepage) {
		$text = __('A static front page is required to activate the slider on homepage.', 'bunyad');
	}
	else {
	
		$homepage = get_post($homepage);
		$slider   = Bunyad::posts()->meta('featured_slider', $homepage->ID);
		$home     = ' <a href="'. get_edit_post_link($homepage->ID) . '">' . $homepage->post_title . '</a>';
		
		// no slider set
		if (!$slider) {
			$text = sprintf(
				__('Edit and see page options under %s and set your featured slider.', 'bunyad'),
				$home
			);
		}
		else {
			$text = sprintf(
				__('You have a featured slider set on your homepage: %s', 'bunyad'),
				$home
			);
		}
	}
	
	if (!$homepage) {
		//$home_title = '<a href="' . admin_url('post-new.php?post_type=page') .'" class="inline button" target="_blank">' . __('Create', 'bunyad') . '</a>' . $change . __('Set to show latest posts.', 'bunyad'); 	
	}
	else {
		//$homepage = get_post($homepage);
		//$home_title = $change . __('Currently set to:', 'bunyad') . ' <a href="'. get_edit_post_link($homepage->ID) . '">' . $homepage->post_title . '</a>';
	}
	
	return '<p>' . $text . '</p>';