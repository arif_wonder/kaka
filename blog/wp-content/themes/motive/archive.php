<?php

/**
 * Archives Page!
 * 
 * This page is used for all kind of archives from custom post types to blog to 'by date' archives.
 * 
 * Bunyad framework recommends this template to be used as generic template wherever any sort of listing 
 * needs to be done.
 * 
 * Types of archives handled:
 * 
 *  - Categories
 *  - Tags
 *  - Taxonomies
 *  - Author Archives
 *  - Date Archives
 * 
 * @link http://codex.wordpress.org/images/1/18/Template_Hierarchy.png
 */

get_header();


/**
 * Set the loop template to use, if not already set
 */

$bunyad_loop_template = Bunyad::registry()->loop_template;

if (empty($bunyad_loop_template))
{
	$bunyad_template = Bunyad::options()->archive_loop_template;
	$bunyad_loop_template = 'loop';
	
	if (in_array($bunyad_template, array('alt', 'classic'))) {
		$bunyad_loop_template = 'loop-' . $bunyad_template;
	}
		
	// set type of loop grid - if using the grid
	Bunyad::registry()->set('loop_grid', ($bunyad_template == 'grid-3' ? 3 : 2));
}

?>

<div class="main wrap">
	<div class="ts-row cf">
		<div class="col-8 main-content cf">

		<?php Bunyad::core()->breadcrumbs(); ?>
		
	
		<?php 
		/* can be combined into one below with is_tag() || is_category() || is_tax() - extended for customization */
		?>
		
		<?php if (is_tag()): ?>
		
			<h2 class="archive-heading">
				<span class="prefix"><?php _e('Tagged:', 'bunyad'); ?></span>
				
				<span class="main-color"><?php echo esc_html(single_tag_title('', false)); ?></span>
			</h2>
			
			<?php if (tag_description()): ?>
				<p class="post-content"><?php echo do_shortcode(tag_description()); // tag_description is pre-sanitized html ?></p>
			<?php endif; ?>
		
		<?php elseif (is_category()): // category page ?>
		
			<h2 class="archive-heading">
				<span class="prefix"><?php _e('Browsing:', 'bunyad'); ?></span>
				
				<span class="main-color cat-color-<?php echo esc_attr(get_queried_object()->term_id); ?>"><?php echo esc_html(single_cat_title('', false)); ?></span>
			</h2>
			
			<?php if (category_description()): ?>
				<div class="archive-desc post-content"><?php echo do_shortcode(category_description());  // category_description is pre-sanitized html  ?></div>
			<?php endif; ?>
			
		<?php elseif (is_tax()): // custom taxonomies ?>
			
			<h2 class="archive-heading">
				<span class="prefix"><?php _e('Browsing:', 'bunyad'); ?></span>

				<span class="main-color"><?php echo esc_html(single_term_title('', false)); ?></span>
			</h2>
			
			<?php if (term_description()): ?>
				<p class="post-content"><?php echo do_shortcode(term_description());  // term_description is pre-sanitized html  ?></p>
			<?php endif; ?>
			
		<?php elseif (is_search()): // search page ?>
			<?php $results = $wp_query->found_posts; ?>
						
			<h2 class="archive-heading">
				<span class="prefix"><?php _e('Search Results:', 'bunyad'); ?></span>
				
				<span class="main-color"><?php printf('%s (%d)',  esc_html(get_search_query()), esc_html($results)); ?></span>
			</h2>
			
		<?php elseif (is_author()): // author page ?>
			
			<?php $authordata = get_userdata(get_query_var('author')); ?>
		
			<h1 class="archive-heading author-title">
				<span class="prefix"><?php _e('Author:', 'bunyad'); ?></span>
				
				<span class="main-color"><?php echo esc_html(get_the_author()); ?></span>
			</h1>

			<?php get_template_part('partials/author-box'); ?>
			
		<?php elseif (is_archive()): ?>
			
			<h2 class="archive-heading">
			<?php if (is_day()): ?>
			
				<span class="prefix"><?php _e('Daily Archives:', 'bunyad'); ?></span>
				<span class="main-color"> <?php echo get_the_date(); ?></span>
			
			<?php elseif (is_month()): ?>
			
				<span class="prefix"><?php _e('Monthly Archives:', 'bunyad'); ?></span>
				<span class="main-color"> <?php echo get_the_date('F, Y'); ?></span>
			
			<?php elseif (is_year()): ?>

				<span class="prefix"><?php _e('Yearly Archives:', 'bunyad'); ?></span>
				<span class="main-color"> <?php echo get_the_date('Y'); ?></span>
			
			<?php endif; ?>				
			</h2>
		
		<?php endif; ?>
	
		<?php 
		
		// slider for categories
		if (is_category()) {
			get_template_part('partials/featured-area');
		}

		// render our loop
		get_template_part(($bunyad_loop_template ? $bunyad_loop_template : 'loop')); 
		
		?>

		</div>
		
		<?php Bunyad::core()->theme_sidebar(); ?>
		
	</div> <!-- .ts-row -->
</div> <!-- .main -->

<?php get_footer(); ?>