<?php

/**
 * Blog/Listing Block - Show posts in one of the category listing style 
 */


if (empty($type)) {
	$type = Bunyad::options()->default_cat_template;
}

// grid template? defaults to loop if not specified
if (in_array($type, array('alt', 'classic'))) {
	$template = 'loop-' . $type;
}
// default modern template
else {
	$template = 'loop';
	
	// set type of loop grid - if using the grid
	$loop_grid = 2;
	
	if ($type == 'grid-3') {
		$loop_grid = 3;
	}
	
	Bunyad::registry()->set('loop_grid', $loop_grid);
}

// save current options so that can they can be restored later
$options = Bunyad::options()->get_all();

Bunyad::options()
	->set('blog_no_pagination', ($pagination == 0 ? 1 : 0)); // inverse the original pagination option; different meaning	


// use locate_template to preserve variable scope and support child themes
include_once locate_template('blocks/common.php');

/**
 * Process the block and setup the query
 */
$block = new Bunyad_Block_Motive($atts, $tag, $this);
$query = $block->process()->query;

Bunyad::registry()->set('loop', $query);

?>

<div class="<?php echo esc_attr($block->term_wrap); ?>">
	
	<?php $block->output_title(); ?>
	
	<?php
	
	// get our loop template
	get_template_part($template);
	
	// restore all options
	Bunyad::options()->set_all($options);
	
	?>

</div>
