<?php 
/**
 * Highlights Block - Highlight posts from a category/taxonomy
 * 
 * Offers three styles: Normal (1 column), Mini (for 3 columns), Full Split (1 full column split into 2 columns)
 */

?>

	<?php 

	// use locate_template to preserve variable scope and support child themes
	include_once locate_template('blocks/common.php');
	
	$block = new Bunyad_Block_Motive($atts, $tag, $this);
	$query = $block->process()->query;

	$count = 0;
	
	?>
	
	<section <?php Bunyad::markup()->attribs('highlights-block', array('class' => array('highlights', $type, $block->term_wrap))); ?>>
	
	<?php $block->output_title(); ?>
	
	
	<?php while ($query->have_posts()): $query->the_post(); $count++; ?>
	
	<?php if ($count === 1): // main post - better highlighted ?>
		
		<article itemscope itemtype="http://schema.org/Article" class="posts-grid">

			<a href="<?php the_permalink(); ?>" class="image-link" itemprop="url">
				<?php
				
					if ($type == 'mini') {
						// use highlight-block image for columns >= 30% of .main-wrap width
						$image = ($block->col_relative_width >= 0.3 ? 'motive-highlight-block' : 'motive-main-block');
					}
					else if ($type == 'full-split') {
						$image = 'motive-highlight-block';
					}
					else {
						// use grid-slider image for columns >= 50% of .main-wrap width
						$image = ($block->col_relative_width > 0.67 ? 'motive-grid-slider' : 'motive-highlight-block');
					}

					the_post_thumbnail($image, array('class' => 'image', 'title' => strip_tags(get_the_title()))); 
				?>
				
				<?php do_action('bunyad_image_overlay'); ?>
			</a>
			
			<?php do_action('bunyad_comments_snippet'); ?>

			<?php do_action('bunyad_listing_meta'); ?>
			
			<h2 itemprop="name"><a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a></h2>
			
			<?php if ($type != 'mini'): ?>
			
			<div class="excerpt text-font">
				<?php echo Bunyad::posts()->excerpt(null, ($excerpt_len !== '' ? $excerpt_len : 20), array('force_more' => true)); ?>
			</div>
							
			<?php endif; ?>
			
		</article>
		
	<?php if ($query->post_count > 1): ?>
					
		<ul class="posts-list thumb">
		
	<?php endif; ?>
	
			
	<?php continue; endif; // main post end ?>
		
		<?php // other posts, in a list ?>

			<li>
		
		<?php if ($type != 'mini'): ?>
			
				<?php if (!$no_thumbs): // if thumbs not disabled ?>
				
				<a href="<?php the_permalink() ?>" class="image-link small">
					<?php the_post_thumbnail('post-thumbnail', array('title' => strip_tags(get_the_title()))); ?>
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<?php endif; ?>
				
				<div class="content">

					<?php do_action('bunyad_listing_meta', 'small'); ?>
				
					<a href="<?php the_permalink(); ?>" class="post-link-small"><?php the_title(); ?></a>
																
				</div>
			
		<?php else: ?>
		
				<a href="<?php the_permalink(); ?>" class="title post-link-small"><?php the_title(); ?></a>
				
		<?php endif; ?>
		
			</li>
		
	<?php endwhile; ?>
		
		<?php if ($query->post_count > 1): ?> </ul> <?php endif; ?>
		
<?php wp_reset_query(); ?>
</section>
