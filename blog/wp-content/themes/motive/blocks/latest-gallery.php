<?php

/**
 * Latest Gallery Block - Display posts by format in a carousel
 */

	// disable view all for output_title() function - manual processing below
	$atts['view_all'] = 0;

	// use locate_template to preserve variable scope and support child themes
	include_once locate_template('blocks/common.php');
	
	$block = new Bunyad_Block_Motive($atts, $tag, $this);
	$query = $block->process()->query;
	
	if (empty($query)) {
		return;
	}

	
	/**
	 * Add overlay and dark bg if needed
	 */
	
	$term_wrap = $block->term_wrap;
	
	if ($title_pos == 'overlay') {
		$term_wrap .= ' overlay'; 
	}
	
	if ($type == 'dark') {
		$term_wrap .= ' dark-bg';
	}
	
	/**
	 * Magic image size
	 */
	
	// sidebar of 8/12
	if ($block->col_relative_width <= 0.667) {
		$image = ($per_slide > 2 ? 'motive-main-block' : 'motive-highlight-block');
	}
	// full-width?
	else {
		$image = ($per_slide > 3 ? 'motive-main-block' : 'motive-highlight-block');
	}
	

?>

<section <?php Bunyad::markup()->attribs('gallery-block', array('class' => array('gallery-block cf', $term_wrap))); ?>>

	<?php $block->output_title(); ?>
	
		
	<?php if ($show_nav): ?>
	
	<div class="carousel-nav-bar">
		<span class="carousel-nav">
			<a href="#" class="prev fa fa-chevron-left" title="<?php esc_attr_e('Previous', 'bunyad'); ?>"></a>
			<a href="#" class="next fa fa-chevron-right" title="<?php esc_attr_e('Next', 'bunyad'); ?>"></a>
		</span>
	</div>		
	
	<?php endif; ?>	
	
	
	<?php if ($query->have_posts()): ?>
	
	<div class="owl-carousel carousel" data-items="<?php echo esc_attr($per_slide); ?>">
		<?php while ($query->have_posts()): $query->the_post(); ?>
		
			<div class="item">
				<a href="<?php the_permalink(); ?>" class="image-link">
					<span class="image">
						<?php the_post_thumbnail($image, array('alt' => '', 'title' => '')); ?>
					</span>
					
					<?php do_action('bunyad_image_overlay'); ?>
					
					<?php if ($title_pos == 'overlay'): ?>
						<span class="post-title heading-text"><span><?php the_title(); ?></span></span>
					<?php endif; ?>
					
				</a>
				
				<?php if ($title_pos != 'overlay'): ?>
					<p class="post-title heading-text"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				<?php endif;?>
				
			</div>
		
		<?php endwhile; ?>
	</div>
	
	<?php endif; ?>
	
	
	<?php if ($view_all): ?>
		<a href="<?php echo esc_url($block->link); ?>" class="heading-view-all"><?php _e('View all', 'bunyad'); ?> <i class="fa fa-caret-right"></i></a>
	<?php endif; ?>

</section>

	
<?php wp_reset_query(); ?>