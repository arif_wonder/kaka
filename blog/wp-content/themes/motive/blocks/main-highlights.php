<?php 
/**
 * Main Highlights Block - Larger posts grid
 */
?>

	<?php 
		// use locate_template to preserve variable scope and support child themes
		include_once locate_template('blocks/common.php');
		
		$block = new Bunyad_Block_Motive($atts, $tag, $this);
		$query = $block->process()->query;
			
		if (empty($query)) {
			return;
		}
		
		$count = 0;
		$larger_images = ($block->col_relative_width >= 0.66);
	?>

	<section <?php Bunyad::markup()->attribs('main-highlights-block', array('class' => array('main-highlights', $type, $block->term_wrap))); ?>>
	
		<?php $block->output_title(); ?>	
		
		<?php while ($query->have_posts()): $query->the_post(); $count++; ?>
		
			<?php if ($count === 1): // main post - better highlighted ?>
			
			<article class="posts-grid">
					
				<a href="<?php the_permalink(); ?>" class="image-link">
					<?php the_post_thumbnail(
								($larger_images ? 'motive-alt-slider' : 'motive-grid-slider'), 
								array('class' => 'image', 'title' => strip_tags(get_the_title()))); ?>
					
					<?php do_action('bunyad_image_overlay'); ?>
				</a>

				<?php do_action('bunyad_comments_snippet'); ?>				

				<?php do_action('bunyad_listing_meta'); ?>
				
				<h2><a href="<?php the_permalink(); ?>" class="first-heading post-link"><?php the_title(); ?></a></h2>

				<div class="text-font excerpt">
					<?php echo Bunyad::posts()->excerpt(null, ($excerpt_len !== '' ? $excerpt_len : 25), array('force_more' => true)); ?>
				</div>

			</article>
			
				<?php if ($query->post_count > 1): ?>
						
				<ul class="ts-row posts-grid">
			
				<?php endif; ?>				
							
			<?php continue; endif; // end first post ?>
			
			<?php // other posts ?>
			
				<li class="column half post">
					
					<a href="<?php the_permalink() ?>" class="image-link"><?php 
						the_post_thumbnail(($larger_images ? 'motive-grid-slider' : 'motive-main-block'), array('title' => strip_tags(get_the_title()))); ?>
						
						<?php do_action('bunyad_image_overlay'); ?>
					</a>
					
					<?php do_action('bunyad_comments_snippet'); ?>
					
					<?php do_action('bunyad_listing_meta', 'small'); ?>
					
					<a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a>
					
				</li>
		
		<?php endwhile; // end loop ?>
		
			<?php if ($query->post_count > 1): // close the ul ?> 
				</ul> 
			<?php endif; ?>
		
	</section>

	<?php wp_reset_query(); ?>