<?php 
/**
 * Multimedia Block - A carousel of latest posts or media with one highlighted post
 */
?>
	
	<?php 
		// use locate_template to preserve variable scope and support child themes
		include_once locate_template('blocks/common.php');
		
		$block = new Bunyad_Block_Motive($atts, $tag, $this);
		$query = $block->process()->query;
		
		if (!is_object($query)) {
			return;
		}
		
		$count = 0;
		$larger_images = ($block->col_relative_width >= 0.66);
	?>

	<section class="multimedia <?php echo esc_attr($block->term_wrap); ?>">
	
		<?php $block->output_title(); ?>
		
		<?php while ($query->have_posts()): $query->the_post(); $count++; ?>
		
		<?php if ($count === 1): // main post - better highlighted ?>
			
			<article itemscope itemtype="http://schema.org/Article" class="posts-grid">
					
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="image-link" itemprop="url">
					<?php 
					// use alt-slider image if column width is >= 66% of .main-wrap
					the_post_thumbnail(($larger_images ? 'motive-alt-slider' : 'motive-grid-slider'), array('class' => 'image', 'title' => strip_tags(get_the_title()))); ?>
					
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<?php do_action('bunyad_comments_snippet'); ?>
				
				<div class="first-title">
					<h2 itemprop="name"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>	
				</div>
				
			</article>
			
		<?php if ($query->post_count > 1): ?>
						
			<ul class="owl-carousel carousel" data-stage-pad="15" data-margin="15" data-items="<?php echo esc_attr($per_slide); ?>">
			
		<?php endif; ?>
		
				
		<?php continue; endif; // main post end ?>
			
			<?php // other posts, in a list ?>

				<li>
					<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="image-link">
						<span class="image"><?php 
							the_post_thumbnail('motive-main-block', array('title' => strip_tags(get_the_title()))); ?>
						</span>
						
						<?php do_action('bunyad_image_overlay'); ?>
						
					</a>
					
					<p class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
					
				</li>
			
		<?php endwhile; ?>
			
			<?php if ($query->post_count > 1): ?> 
			
			</ul>	
				
				<div class="carousel-nav-bar">
					<span class="carousel-nav">
						<a href="#" class="prev fa fa-chevron-left" title="<?php esc_attr_e('Previous', 'bunyad'); ?>"></a>
						<a href="#" class="next fa fa-chevron-right" title="<?php esc_attr_e('Next', 'bunyad'); ?>"></a>
					</span>
				</div>
			
			<?php endif; ?>
			
	<?php wp_reset_query(); ?>
	
	</section>