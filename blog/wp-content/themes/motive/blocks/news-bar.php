<?php 
/**
 * New Bar - A strip of news
 */
?>

	<?php 
		// use locate_template to preserve variable scope and support child themes
		include_once locate_template('blocks/common.php');
		
		// force disable view all
		$atts['view_all'] = 0;
		
		$block = new Bunyad_Block_Motive($atts, $tag, $this);
		$query = $block->process()->query;
		
		if (!is_object($query)) {
			return;
		}
		
	?>

	<section class="news-bar">

		<?php $block->output_title(); ?>
		
		<ul class="posts-list">
		<?php while ($query->have_posts()): $query->the_post(); ?>
			<li class="post">
				
				<?php if (!$no_thumbs): // if thumbs not disabled ?>
				
				<a href="<?php the_permalink() ?>" class="image-link small">
					<?php the_post_thumbnail('post-thumbnail', array('title' => strip_tags(get_the_title()))); ?>
				
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<?php endif; ?>
				
				<div class="content">
					<?php do_action('bunyad_listing_meta', 'small'); ?>
					<a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a>
				</div>
				
				<div class="excerpt text-font">
					<?php echo Bunyad::posts()->excerpt(null, ($excerpt_len !== '' ? $excerpt_len : 8), array('add_more' => false)); ?>
				</div>
				
			</li>
		<?php endwhile; ?>
		</ul>
	
	</section>