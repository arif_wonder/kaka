<?php

/**
 * Block for review shortcode
 * 
 * Collect criteria and create an average rating to display.
 * 
 * Note: supports microformat schema. Add hreview, reviewer, dtreviewed
 * relevant fields in content.php
 */

$meta = Bunyad::posts()->meta();
$ratings = array();
foreach ($meta as $key => $value)
{
	if (preg_match('/criteria_rating_([0-9]+)$/', $key, $match)) {
		
		$ratings[] = array(
			'number' => $match[1],
			'rating' => $value,
			'label'  => $meta['_bunyad_criteria_label_' . $match[1]]
		);
	}
}

$overall = Bunyad::posts()->meta('review_overall');
$heading = Bunyad::posts()->meta('review_heading');
$type    = Bunyad::posts()->meta('review_type');

// add verdict text 
$verdict = Bunyad::posts()->meta('review_verdict');
$verdict_text = Bunyad::posts()->meta('review_verdict_text');

// container classes
$classes[] = 'review-box';
if ($position == 'top-left') {
	array_push($classes, 'alignleft', 'column', 'half');
}

// add stars class
if ($type == 'stars') {
	array_push($classes, 'stars');
}

array_push($classes, $position);

?>

<section class="<?php echo esc_attr(implode(' ', $classes)); ?>">
	<?php if ($heading): ?>
	
	<h3 class="heading"><?php echo esc_html($heading); ?></h3>
	
	<?php endif; ?>
		
	<ul>
	<?php foreach ((array) $ratings as $rating): 
	
			$percent = round(($rating['rating'] / Bunyad::reviews()->rating_max) * 100);
	
			if ($type == 'percent') {
				$rating['rating'] = $percent . ' %';
			}
	?>
	
		<li class="criterion-wrap">
		
		<?php if ($type == 'stars'): ?>
		
			<div class="criterion">
				<span class="label"><?php echo esc_html($rating['label']); ?></span>

				<div class="main-stars">
					<span style="width: <?php echo esc_attr($percent); ?>%;"><strong class="number"><?php echo esc_html($rating['rating']); ?></strong></span>
				</div>
			
			</div>
				
		<?php else: ?>
		
			<div class="criterion">
				<span class="label" style="width: <?php echo esc_attr($percent); ?>%;"><?php echo esc_html($rating['label']); ?></span>
				<span class="rating-wrap"><span class="number"><?php echo esc_html($rating['rating']); ?></span></span>
			</div>
				
		<?php endif; ?>

		</li>
	
	<?php endforeach; ?>
	
	<?php if (Bunyad::options()->user_rating): ?>
	
		<li class="criterion-wrap user-ratings<?php echo (!Bunyad::reviews()->can_rate() ? ' voted' : ''); ?>" data-post-id="<?php echo get_the_ID(); ?>">
			
			<?php if ($type == 'stars'): ?>
				
				<div class="criterion">
					<span class="label"><?php _e('User Ratings', 'bunyad'); ?> <span class="votes">(<?php 
						printf(__('%s Votes', 'bunyad'), '<span class="vote-number">' . Bunyad::reviews()->votes_count() . '</span>'); ?>)</span>
					</span>
					
					<div class="main-stars">
						<span style="width: <?php echo esc_attr(Bunyad::reviews()->get_user_rating(null, 'percent')); ?>%;"><strong class="number"><?php 
							echo esc_html(Bunyad::reviews()->get_user_rating()); ?></strong></span>
					</div>
				</div>
				
			<?php else: ?>
			
				<div class="criterion">
					<span class="label"  style="width: <?php echo Bunyad::reviews()->get_user_rating(null, 'percent'); ?>%;"><?php _e('User Ratings', 'bunyad'); ?> <span class="votes">(<?php 
						printf(__('%s Votes', 'bunyad'), '<span class="vote-number">' . Bunyad::reviews()->votes_count() . '</span>'); ?>)</span>
					</span>
					
					<span class="rating-wrap"><span class="number"><?php 
						echo esc_html(Bunyad::reviews()->get_user_rating(null, $type)) . ($type == 'percent' ? ' %' : ''); ?></span></span>
				</div>
			
			<?php endif; ?>
			
						
			<div class="user-rate">
				<?php _e('Your Rating:', 'bunyad'); ?> 
				<select name="rating">
					
					<?php foreach (array_reverse(range(0, Bunyad::reviews()->rating_max)) as $rating): ?>
					
					<option value="<?php echo esc_attr($rating); ?>"><?php echo esc_html($rating); ?></option>
										
					<?php endforeach; ?>
					
				</select>
				
				<input type="button" class="rate-button" value="<?php _e('Rate', 'bunyad'); ?>" />
				
			</div>
			
		</li>
		
	<?php endif;?>
	
	</ul>
	
	
	<div class="verdict-box">
	
		<div class="summary">
		
			<span class="heading heading-text"><?php _e('Summary', 'bunyad'); ?></span>	
			<div class="text-font"><?php echo do_shortcode(wpautop($verdict_text)); // valid html - sanitized using wp_kses_post pre-save ?></div>
			
		</div>
		
		<div class="overall">
		
			<span class="number rating"><span class="value"><?php

				$overall_percent = round($overall / Bunyad::reviews()->rating_max * 100);
				
				echo ($type != 'points' ?  $overall_percent . '<span class="percent">%</span>' : esc_html($overall)); ?></span>
				
				<?php if ($type == 'stars'): ?>
				<div class="overall-stars">
					<div class="main-stars">
						<span style="width: <?php echo esc_attr($overall_percent); ?>%;"><strong class="number"><?php echo esc_html($overall_percent); ?></strong></span>
					</div>
				</div>
				
				<?php endif; ?>
				
				<span class="best"><span class="value-title" title="<?php echo esc_attr(($type != 'points' ? 100 : Bunyad::reviews()->rating_max)); ?>"></span></span>
			</span>
			<span class="verdict"><?php echo esc_html($verdict); ?></span>
			
		</div>
	</div>
	
</section>