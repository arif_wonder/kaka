<?php

/**
 * Slider Block - Add the in-content post slider
 */


// use locate_template to preserve variable scope and support child themes
include_once locate_template('blocks/common.php');

$block = new Bunyad_Block_Motive($atts, $tag, $this);

if (!$type) {
	$block->_data['query_args'] = array('meta_key' => '_bunyad_featured_post', 'meta_value' => 1, 'ignore_sticky_posts' => 1);	
}

// process and set the query
$query = $block->process()->_query;

// setup configuration vars
$data_vars = array(
	'data-animation' => Bunyad::options()->slider_animation,
	'data-autoplay' => Bunyad::options()->slider_autoplay,
	'data-autoplay-interval' => Bunyad::options()->slider_autoplay_interval,
);

$slider_type = 'main-content';
$slider = 'main';

/**
 * Include our slider
 * 
 * Do NOT replace with get_template_part() as the variable scope inside  get_template_part() 
 * function will change. locate_template() is compatible with child themes.
 * 
 * @see locate_template()
 */

include locate_template('partials/slider/' . $slider . '.php');