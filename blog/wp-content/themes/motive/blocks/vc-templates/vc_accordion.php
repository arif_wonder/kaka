<?php

/**
 * Call Bunyad Shortcode's accordion
 */

extract(shortcode_atts(array(
    'title' => '',
    'interval' => 0,
    'el_class' => '',
    'collapsible' => 'no',
    'active_tab' => '1'
), $atts));

echo wpb_js_remove_wpautop('[accordions]' . $content . '[/accordions]');