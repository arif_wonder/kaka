<?php
/**
 * Call Bunyad Shortcode's accordion
 */

extract(shortcode_atts(array(
	'title' => '',
	'load'  => 'hide',
), $atts));


echo wpb_js_remove_wpautop('[accordion title="'. esc_attr($title) .'" load="'. esc_attr($load) .'"]' . $content . '[/accordion]');
