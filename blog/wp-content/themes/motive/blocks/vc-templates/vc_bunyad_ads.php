<?php

/**
 * Call Bunyad Advertisement widget
 */

$atts = array_merge(array('title' => '', 'code' => ''), $atts);
extract(shortcode_atts(array('code' => ''), $atts));

if (!empty($code)) {
	$atts['code'] =  rawurldecode(base64_decode($code));
}

$type = 'Bunyad_Ads_Widget';
$args = array();

the_widget($type, $atts, $args);
