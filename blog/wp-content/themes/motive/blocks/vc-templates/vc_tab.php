<?php
/**
 * Override visual composer tabs element with Bunyad shortcode tabs
 */

$output = $title = $tab_id = '';
extract(shortcode_atts($this->predefined_atts, $atts));

echo wpb_js_remove_wpautop('[tab title="'. esc_attr($title) .'"]' . $content . '[/tab]');
