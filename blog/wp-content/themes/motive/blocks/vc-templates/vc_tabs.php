<?php
/**
 * Override visual composer tabs element with Bunyad shortcode tabs
 */

extract( shortcode_atts( array(
	'title' => '',
	'el_class' => ''
), $atts ) );


echo wpb_js_remove_wpautop('[tabs'. ('vc_tour' == $this->shortcode ? ' type="vertical"' : '') .']' . $content . '[/tabs]');