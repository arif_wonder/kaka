<?php

/**
 * Category Template
 * 
 * Sets up the correct loop format to use. Additionally, meta is processed for other
 * layout preferences.
 */

$category = get_category(get_query_var('cat'), false);
$cat_meta = Bunyad::options()->get('cat_meta_' . $category->term_id);

// no template? fall-back to default template
if (!$cat_meta OR !$cat_meta['template']) {
	$cat_meta['template'] = Bunyad::options()->default_cat_template;
}

/**
 * Select the listing template to use
 */
if (in_array($cat_meta['template'], array('alt', 'classic'))) {
	$loop_template = 'loop-' . $cat_meta['template'];
}
else {
	// default to grid template
	$loop_template = 'loop';
	

	// set type of loop grid - if using the grid
	$loop_grid = 2;
	
	if ($cat_meta['template'] == 'grid-3') {
		$loop_grid = 3;
	}
	
	Bunyad::registry()->set('loop_grid', $loop_grid);
	
}

// have a sidebar preference?
if (!empty($cat_meta['sidebar'])) {
	Bunyad::core()->set_sidebar($cat_meta['sidebar']);
}


// store variables in registry
Bunyad::registry()
	->set('loop_template', $loop_template)
	->set('listing_class', 'term-wrap-' . $category->term_id);

// render
get_template_part('archive');
