<?php 
/**
 * Template to output comment form - called via single.php
 * 
 * @see comments_template()
 */
?>

	<?php if (post_password_required()): ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'bunyad'); ?></p>
	<?php return; endif;?>

	<div id="comments" class="comments">

	<?php if (have_comments()) : ?>
	
		<h3 class="section-head dark cf">
			<span class="title"><?php _e('Discussion', 'bunyad'); ?></span><span class="number"><?php comments_number(''); ?></span>
		</h3>
	
		<ol class="comments-list">
			<?php
				get_template_part('partials/comment');
				wp_list_comments(array('callback' => 'bunyad_motive_comment', 'max-depth' => 4));
			?>
		</ol>

		<?php if (get_comment_pages_count() > 1 && get_option('page_comments')): // are there comments to navigate through ?>
		<nav class="comment-nav">
			<div class="nav-previous"><?php previous_comments_link(__( '&larr; Older Comments', 'bunyad')); ?></div>
			<div class="nav-next"><?php next_comments_link(__( 'Newer Comments &rarr;', 'bunyad')); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

	<?php elseif (!comments_open() && ! is_page() && post_type_supports(get_post_type(), 'comments')):	?>
		<p class="nocomments"><?php _e('Comments are closed.', 'bunyad'); ?></p>
	<?php endif; ?>
	
	
	<?php 
	
	/**
	 * Configure and output the comment form
	 */
	
	$commenter = wp_get_current_commenter();
	
	$comment_field = '
			<p>
				<textarea name="comment" id="comment" cols="45" rows="10" aria-required="true" placeholder="'. esc_attr__('Your Comment', 'bunyad') .'"></textarea>
			</p>
	';
	
	comment_form(array(
		'title_reply' => '<span class="section-head alt cf"><span>' . __('Leave A Reply', 'bunyad') . '</span></span>',
		'title_reply_to' => '<span class="section-head alt cf">' . __('Reply To %s', 'bunyad') . '</span>',
		'comment_notes_before' => $comment_field,
		'comment_notes_after'  => '',
	
		'logged_in_as' => '<p class="logged-in-as">' . sprintf(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'bunyad'), 
									admin_url('profile.php'), $user_identity, wp_logout_url(get_permalink())) . '</p>' . $comment_field,
	
		'comment_field' => '',
	
		'id_submit' => 'comment-submit',
		'label_submit' => __('Submit', 'bunyad'),
	
		'cancel_reply_link' => __('Cancel Reply', 'bunyad'),
	

		'fields' => array(
			'author' => '
				<p>
					<label for="author">' . _x('Your Name', 'Comment Form', 'bunyad') . ' <span class="info">('. _x('required', 'Comment Form', 'bunyad')  .')</span></label> 
					<input name="author" id="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" aria-required="true" />
				</p>',
	
			'email' => '
				<p>
					<label for="email">' . _x('Your Email', 'Comment Form', 'bunyad') . ' <span class="info">('. _x('required', 'Comment Form', 'bunyad')  .')</span></label> 
					<input name="email" id="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" aria-required="true" />
				</p>
			',
	
			'url' => '
				<p>
					<label for="url">' . _x('Your Website', 'Comment Form', 'bunyad') . ' <span class="info">('. _x('optional', 'Comment Form', 'bunyad')  .')</span></label> 
					<input name="url" id="url" type="text" value="' . esc_attr($commenter['comment_author_url']) . '" />
				</p>
			'
		),
		
	)); ?>

</div><!-- #comments -->
