<?php

/**
 * Content Template is used for every post format and used on single posts
 */

// post has review? 
$review = Bunyad::posts()->meta('reviews');

?>

<article <?php
	// hreview has to be first class because of rich snippet classes limit 
	Bunyad::markup()->attribs('post-wrapper', array(
		'id'        => 'post-' . get_the_ID(),
		'class'     => ($review ? 'hreview ' : '') . join(' ', get_post_class()),
		'itemscope' => '', 
		'itemtype'  => 'http://schema.org/Article'
	)); ?>>
	
	<header class="post-header cf">
		<?php if (!Bunyad::posts()->meta('featured_disable')): ?>
		
		<div class="featured">
		
			<?php if (get_post_format() == 'gallery'): // get gallery template ?>
			
				<?php get_template_part('partials/gallery-format'); ?>
				
			<?php elseif (Bunyad::posts()->meta('featured_video')): // featured video available? ?>
			
				<div class="featured-vid">
					<?php echo apply_filters('bunyad_featured_video', Bunyad::posts()->meta('featured_video')); // sanitized html via lib/admin/meta-boxes.php ?>
				</div>
				
			<?php else: ?>
			
				<?php 
					/**
					 * Normal featured image
					 */
			
					$caption = get_post(get_post_thumbnail_id())->post_excerpt;
					$url     = get_permalink();
					
					// on single page? link to image
					if (is_single()):
						$url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); 
						$url = $url[0];
					endif;
					
					// which image?
					$col_relative_width = Bunyad::get('motive')->block_relative_width();
	
					// more than 67% of .main-wrap?
					$image = ($col_relative_width > 0.67 ? 'motive-slider-full' : 'motive-alt-slider');
					
					 
				?>
			
			
				<a href="<?php echo esc_url($url); ?>" itemprop="image" class="image-link"><?php the_post_thumbnail(
						$image,  // larger image if no sidebar
						array('title' => strip_tags(get_the_title()))
					); ?>
					
					<?php 
					if (!is_single()): 
						do_action('bunyad_image_overlay');
					endif; 
					?>	
				</a>
								
				<?php if (!empty($caption)): // have caption ? ?>
						
					<div class="caption"><?php echo esc_html($caption); ?></div>
						
				<?php endif;?>
				
			<?php endif; // normal featured image ?>
		</div>
		
		<?php endif; // featured check ?>

		<?php 
			/**
			 * Set h1 tag on single post page
			 */
			$tag = 'h1';
			if (!is_single() OR is_front_page()) {
				$tag = 'h2';
			}
		?>

		<<?php echo esc_attr($tag); ?> class="post-title item fn" itemprop="name">
		<?php if (is_single()): the_title(); else: ?>
		
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
				
		<?php endif;?>
		</<?php echo esc_attr($tag); ?>>
		
		<?php do_action('bunyad_comments_snippet'); ?>
		
	</header><!-- .post-header -->
	
	<div class="post-meta">
		<span class="posted-by"><?php _ex('By', 'Post Meta', 'bunyad'); ?> 
			<span class="reviewer" itemprop="author"><?php the_author_posts_link(); ?></span>
		</span>
		 
		<span class="posted-on"><?php _ex('on', 'Post Meta', 'bunyad'); ?>
			<span class="dtreviewed">
				<time class="value-datetime" datetime="<?php echo esc_attr(get_the_time('c')); ?>" itemprop="datePublished"><?php echo esc_html(get_the_date()); ?></time>
			</span>
		</span>
		
		<span class="cats">
			<?php 

			// show categories
			$categories = get_the_category();
			if (!empty($categories)):
				foreach ($categories as $category):
				
			?>
			
				<a href="<?php echo esc_url(get_category_link($category->term_id)); ?>" class="cat cat-color-<?php echo esc_attr($category->term_id); ?>"><?php 
					echo esc_html($category->name); ?></a>
			
			<?php
				endforeach;
			endif;
			
			?>
			
		</span>
	</div>
	
	<div class="post-container cf">
		<div <?php Bunyad::markup()->attribs('post-content', array(
				'class' => 'post-content text-font description' . (Bunyad::posts()->meta('content_slider') ? ' post-slideshow' : ''),
				'itemprop' => 'articleBody')); ?>>
			
			<?php
			// excerpts or main content?
			if (is_single() OR !Bunyad::options()->show_excerpts_classic OR Bunyad::posts()->meta('content_slider')): 
				Bunyad::posts()->the_content();
			else:
				echo Bunyad::posts()->excerpt(null, Bunyad::options()->excerpt_length_classic, array('force_more' => true));
			endif;
			
			?>
			
			<?php 
			// multi-page post - add numbered pagination
			if (!Bunyad::posts()->meta('content_slider')):
				wp_link_pages(array(
					'before' => '<div class="main-pagination post-pagination">', 
					'after' => '</div>', 
					'link_before' => '<span class="page-link">',
					'link_after' => '</span>'));
			endif;

			// multi-page content slideshow post?
			if (Bunyad::posts()->meta('content_slider')):
				get_template_part('partials/content/pagination-next');
			endif;
			
			?>
			
		</div><!-- .post-content -->		
	</div>

	<div class="post-footer cf">
			
		<?php if ((is_single() OR Bunyad::options()->show_tags_classic) && Bunyad::options()->show_tags): ?>
		
		<div class="post-tags"><?php the_tags('<i class="fa fa-tags"></i><span>' . __('Tags: ', 'bunyad') . '</span>', ''); ?></div>
		
		<?php endif; ?>


		<?php get_template_part('partials/content/social-share'); ?>
		
	</div>
		
</article>

<?php if (is_single() && Bunyad::options()->author_box) : // author box? ?>

	<?php get_template_part('partials/author-box'); ?>

<?php endif; ?>


<?php if (is_single() && Bunyad::options()->post_navigation): ?>

<section class="navigate-posts">

	<div class="previous"><?php 
		previous_post_link('<span class="main-color title"><i class="fa fa-angle-left"></i> ' . __('Previous Article', 'bunyad') .'</span><span class="link">%link</span>'); ?>
	</div>
	
	<div class="next"><?php 
		next_post_link('<span class="main-color title">'. __('Next Article', 'bunyad') .' <i class="fa fa-angle-right"></i></span><span class="link">%link</span>'); ?>
	</div>
	
</section>

<?php endif; ?>


<?php
	// related posts template 
	get_template_part('partials/content/related-posts'); 
?>
