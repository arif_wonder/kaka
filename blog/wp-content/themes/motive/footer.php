
	<footer <?php Bunyad::markup()->attribs('main-footer', array('class' => array('main-footer', Bunyad::options()->footer_style))); ?>>
	
	<?php if (!Bunyad::options()->disable_footer): ?>
	
		<section class="upper-footer">
			<div class="wrap">
			
			<?php if (is_active_sidebar('motive-main-footer')): ?>
				<ul class="widgets ts-row cf">
					<?php dynamic_sidebar('motive-main-footer'); ?>
				</ul>
			<?php endif; ?>
			
			
			<?php if (!Bunyad::options()->disable_middle_footer): ?>
				<div class="middle-footer">
					<?php if (is_active_sidebar('motive-mid-footer')): ?>
						<ul class="widgets ts-row cf">
							<?php dynamic_sidebar('motive-mid-footer'); ?>
						</ul>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			
			</div>
		</section>
	
	<?php endif; ?>
	
	
	<?php if (!Bunyad::options()->disable_lower_footer): ?>
		<section class="lower-footer">
			<div class="wrap">
		
			<?php if (is_active_sidebar('motive-lower-footer')): ?>
			
				<div class="widgets">
					<?php dynamic_sidebar('motive-lower-footer'); ?>
				</div>
			
			<?php endif; ?>
		
			</div>
		</section>
	<?php endif; ?>
	
	</footer>
	
</div> <!-- .main-wrap -->

<?php wp_footer(); ?>

</body>
</html>