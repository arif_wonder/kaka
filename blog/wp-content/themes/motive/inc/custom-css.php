<?php

class Bunyad_CustomCSS_Motive
{
	public function __construct()
	{
		add_action('after_setup_theme', array($this, 'init'), 12);
	}	
	
	public function init()
	{
		// set external css handler
		if ($this->has_custom_css()) {			
			add_action('template_redirect', array($this, 'global_custom_css'), 1);
		}
		
		add_action('wp_enqueue_scripts', array($this, 'register_custom_css'), 99);
		
	}
	
	/**
	 * Check if the theme has any custom css
	 */
	public function has_custom_css()
	{
		if (count(Bunyad::options()->get_all('css_'))) {
			return true;
		} 
		
		// check if a cat has custom color
		foreach ((array) Bunyad::options()->get_all('cat_meta_') as $cat) 
		{
			if (!empty($cat)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Action callback: Output Custom CSS
	 */
	public function global_custom_css()
	{		
		// custom css requested?
		if (empty($_GET['bunyad_custom_css']) OR intval($_GET['bunyad_custom_css']) != 1) {
			return;
		}
		
		// set 200 - might be 404
		status_header(200);
		header("Content-type: text/css; charset: utf-8"); 

		include_once locate_template('inc/css-generator.php');
		
		/*
		 * Output the CSS customizations
		 */
		$render = new Bunyad_Custom_Css;
		$render->args = $_GET;
		echo $render->render();
		exit;
	}
	
	
	/**
	 * Action callback: Register Custom CSS with low priority 
	 */
	public function register_custom_css()
	{
		if (is_admin()) {
			return;
		}
		
		// add custom css
		if ($this->has_custom_css()) {
			
			$query_args = array();
			
			/*
			 * Global color changes?
			 */ 
			if (is_category() OR is_single()) {
	
				$object = get_queried_object();
				$query_args['anchor_obj'] = '';
				
				if (is_category()) {
					$query_args['anchor_obj'] = $object->cat_ID;
				}
				else {
					// be consistent with the behavior that's like cat labels
					$categories = current((array) get_the_category($object->ID));
					
					if (is_object($categories)) {
						$query_args['anchor_obj'] = $categories->cat_ID;
					}
				}
				
				// only used for main color
				$meta = Bunyad::options()->get('cat_meta_' . $query_args['anchor_obj']);
				if (empty($meta['color'])) {
					unset($query_args['anchor_obj']);
				}				
			}
			
			$query_args = array_merge($query_args, array('bunyad_custom_css' => 1));
			
			/*
			 * Custom CSS Output Method - external or on-page?
			 */
			if (Bunyad::options()->css_custom_output == 'external') 
			{
				wp_enqueue_style('custom-css', add_query_arg($query_args, get_site_url() . '/'));
						
				// add css that's supposed to be per page
				$this->add_per_page_css();
			}
			else {

				include_once locate_template('inc/css-generator.php');

				// associate custom css at the end
				$source = 'motive-core';
				
				$check  = array('motive-responsive-skin', 'motive-responsive', 'motive-skin');
				foreach ($check as $sheet) {
					if (wp_style_is($sheet, 'enqueued')) {
						$source = $sheet;
						break;
					}
				}
				
				// add to on-page custom css
				$render = new Bunyad_Custom_Css;
				$render->args = $query_args;
				Bunyad::core()->enqueue_css($source, $render->render() . $this->add_per_page_css(true));
			}
		}
	}
	
	/**
	 * Custom CSS for pages and posts that shouldn't be cached through css-generator.php because 
	 * the size will increase exponentially.
	 * 
	 */
	public function add_per_page_css($return = false) 
	{
		if (!is_admin()) {
			
			if (is_singular() && Bunyad::posts()->meta('bg_image')) {
			
				$bg_type = Bunyad::posts()->meta('bg_image_bg_type');
				$the_css = 'background: url("' . esc_attr(Bunyad::posts()->meta('bg_image')) . '");';
			
				if (!empty($bg_type)) {
				
					if ($bg_type == 'cover') {
						$the_css .= 'background-repeat: no-repeat; background-attachment: fixed; background-position: center center; '  
			 			. '-webkit-background-size: cover; -moz-background-size: cover;-o-background-size: cover; background-size: cover;';
					}
					else {
						$the_css .= 'background-repeat: ' . esc_attr($bg_type) .';';
					}
				}
				
				Bunyad::registry()->set('per_page_css', Bunyad::registry()->per_page_css . "\n" . 'body.boxed { ' . $the_css . ' }'); 
			}
			
			if ($return) {
				return Bunyad::registry()->per_page_css;
			}
			
			
			if (Bunyad::registry()->per_page_css) {
				// enqueue it for inline css
				Bunyad::core()->enqueue_css(
					(wp_style_is('custom-css', 'enqueued') ? 'custom-css' : 'motive-core'), 
					Bunyad::registry()->per_page_css
				);
			}
		}
	}
}

// init and make available in Bunyad::get('motive_codes')
Bunyad::register('motive_custom_css', array(
	'class' => 'Bunyad_CustomCSS_Motive',
	'init' => true
));