<?php
/**
 * Additional actions to perform at import
 *
 * @see Bunyad_Admin_Importer
 */

class Bunyad_Import_Motive
{
	public function __construct()
	{
		add_filter('bunyad_import_attachments_original', array($this, 'original_attachments'));
		add_action('bunyad_import_process_posts_pre', array($this, 'process_posts'));
		add_action('bunyad_import_completed', array($this, 'set_home'));
		add_filter('bunyad_import_menu_fields', array($this, 'import_menu_fields'));
		add_action('bunyad_import_completed', array($this, 'import_fix_menu'));
	}
	
	/**
	 * Attachment URLs to preserve - do not replace with a random image
	 * 
	 * @param array $map
	 */
	public function original_attachments($map)
	{
		$map = array(
			'http://motive.theme-sphere.com/wp-content/uploads/2014/10/demo-720-ad.jpg'
		);
		
		return $map;
	}

	/**
	 * Action callback: Replace category ids in shortcodes - for pages
	 * @param Bunyad_Admin_Importer_WpImport $object
	 */
	public function process_posts($object)
	{
		foreach ($object->posts as $post_key => $post) {

			if ($post['post_type'] != 'page' OR $post['status'] != 'publish') {
				continue;
			}

			// modify shortcode categories
			preg_match_all('#\[([a-z\_]+)\s.+?cat=\"(\d+)\"[^\]]+\]#', $post['post_content'], $matches);
			foreach ((array) $matches[0] as $key => $match) {

				$term_id = $object->processed_terms[ $matches[2][$key] ];
				if (!isset($term_id)) {
					continue;
				}

				$replace = str_replace('cat="'. $matches[2][$key] .'"', 'cat="' . $term_id . '"', $match);
				$post['post_content'] = str_replace($match, $replace, $post['post_content']);
			}

			$object->posts[$post_key]['post_content'] = $post['post_content'];
		}
	}

	public function set_home()
	{
		// set the home page
		$home = get_page_by_title('Advanced Home');

		if (is_object($home)) {
			update_option('show_on_front', 'page');
			update_option('page_on_front', $home->ID);
		}
	}

	/**
	 * Custom Menu fields for the sample menu
	 * 
	 * @param array $values
	 */
	public function import_menu_fields($values = array())
	{

		$alt_home = get_page_by_title('Alternate Home 2');
		if (!empty($alt_home->ID)) {
			$alt_home = get_permalink($alt_home);
		}


		return array(
			'mega_menu' => array('Features' => 'normal', 'Lifestyle' => 'category', 'Technology' => 'category-featured'),
			'url' => array(
				'Home With Default Grid Slider' => home_url(), 
				'Alternate Home' => $alt_home
			),
		);
	}

	/**
	 * Action callback: Fix menu on sample import
	 * 
	 * @param object $import
	 */
	public function import_fix_menu($import)
	{
		// remove an item from menu
		$item = get_page_by_title('Shop With Sidebar', OBJECT, 'nav_menu_item');
		
		if (is_object($item)) {
			wp_delete_post($item->ID);
		}
	}


}


// init and make available in Bunyad::get('motive_import')
Bunyad::register('motive_import', array(
	'class' => 'Bunyad_Import_Motive',
	'init' => true
));