<?php

/**
 * Overrides for Bunyad Shortcodes Plugin
 * 
 * WARNING: Only use to extend Bunyad Shortcodes functionality or override the markup.
 * 
 * @see Bunyad_ShortCodes
 *
 */
class Bunyad_ShortCodes_Motive extends Bunyad_ShortCodes
{
	public function __construct()
	{
		// override Bunyad_Shortcode instance
		Bunyad_ShortCodes::set_proxy($this);
		
		parent::__construct();
		
		// remove unsupported
		unset($this->shortcodes['default']['box']);
		
		
		if (is_admin()) {
			add_filter('bunyad_shortcodes_lists_options', array($this, 'gui_list_styles'));	
		}

		// set shortcode handler
		Bunyad::options()->set('shortcodes', $this);
		
		$this->set_config('row_classes', array('ts-row', 'cf'));
	}
	
	/**
	 * Extended Shortcode: Subscription Form
	 * 
	 * @see Bunyad_ShortCodes::sc_feedburner_form()
	 */
	public function sc_feedburner_form($atts, $content = null)
	{
		extract(shortcode_atts(array('heading' => '', 'label' => '', 'button_text' => '', 'user' => ''), $atts), EXTR_SKIP);
		
		return '
		<div class="feedburner">
			<p class="heading">'. esc_html($heading) .'</p>
			<form method="post" action="http://feedburner.google.com/fb/a/mailverify">
			
				<input type="hidden" value="'. esc_attr($user) .'" name="uri" />
				<input type="hidden" name="loc" value="en_US" />
			
				<label for="feedburner-email">' . esc_html($label) .'</label>
				<input type="text" id="feedburner-email" name="email" class="feedburner-email" placeholder="'. esc_attr($label) .'" />
				
				<button class="feedburner-subscribe" type="submit" name="submit"><i class="fa fa-envelope"></i></button>
				
			</form>
		</div>
		';
	}

	/**
	 * Filter callback: Modify list styles in the GUI Shortcode Generator
	 * 
	 * @param array $options
	 */
	public function gui_list_styles($options)
	{
		$options['type'] = array(
			'name'  => 'type',
			'label' => __('List Style', 'bunyad-shortcodes'),
			'type'  => 'select',
			'options' => array(
				'none' => __('No Style - Browser Defaults', 'bunyad'),
				'styled' => __('Theme Style', 'bunyad'),
				'styled circle' => __('Circle Style (Ordered Lists Only)', 'bunyad'),
				'styled simple' => __('Simple Style', 'bunyad'),
		));
		
		unset($options['style']);
				
		return $options;
	}
}

// init and make available in Bunyad::get('motive_codes')
Bunyad::register('motive_codes', array(
	'class' => 'Bunyad_Shortcodes_Motive',
	'init' => true
));