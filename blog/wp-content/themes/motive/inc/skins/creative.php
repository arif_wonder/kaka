<?php

class Bunyad_Skin_Creative_Motive 
{
	public function __construct() 
	{
		add_filter('bunyad_vc_map_common', array($this, 'vc_map_common'));
		add_filter('bunyad_default_block_attribs', array($this, 'default_attribs'));
		add_filter('bunyad_excerpt_hellip', '__return_empty_string');
		
		add_action('init', array($this, 'init'));
	}
	
	public function init()
	{
		// add "more" text for excerpts
		Bunyad::posts()->more_html = '<span class="read-more arrow"><a href="%s" title="%s">&rarr;</a></span>';
		Bunyad::core()->add_body_class('skin-creative');
	}
	
	/**
	 * Filter callback: Add to the default block shortcode attributes
	 * 
	 * @param array $attribs
	 * @see Bunyad_Theme_Motive::setup_shortcodes()
	 */
	public function default_attribs($attribs)
	{
		$attribs['sub_title'] = '';
		return $attribs;
	}
	
	/**
	 * Filter callback: Add a subtitle attribute to Visual Composer
	 * 
	 * @param array $common
	 */
	public function vc_map_common($common)
	{
		$sub_title = array(
			'type' => 'textfield',
			'heading' => __('Sub Heading (Optional)', 'bunyad'),
			'description' => __('A special sub-heading for large headings on creative skin.', 'bunyad'),
			'value'  => '',
			'param_name' => 'sub_title',
		);
		
		// add the field after title field
		$offset = array_search('title', array_keys($common));
		$common = array_merge(
			array_slice($common, 0, $offset+1),
			array('sub_title' => $sub_title),
			array_slice($common, $offset-1)
		);
		
		return $common;
	}
}


// init and make available in Bunyad::get('motive_skin_creative')
Bunyad::register('motive_skin_creative', array(
	'class' => 'Bunyad_Skin_Creative_Motive',
	'init' => true
));