<?php

/**
 * Theme related configuration and modifiers for Visual Composer page builder
 */
class Bunyad_Visual_Composer
{
	public function __construct()
	{
		add_action('vc_before_init', array($this, 'vc_setup'));
		add_action('vc_after_init', array($this, 'after_setup'));
		
		add_filter('vc_shortcodes_css_class', array($this, 'custom_classes'), 10, 2);
		
		// row filters
		add_filter('vc_theme_after_vc_row', array($this, 'after_vc_row'), 10, 2);
		add_filter('vc_theme_after_vc_row_inner', array($this, 'after_vc_row'), 10, 2);
		
		add_filter('vc_shortcode_output', array($this, 'fix_widget_titles'), 10, 2);
		add_filter('wpb_widget_title', array($this, 'vc_widget_titles'), 10, 2);
		
		Bunyad::registry()->layout = array(
			'row_depth' => 0,
			'row_open'  => false,
			'row_cols'  => array(),
			'row_parent_open' => false
		);
		
		// temporary fix for visual composer issues
		add_action('admin_init', array($this, 'fix_frontend_title'));
		
		// fix page template for visual composer
		add_action('save_post', array($this, 'set_page_template'));
		
		// load pre-made templates
		add_filter('vc_load_default_templates', array($this, 'load_templates'));
		
	
		// add extra checkbox param that supports default settings
		vc_add_shortcode_param('checkbox_ext', array($this, 'add_param_checkbox'), get_template_directory_uri() . '/admin/js/vc-param-ext.js');
	}
	
	/**
	 * Action callback: Setup at VC init
	 */
	public function vc_setup()
	{
		// set as theme and disable update nag - use local updates
		vc_set_as_theme(true);
		
		// set shortcode directory
		vc_set_shortcodes_templates_dir(locate_template('blocks/vc-templates'));
		
		// register blocks
		add_action('init', array($this, 'register_blocks'));
		
		// register widgets as vc blocks
		$this->register_widgets();
		
		// remove non-supported blocks
		add_action('admin_init', array($this, 'remove_unsupported'));
		
		// fix draft posts issue on front-end editing
		add_filter('bunyad_block_query_args', array($this, 'frontend_ignore_drafts'), 10, 1);
	}
	
	/**
	 * Action callback:  Run after VC is setup
	 */
	public function after_setup()
	{
		remove_action('wp_head', array(visual_composer(), 'addMetaData'));
		remove_action('body_class', array(visual_composer(), 'bodyClass'));
		
		// add fixes for front-end
		if (vc_is_page_editable()) {
			wp_enqueue_script('motive-vc', get_template_directory_uri() . '/js/bunyad-vc.js', array('jquery'), false, true);
		}
	}
	
	/**
	 * Action callback: Temporary fix for VC bug
	 */
	public function fix_frontend_title()
	{
		// TEMPORARY FIX: Visual composer front-end topic title changing fix
		if (vc_is_inline() && !empty($_GET['post_id'])) {
			$GLOBALS['wp_the_query'] = new WP_Query(array('p' => intval($_GET['post_id']), 'post_type' => 'any'));
		}		
	}
	
	/**
	 * Filter callback: Drafts are shown in listings on VC front-end - fix that.
	 */
	public function frontend_ignore_drafts($args)
	{
		$args['post_status'] = array('publish');
		return $args;
	}	
	
	/**
	 * Filter callback: Modify default WP widget titles output from VC
	 * 
	 * @param unknown_type $content
	 */
	public function fix_widget_titles($content, $shortcode)
	{
		// only work on vc_wp_* shortcodes - ignore the rest
		if (is_object($shortcode) && method_exists($shortcode, 'settings') && !strstr($shortcode->settings('base'), 'vc_wp_')) {
			return $content;
		}
		
		return preg_replace('#<h2 class="widgettitle">(.+?)</h2>#', '<h3 class="widget-title section-head cf main-color"><span>\\1</span></h3>', $content);
	}
	
	/**
	 * Filter callback: Modify default VC widgets title output
	 * 
	 * @param string $output
	 * @param array $params
	 */
	public function vc_widget_titles($output, $params = array()) 
	{
		if (empty($params['title'])) {
			return $output;
		}

		$output = '<h3 class="wpb_heading section-head cf main-color ' . $params['extraclass'] . '"><span class="title">' . $params['title'] . '</span></h3>';
		
		return $output;
	}
	
	/**
	 * Action callback: Set proper page template on save for Visual Composer pages
	 * 
	 * @param integer $post_id
	 */
	public function set_page_template($post_id)
	{
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}
		
		// permissions?
		if (!current_user_can('edit_page', $post_id)) {
			return;
		}
		
		// has visual composer content? wpb_vc_js_status=true would be limiting
		if (!empty($_POST['post_type']) && $_POST['post_type'] == 'page' && strstr($_POST['post_content'], '[vc_row')) {
			
			// set page template
			update_post_meta($post_id, '_wp_page_template', 'page-blocks.php');
		}
	}
	
	/**
	 * Register blocks with Visual Composer
	 */
	public function register_blocks()
	{		
		// only enable in admin or when front-end editor is called
		if (!is_admin() && !vc_is_frontend_editor() && !vc_is_page_editable()) {
			return;
		}
		
		/**
		 * Get categories list for dropdown options in VC 
		 */
		$categories = get_terms('category', array(
			'hide_empty' => 0,
			'hide_if_empty' => false,
			'hierarchical' => 1, 
			'order_by' => 'name' 
		));
	
		$categories = array_merge(
			array(__('-- None / Not Limited --', 'bunyad') => ''), 
			$this->_recurse_terms_array(0, $categories)
		);
		
		/**
		 * The default options generally shared between blocks 
		 */
		$common = array(	
			'cat' => array(
				'type' => 'dropdown',
				'heading' => __('Main Category', 'bunyad'),
				'description' => __('Posts will be limited to this category and category name and color will be used where applicable.', 'bunyad'),
				'value'  => $categories,
				'param_name' => 'cat',
				'admin_label' => true,
			),
	
			'posts' => array(
				'type' => 'textfield',
				'heading' => __('Number of Posts', 'bunyad'),
				'value'  => 5,
				'param_name' => 'posts',
				'admin_label' => false,
			),
			
			'sort_by' => array(
				'type' => 'dropdown',
				'heading' => __('Sort By', 'bunyad'),
				'value'  => array(
					__('Published Date', 'bunyad') => '',
					__('Modified Date', 'bunyad') => 'modified',
					__('Random', 'bunyad') => 'random',
					__('Comment Count', 'bunyad') => 'comments',
					__('Alphabetical', 'bunyad') => 'alphabetical',
					__('Review Rating', 'bunyad') => 'rating'
				),
				'param_name' => 'sort_by',
				'admin_label' => false,
			),
			
			'sort_order' => array(
				'type' => 'dropdown',
				'heading' => __('Sort Order', 'bunyad'),
				'value'  => array(
					__('Descending - Higher to lower', 'bunyad') => 'desc',
					__('Ascending - Lower to Higher', 'bunyad') => 'asc',
				),
				'param_name' => 'sort_order',
				'admin_label' => false,
			),
			
			'title' => array(
				'type' => 'textfield',
				'heading' => __('Custom Heading  (Optional)', 'bunyad'),
				'description' => __('By default, the main selected category\'s name is used as the title.', 'bunyad'),
				'value'  => '',
				'param_name' => 'title',
				'admin_label' => true,
			),
			
			'title_type' => array(
				'type' => 'dropdown',
				'heading' => __('Heading Type', 'bunyad'),
				'description' => __('Use Small Headings for 1/3 columns. Default headings are good for full-width and half column blocks.', 'bunyad'),
				'value'  => array(
					__('Default', 'bunyad') => '',
					__('Small Headings', 'bunyad') => 'small',
					__('Disabled', 'bunyad') => 'none',
				),
				'param_name' => 'title_type',
			),
			
			'view_all' => array(
				'type' => 'dropdown',
				'heading' => __('View All Link?', 'bunyad'),
				'description' => __('Show "view all" link in heading?', 'bunyad'),
				'value' => array(
					__('Yes', 'bunyad') => 1,
					__('No', 'bunyad') => 0
				),
				'param_name' => 'view_all',
			),
			
			'link' => array(
				'type' => 'textfield',
				'heading' => __('Heading Link (Optional)', 'bunyad'),
				'description' => __('By default, the main selected category\'s link is used.', 'bunyad'),
				'value'  => '',
				'param_name' => 'link',
			),
			
			'main_color' => array(
				'type' => 'colorpicker',
				'heading' => __('Block Color (Optional)', 'bunyad'),
				'description' => __('Affects heading and image hovers. By default, the main category\'s color is used.', 'bunyad'),
				'value'  => '',
				'param_name' => 'main_color',
			),
			
			'tags' => array(
				'type' => 'textfield',
				'heading' => __('Posts From Tags', 'bunyad'),
				'description' => __('A single or multiple tags. Enter tag slugs separated by commas. Example: food,sports', 'bunyad'),
				'value'  => '',
				'param_name' => 'tags',
			),
			
			'term_ids' => array(
				'type' => 'textfield',
				'heading' => __('From Multiple Categories', 'bunyad'),
				'description' => __('If you need posts from more categories. Enter cat ids separated by commas. Example: 10,21,31', 'bunyad'),
				'value'  => '',
				'param_name' => 'term_ids',
			),
			
			'post_format' => array(
				'type' => 'dropdown',
				'heading' => __('Post Format', 'bunyad'),
				'description' => __('', 'bunyad'),
				'value'  => array(
					__('All', 'bunyad') => '',
					__('Video', 'bunyad') => 'video',
					__('Gallery', 'bunyad') => 'gallery',
					__('Image', 'bunyad') => 'image',
				),
				'param_name' => 'post_format',
			),
			
			
			/* 'post_meta' => array(
				'type' => 'checkbox_ext',
				'heading' => __('Listing Meta', 'bunyad'),
				'description' => __('', 'bunyad'),
				'value' => 'date',
				'options'  => array(
					__('Date') => 'date',
					__('Category') => 'category',
				),
				'param_name' => 'post_meta',
			), */
						
			'offset' => array(
				'type' => 'textfield',
				'heading' => __('Advanced: Offset', 'bunyad'),
				'description' => __('An offset can be used to skip first X posts from the results.', 'bunyad'),
				'value'  => '',
				'param_name' => 'offset',
			),
			
			'post_type' => array(
				'type' => 'posttypes',
				'heading' => __('Advanced: Post Types', 'bunyad'),
				'description' => __('Use this feature if Custom Post Types are needed.', 'bunyad'),
				'value'  => '',
				'param_name' => 'post_type',
			),
			
			'excerpt_len' => array(
				'type' => 'textfield',
				'heading' => __('Custom Excerpt Length', 'bunyad'),
				'description' => __('Override the default number of words for any excerpt in this block.', 'bunyad'),
				'value'  => '',
				'param_name' => 'excerpt_len',
			),
		);
		
		$common = apply_filters('bunyad_vc_map_common', $common);
		
		/**
		 * Main highlights/Spotlight block
		 */
		$main_highlights = $common;
		$main_highlights['posts']['value'] = 5;
		
		vc_map(array(
			'name' => __('Spotlight Block', 'bunyad'),
			'description' => __('News in spotlight. Best as first block on home.', 'bunyad'),
			'base' => 'main_highlights',
			'icon' => 'motive-block',
			'class' => '',
			'weight' => 1,
			'category' => __('Home Blocks', 'bunyad'),
			'params' => $main_highlights,
		));
		
		
		/**
		 * News Bar block
		 */
		$news_bar = $common;
		$news_bar['posts']['value'] = 6;

		vc_map(array(
			'name' => __('News Bar Block', 'bunyad'),
			'description' => __('News strip best for small columns.', 'bunyad'),
			'base' => 'news_bar',
			'icon' => 'motive-block',
			'class' => '',
			'weight' => 1,
			'category' => __('Home Blocks', 'bunyad'),
			'params' => array_merge(array(
				'no_thumbs' => array(
					'type' => 'dropdown',
					'heading' => __('Small Thumbnails', 'bunyad'),
					'description' => __('Show the small thumbnails in this block?', 'bunyad'),
					'value'  => array(
						__('Show', 'bunyad') => 0,
						__('Hide', 'bunyad') => 1,
					),
					'param_name' => 'no_thumbs',
					
					// show only for full-split or default type
					'dependency' => array(
						'element' => 'type',
						'value' => array('', 'full-split'),
					)
				)), $news_bar
			) // params
		));
		
		
		/**
		 * Blog/Listing block
		 */
		$blog = array_merge(array(
				'type' => array(
					'type' => 'dropdown',
					'heading' => __('Listing Type', 'bunyad'),
					'description' => __('', 'bunyad'),
					'value'  => array(
						__('Default Style (In Theme Settings)', 'bunyad') => '',
						__('Grid Style - 2 Column', 'bunyad') => 'grid-2',
						__('Grid Style - 3 Column', 'bunyad') => 'grid-3',
						__('Blog List Style', 'bunyad') => 'alt',
						__('Classic - Large Blog Style', 'bunyad') => 'classic',
					),
					'param_name' => 'type',
				),
				
				'pagination' => array(
					'type' => 'dropdown',
					'heading' => __('Pagination', 'bunyad'),
					'value'  => array(
						__('Disabled', 'bunyad') => '',
						__('Enabled', 'bunyad') => '1',
					),
					'param_name' => 'pagination',
				),
			), $common
		);

		$blog['posts']['value'] = 8;
		
		unset($blog['excerpt_len']);
		
		vc_map(array(
			'name' => __('Post Listing Block', 'bunyad'),
			'description' => __('For blog/category style listings.', 'bunyad'),
			'base' => 'blog',
			'icon' => 'motive-block',
			'class' => '',
			'weight' => 1,
			'category' => __('Home Blocks', 'bunyad'),
			'params' => $blog,
		));
		
		
		/**
		 * Highlights block
		 */
		vc_map(array(
			'name' => __('Highlights Block', 'bunyad'),
			'base' => 'highlights',
			'description' => __('Run-down of news from a category.', 'bunyad'),
			'class' => '',
			'icon' => 'motive-block',
			'category' => __('Home Blocks', 'bunyad'),
			'weight' => 1,
			'params' => array_merge(array(
		
				'type' => array(
					'type' => 'dropdown',
					'heading' => __('Listing Type', 'bunyad'),
					'description' => __('Generally, use split for 1 column, default for 2 columns, mini for 3 columns', 'bunyad'),
					'value'  => array(
						__('Default', 'bunyad') => '',
						__('Split - One article at left, rest on right', 'bunyad') => 'full-split',
						__('Mini - No excerpt, no thumbnails, smaller image', 'bunyad') => 'mini',
					),
					'param_name' => 'type',
				),
				
				'no_thumbs' => array(
					'type' => 'dropdown',
					'heading' => __('Small Thumbnails', 'bunyad'),
					'description' => __('Show the small thumbnails in this block?', 'bunyad'),
					'value'  => array(
						__('Show', 'bunyad') => 0,
						__('Hide', 'bunyad') => 1,
					),
					'param_name' => 'no_thumbs',
					
					// show only for full-split or default type
					'dependency' => array(
						'element' => 'type',
						'value' => array('', 'full-split'),
					)
				),
				
			), $common), // array_merge
		));
		
		/**
		 * Slider block
		 */
		vc_map(array(
			'name' => __('Posts Slider', 'bunyad'),
			'base' => 'slider',
			'description' => __('Featured posts slider.', 'bunyad'),
			'class' => '',
			'icon' => 'motive-block',
			'category' => __('Home Blocks', 'bunyad'),
			'weight' => 1,
			'params' => array_merge(array(
				'type' => array(
					'type' => 'dropdown',
					'heading' => __('Featured Slider?', 'bunyad'),
					'description' => __('', 'bunyad'),
					'value'  => array(
						__('No', 'bunyad') => 'latest',
						__('Yes - Use Posts Marked as "Featured Slider Post?', 'bunyad') => 0,
					),
					'param_name' => 'type',
				),
				
				'excerpt_len' => array(), // remove
				
			), $common), // array_merge
		));
		
	
		/**
		 * Multimedia block
		 */
		
		// move post format to top
		$multimedia = array_merge(array(
			'post_format' => $common['post_format'],
			'per_slide' => array(
				'type' => 'textfield',
				'heading' => __('Posts Per Slide', 'bunyad'),
				'description' => __('Select the nubmer of posts to show in one each carousel slide.', 'bunyad'),
				'value'  => 3,
				'param_name' => 'per_slide',
			)), $common
		);
		
		$multimedia['posts']['value'] = 7;
		
		vc_map(array(
			'name' => __('Multimedia Block', 'bunyad'),
			'description' => __('A hybrid carousel.', 'bunyad'),
			'base' => 'multimedia',
			'class' => '',
			'icon' => 'motive-block',
			'weight' => 1,
			'category' => __('Home Blocks', 'bunyad'),
			'params' => $multimedia,
		));
		
		/**
		 * Posts Carousel / Latest gallery block
		 */
		
		// borrows post_format moving to top from $multimedia
		$latest_gallery = array_merge(array(
			'per_slide' => array(
				'type' => 'textfield',
				'heading' => __('Posts Per Slide', 'bunyad'),
				'description' => __('Select the nubmer of posts to show in one each carousel slide.', 'bunyad'),
				'value'  => 3,
				'param_name' => 'per_slide',
			),
			
			'show_nav' => array(
				'type' => 'dropdown',
				'heading' => __('Navigation', 'bunyad'),
				'description' => __('Enable to show navigation bar and arrows below.', 'bunyad'),
				'value'  => array(__('Enabled', 'bunyad') => '1', __('Disabled', 'bunyad') => 0),
				'param_name' => 'show_nav',
			),
			
			'title_pos' => array(
				'type' => 'dropdown',
				'heading' => __('Posts Title Position', 'bunyad'),
				'description' => __('Show whether to show post titles below or as an overlay on the post image.', 'bunyad'),
				'value'  => array(__('Show Below', 'bunyad') => '', __('Overlay', 'bunyad') => 'overlay'),
				'param_name' => 'title_pos',
			),
			
			'type' => array(
				'type' => 'dropdown',
				'heading' => __('Carousel Layout Type', 'bunyad'),
				'description' => __('There is a darker version of the carousel available.', 'bunyad'),
				'value'  => array(__('Normal Block', 'bunyad') => '', __('Dark Carousel', 'bunyad') => 'dark'),
				'param_name' => 'type',
			)	
			
		), $multimedia);
		
		$latest_gallery['posts']['value'] = 6;
		
		vc_map(array(
			'name' => __('Posts Carousel', 'bunyad'),
			'description' => __('Show posts in a carousel.', 'bunyad'),
			'base' => 'latest_gallery',
			'class' => '',
			'icon' => 'motive-block',
			'weight' => 1,
			'category' => __('Home Blocks', 'bunyad'),
			'params' => $latest_gallery,
		));		
		
		/**
		 * Modify default Visual Composer elements to behave correctly
		 */
		
		// change weight of VC row to be highest in order
		vc_map_update('vc_row', array('weight' => 50));
		
		vc_map_update('vc_accordion_tab', array('params' => 
			array(
				array(
					'type' => 'textfield',
					'heading' => __('Title', 'bunyad'),
					'param_name' => 'title',
					'description' => __('Accordion section title.', 'bunyad')
				),
				
				array(
					'type' => 'dropdown',
					'heading' => __('Show', 'bunyad'),
					'param_name' => 'load',
					'value' => array(__('No', 'bunyad') => 'hide', __('Yes', 'bunyad') => 'show'),
				),
			)
		));
		
	}
	
	/**
	 * Register WordPress widget as VC elements
	 */
	public function register_widgets()
	{
		
		/**
		 * Register Bunyad Widgets
		 */
		vc_map(array(
			'name' => __('Bunyad Advertisement', 'bunyad'),
			'description' => __('Advertisement code widget.', 'bunyad'),
			'base' => 'vc_bunyad_ads',
			'icon' => 'icon-wpb-wp',
			'php_class_name' => 'WPBakeryShortCode_VC_Raw_html',
			'category' => __('Home Blocks', 'bunyad'),
			'weight' => 0,
			'params' => array(
				'code' => array(
					'type' => 'textarea_raw_html',
					'heading' => __('Ad Code', 'bunyad'),
					'description' => __('Enter your ad code here.', 'bunyad'),
					'param_name' => 'code',
				),
			),
		));
	}
	
	/**
	 * Remove unsupported elements from Visual Composer
	 */
	public function remove_unsupported()
	{
		
		// remove metabox
		$pt_array = vc_editor_post_types();
		foreach ($pt_array as $type) {
			remove_meta_box('vc_teaser', $type, 'side');
		}
		
		// remove elements
		vc_remove_element('vc_posts_grid');
		vc_remove_element('vc_posts_slider');
		vc_remove_element('vc_carousel');
		
		
		// remove params
		vc_remove_param('vc_accordion', 'active_tab');
		vc_remove_param('vc_accordion', 'collapsible');
		//vc_remove_param('vc_column', 'offset');
	}
	
	/**
	 * Create category drop-down via recursion on parent-child relationship
	 * 
	 * @param integer  $parent
	 * @param object   $terms
	 * @param integer  $depth
	 */
	public function _recurse_terms_array($parent, $terms, $depth = 0)
	{	
		$the_terms = array();
			
		$output = array();
		foreach ($terms as $term) {
			
			// add tab to children
			if ($term->parent == $parent) {
				$output[str_repeat(" - ", $depth) . $term->name] = $term->term_id;			
				$output = array_merge($output, $this->_recurse_terms_array($term->term_id, $terms, $depth+1));
			}
		}
		
		return $output;
	}
	
	/**
	 * Store VC Row info
	 * 
	 * @param array $atts
	 * @param string|null $content
	 */
	public function after_vc_row($atts, $content = null)
	{
		$layout = Bunyad::registry()->layout;
									
		$layout['row_open'] = false;

		// reduce depth each time a row is closed - at depth=0 is the parent row
		$layout['row_depth']--;
				
		if ($layout['row_parent_open'] && $layout['row_depth'] == 0) {
			$layout['row_parent_open'] = false;
			$layout['row_cols'] = array();
			$layout['row_depth'] = 0;
		}
				
		Bunyad::registry()->layout = $layout;
	}

	
	/**
	 * Filter callback: Change default classes for vc_row and vc_column to be 
	 * compatible with the CSS classes used in the theme. 
	 * 
	 * @param string $classes
	 * @param string $tag
	 */
	public function custom_classes($classes, $tag)
	{
		$layout = Bunyad::registry()->layout;
		
		if ($tag == 'vc_row' OR $tag == 'vc_row_inner') {
			
			// increase depth if inside a parent row
			if ($layout['row_parent_open']) {
				$layout['row_depth']++;
			}
			
			// parent row
			if ($tag == 'vc_row') {
				$layout['row_parent_open'] = true;
				$layout['row_depth']++;
			}
			
			$layout['row_open'] = true;
			Bunyad::registry()->layout = $layout;

		}
		
		// front-end editing in process?
		if (vc_is_frontend_editor() OR vc_is_page_editable()) {
			
			if ($tag == 'vc_row' OR $tag == 'vc_row_inner') {
				$classes = str_replace('vc_row-fluid', 'ts-row block', $classes);
			}
			
			return $classes;
		}
		
		// replacemenets for rows - add block too
		if ($tag == 'vc_row' OR $tag == 'vc_row_inner') {
			$classes = trim(str_replace(array('vc_row-fluid', 'vc_row'), array('', 'ts-row block cf'), $classes));
		}
		
		if ($tag == 'vc_column' OR $tag == 'vc_column_inner') {
			
			/**
			 * Change the class
			 */
			preg_match('/vc_col-sm-(\d{1,2})/', $classes, $matches);
			
			// change the class
			$classes = str_replace($matches[0], 'col-' . $matches[1], $classes);
			

			/**
			 * Store current column width - relative to a parent if any
			 */

			// a row is open?
			if ($layout['row_open']) 
			{
				
				// set column width relative to the top-parent column - in grid format
				$layout['col_width'] = $matches[1];
				
				// column of top-level row?
				if ($layout['row_depth'] == 1) {
					$layout['col_parent'] = $matches[1];
					$layout['col_relative_width'] = ($layout['col_parent'] / 12);
				}
				else {
					// column of a row_inner
					
					// add to current row columns
					if ($layout['row_open']) {
						array_push($layout['row_cols'], $matches[1]);
					}
					
					// calculate relative to the parent column
					$layout['col_relative_width'] = ($layout['col_parent'] / 12) * ($layout['col_width'] / 12);
				}
				
				// save layout array in registry
				Bunyad::registry()->layout = $layout;
			}
			
		}
		
		return $classes;
	}
	
	/**
	 * Load premade layouts for visual composer
	 */
	public function load_templates($data)
	{
		$templates = include locate_template('inc/visual-composer/templates.php');
		
		//if (is_array($templates)) {			
			//vc_add_default_templates($templates);
		//}
		
		// remove default templates except a few
		$keep = array('Landing Page', 'Service List');
		foreach ($data as $key => $default) {
			if (!in_array($default['name'], $keep)) {
				unset($data[$key]);
			} 
		}
		
		return array_merge($templates, $data);
	}
	
	
	/**
	 * Add a custom checkbox handler for VC that supports default values 
	 * 
	 * @param array $param
	 * @param mixed $param_value
	 */
	function add_param_checkbox($param, $param_value = '') {
		
		$param_line    = '';
		$current_value = explode(',', $param_value);
		
		// multiple checkboxes
		foreach ((array) $param['options'] as $label => $v) {
			$checked = in_array( $v, $current_value ) ? ' checked="checked"' : '';
			$param_line .= ' <input id="' . esc_attr($param['param_name'] . '-' . $v) . '" value="' . esc_attr($v) . '" class="wpb_vc_param_value ' . 
				esc_attr($param['param_name'] . ' ' . $param['type']) . '" type="checkbox" name="' . esc_attr($param['param_name']) . '"' . $checked . '> ' . $label;
		}
		
		return $param_line;
	}
	
}

/**
 * Compat functions to turn VC after_ functions - for rows - into proper filters
 */

if (!function_exists('vc_theme_after_vc_row')) {
	function vc_theme_after_vc_row($atts, $content = null) {
		$content = apply_filters('vc_theme_after_vc_row', $content, $atts);
		
		if (!empty($content)) {
			return $content;
		}
	}
}

if (!function_exists('vc_theme_after_vc_row_inner')) {
	function vc_theme_after_vc_row_inner($atts, $content = null) {
		$content = apply_filters('vc_theme_after_vc_row_inner', $content, $atts);
		
		if (!empty($content)) {
			return $content;
		}
	}
}

// generic Class for Widgets mapping as VC elements
if (!class_exists('Bunyad_VC_Widget')) {
	class Bunyad_VC_Widget extends WPBakeryShortCode {}
}

// init and make available in Bunyad::get('vc')
Bunyad::register('vc', array(
	'class' => 'Bunyad_Visual_Composer',
	'init' => true
));
	