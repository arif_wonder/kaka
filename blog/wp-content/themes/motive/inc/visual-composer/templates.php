<?php
/**
 * Pre-made layout templates for visual composer
 */

$main_home = <<<EOF
[vc_row][vc_column width="1/1" css=".vc_custom_1413679058783{margin-bottom: 10px !important;}"][blog type="grid-3" posts="3" sort_order="desc" title="Most Popular" sort_by="comments" view_all="1" link="http://motive.theme-sphere.com/news-demo/all-posts/"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][vc_separator color="grey"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][latest_gallery per_slide="4" show_nav="1" title_post="overlay" type="dark" posts="8" sort_order="desc" view_all="1" title_pos="overlay" title="Latest In Lifestyle " cat="11"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][vc_separator color="grey"][/vc_column][/vc_row][vc_row][vc_column width="2/3" el_class="content-column" css=".vc_custom_1413679137749{margin-top: 10px !important;}"][main_highlights posts="1" sort_order="desc" view_all="1" title="Today's Highlight" link="http://motive.theme-sphere.com/news-demo/all-posts/"][/vc_column][vc_column width="1/3" el_class="sidebar" css=".vc_custom_1413679148907{margin-top: 10px !important;}"][multimedia post_format="video" posts="7" sort_order="desc" view_all="1" title="Multimedia" per_slide="2" link="http://motive.theme-sphere.com/news-demo/all-posts/"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][latest_gallery per_slide="4" show_nav="1" cat="6" posts="8" sort_order="desc" title="From Tech Desk" view_all="1" title_pos="overlay"][/vc_column][/vc_row][vc_row][vc_column width="2/3" el_class="content-column"][vc_row_inner][vc_column_inner width="1/1"][blog type="alt" posts="7" sort_order="desc" view_all="1" title="All Posts" offset="1" link="http://motive.theme-sphere.com/news-demo/all-posts/"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/3" el_class="sidebar"][vc_widget_sidebar sidebar_id="cs-5"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][latest_gallery per_slide="5" show_nav="1" cat="6" posts="15" sort_order="desc" title="Latest in Technology" view_all="0"][/vc_column][/vc_row]
EOF;

$alt_home = <<<EOF
[vc_row][vc_column width="1/1"][latest_gallery per_slide="4" show_nav="1" posts="8" sort_order="desc" view_all="0" sort_by="comments" title="Featured Articles"][/vc_column][/vc_row][vc_row][vc_column width="2/3" el_class="content-column"][vc_row_inner][vc_column_inner width="1/1"][highlights no_thumbs="1" cat="12" posts="5" sort_order="desc" view_all="1" type="full-split" title="World News" excerpt_len="15"][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width="1/1"][highlights no_thumbs="1" cat="2" posts="5" sort_order="desc" view_all="1" type="full-split" title="Sports News" excerpt_len="15"][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width="1/1"][blog cat="4" posts="4" sort_order="desc" title="On Health" view_all="1"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/3" el_class="sidebar"][vc_widget_sidebar sidebar_id="cs-6"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][vc_separator color="grey"][/vc_column][/vc_row][vc_row][vc_column width="1/3"][latest_gallery per_slide="1" show_nav="1" cat="8" posts="6" sort_order="desc" view_all="0"][/vc_column][vc_column width="1/3"][latest_gallery per_slide="1" show_nav="1" cat="2" posts="6" sort_order="desc" view_all="0"][/vc_column][vc_column width="1/3"][latest_gallery per_slide="1" show_nav="1" cat="11" posts="6" sort_order="desc" view_all="0"][/vc_column][/vc_row]
EOF;

$alt_home2 = <<<EOF
[vc_row][vc_column width="2/3" el_class="content-column"][blog type="classic" posts="8" sort_order="desc" view_all="1" pagination="1"][/vc_column][vc_column width="1/3" el_class="sidebar"][vc_widget_sidebar sidebar_id="cs-9"][/vc_column][/vc_row]
EOF;

$in_content_slider = <<<EOF
[vc_row][vc_column width="1/1"][blog type="grid-3" posts="3" sort_order="desc"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][vc_separator color="grey"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][slider type="latest" posts="5" sort_order="desc" offset="3"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][vc_separator color="grey"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][blog type="grid-2" posts="6" sort_order="desc" offset="8"][/vc_column][/vc_row]
EOF;

$main_home_creative = <<<EOF
[vc_row][vc_column width="1/1"][blog posts="3" sort_by="comments" sort_order="desc" title="Featured Articles" sub_title="Only Our best Posts" view_all="1" post_meta="date,category" type="grid-3"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][latest_gallery per_slide="4" show_nav="1" posts="8" sort_order="desc" view_all="0" sort_by="comments" title="Latest in Fashion" sub_title="Fashion News And Trends" post_meta="date,category" title_pos="overlay" cat="12"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][latest_gallery per_slide="3" show_nav="1" title_pos="overlay" type="dark" posts="6" sort_order="desc" title="Beauty and love" sub_title="All the beautiful things" view_all="0" post_meta="date" cat="5"][/vc_column][/vc_row][vc_row][vc_column width="2/3" el_class="content-column"][vc_row_inner][vc_column_inner width="1/1"][blog posts="8" sort_order="desc" title="Recent Posts" view_all="1" sub_title="All of The Latest Articles" post_meta="date" type="alt"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/3" el_class="sidebar"][vc_widget_sidebar sidebar_id="cs-6"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][latest_gallery per_slide="3" show_nav="1" cat="4" posts="6" sort_order="desc" title="Culture Club" sub_title="Learn what's hip in town" view_all="0" post_meta="date" offset="1" title_pos="overlay"][/vc_column][/vc_row]
EOF;

$main_home_magazine = <<<EOF
[vc_row][vc_column width="2/3"][main_highlights cat="2" posts="5" sort_order="desc"][/vc_column][vc_column width="1/3"][news_bar posts="7" sort_order="desc" title="Recent Stories"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][highlights type="full-split" cat="6" posts="5" sort_order="desc" no_thumbs="0" excerpt_len="15"][/vc_column][/vc_row][vc_row el_class="highlights-box"][vc_column width="1/2"][highlights cat="4" posts="4" sort_order="desc" no_thumbs="0" excerpt_len="15"][/vc_column][vc_column width="1/2"][highlights cat="2" posts="4" sort_order="desc" no_thumbs="0" excerpt_len="15"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][vc_bunyad_ads]<div class="visible-lg">
	<a href="#"><img src="http://motive.theme-sphere.com/wp-content/uploads/2014/10/demo-720-ad.jpg" alt="Motive Ad" /></a>
</div>

<div class="visible-md visible-sm">
	<a href="#"><img src="http://motive.theme-sphere.com/wp-content/uploads/2014/10/demo-468-ad.jpg" alt="Motive Ad" /></a>
</div>

<div class="visible-xs">
	<a href="#"><img src="http://motive.theme-sphere.com/wp-content/uploads/2014/10/demo-320-ad.jpg" alt="Motive Ad" /></a>
</div>
[/vc_bunyad_ads][/vc_column][/vc_row][vc_row][vc_column width="1/3"][highlights type="mini" cat="12" posts="4" sort_order="desc"][/vc_column][vc_column width="1/3"][highlights type="mini" cat="10" posts="4" sort_order="desc"][/vc_column][vc_column width="1/3"][highlights cat="5" posts="4" sort_order="desc" type="mini"][/vc_column][/vc_row][vc_row][vc_column width="2/3"][multimedia posts="7" sort_order="desc" post_format="video" title="Videos"][/vc_column][vc_column width="1/3"][highlights type="mini" no_thumbs="0" cat="6" posts="9" sort_order="desc"][/vc_column][/vc_row][vc_row][vc_column width="1/1"][blog type="grid-2" posts="4" sort_order="desc" title="Recent"][/vc_column][/vc_row]
EOF;



return apply_filters('bunyad_vc_templates', array(
	array(
		'name' => __('Motive News: Default Home', 'bunyad'),
		'content' => $main_home,
	),
	
	array(
		'name' => __('Motive News: Alternate Home', 'bunyad'),
		'content' => $alt_home,
	),
	
	array(
		'name' => __('Motive News: Home Blog Style', 'bunyad'),
		'content' => $alt_home,
	),
	
	array(
		'name' => __('In-Content Slider Home', 'bunyad'),
		'content' => $in_content_slider,
	),
	
	array(
		'name' => __('Creative: Default Home', 'bunyad'),
		'content' => $main_home_creative,
	),
	
	array(
		'name' => __('Magazine: Default Home', 'bunyad'),
		'content' => $main_home_magazine,
	),
));