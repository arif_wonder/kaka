<?php

/**
 * Overrides output of "Bunyad Widgets" plugin by using action hooks.
 * 
 * Makes the markup more suitable for this theme's purpose.
 */

class Bunyad_Widgets_Output
{

	public function __construct()
	{
		add_action('init', array($this, 'init'));
	}
	
	/**
	 * Bind hooks on init
	 */
	public function init()
	{
		add_action('bunyad_widget_tabbed_recent_loop', array($this, 'tabbed_recent'), 10, 2);
		add_action('bunyad_widget_latest_posts_loop', array($this, 'generic_posts'), 10, 2);
		add_action('bunyad_widget_popular_posts_loop', array($this, 'generic_posts'), 10, 2);
		
		add_action('bunyad_widget_latest_review_loop', array($this, 'latest_reviews'), 10, 2);
		add_action('bunyad_widget_comments_loop', array($this, 'latest_comments'), 10, 2);
		add_action('bunyad_widget_twitter_loop', array($this, 'latest_tweets'), 10, 2);
		
		add_filter('widget_tag_cloud_args', array($this, 'tag_cloud_args'));
				
		// enable type for latest posts widget
		add_filter('bunyad_widget_latest_posts_form', array($this, 'latest_posts_fields'));
	}
	
	public function tag_cloud_args($args = array())
	{
		$args = array_merge($args, array('largest' => 10.5, 'smallest' => 10.5));
		return $args;
	}
	
	/**
	 * Generic post listing for widgets in Motive - overrides output of Bunyad widgets 
	 * 
	 * @param array $args
	 * @param WP_Query $query
	 */
	public function generic_posts($args, $query)
	{
		$args = array_merge(array('before_widget' => '', 'after_widget' => '', 'type' => ''), $args);
		
		extract($args);
				
		if (!$query->have_posts()) {
			return;
		}
		
		?>
			<?php echo $before_widget; ?>
			
			<?php if (!empty($title)): ?>
				<?php echo $before_title . $title . $after_title; ?>
			<?php endif;?>
			
			
			<?php if ($type == 'blocks'): // image blocks type listing?>
						
				<div class="posts-grid large">
				
				<?php while ($query->have_posts()) : $query->the_post(); ?>
					<article itemscope itemtype="http://schema.org/Article">
						
						<a href="<?php the_permalink() ?>" class="image-link"><?php 
							the_post_thumbnail('motive-image-block', array('title' => strip_tags(get_the_title()))); ?>
							
							<?php do_action('bunyad_image_overlay'); ?>
						</a>
						
						<?php do_action('bunyad_comments_snippet'); ?>
						
						<?php do_action('bunyad_listing_meta', 'small-widget'); ?>
						
						<a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a>
						
					</article>
					
				<?php endwhile; ?>
				
				</div>
				
			<?php elseif ($type == 'numbered'): // numbered list ?>
			
				<ol class="posts-list numbered-list">
				
				<?php while ($query->have_posts()) : $query->the_post(); ?>
					<li>
						<div class="content">
						
							<?php do_action('bunyad_listing_meta', 'small-widget'); ?>
							
							<a href="<?php the_permalink(); ?>" class="post-link-small"><?php the_title(); ?></a>
	
						</div>
					
					</li>
				<?php endwhile; ?>
				</ol>
			
			<?php else:  // default listing  ?>
				
				<ul class="posts-list">
				
				<?php while ($query->have_posts()) : $query->the_post(); ?>
					<li>
					
						<a href="<?php the_permalink() ?>" class="image-link small">
							<?php the_post_thumbnail('post-thumbnail', array('title' => strip_tags(get_the_title()))); ?>
							
							<?php do_action('bunyad_image_overlay'); ?>						
						</a>
						
						<div class="content">
						
							<?php do_action('bunyad_listing_meta', 'small-widget'); ?>
							
							<a href="<?php the_permalink(); ?>" class="post-link-small"><?php the_title(); ?></a>
	
						</div>
					
					</li>
				<?php endwhile; ?>
				</ul>
			
			<?php endif; ?>
			
			<?php echo $after_widget; ?>
					
		<?php
		
	}
	
	/**
	 * Overrides the Bunyad - Latest Reviews output
	 */
	public function latest_reviews($args, $query)
	{
		$args = array_merge(array('before_widget' => '', 'after_widget' => ''), $args);
		extract($args);
		
		if (!$query->have_posts()) {
			return;
		}
		
		?>
			<?php echo $before_widget; ?>
			
			<?php if (!empty($title)): ?>
				<?php echo $before_title . esc_html($title) . $after_title; ?>
			<?php endif;?>
			
			
			<?php while ($query->have_posts()) : $query->the_post(); ?>
			
			<?php
				// highlight first review post 
				if ($query->current_post == 0): ?>
					
				
				<article itemscope itemtype="http://schema.org/Article" class="posts-grid">
					
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="image-link" itemprop="url">
						<?php the_post_thumbnail('motive-image-block', array('class' => 'image', 'title' => strip_tags(get_the_title()))); ?>
						
						<?php do_action('bunyad_image_overlay'); ?>
					</a>
					
					<?php do_action('bunyad_comments_snippet'); ?>
	
					<?php do_action('bunyad_listing_meta', 'small-widget'); ?>
					
					<h3 itemprop="name"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="post-link"><?php the_title(); ?></a></h3>
				
				</article>
				
				<ul class="posts-list">
					
				
			<?php 
					continue;
				endif;
			?>
			
				<li>
				
					<a href="<?php the_permalink() ?>" class="image-link small"><?php 
						the_post_thumbnail('post-thumbnail', array('title' => strip_tags(get_the_title()))); ?>
						
						<?php do_action('bunyad_image_overlay'); ?>	
					</a>
					
					<div class="content cf">
					
						<?php do_action('bunyad_listing_meta', 'small-widget'); ?>
					
						<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="post-link-small">
							<?php the_title(); ?></a>

					</div>
				
				</li>
			<?php endwhile; ?>
			</ul>
			
			<?php echo $after_widget; ?>
					
		<?php
	}
	
	/**
	 * Overrides the Bunyad - Recent Comments output
	 */
	public function latest_comments($args, $query)
	{
		$args = array_merge(array('before_widget' => '', 'after_widget' => '', 'title' => ''), $args);
		
		extract($args);
		
		global $comment;
		
		?>
		
			<?php echo $before_widget; ?>
			
			<?php if ($title): ?>
				<?php echo $before_title . esc_html($title) . $after_title; ?>
			<?php endif;?>
		
			<ul class="posts-list comments-list">
			
			<?php foreach ($query as $comment):	?>
					
					<li class="comment">
					
						<a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>" class="avatar-link"><?php echo get_avatar($comment, 45); ?></a>
						
						<div class="content">
							<span class="comment-meta"><?php printf('%s on %s', 
								'<span class="author">' . get_comment_author_link($comment->comment_ID) . '</span>', 
								'<a href="' . esc_url(get_comment_link($comment->comment_ID)) . '" class="comment-link">' . get_the_title($comment->comment_post_ID) . '</a>'); ?>
							</span>
							
							<?php echo apply_filters('bunyad_widget_comments_date', '<time datetime="'. get_comment_time(DATE_W3C) .'">' . get_comment_date() . '</time>'); ?>
						</div>
						
						<div class="text text-font"><?php 
							echo wp_trim_words(
								$comment->comment_content, 10, 
								'&hellip; <div class="read-more"><a href="'. esc_url(get_comment_link($comment->comment_ID)) .'">'. __('More', 'bunyad') .'</a></div>'
							); ?></div>
					
					</li>
	
					<?php				
					endforeach; 
					?>
			</ul>
		
		<?php
		
		echo $after_widget;
		
	}
	
	/**
	 * Override output of "Bunyad - Recent Tabs" widget
	 * 
	 * @param array $args
	 * @param array $posts
	 */
	public function tabbed_recent($args, $posts)
	{		
		$args = array_merge(array('before_widget' => '', 'after_widget' => ''), $args);
		
		extract($args);
		
		echo $before_widget; 
		
		?>

		<ul class="tabs-list cf">
		
			<?php
			$count = 0; 
			foreach ($posts as $key => $val): $count++; $active = ($count == 1 ? 'active' : ''); 
			?>
			
			<li class="<?php echo esc_attr($active); ?>">
				<a href="#" data-tab="<?php echo esc_attr($count); ?>"><?php echo esc_html($key); ?></a>
			</li>
			
			<?php endforeach; ?>
			
		</ul>
		
		<div class="tabs-data">
			<?php
				$i = 0; 
				foreach ($posts as $tab => $tab_posts): $i++; $active = ($i == 1 ? 'active' : ''); ?>
				
			<div class="tab-posts <?php echo esc_attr($active . ' ' . $tabs[$tab]['cat_type']); ?>" id="recent-tab-<?php echo esc_attr($i); ?>">
			
			<?php if ($tabs[$tab]['cat_type'] == 'comments'): ?>
			
				<div class="latest-comments">
					<?php $this->latest_comments(array(), $tab_posts); ?>
				</div>

			<?php elseif ($tabs[$tab]['cat_type'] == 'popular'): ?>
				<ol class="popular">
					<?php while ($tab_posts->have_posts()): $tab_posts->the_post(); ?>
	
					<li>		
						<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>">
							<?php if (get_the_title()) the_title(); else the_ID(); ?></a>				
					</li>
					
					<?php endwhile; ?>
				</ol>
				
			<?php else: ?>
			
				<?php $this->generic_posts(array(), $tab_posts); ?>
			
			<?php endif; ?>
				
			</div>
			<?php endforeach; ?>
		
		</div>
		
		<?php 
		
		echo $after_widget;
	}
	
	/**
	 * Output for Twitter widget to show latest tweets

	 * @param array $args
	 * @param array $tweets
	 */
	public function latest_tweets($args, $tweets)
	{
		if (empty($tweets)) {
			return;
		}

		$args = array_merge(array('before_widget' => '', 'after_widget' => '', 'title' => ''), $args);
		
		extract($args);
		
		?>
	
		<?php echo $before_widget; ?>
		<?php echo $before_title . esc_html($title) . $after_title; ?>
		
		<div class="twitter-widget">
		
			<span class="tweet-nav">
				<a href="#" class="prev"><i class="fa fa-chevron-left"></i></a>
				<a href="#" class="next"><i class="fa fa-chevron-right"></i></a>
			</span>
		
			<ul class="tweets">
			<?php 
				foreach ($tweets as $tweet): 
				
					// convert links and @ references
					$tweet->text = esc_html($tweet->text);
					$tweet->text = preg_replace('/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;\'">\:\s\<\>\)\]\!])/', '<a href="\\1">\\1</a>', $tweet->text);
					$tweet->text = preg_replace('/\B@([_a-z0-9]+)/i', '<a href="http://twitter' . '.com/\\1">@\\1</a>', $tweet->text);
					
					$time = strtotime($tweet->created_at);
					$tweet_link = esc_url('https://twitter.com/' . $tweet->user->screen_name . '/status/' . $tweet->id);
			?>
			
				<li>
					<span class="tweet"><?php echo wp_kses_post($tweet->text); ?></span>
					<span class="tweet-author">
					<a href="<?php echo esc_url('https://twitter.com/' . $tweet->user->screen_name); ?>"><i class="fa fa-twitter"></i>@<?php 
						echo esc_html($tweet->user->screen_name); ?></a>
					</span>
					
					
					<time class="tweet-time" datetime="<?php echo date_i18n(DATE_W3C, $time); ?>">
						<a href="<?php echo esc_url($tweet_link); ?>"><?php printf(__('%s ago', 'bunyad'), human_time_diff($time)); ?></a>
					</time>
				</li>
			
			<?php endforeach; ?>
			</ul>			
		</div>
		
		<?php 
		
		echo $after_widget;
	}
	
	/**
	 * Filter callback: Enable type field for latest posts widget
	 * 
	 * @param array $fields
	 */
	public function latest_posts_fields($fields) 
	{
		$fields = array_merge($fields, array('type' => ''));
		return $fields;
	}
	
}

// init and make available in Bunyad::get('widgets')
Bunyad::register('widgets', array(
	'class' => 'Bunyad_Widgets_Output',
	'init' => true
));
