/**
 * Fixes for Visual Composer
 */
jQuery(function($) {
	
	// VC front-end fixes
	if (typeof parent.vc != undefined) {
		
		// re-init sliders when vc is loaded
		(function check() {
			if (!parent.vc.loaded) {
				setTimeout(check, 100);
				return;
			}
			
			Bunyad_Theme.sliders();
		})();
	}
});