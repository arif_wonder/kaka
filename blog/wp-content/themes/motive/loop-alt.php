<?php 

/**
 * Alternate "loop" to display posts in blog style.
 */


$bunyad_loop = Bunyad::registry()->loop;

if (!is_object($bunyad_loop)) {
	$bunyad_loop = $wp_query;
}

if ($bunyad_loop->have_posts()):

?>
		
	<div class="posts-list listing-blog <?php echo esc_attr(Bunyad::registry()->listing_class); ?>">
	
		<?php while ($bunyad_loop->have_posts()): $bunyad_loop->the_post(); ?>
		
			<article <?php post_class('cf'); ?> itemscope itemtype="http://schema.org/Article">
			
				<?php if (has_post_thumbnail()): ?>
			
				<div class="post-thumb">
				
					<a href="<?php the_permalink() ?>" itemprop="url" class="image-link"><?php 
						the_post_thumbnail('motive-highlight-block', array('title' => strip_tags(get_the_title()), 'itemprop' => 'image')); ?>
				
						<?php do_action('bunyad_image_overlay'); ?>
					</a>
				
					<?php do_action('bunyad_comments_snippet'); ?>
				</div>				
						
				<?php endif; ?>

				<div class="content">
				
					<?php do_action('bunyad_listing_meta'); ?>
				
					<h2 itemprop="name" class="post-title"><a href="<?php the_permalink(); ?>" itemprop="url" class="post-link"><?php the_title(); ?></a></h2>
					
					<div class="excerpt text-font"><?php
						echo Bunyad::posts()->excerpt(null, Bunyad::options()->excerpt_length_alt, array('force_more' => Bunyad::options()->read_more_alt)); ?></div>
					
				</div>
			
			</article>
		
		<?php endwhile; ?>
				
	</div>
	
	<?php if (!Bunyad::options()->blog_no_pagination): // pagination can be disabled ?>

		<?php echo Bunyad::posts()->paginate(array('wrapper_before' => '<div class="main-pagination">', 'wrapper_after' => '</div>'), $bunyad_loop); ?>
				
	<?php endif; ?>
	

	<?php get_template_part('partials/category-more-stories'); ?>
		

<?php elseif (is_search()): // show error on search only ?>

	<?php get_template_part('partials/no-results'); ?>
	
<?php endif; ?>

<?php wp_reset_query(); ?>