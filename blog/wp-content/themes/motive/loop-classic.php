<?php 

/**
 * Classic "loop" to display posts when using an existing query. Uses content.php template
 * to render the posts
 */


$bunyad_loop = Bunyad::registry()->loop;

if (!is_object($bunyad_loop)) {
	$bunyad_loop = $wp_query;
}

if ($bunyad_loop->have_posts()):

?>
		
	<div class="listing-classic <?php echo esc_attr(Bunyad::registry()->listing_class); ?>">
	
			<?php 
			
			while ($bunyad_loop->have_posts()): 
				$bunyad_loop->the_post();
			?>

				<?php get_template_part('content', get_post_format()); ?>
				
			<?php 
			endwhile; 
			?>
				
	</div>

	<?php if (!Bunyad::options()->blog_no_pagination): // pagination can be disabled ?>
	
		<?php echo Bunyad::posts()->paginate(array('wrapper_before' => '<div class="main-pagination">', 'wrapper_after' => '</div>'), $bunyad_loop); ?>
	
	<?php endif; ?>
		

<?php elseif (is_search()): // show error on search only ?>

	<?php get_template_part('partials/no-results'); ?>
	
<?php endif; ?>

<?php wp_reset_query(); ?>