<?php 

/**
 * Default loop template - Displays posts in grid format
 */

	
$bunyad_loop = Bunyad::registry()->loop;
$bunyad_loop_grid   = Bunyad::registry()->loop_grid;
	
if (!is_object($bunyad_loop)) {
	$bunyad_loop = $wp_query;
}

if ($bunyad_loop->have_posts()):

	/**
	 * Determine the image to use based on column width
	 */
	$col_relative_width = Bunyad::get('motive')->block_relative_width();
	
	if ($bunyad_loop_grid == 3) {
		// more than 30% of .main-wrap?
		$image =  ($col_relative_width >= 0.3 ? 'motive-highlight-block' : 'motive-main-block');
	}
	else {
		// more than 67% of .main-wrap?
		$image = ($col_relative_width > 0.67 ? 'motive-grid-slider' : 'motive-highlight-block');
	}
	
?>

	<div class="ts-row posts-grid listing-grid grid-<?php echo esc_attr($bunyad_loop_grid); ?> <?php echo Bunyad::registry()->listing_class; ?>">
		
		<?php while ($bunyad_loop->have_posts()): $bunyad_loop->the_post(); ?>
			
		<div class="column <?php echo ($bunyad_loop_grid == 3 ? 'one-third' : 'half'); ?>">
		
			<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
				
				<a href="<?php the_permalink(); ?>" class="image-link">
					<?php the_post_thumbnail($image, array('class' => 'image', 'title' => strip_tags(get_the_title()), 'itemprop' => 'image')); ?>
					
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<div class="meta">

					<?php do_action('bunyad_listing_meta'); ?>
						
					<?php do_action('bunyad_comments_snippet'); ?>
					
				</div>
				
				<h2 itemprop="name"><a href="<?php the_permalink(); ?>" itemprop="url" class="post-link"><?php the_title(); ?></a></h2>
				
				<div class="excerpt text-font"><?php echo Bunyad::posts()->excerpt(
					null, Bunyad::options()->get('excerpt_length_grid_' . $bunyad_loop_grid), array('force_more' => true)
				); ?></div>
			
			</article>
		</div>
			
		<?php endwhile;  ?>
				
	</div>
	
	
	<?php if (!Bunyad::options()->blog_no_pagination): // pagination can be disabled ?>

		<?php echo Bunyad::posts()->paginate(array(
			'wrapper_before' => '<div class="main-pagination '. Bunyad::registry()->listing_class .'">', 'wrapper_after' => '</div>'), $bunyad_loop); ?>
				
	<?php endif; ?>
	

	<?php get_template_part('partials/category-more-stories'); ?>


<?php elseif (is_search()): // show error on search only ?>

	<?php get_template_part('partials/no-results'); ?>
	
<?php endif; ?>

<?php wp_reset_query(); ?>