<?php
/*
	Template Name: Homepage & Blocks (Advanced)
*/

get_header();

if (Bunyad::posts()->meta('featured_slider')):
	get_template_part('partials/featured-area');
endif;

?>

<div class="main wrap">

	<div class="ts-row cf">
		<div class="col-8 main-content cf">
		
			<?php 
			if (Bunyad::posts()->meta('show_breadcrumbs') !== 'no'):
				Bunyad::core()->breadcrumbs(); 
			endif;
			?>
			
			<?php if (have_posts()): the_post(); endif; // load the page ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class('page-content'); ?>>

			<?php if (Bunyad::posts()->meta('page_title') != 'no'): ?>
			
				<header class="post-header">
				
					<h1 class="main-heading">
						<?php the_title(); ?>
					</h1>
				</header><!-- .post-header -->
				
			<?php endif; ?>
		
						
			<?php Bunyad::posts()->the_content(); ?>


			</div>
			
		</div>
		
		<?php Bunyad::core()->theme_sidebar(); ?>
		
	</div> <!-- .ts-row -->
</div> <!-- .main -->

<?php get_footer(); ?>
