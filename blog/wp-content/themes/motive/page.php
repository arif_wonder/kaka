<?php

/**
 * Default Page Template
 */


get_header();


if (Bunyad::posts()->meta('featured_slider')):
	get_template_part('partials/featured-area');
endif;

?>

<div class="main wrap">

	<div class="ts-row cf">
		<div class="col-8 main-content cf">
			
			<?php 
			if (Bunyad::posts()->meta('show_breadcrumbs') !== 'no'):
				Bunyad::core()->breadcrumbs(); 
			endif;
			?>
			
			<?php if (have_posts()): the_post(); endif; // load the page ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?>>

			<?php if (Bunyad::posts()->meta('page_title') != 'no'): ?>
			
				<header class="post-header">				
					
				<?php if (has_post_thumbnail() && !Bunyad::posts()->meta('featured_disable')): ?>
					<div class="featured">
						<a href="<?php $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); echo esc_url($url[0]); ?>" title="<?php the_title_attribute(); ?>">
						
						<?php if ((!in_the_loop() && Bunyad::posts()->meta('layout_style') == 'full') OR Bunyad::core()->get_sidebar() == 'none'): // largest images - no sidebar? ?>
						
							<?php the_post_thumbnail('motive-slider-full', array('title' => strip_tags(get_the_title()))); ?>
						
						<?php else: ?>
							
							<?php the_post_thumbnail('motive-alt-slider', array('title' => strip_tags(get_the_title()))); ?>
							
						<?php endif; ?>
						
						</a>
					</div>
				<?php endif; ?>
				
					<h1 class="main-heading">
						<?php the_title(); ?>
					</h1>
				</header><!-- .post-header -->
				
			<?php endif; ?>
		
			<div class="post-content text-font">			
				
				<?php Bunyad::posts()->the_content(); ?>
				
			</div>

			</article>
			
		</div>
		
		<?php Bunyad::core()->theme_sidebar(); ?>
		
	</div> <!-- .ts-row -->
</div> <!-- .main -->

<?php get_footer(); ?>
