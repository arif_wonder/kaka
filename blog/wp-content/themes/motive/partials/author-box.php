<?php 
/**
 * Partial template for Author Box
 */

?>
			<section class="author-info">
			
				<?php echo get_avatar(get_the_author_meta('user_email'), 78); ?>
				
				<div class="description">
					<?php the_author_posts_link(); ?> <?php echo wp_kses_post(the_author_meta('article_desc')); // sanitized html only ?>
					
					<p class="bio text-font"><?php the_author_meta('description'); ?></p>
					
					
					<ul class="social-icons">
					<?php 
					
						// author fields
						$fields = array(
							'url' => array('icon' => 'home', 'label' => __('Website', 'bunyad')),
							'bunyad_gplus' => array('icon' => 'google-plus', 'label' => __('Google+', 'bunyad')), 
							'bunyad_facebook' => array('icon' => 'facebook', 'label' => __('Facebook', 'bunyad')),
							'bunyad_twitter' => array('icon' => 'twitter', 'label' => __('Twitter', 'bunyad')), 
							'bunyad_linkedin' => array('icon' => 'linkedin', 'label' => __('LinkedIn', 'bunyad')),							
							'bunyad_dribbble' => array('icon' => 'dribbble', 'label' => __('Dribble', 'bunyad')),
							'bunyad_public_email' => array('icon' => 'envelope-o', 'label' => __('Email', 'bunyad')),
						);
						
						$the_meta = '';
						foreach ($fields as $meta => $data): 
						
							if (!get_the_author_meta($meta)) {
								continue;
							}
							
							$type     = $data['icon'];
							$the_meta = get_the_author_meta($meta);
							
							if ($meta == 'bunyad_public_email') {
								$the_meta = 'mailto:' . $the_meta;
							}
					?>
						
						<li>
							<a href="<?php echo esc_url($the_meta); ?>" class="icon fa fa-<?php echo esc_attr($type); ?>" title="<?php echo esc_attr($data['label']); ?>"> 
								<span class="visuallyhidden"><?php echo esc_html($data['label']); ?></span></a>				
						</li>
						
						
					<?php endforeach; ?>
					</ul>
					
				</div>
				
			</section>