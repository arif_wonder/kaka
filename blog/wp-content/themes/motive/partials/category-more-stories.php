<?php
/**
 * Display More Stories on category archives 
 */


	if (!is_category()) {
		return;
	}
	
	// category
	$category = get_category(get_query_var('cat'), false);
	$cat_meta = Bunyad::options()->get('cat_meta_' . $category->term_id);
	
	$cat_meta['stories'] = empty($cat_meta['stories']) ? Bunyad::options()->cat_more_stories : $cat_meta['stories'];
	
	if ($cat_meta['stories'] != 'no'): // more stories on categories? 

		//$offset = get_option('posts_per_page') * (get_query_var('page') ? get_query_var('page') : 1);
		$offset = 0;
		
		$query = new WP_Query(apply_filters(
			'bunyad_more_stories_query',
			array('cat' => get_queried_object()->term_id, 'offset' => $offset, 'posts_per_page' => 8, 'orderby' => 'rand')
		));
			
		if ($query->have_posts()):
	?>
	
	<section class="more-stories term-wrap-<?php echo esc_attr($category->term_id); ?>">
		<h3 class="section-head cf cat-border-<?php echo esc_attr($category->term_id); ?>"><span class="title"><?php 
				printf(__('More Stories In %s', 'bunyad'), esc_html($category->name)); ?></span></h3>
		
		<ul class="posts-list">
		<?php while ($query->have_posts()): $query->the_post(); ?>
		
			<li class="post">
				<?php do_action('bunyad_listing_meta', array('type' => 'small', 'review' => false)); ?>
						
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="post-link">
					<?php the_title(); ?></a>
					
				<?php $cur_cat = current(get_the_category()); ?>
				
				<a href="<?php echo esc_url(get_term_link($cur_cat)); ?>" class="cat-label main-color cat-color-<?php echo esc_attr($cur_cat->term_id); ?>"><?php 
					echo esc_html($cur_cat->name); ?></a>
					
			</li>
		
		<?php endwhile; ?>
		</ul>
		
	</section>

	<?php 
			wp_reset_query();
	
		endif; // have_posts check
	
	endif; ?>