<?php

if (!function_exists('bunyad_motive_comment')):

	/**
	 * Callback for displaying a comment
	 * 
	 * @todo eventually move to bunyad templates with auto-generated functions as template containers
	 * 
	 * @param mixed   $comment
	 * @param array   $args
	 * @param integer $depth
	 */
	function bunyad_motive_comment($comment, $args, $depth)
	{
		$GLOBALS['comment'] = $comment;
		
		// get single post author
		$post_author = (get_post() ? get_post()->post_author : 0);
		
		// type of comment?
		switch ($comment->comment_type):
			case 'pingback':
			case 'trackback':
			?>
			
			<li class="post pingback">
				<p><?php _e('Pingback:', 'bunyad'); ?> <?php comment_author_link(); ?><?php edit_comment_link(__('Edit', 'bunyad'), '<span class="edit-link">', '</span>'); ?></p>
			<?php
				break;


			default:
			?>
		
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
				<article id="comment-<?php comment_ID(); ?>" class="comment" itemprop="comment" itemscope itemtype="http://schema.org/UserComments">
				
					<header class="comment-head">
					
						<span class="comment-avatar"><?php echo get_avatar($comment, 40); ?></span>
						
						<div class="comment-meta">
							<span class="comment-author" itemprop="creator" itemscope itemtype="http://schema.org/Person">
								<span itemprop="name"><?php comment_author_link(); ?></span>
							</span>
							
							<?php if (!empty($comment->user_id) && $post_author == $comment->user_id): ?>
								<span class="post-author"><?php _e('Post Author', 'bunyad'); ?></span>
							<?php endif; ?>
							
							<a href="<?php comment_link(); ?>" class="comment-time" title="<?php comment_date(); _e(' at ', 'bunyad'); comment_time(); ?>">
								<time pubdate itemprop="commentTime" datetime="<?php comment_time(DATE_W3C); ?>"><?php comment_date(); ?> <?php comment_time(); ?></time>
							</a>
			
							<?php edit_comment_link(__( 'Edit', 'bunyad' ), '<span class="edit-link">', '</span>' ); ?>
						</div> <!-- .comment-meta -->
					
					</header> <!-- .comment-head -->
		
					<div class="comment-content">
						<div itemprop="commentText" class="text text-font"><?php comment_text(); ?></div>
						
						<?php if ($comment->comment_approved == '0'): ?>
							<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'bunyad'); ?></em>
						<?php endif; ?>
						
			
						<div class="reply">
							<?php
							comment_reply_link(array_merge($args, array(
								'reply_text' => __('Reply', 'bunyad'),
								'depth'      => $depth,
								'max_depth'  => $args['max_depth']
							))); 
							?>
							
						</div><!-- .reply -->
						
					</div>
				</article><!-- #comment-N -->
	
		<?php
				break;
		endswitch;
		
	}
	
endif;
