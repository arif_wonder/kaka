<?php
/**
 * Partial: Pagination used in multi-page content slideshows
 */

		global $page, $more, $numpages; // get multi-page numbers
		
		if (!$more) {
			return;
		}
?>

			
		<div class="post-pagination-next" data-type="<?php echo esc_attr(Bunyad::posts()->meta('content_slider')); ?>">
					
			<span class="button left"><?php 
				echo ($page - 1 > 0 ? _wp_link_page($page - 1) : '<a href="#">'); ?><i class="prev fa fa-chevron-left"></i></a></span>
				
			<span class="info"><?php printf(__('%s of %s', 'bunyad'), $page, $numpages); ?></span>
			
			<span class="button right"><?php 
				echo ($page + 1 > 0 ? _wp_link_page($page + 1) : '<a href="#">'); ?><i class="next fa fa-chevron-right"></i></a></span>
			

			<?php /*wp_link_pages(array(
					'before' => '<div class="links">', 
					'after' => '</div>', 
					'link_before' => '<span class="button">',
					'next_or_number' => 'next',
					'nextpagelink' => __('Next', 'bunyad') . ' <i class="next fa fa-chevron-right"></i>',
					'previouspagelink' => '<i class="prev fa fa-chevron-left"></i> ' . __('Prev', 'bunyad'),
					'link_after' => '</span>')); */?>
		</div>
