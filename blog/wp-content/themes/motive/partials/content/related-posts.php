<?php
/**
 * Partial: Related Posts
 */
?>

<?php if (is_single() && Bunyad::options()->related_posts): 

		$related = Bunyad::posts()->get_related(Bunyad::core()->get_sidebar() == 'none' ? 3 : 3);
		
		if (!$related) {
			return;
		}
?>

<section class="related-posts">
	<h3 class="section-head cf"><span class="title"><?php _e('Related Stories', 'bunyad'); ?></span></h3> 
	<ul class="ts-row">
	
	<?php foreach ($related as $post): setup_postdata($post); ?>
	
		<li class="column posts-grid one-third">
			
			<article>
					
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="image-link">
					<?php the_post_thumbnail(
						(Bunyad::core()->get_sidebar() == 'none' ? 'motive-highlight-block' : 'motive-main-block'),
						array('class' => 'image', 'title' => strip_tags(get_the_title()))); ?>
						
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<h3><a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a></h3>
				
			</article>
		</li>
		
	<?php endforeach; wp_reset_postdata(); ?>
	</ul>
</section>

<?php endif; ?>