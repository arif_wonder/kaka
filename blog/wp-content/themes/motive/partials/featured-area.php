<?php

/**
 * Partial Template: Display the featured slider based on settings
 */

// setup configuration vars
$data_vars = array(
	'data-animation' => Bunyad::options()->slider_animation,
	'data-autoplay' => Bunyad::options()->slider_autoplay,
	'data-autoplay-interval' => Bunyad::options()->slider_autoplay_interval,
);

/**
 * Slider Type - set relevant variables
 */

$slider_type = Bunyad::posts()->meta('featured_slider');

switch ($slider_type) {
			
	case 'split':
		$slider = 'split';
		$number = 7;
		break;
		
	case 'main':
	case 'main-content':
		$slider = 'main';
		$number = 5;
		break;
		
	default:
		$slider = 'grid';
		$number = 5;
		break;
}

// get latest featured posts
$args = apply_filters(
	'bunyad_block_query_args', 
	array('meta_key' => '_bunyad_featured_post', 'meta_value' => 1, 'order' => 'date', 'posts_per_page' => $number, 'ignore_sticky_posts' => 1),
	'slider'
);

// limited to tag?
if (Bunyad::posts()->meta('slider_tags')) {
	$args['tag_slug__in'] = explode(',', Bunyad::posts()->meta('slider_tags'));
}

// manual post ids?
if (Bunyad::posts()->meta('slider_posts')) {
	$args['post__in'] = explode(',', Bunyad::posts()->meta('slider_posts'));
}

/**
 * Is it a category slider?
 */
if (is_category()) {
	$cat = get_query_var('cat');
	$meta = Bunyad::options()->get('cat_meta_' . $cat);
	
	$slider_type = 'main-content';
	$slider = 'main';
	
	// slider not enabled? quit!
	if (empty($meta['slider'])) {
		return;
	}
		
	$args['cat'] = $cat;
	
	// latest posts?
	if ($meta['slider'] == 'latest') {
		unset($args['meta_key'], $args['meta_value']);
	}
}

/**
 * Main slider posts query
 */

// use latest posts?
if (Bunyad::posts()->meta('slider_type') == 'latest') {
	unset($args['meta_key'], $args['meta_value']);
}

$query = apply_filters('bunyad_featured_area_query', new WP_Query($args));

if (!$query->have_posts()) {
	return;
}

/**
 * Include our slider
 * 
 * Do NOT replace with get_template_part() as the variable scope inside  get_template_part() 
 * function will change. locate_template() is compatible with child themes.
 * 
 * @see locate_template()
 */

include locate_template('partials/slider/' . $slider . '.php');
