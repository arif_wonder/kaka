<?php
/**
 * Partial: Centered Header
 */
?>

		<header class="centered">
		
			<div class="title">
			
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
				
					<?php if (Bunyad::options()->header_date): ?>
						<span class="date"><?php echo date_i18n('l, M d, Y'); ?></span>
					<?php endif; ?>
				
					<?php get_template_part('partials/header/logo'); ?>
				
				</a>
			
			</div>
			
		</header>