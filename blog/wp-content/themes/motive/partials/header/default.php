<?php
/**
 * Partial: Default Header
 */
?>

		<header>
		
			<div class="title">
			
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
				
					<?php get_template_part('partials/header/logo'); ?>
				
				</a>
			
			</div>
			
			<div class="right">
				<?php 
					dynamic_sidebar('motive-header-right');
				?>
			</div>
			
		</header>
