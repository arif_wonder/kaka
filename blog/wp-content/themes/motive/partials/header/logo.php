<?php
/**
 * Partial: Logo 
 */
?>
				<?php if (Bunyad::options()->image_logo): // custom logo ?>
					
					<img src="<?php echo esc_attr(Bunyad::options()->image_logo); ?>" class="logo-image" alt="<?php 
						 echo esc_attr(get_bloginfo('name', 'display')); ?>" <?php 
						 echo (Bunyad::options()->image_logo_retina ? 'data-at2x="'. Bunyad::options()->image_logo_retina .'"' : ''); 
					?> />
						 
				<?php else: ?>
					
					<span class="text"><?php echo do_shortcode(Bunyad::options()->text_logo); // pre-sanitized on save - valid html allowed ?></span>
					
					<?php if (Bunyad::options()->slogan_logo): ?>
						<div class="slogan"><?php echo esc_html(Bunyad::options()->slogan_logo); ?></div>			
					<?php endif; ?>
					
					
				<?php endif; ?>