<?php
/**
 * Partial: Top bar template - displayed above header
 */
?>

<?php if (!Bunyad::options()->disable_topbar): ?>
	<div <?php Bunyad::markup()->attribs('top-bar', array('class' => array('top-bar', Bunyad::options()->topbar_style))); ?>>

		<div class="wrap">
			<section class="top-bar-content cf">
			
				<?php if (!Bunyad::options()->disable_topbar_ticker): ?>
				<div class="trending-ticker">
					<span class="heading"><?php echo Bunyad::options()->topbar_ticker_text; // filtered html allowed for admins ?></span>

					<ul>
						<?php $query = new WP_Query(apply_filters('bunyad_ticker_query_args', array('orderby' => 'date', 'order' => 'desc'))); ?>
						
						<?php while($query->have_posts()): $query->the_post(); ?>
						
							<li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
						
						<?php endwhile; ?>
						
						<?php wp_reset_postdata(); ?>
					</ul>
				</div>
				<?php endif; ?>

				<div class="search-box">
					<a href="#" class="top-icon fa fa-search"><span class="visuallyhidden"><?php _e('Search', 'bunyad'); ?></span></a>
					
					<div class="search">
					<form action="<?php echo esc_url(home_url('/')); ?>" method="get">
						<input type="text" name="s" class="query" value="<?php the_search_query(); ?>" placeholder="<?php _e('To search, type and press enter.', 'bunyad'); ?>" />
					</form>
					</div> <!-- .search -->
				</div>
				
				<?php dynamic_sidebar('motive-top-bar'); ?>			
				
			</section>
		</div>
		
	</div>
<?php endif; ?>