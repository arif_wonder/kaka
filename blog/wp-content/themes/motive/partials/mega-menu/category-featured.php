<?php 
/**
 * Mega Menu Category - Display sub nav, featured, and recent posts from a parent category
 */

?>

<div class="mega-menu category-ext ts-row term-wrap-<?php echo $item->object_id; ?>">

	<div class="column sub-cats">
		
		<ol class="sub-nav">
			<?php echo $sub_menu; // markup generated via WordPress default walker ?>
		</ol>
	
	</div>

	<section class="column featured">
		
		<?php 
			$query = new WP_Query(apply_filters(
				'bunyad_mega_menu_query_args', 
				array('cat' => $item->object_id, 'meta_key' => '_bunyad_featured_post', 'meta_value' => 1, 'order' => 'date', 'posts_per_page' => 1, 'ignore_sticky_posts' => 1),
				'category-featured'
			));
		?>
		
		<span class="heading"><?php _ex('Featured', 'Categories Mega Menu', 'bunyad'); ?></span>
		
		<div class="posts-grid">
		<?php while ($query->have_posts()): $query->the_post(); ?>
		
			<article>
				
				<a href="<?php the_permalink() ?>" class="image-link">
					<?php the_post_thumbnail('motive-highlight-block', array('title' => strip_tags(get_the_title()))); ?>
					
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<?php do_action('bunyad_comments_snippet'); ?>
				
				<?php do_action('bunyad_listing_meta'); ?>
				
				<a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a>
				
			</article>
		
		<?php endwhile; wp_reset_postdata();  ?>
		</div>
			
	</section>  

	<section class="column recent-posts">
	
		<span class="heading"><?php _ex('Recent', 'Categories Mega Menu', 'bunyad'); ?></span>
			
		<?php 
			$query = new WP_Query(apply_filters(
				'bunyad_mega_menu_query_args',
				array('cat' => $item->object_id, 'posts_per_page' => 3, 'ignore_sticky_posts' => 1),
				'category-recent'
			));
		?>
		
		<div class="posts-list compact">
		
		<?php while ($query->have_posts()) : $query->the_post(); ?>
			<div class="post">
			
				<a href="<?php the_permalink() ?>" class="image-link small">
					<?php the_post_thumbnail('post-thumbnail', array('title' => strip_tags(get_the_title()))); ?>
					
					<?php do_action('bunyad_image_overlay'); ?>						
				</a>
				
				<div class="content">
				
					<?php do_action('bunyad_listing_meta', 'small'); ?>
					
					<?php if (class_exists('Bunyad') && Bunyad::options()->review_show_widgets): ?>
						<?php echo apply_filters('bunyad_review_main_snippet', ''); ?>
					<?php endif; ?>
				
					<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>">
						<?php if (get_the_title()) the_title(); else the_ID(); ?></a>

				</div>
			
			</div>
		<?php endwhile; wp_reset_postdata(); ?>
		</div>
				
	</section>

</div>
