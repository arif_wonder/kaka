<?php 
/**
 * Mega Menu Category - Display recent posts from a parent category
 */

?>

<div class="mega-menu category ts-row term-wrap-<?php echo $item->object_id; ?>">

	<div class="column sub-cats">
		
		<ol class="sub-nav">
			<?php echo $sub_menu;  // markup generated via WordPress default walker ?>
		</ol>
	
	</div>

	<section class="column recent-posts">
			
		<?php
			$query = new WP_Query(apply_filters(
				'bunyad_mega_menu_query_args', 
				array('cat' => $item->object_id, 'posts_per_page' => 3,  'ignore_sticky_posts' => 1),
				'category'
			));
		?>
		
		<div class="ts-row posts-grid">
		
		<?php while ($query->have_posts()): $query->the_post(); ?>
		
			<div class="col-4">
				
				<a href="<?php the_permalink() ?>" class="image-link">
					<?php the_post_thumbnail('motive-highlight-block', array('title' => strip_tags(get_the_title()))); ?>
					
					<?php do_action('bunyad_image_overlay'); ?>
				</a>
				
				<?php do_action('bunyad_comments_snippet'); ?>
				
				<?php do_action('bunyad_listing_meta', 'small'); ?>
				
				<a href="<?php the_permalink(); ?>" class="post-link"><?php the_title(); ?></a>
				
			</div>
		
		<?php endwhile; wp_reset_postdata();  ?>
		
		</div>
		
	</section>

</div>