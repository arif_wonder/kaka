<?php 
/**
 * Partial Template - Displayed when no search results are found on a listing loop
 */
?>
	<article id="post-0" class="page no-results not-found">
		<div class="post-content">
			<h1><?php _e('Nothing Found!', 'bunyad'); ?></h1>
			<p><?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'bunyad'); ?></p>
		</div><!-- .entry-content -->
	</article><!-- #post-0 -->