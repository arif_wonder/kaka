<?php 
/**
 * Grid Featured Area - Not a slider but a grid featured area
 */
?>
	
	<div class="main-featured full">
		<div class="wrap cf">
		
		<div <?php Bunyad::markup()->attribs('featured-grid', array_merge(array('class' => 'featured-grid'), $data_vars)); ?>>
			<ul class="grid">
			
				<li class="first col-6">
					<?php while ($query->have_posts()): $query->the_post(); ?>
	
					<?php $cat = current(get_the_category()); ?>

					<div class="large item">
						<?php the_post_thumbnail('motive-grid-slider', array('alt' => esc_attr(get_the_title()), 'title' => '')); ?>					
						
						<a href="<?php the_permalink(); ?>" class="meta-link">	
							<span class="image-overlay cat-<?php echo esc_attr($cat->cat_ID); ?>"></span>
												
							<div class="meta-overlay">
								<div class="meta">
	
									<time class="the-date" datetime="<?php echo esc_attr(get_the_time(DATE_W3C)); ?>"><?php echo get_the_date(); ?></time>
								
									<h3 class="heading-text"><?php the_title(); ?></h3>
		
								</div>
							</div>
						</a>	
						
						<?php do_action('bunyad_comments_snippet'); ?>
						
					</div>
					
					<?php break; ?>
					
					<?php endwhile; ?>
				
				</li>

					
				<li class="second col-6">
					<?php while ($query->have_posts()): $query->the_post(); ?>
					
					<?php $cat = current(get_the_category()); ?>
					
					<div class="col-6 small item">
												
						<?php the_post_thumbnail('motive-image-block', array('alt' => esc_attr(get_the_title()), 'title' => '')); ?>
						
						<a href="<?php the_permalink(); ?>" class="meta-link">	
							<span class="image-overlay cat-<?php echo esc_attr($cat->cat_ID); ?>"></span>
												
							<div class="meta-overlay">
								<div class="meta">
	
									<time class="the-date" datetime="<?php echo esc_attr(get_the_time(DATE_W3C)); ?>"><?php echo get_the_date(); ?></time>
								
									<h3 class="heading-text"><?php the_title(); ?></h3>
		
								</div>
							</div>
						</a>
						
						<?php do_action('bunyad_comments_snippet'); ?>
						
					</div>
					
					<?php endwhile; ?>

				</li>
				
			</ul>

			<?php wp_reset_query(); ?>
			
		</div>

		</div> <!--  .wrap  -->
	</div>