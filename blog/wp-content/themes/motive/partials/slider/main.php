<?php 
/**
 * Main Classic Slider - Full-width
 */
?>

<?php if ($slider_type != 'main-content'): ?>

	<div class="main-featured full">
		<div class="wrap cf">
		
<?php endif; ?>
		
		<div <?php Bunyad::markup()->attribs('main-slider', array_merge(array('class' => 'main-slider as-primary'), $data_vars)); ?>>
				
				<?php while ($query->have_posts()): $query->the_post(); ?>
	
					<?php 
					
					$cat = current(get_the_category()); 
					$col_relative_width = Bunyad::get('motive')->block_relative_width();
					
					if ($slider_type == 'main-content' && $col_relative_width < 0.7) {
						$image = 'motive-alt-slider';
					}
					else {
						$image = 'motive-slider-full';
					}

					?>

					<div>
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail($image, array('alt' => esc_attr(get_the_title()), 'title' => '')); ?>					
													
							<div class="meta">
		
								<time class="the-date" datetime="<?php echo esc_attr(get_the_time(DATE_W3C)); ?>"><?php echo get_the_date(); ?></time>
									
								<h3 class="heading-text"><?php the_title(); ?></h3>
			
							</div>
						
						</a>
												
						<?php do_action('bunyad_comments_snippet'); ?>
						
					</div>
				
				<?php endwhile; ?>

				
		</div> <!-- .main-slider -->

<?php if ($slider_type != 'main-content'): ?>

		</div> <!-- .wrap  -->
	</div>
<?php endif;?>

	
	<?php wp_reset_query(); ?>