<?php 
/**
 * Split Slider - Slider at left, grid at right
 */

$main_count = $number - 2;

?>

	<div class="main-featured">
		<div class="wrap cf">
		
		<div class="slider-split ts-row">
			<div class="col-8">
				<ul <?php Bunyad::markup()->attribs('main-slider', array_merge(array('class' => 'main-slider as-primary alt'), $data_vars)); ?>>
	
					<?php while ($query->have_posts()): $query->the_post(); ?>
		
						<?php $cat = current(get_the_category()); ?>
	
						<li>
							<a href="<?php the_permalink(); ?>" class="image-link">
								<?php the_post_thumbnail('motive-alt-slider', array('alt' => esc_attr(get_the_title()), 'title' => '')); ?>					
													
								<div class="meta">
			
									<time class="the-date" datetime="<?php echo esc_attr(get_the_time(DATE_W3C)); ?>"><?php echo get_the_date(); ?></time>
										
									<h3 class="heading-text"><?php the_title(); ?></h3>
				
								</div>
							
							</a>
													
							<?php do_action('bunyad_comments_snippet'); ?>
						</li>
					
					<?php 
												
							if (($query->current_post + 1) >= $main_count):
								break;
							endif;
					
						endwhile; ?>
					
				</ul> <!-- .main-slider -->
			</div>
			
			<div class="col-4 blocks">
				
				<?php while ($query->have_posts()): $query->the_post(); ?>
				
				<div class="block">
					<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('motive-slider-alt-block', array('alt' => esc_attr(get_the_title()), 'title' => '')); ?>					
											
					<div class="meta">
	
						<time class="the-date" datetime="<?php echo esc_attr(get_the_time(DATE_W3C)); ?>"><?php echo get_the_date(); ?></time>
							
						<h3 class="heading-text"><?php the_title(); ?></h3>
	
					</div>
					
					</a>
											
					<?php do_action('bunyad_comments_snippet'); ?>
				</div>
				
				<?php endwhile; ?>
				
			</div>
	
			<?php wp_reset_query(); ?>
		</div>
		
		</div> <!--  .wrap  -->
	</div>