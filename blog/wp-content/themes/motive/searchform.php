<?php
/**
 * Search template to use in places like Search Widget 
 */
?>
	<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
		<label>
			<span class="screen-reader-text"><?php echo _x('Search for:', 'search', 'bunyad'); ?></span>
			<input type="search" class="search-field" placeholder="<?php echo esc_attr_x('To search, type and press enter.', 'search', 'bunyad') ?>" value="<?php 
				echo get_search_query(); // escaped ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'search', 'bunyad'); ?>" />
		</label>
		<button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
	</form>