<?php 	

class Admin	extends CI_Controller	
{
	public function __construct() {
		parent::__construct();

		$this->load->model('ModelAdmin');
		$this->load->library('form_validation');         
	}

	public function index()
	{	
		
		$this->load->view('admin/view_login');
		
	}

	public function login()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['login'])) {
			
			
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				
				$this->load->view('admin/view_login');
				
			}
			else
			{
				$data = array(

					"username" => $_POST['username'],
					"password" => $_POST['password']

				);


				$user = $this->ModelAdmin->login($data);

				if(!empty($user))
				{
					$this->session->set_userdata('user', $user);
					
					if($user->role != 'IPs Whitelisting'){

						redirect(base_url().'admin/panelAdd');
						
					}else{

						redirect(base_url().'admin/whitelistingIpsAdd');
					}

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Invalid Login.</div>');
					redirect(base_url().'admin');
				}
			}
			
			




		}


	}

	public function dashboard()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');
		
		$data['title']  = 'Dashboard';
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_dashboard.php");
		$this->load->view("admin/view_footer.php");

	}

	public function userAdd()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Add User';
		$data['panels']  = $this->ModelAdmin->getPanels();
		$data['roles']  = $this->ModelAdmin->getRoles();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_add_user.php");
		$this->load->view("admin/view_footer.php");

	}


	public function storeUser()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['userAdd'])) {
			
			$this->form_validation->set_rules('panel_id', 'Panel', 'required');
			$this->form_validation->set_rules('role_id', 'Role', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[mt_farhan_users.username]');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
			$this->form_validation->set_rules('confirmPassword', 'Password Confirmation', 'trim|required|matches[password]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[mt_farhan_users.email]');
			$this->form_validation->set_rules('mobile', 'Mobile', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Add User';
				$data['panels']  = $this->ModelAdmin->getPanels();
				$data['roles']  = $this->ModelAdmin->getRoles();
				
				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_add_user.php");
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{
				$data = array(

					"panel_id"  => $_POST['panel_id'],
					"role_id"  => $_POST['role_id'],
					"username" => $_POST['username'],
					"email" => $_POST['email'],
					"password" => $this->encryption->encrypt($_POST['password']),
					"mobile" => $_POST['mobile'],

				);


				$user = $this->ModelAdmin->userAdd($data);

				if($user)
				{
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> User Added Successfully.</div>');
					redirect(base_url().'admin/userAdd');

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/userAdd');
				}
			}
			
			




		}


	}


	public function userList()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Users List';
		$data['users']  = $this->ModelAdmin->getUsers();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_list_users.php",$data);
		$this->load->view("admin/view_footer.php");

	}


	public function userEdit($id='')
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Edit User';
		$data['panels']  = $this->ModelAdmin->getPanels();
		$data['roles']  = $this->ModelAdmin->getRoles();
		$data['user']  = $this->ModelAdmin->getUserById($id);

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_edit_user.php");
		$this->load->view("admin/view_footer.php");

	}


	public function updateUser($id='')
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['userEdit'])) {
			
			$this->form_validation->set_rules('panel_id', 'Panel', 'required');
			$this->form_validation->set_rules('role_id', 'Role', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
			$this->form_validation->set_rules('confirmPassword', 'Password Confirmation', 'trim|required|matches[password]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('mobile', 'Mobile', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Edit User';
				$data['panels']  = $this->ModelAdmin->getPanels();
				$data['roles']  = $this->ModelAdmin->getRoles();
				$data['user']  = $this->ModelAdmin->getUserById($id);
				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_edit_user.php");
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{	
				
				$data = array(

					"panel_id"  => $_POST['panel_id'],
					"role_id"  => $_POST['role_id'],
					"username" => $_POST['username'],
					"email" => $_POST['email'],
					"password" => $this->encryption->encrypt($_POST['password']),
					"mobile" => $_POST['mobile'],

				);
				

				$user = $this->ModelAdmin->updateUser($data,$id);

				if($user)
				{
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> User Updated Successfully.</div>');
					redirect(base_url().'admin/userList');

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/userList');
				}
			}
			
			




		}


	}

	public function userDelete($id)
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$user = $this->ModelAdmin->userDelete($id);
		if($user)
		{
			$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> User Deleted Successfully.</div>');
			redirect(base_url().'admin/userList');

		}else{

			$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
			redirect(base_url().'admin/userList');
		}
	}


	public function roleAdd()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Add Role';

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_add_role.php");
		$this->load->view("admin/view_footer.php");

	}

	public function storeRole()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['roleAdd'])) {
			
			
			$this->form_validation->set_rules('role', 'Role Name', 'required|is_unique[mt_farhan_roles.role]');


			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Add Role';

				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_add_role.php");
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{
				$data = array(

					"role"  => $_POST['role'],
					

				);


				$role = $this->ModelAdmin->roleAdd($data);

				if($role)
				{
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Role Added Successfully.</div>');
					redirect(base_url().'admin/roleAdd');

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/roleAdd');
				}
			}
			
			




		}


	}


	public function roleList()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Roles List';
		
		$data['roles']  = $this->ModelAdmin->getRoles();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_list_roles.php",$data);
		$this->load->view("admin/view_footer.php");

	}

	public function roleEdit($id='')
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Edit Role';
		
		$data['role']  = $this->ModelAdmin->getRoleById($id);

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_edit_role.php");
		$this->load->view("admin/view_footer.php");

	}


	public function updateRole($id='')
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['roleEdit'])) {
			
			
			$this->form_validation->set_rules('role', 'Role', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Edit Role';
				
				$data['role']  = $this->ModelAdmin->getRoleById($id);

				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_edit_role.php");
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{	
				
				$data = array(

					
					"role" => $_POST['role'],
					
				);
				

				$role = $this->ModelAdmin->updateRole($data,$id);

				if($role)
				{
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Role Updated Successfully.</div>');
					redirect(base_url().'admin/roleList');

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/roleList');
				}
			}
			
			




		}


	}


	public function roleDelete($id)
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$user = $this->ModelAdmin->roleDelete($id);
		if($user)
		{
			$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Role Deleted Successfully.</div>');
			redirect(base_url().'admin/roleList');

		}else{

			$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
			redirect(base_url().'admin/roleList');
		}
	}


	public function whitelistingIpsAdd()
	{

		$data['title']  = 'Whitelisting IPs';
		
		$data['whitelistingIps'] = $this->ModelAdmin->getWhitelistingIps();

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_whitelisting_ips.php",$data);
		$this->load->view("admin/view_footer.php");

	}


	public function logout()
	{
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		$this->session->sess_destroy();
		redirect('admin');
	}


	public function storeWhitelisitng()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			
			if($_POST['ips'] !=  '')
			{

				$data = array(

					"ips"  => implode(",",$_POST['ips'])
					

				);
			}else{


				$data = array(

					"ips" => ""
				);
			}
			
			


			$role = $this->ModelAdmin->updateWhitelisting($data);

			if($role)
			{
				echo '<div class="alert alert-success"><strong>Success!</strong> Whitelisting IPs Added Successfully.</div>';
				

			}else{

				echo '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>';
			}
		}
		
		




	}

	public function panelAdd()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Add Panel';

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_add_panel.php");
		$this->load->view("admin/view_footer.php");

	}
	
	public function storePanel()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['panelAdd'])) {
			
			
			$this->form_validation->set_rules('panel', 'Panel Name', 'required|is_unique[mt_farhan_panels.panel]');


			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Add Panel';

				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_add_panel.php");
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{
				$data = array(

					"panel"  => $_POST['panel'],
					

				);


				$panel = $this->ModelAdmin->panelAdd($data);

				if($panel)
				{
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Panel Added Successfully.</div>');
					redirect(base_url().'admin/panelAdd');

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/panelAdd');
				}
			}
			
			




		}


	}

	public function panelList()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Panels List';
		
		$data['panels']  = $this->ModelAdmin->getPanels();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_list_panels.php",$data);
		$this->load->view("admin/view_footer.php");

	}


	public function panelEdit($id='')
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Edit Panel';
		
		$data['panel']  = $this->ModelAdmin->getPanelById($id);

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_edit_panel.php");
		$this->load->view("admin/view_footer.php");

	}

	public function updatePanel($id='')
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['panelEdit'])) {
			
			
			$this->form_validation->set_rules('panel', 'Panel', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Edit Panel';
				
				$data['panel']  = $this->ModelAdmin->getPanelById($id);

				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_edit_panel.php");
				$this->load->view("admin/view_footer.php");

				
			}
			else
			{	
				
				$data = array(

					
					"panel" => $_POST['panel'],
					
				);
				

				$panel = $this->ModelAdmin->updatePanel($data,$id);

				if($panel)
				{
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Panel Updated Successfully.</div>');
					redirect(base_url().'admin/panelList');

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/panelList');
				}
			}
			
			




		}


	}


	public function panelDelete($id)
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');
		
		$panel = $this->ModelAdmin->panelDelete($id);
		if($panel)
		{
			$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Panel Deleted Successfully.</div>');
			redirect(base_url().'admin/panelList');

		}else{

			$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
			redirect(base_url().'admin/panelList');
		}
	}

	public function csv()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');
		
		$data['title']  = 'CSV';
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_csv.php");
		$this->load->view("admin/view_footer.php");
	}

	public function storeCsv()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['csvAdd'])) {
			
			
			
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'csv';
			$config['max_size']             = 100;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('csv'))
			{
				$error = array('error' => $this->upload->display_errors());
				
				$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong>'.$error['error'].'</div>');
				redirect(base_url().'admin/csv');
				
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$fileName = $data['upload_data']['file_name'];
				$csv = array();
				$lines = file('./uploads/'.$fileName, FILE_IGNORE_NEW_LINES);

				foreach ($lines as $key => $value)
				{
					$csv[] = str_getcsv($value);
				}
				$order_ids = array_column($csv, 0);
				$order_ids = array_filter($order_ids);
				if($this->ModelAdmin->updateOrders($order_ids))
				{		
					$this->ModelAdmin->updateMainOrders($order_ids);
					unlink('./uploads/'.$fileName);
					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Orders Updated Successfully.</div>');
					redirect(base_url().'admin/csv');

				}else{
					unlink('./uploads/'.$fileName);
					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/csv');
				}
				
			}



			
			
		}
	}

	public function pvAdd()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Calculate PV';

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_add_pv.php");
		$this->load->view("admin/view_footer.php");

	}

	public function storePV()
	{
		$this->load->model('ModelOrder');
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['pvAdd'])) {
			
			
			$this->form_validation->set_rules('from', 'From', 'required');
			$this->form_validation->set_rules('to', 'To', 'required');


			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Calculate PV';

				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_add_pv.php");
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{
				$from =$_POST['from'];
				
				$to =$_POST['to'];

				$orders = $this->ModelAdmin->calculatePV($from,$to);

				foreach($orders as $order)
				{

					$order_id  = $order->order_id;

					$itemPurchaseValue = $this->ModelOrder->getItemForPurchaseValue($order_id); // CHANGES BY FARHAN
							                       
					$items = json_decode($itemPurchaseValue->json_details);

					$total = 0;

					$pv_status = $this->ModelOrder->getOptionPvStatus($itemPurchaseValue->merchant_id);
							                       
					$myarray =array();

					if(isset($pv_status))
					{
                       	if($pv_status->option_value == '1')
                       	{

                       		$merchant_tax = $this->ModelOrder->getOptionMerchantTax($itemPurchaseValue->merchant_id);

                       		if($merchant_tax->option_value == '')
                       		{

                       			foreach($items as $key => $item) {

                       				$pv_percentage = $this->ModelOrder->getOptionPvPercentage($itemPurchaseValue->merchant_id);
                       				$purchase_value = $item->price - ($item->price*$pv_percentage->option_value/100);
                       				$item_name = $item->qty." x ".$this->ModelOrder->getItemById($item->item_id);
                       				$purchaseArray = array(

                       					"item_price" => $item->qty*$purchase_value,
                       					"item_name"  => $item_name
                       				);

                       				array_push($myarray, $purchaseArray);

                       				if(isset($item->addon_ids))
                       				{	
                       					$subItemArray =[];
                       					$addon_price = 0;

                       					foreach($item->addon_ids as $addon_id)
                       					{
                       						$addon = $this->ModelOrder->getAddonItemPrice($addon_id);
                       						$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
                       						$addon_price +=  $item->qty*intval($addon_price2);
                       						$subItemArray[] = array(

                       							"sub_item_price" => $item->qty*$addon_price2,
                       							"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
                       						);


                       						$myarray[$key]['sub_items'] = $subItemArray;
                       					}


                       					$total = $total + $addon_price;

                       				}

                       				$total += $item->qty*$purchase_value;



                       			}



                       		}else{

                       			foreach($items as $item) {


                       				$pv_percentage = $this->ModelOrder->getOptionPvPercentage($itemPurchaseValue->merchant_id);
                       				$purchase_value = $item->price - ($item->price*$pv_percentage->option_value/100);
                       				$purchase_value = $purchase_value + ($purchase_value*$merchant_tax->option_value/100);
                       				$item_name = $item->qty." x ".$this->ModelOrder->getItemById($item->item_id);
                       				$purchaseArray = array(

                       					"item_price" => $item->qty*$purchase_value,
                       					"item_name"  => $item_name
                       				);

                       				array_push($myarray, $purchaseArray);

                       				if(isset($item->addon_ids))
                       				{	
                       					$subItemArray =[];
                       					$addon_price = 0;
                       					foreach($item->addon_ids as $addon_id)
                       					{														

                       						$addon = $this->ModelOrder->getAddonItemPrice($addon_id);
                       						$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
                       						$addon_price += intval($addon_price2);
                       						$subItemArray[] = array(

                       							"sub_item_price" => $item->qty*$addon_price2,
                       							"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
                       						);

                       						$myarray[$key]['sub_items'] = $subItemArray;

                       					}

                       					$total = $total + $addon_price;

                       				}

                       				$total += $item->qty*$purchase_value;


                       				}

                       			}

                       	}else{

                       		foreach($items as $key => $item) {

                       			$purchase_value = $this->ModelOrder->getPurchaseValueFromItem($item->item_id);

                       			$item_name = $item->qty." x ".$this->ModelOrder->getItemById($item->item_id);
                       			$purchaseArray = array(

                       				"item_price" => $item->qty*$purchase_value,
                       				"item_name"  => $item_name
                       			);

                       			array_push($myarray, $purchaseArray);
                       			
                       			if(isset($item->addon_ids))
                       			{	
                       				$subItemArray =[];
                       				$addon_price = 0;
                       				foreach($item->addon_ids as $addon_id)
                       				{

                       					$addon = $this->ModelOrder->getAddonItemPrice($addon_id);
                       					$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
                       					$addon_price += $item->qty*intval($addon_price2);
                       					$subItemArray[] = array(

                       						"sub_item_price" => $item->qty*$addon_price2,
                       						"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
                       					);

                       					$myarray[$key]['sub_items'] = $subItemArray;

                       				}

                       				$total = $total + $addon_price;

                       			}

                       			$total += $item->qty*$purchase_value;

                       		}			

                       	}		

                       	$data = array(

                       		"purchase_item"  => json_encode($myarray),
                       		"purchase_value" => $total

                       	);

                       	$this->ModelOrder->purchaseValueUpdate($data,$order_id);

                     	}

                	}
 					
 					redirect(base_url().'admin/pvAdd?success=true');
 				    
            	}
               
           }
       }


       public function notifcation()
       {

       		$user = $this->session->userdata('user');
			if($user->role == 'IPs Whitelisting')
				redirect(base_url().'admin/whitelistingIpsAdd');
			
			$data['title']  = 'Create Notification';
			$this->load->view("admin/view_header.php",$data);
			$this->load->view("admin/mobile/create_notification.php");
			$this->load->view("admin/view_footer.php");


       }




       	public function sendNotifcation()
       	{	
       		$user = $this->session->userdata('user');
       		if($user->role == 'IPs Whitelisting')
       			redirect(base_url().'admin/whitelistingIpsAdd');

       		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['sendNotification'])) {
       			
       			$this->form_validation->set_rules('title', 'Title', 'required');
       			$this->form_validation->set_rules('body', 'Body', 'required');
       		

       			if ($this->form_validation->run() == FALSE)
       			{
       				
       				$data['title']  = 'Notification';
       			
       				$this->load->view("admin/view_header.php",$data);
       				$this->load->view("admin/mobile/create_notification.php");
       				$this->load->view("admin/view_footer.php");
       				
       			}
       			else
       			{
       				$firebase_ids = $this->ModelAdmin->getFireBaseId();

       				$firebase_ids = array_column($firebase_ids,"firebase_id");

       				$url = 'http://finalcheck.speedypixelgame.com/services/callback/sendNotifications';

       				$title = $_POST['title'];
       				$body = $_POST['body'];

       				$fields = array(
       				            'title' => $title,
       				            'body' => $body,
       				            'fire_base_ids' => $firebase_ids,
       				            'redirect_to'	=> base_url().'orderManagement3/admin/notifcation',
       				            'id'			=> ''
       				        );

       				//url-ify the data for the POST
       				$fields_string = http_build_query($fields);

       				//open connection
       				$ch = curl_init();

       				//set the url, number of POST vars, POST data
       				curl_setopt($ch,CURLOPT_URL, $url);
       				curl_setopt($ch,CURLOPT_POST, 1);
       				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

       				//execute post
       				$result = curl_exec($ch);


       				//close connection
       				curl_close($ch);

       			}
       		

       		}


       }


      public function syncOrders()
      {

      	$order_id = $this->ModelAdmin->getFromOrderTable();

      	$order_duplicate_id = $this->ModelAdmin->getFromOrderDuplicateTable();
      
      	if($order_id > $order_duplicate_id)
      	{

      		$orders = $this->ModelAdmin->fetchOrder($order_duplicate_id);

      		foreach($orders as $order)
      		{

      			$this->ModelAdmin->insertOrders($order);
      		}

      	}
      
      }

      // AGENT

    public function agentAttendance()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Agents Attendance';
		$data['agents']  = $this->ModelAdmin->getAgents();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/agents/agent_attendance.php",$data);
		$this->load->view("admin/view_footer.php");

	}

	public function agents()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Agents List';
		$date = date('Y-m-d');
		$data['agents']  = $this->ModelAdmin->getAgents();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/agents/agents_list.php",$data);
		$this->load->view("admin/view_footer.php");
	}

	public function agentReport($agent_id)
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Agents Report';
		$date = date('Y-m-d');

		$data['lastReport'] = $this->ModelAdmin->getAgentReportById($agent_id);

		$data['agent'] = $this->ModelAdmin->getAgentById($agent_id);

		$data['agentReports']  = $this->ModelAdmin->getAgentReportByIdDate($agent_id,$date);

		if(sizeof($data['agentReports']) == 0 && sizeof($data['lastReport']) > 0)
		{
			$data['title']  = 'Agents Report';
			$data = array(

							"agent_id" => $agent_id,
							"margin" => $data['lastReport']->current_balance,
							"current_balance" => $data['lastReport']->current_balance,
							"description" => "Starting Balance of the day",
							"added_by"	=> "Added by ".$user->username,
							"order_creation_time" => date('Y-m-d h:i:s'),
							"delivery_time" => date('Y-m-d h:i:s')

						);
			$this->ModelAdmin->agentWalletReportAdd($data);
			$data['lastReport'] = $this->ModelAdmin->getAgentReportById($agent_id);
			$data['agent'] = $this->ModelAdmin->getAgentById($agent_id);
			$data['agentReports']  = $this->ModelAdmin->getAgentReportByIdDate($agent_id,$date);
			

		}
		
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/agents/agent_report.php",$data);
		$this->load->view("admin/view_footer.php");
	}

    public function addAgentWallet($agent_id)
	{	
		
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Add Agent Wallet';
		$data['agents']  = $this->ModelAdmin->getAgents();
		
		$data['agent'] = $this->ModelAdmin->getAgentById($agent_id);
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/agents/add_agent.php",$data);
		$this->load->view("admin/view_footer.php");

	}

	public function storeAgentWallet()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['agentWalletAdd'])) {
			
			
			
			$this->form_validation->set_rules('wallet', 'Wallet', 'required');
			
			


			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Add Agent Wallet';
			
				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/agents/add_agent.php",$data);
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{

				$agent_id = $_POST['agent_id'];
				$wallet = $_POST['wallet'];

				$agentWallet = $this->ModelAdmin->getAgentWalletReportById($agent_id);

				if(sizeof($agentWallet) > 0)
				{
					$previous_balance = $agentWallet->current_balance;
					$current_balance = $previous_balance + $wallet;
					$margin = $wallet;

					$data = array(

							"agent_id" => $agent_id,
							"margin" => $margin,
							"current_balance" => $current_balance,
							"added_by"	=> "Added by ".$user->username,
							"order_creation_time" => date('Y-m-d h:i:s'),
							"delivery_time" => date('Y-m-d h:i:s')

						);

				}else{

					$margin = $wallet;
					$current_balance = $wallet;

					$data = array(

							"agent_id" => $agent_id,
							"margin" => $margin,
							"current_balance" => $current_balance,
							"added_by"	=> "Added by ".$user->username,
							"order_creation_time" => date('Y-m-d h:i:s'),
							"delivery_time" => date('Y-m-d h:i:s')

						);
				}

				$agentWallet = $this->ModelAdmin->agentWalletReportAdd($data);

				if($agentWallet)
				{

					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Agent Wallet Added Successfully.</div>');
					redirect(base_url().'admin/agentReport/'.$agent_id);

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/agentReport/'.$agent_id);
				}
			}
			
			




		}
	}

	public function addMiscellaneous($agent_id)
	{	
		
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Add Agent Wallet';
		$data['agents']  = $this->ModelAdmin->getAgents();
		
		$data['agent'] = $this->ModelAdmin->getAgentById($agent_id);
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/agents/add_miscellaneous.php",$data);
		$this->load->view("admin/view_footer.php");

	}

	public function storeMiscellaneous()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['miscellaneousAdd'])) {
			
			
			
			$this->form_validation->set_rules('wallet', 'Wallet', 'required');
			
			


			if ($this->form_validation->run() == FALSE)
			{
				
				$data['title']  = 'Add Agent Wallet';
			
				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/agents/add_agent.php",$data);
				$this->load->view("admin/view_footer.php");
				
			}
			else
			{

				$agent_id = $_POST['agent_id'];
				$wallet = -$_POST['wallet'];

				$agentWallet = $this->ModelAdmin->getAgentWalletReportById($agent_id);

				if(sizeof($agentWallet) > 0)
				{
					$previous_balance = $agentWallet->current_balance;
					$current_balance = $previous_balance + $wallet;
					$margin = $wallet;

					$data = array(

							"agent_id" => $agent_id,
							"margin" => $margin,
							"current_balance" => $current_balance,
							"added_by"	=> "Added by ".$user->username,
							"order_creation_time" => date('Y-m-d h:i:s'),
							"delivery_time" => date('Y-m-d h:i:s')

						);

				}else{

					$margin = $wallet;
					$current_balance = $wallet;

					$data = array(

							"agent_id" => $agent_id,
							"margin" => $margin,
							"current_balance" => $current_balance,
							"added_by"	=> "Added by ".$user->username,
							"order_creation_time" => date('Y-m-d h:i:s'),
							"delivery_time" => date('Y-m-d h:i:s')

						);
				}

				$agentWallet = $this->ModelAdmin->agentWalletReportAdd($data);

				if($agentWallet)
				{

					$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Miscellaneous Added Successfully.</div>');
					redirect(base_url().'admin/agentReport/'.$agent_id);

				}else{

					$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
					redirect(base_url().'admin/agentReport/'.$agent_id);
				}
			}
			
			




		}
	}


	public function reportSearchByDate()
	{
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$from = $_POST['from'];
		$to = $_POST['to'];
		$agent_id = $_POST['agent_id'];

		$data['title']  = 'Agents Report';

		$data['lastReport'] = $this->ModelAdmin->getAgentReportById($agent_id);

		$data['agent'] = $this->ModelAdmin->getAgentById($agent_id);

		$data['agentReports']  = $this->ModelAdmin->getAgentReportFromTo($agent_id,$from,$to);
		
		
		
		$this->load->view("admin/agents/ajax_agent_report.php",$data);
		$this->load->view("admin/view_footer.php");
	}

	//  function check_agent_wallet() {
	//     $agent_id = $this->input->post('agent_id');// get fiest name
	    
	//     $this->db->select('id');
	//     $this->db->from('agent_wallet');
	//     $this->db->where('agent_id', $agent_id);
	//     $this->db->where('date', date('Y-m-d'));
	//     $query = $this->db->get();
	//     $num = $query->num_rows();
	//     if ($num > 0) {
	//     	$this->session->set_flashdata('credential', '<div class="alert alert-danger">Today Agent already exist!</div>');
	//         return FALSE;
	//     } else {
	//         return TRUE;
	//     }
	// }

	public function agentWalletList()
	{	
		$user = $this->session->userdata('user');
		if($user->role == 'IPs Whitelisting')
			redirect(base_url().'admin/whitelistingIpsAdd');

		$data['title']  = 'Agent Wallet List';
		$date = date('Y-m-d');
		$data['agentWallets']  = $this->ModelAdmin->getAgentWallet($date);
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/agents/agent_wallet_lists.php",$data);
		$this->load->view("admin/view_footer.php");

	}

	// public function agentWalletEdit($id='')
	// {	
	// 	$user = $this->session->userdata('user');
	// 	if($user->role == 'IPs Whitelisting')
	// 		redirect(base_url().'admin/whitelistingIpsAdd');

	// 	$data['title']  = 'Edit Agent Wallet';
		
	// 	$data['agentWallet']  = $this->ModelAdmin->getAgentWalletById($id);
	// 	$data['agents']  = $this->ModelAdmin->getAgents();
	// 	$this->load->view("admin/view_header.php",$data);
	// 	$this->load->view("admin/agents/agent_wallet_edit.php",$data);
	// 	$this->load->view("admin/view_footer.php");

	// }

	// public function updateAgentWallet($id='')
	// {	
	// 	$user = $this->session->userdata('user');
	// 	if($user->role == 'IPs Whitelisting')
	// 		redirect(base_url().'admin/whitelistingIpsAdd');

	// 	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['agentWalletEdit'])) {
			
			
	// 		$this->form_validation->set_rules('wallet', 'Wallet', 'required');
			
	// 		if ($this->form_validation->run() == FALSE)
	// 		{	
				
	// 			$data['agentWallet']  = $this->ModelAdmin->getAgentWalletById($id);
	// 			$data['agents']  = $this->ModelAdmin->getAgents();
	// 			$this->load->view("admin/view_header.php",$data);
	// 			$this->load->view("admin/agents/agent_wallet_edit.php",$data);
	// 			$this->load->view("admin/view_footer.php");

				
	// 		}
	// 		else
	// 		{	
				
	// 			$data = array(

					
	// 				"wallet" => $_POST['wallet'],
					
	// 			);
				

	// 			$agentWallet = $this->ModelAdmin->updateAgentWallet($data,$id);

	// 			if($agentWallet)
	// 			{
	// 				$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Agent Wallet Updated Successfully.</div>');
	// 				redirect(base_url().'admin/agentWalletList');

	// 			}else{

	// 				$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
	// 				redirect(base_url().'admin/agentWalletList');
	// 			}
	// 		}
			
			




	// 	}


	// }
}	

?>