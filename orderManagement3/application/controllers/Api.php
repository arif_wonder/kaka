<?php 
date_default_timezone_set('Asia/Kolkata');
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json;  charset=utf-8');
class Api extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		
	
		$this->load->model('ModelApi');
		$this->load->model('ModelOrder');
		
		
		
		
	}

	public function getWalletReport()
	{	

		if($_SERVER['REQUEST_METHOD'] == "POST")
		{

		$agent_id = $this->input->post('agent_id');

		$date = $this->input->post('date');
		
		$result = $this->ModelApi->getWalletReport($agent_id,$date);

	

		if(sizeof($result) > 0)
			{
				$json=array("status"=>1,"info"=>$result);

			}else{

				$json=array("status"=>0,"info"=>"No Data");
			}
		

		}else{

			$json=array("status"=>2,"info"=>"Bad Request");
		}


		echo json_encode($json);

	} 


	public function insertWalletReport()
	{	
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{

				$agent_id = $this->input->post('agent_id');

				$order_id = $this->input->post('order_id');



				$itemPurchaseValue = $this->ModelOrder->getItemForPurchaseValue($order_id); 

				if($itemPurchaseValue->purchase_value == 0)
				{

					$orders2 ='';
					$items = json_decode($itemPurchaseValue->json_details);
					$total = 0;
					$pv_status = $this->ModelOrder->getOptionPvStatus($itemPurchaseValue->merchant_id);
					$myarray =array();

					if($pv_status->option_value == '1')
					{

					
								$merchant_tax = $this->ModelOrder->getOptionMerchantTax($itemPurchaseValue->merchant_id);
								
								if($merchant_tax->option_value == '')
								{
							
								
									foreach($items as $key => $item) 
											{
												$pv_percentage = $this->ModelOrder->getOptionPvPercentage($itemPurchaseValue->merchant_id);
												$purchase_value = $item->price - ($item->price*$pv_percentage->option_value/100);
												$item_name = $item->qty." x ".$this->ModelOrder->getItemById($item->item_id);
												$purchaseArray = array(

																	"item_price" => $item->qty*$purchase_value,
																	"item_name"  => $item_name
																);
												array_push($myarray, $purchaseArray);

																
												if(isset($item->addon_ids))
													{	
														$subItemArray =[];
														$addon_price = 0;

														foreach($item->addon_ids as $addon_id)
														{
																			// $val_subs=explode('|',$item);
																			
															$addon = $this->ModelOrder->getAddonItemPrice($addon_id);
															$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
															$addon_price +=  $item->qty*intval($addon_price2);
															$subItemArray[] = array(

																					"sub_item_price" => $item->qty*$addon_price2,
																					"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
																				);
																			

															$myarray[$key]['sub_items'] = $subItemArray;
													
														}
																	
															$total = $total + $addon_price;
																
														}

														$total += $item->qty*$purchase_value;
				
												}
									
								

								}else{

									foreach($items as $item)
										{

										$pv_percentage = $this->ModelOrder->getOptionPvPercentage($itemPurchaseValue->merchant_id);
										$purchase_value = $item->price - ($item->price*$pv_percentage->option_value/100);
										$purchase_value = $purchase_value + ($purchase_value*$merchant_tax->option_value/100);
										$item_name = $item->qty." x ".$this->ModelOrder->getItemById($item->item_id);
										$purchaseArray = array(

															"item_price" => $item->qty*$purchase_value,
															"item_name"  => $item_name
															);
										array_push($myarray, $purchaseArray);

												if(isset($item->addon_ids))
													{	
														$subItemArray =[];
														$addon_price = 0;
															foreach($item->addon_ids as $addon_id)
																{														
																			
																	$addon = $this->ModelOrder->getAddonItemPrice($addon_id);
																	$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
																	$addon_price2 = $addon_price2 + ($addon_price2*$merchant_tax->option_value/100);
																	$addon_price += intval($addon_price2);
																	$subItemArray[] = array(

																				"sub_item_price" => $item->qty*$addon_price2,
																				"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
																			);
																			
																	$myarray[$key]['sub_items'] = $subItemArray;
																		
																}
																$total = $total + $addon_price;
																
														}
														
														$total += $item->qty*$purchase_value;

														

										}

								}



					}else{


							foreach($items as $key => $item) 
							{
								$purchase_value = $this->ModelOrder->getPurchaseValueFromItem($item->item_id);
																
								$item_name = $item->qty." x ".$this->ModelOrder->getItemById($item->item_id);
								$purchaseArray = array(

											"item_price" => $item->qty*$purchase_value,
											"item_name"  => $item_name
																				);
										
											array_push($myarray, $purchaseArray);
											
																		
											if(isset($item->addon_ids))
												{	
													$subItemArray =[];
													$addon_price = 0;
													foreach($item->addon_ids as $addon_id)
													{
													
														$addon = $this->ModelApi->getAddonItemPrice($addon_id);
														$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
														$addon_price += $item->qty*intval($addon_price2);
														$subItemArray[] = array(

																			"sub_item_price" => $item->qty*$addon_price2,
																			"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
																			);
																						
														$myarray[$key]['sub_items'] = $subItemArray;
																					
													}
													
													$total = $total + $addon_price;
																			
												}
													$total += $item->qty*$purchase_value;

							}			
					}

								$data = array(


									"purchase_item"  => json_encode($myarray),
  									"purchase_value" => $total

								);

								$this->ModelOrder->purchaseValueUpdate($data,$order_id);

				}else 
				{


					$total = $itemPurchaseValue->purchase_value;

					$items = json_decode($itemPurchaseValue->purchase_item);
					
							
				}

				$orderDetail = $this->ModelOrder->getOrderById2($order_id);	

				$agentWallet = $this->ModelApi->getAgentWallet($agent_id);

				$pv = $total;

				$dv = $orderDetail->total_w_tax;

				$payment_type = $orderDetail->payment_type;

				$order_status = $orderDetail->status;

				$order_creation_time = $orderDetail->date_created;

				if($payment_type == 'cod')
				{
					$margin = $dv - $pv;

					$agentWallet = $agentWallet + $margin;
					$description = 'cod';
					

				}else{

					$dv = 0;
					$agentWallet = $agentWallet - $pv;
					$margin = $dv - $pv;
					$description = 'online';

				}


				$this->ModelApi->updateAgentWallet($agentWallet,$agent_id);

				$agentWallet = $this->ModelApi->getAgentWallet($agent_id);

				$date = date('Y-m-d');
				
				$getLastReportPetrol = $this->ModelApi->getLastReportPetrol($agent_id,$date);

				if(empty($getLastReportPetrol))
				{
					$getLastReportPetrol = 0;
				}

				$data = array(

					"agent_id" => $agent_id,
					"order_id"	=>$order_id,
					"pv"		=>$pv,
					"dv"		=>$dv,
					"margin"	=> $margin,
					"current_balance" => $agentWallet,
					"order_status"	=> $order_status,
					"description"	=> $description,
					"petrol"		=> $getLastReportPetrol + 18,
					"order_creation_time" => $order_creation_time, 
					"delivery_time"		=> date("Y-m-d H:i:s")


				);

				$this->ModelApi->insertWalletReport($data);

				$json=array("status"=>1,"info"=>"Your Order has been marked as delivered");
		
		}else{


			$json=array("status"=>2,"info"=>"Bad Request");
		}

		echo json_encode($json);
		
	}
}

 ?>