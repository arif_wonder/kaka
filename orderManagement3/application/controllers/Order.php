<?php
date_default_timezone_set('Asia/Kolkata');
header('Access-Control-Allow-Origin: *');
class Order extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		
		
	
		if($this->session->userdata('order_user'))
		{
			$this->load->model('ModelOrder');
		}else{

			redirect(base_url().'login');
			exit;
		}
		

	}

	public function index()
	{	
		
		$password = $this->session->userdata('password');
		$user = $this->session->userdata('order_user');

		$data['role'] = $user->role;
		$roles = array('Super Admin','Manager','Junior Manager','Data Manager');
		if ($user->panel == 'Super Panel' && in_array($user->role,$roles)) {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();


			// CODE ADDED BY FARHAN
			// FOR FILTERS
			$from = date('Y-m-d');  
			$data['getFilteredLists'] = $this->ModelOrder->getFilteredListByDeliveryDate($from,$to='');

			// FOR STOP REFRESH
			$data['refresh'] = $this->session->userdata('refresh');
			// END BY FARHAN

			$this->load->view('view_order',$data);
		}
		elseif ($user->panel == 'Panel A' && in_array($user->role,$roles)) {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgentsAA();

			$this->load->view('view_panelA.php',$data);
			
		}elseif ($user->panel == 'Panel B' && in_array($user->role,$roles)) {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgentsBB();

			$this->load->view('view_panelB.php',$data);
		}


	}

// new work
	public function unAssign()
	{
		extract($_POST);

		$agentName = $this->ModelOrder->getAgentByOrderId($order_id);

		$getNoOfOrders = $this->ModelOrder->getNoOfOrders($agentName);
		$target = array($order_id);
		$haystack = explode(",",$getNoOfOrders->order_status);
		$arrDiff = array_diff($haystack, $target);

		if(in_array($order_id, $haystack)) {

			$arrDiff = array_diff($haystack, $target);
			$no_of_orders = array(

				"no_of_orders" => $getNoOfOrders->no_of_orders - 1,
				"order_status" => implode(",",$arrDiff)
			);
			$this->ModelOrder->updateNoOfOrders($no_of_orders,$agentName);
		}
		$data11 = array(
			'delivery_agent' =>  ''
		);		
		$this->ModelOrder->deliveryAgentUpdate($data11,$order_id);
		$data22 = array(
			'status' =>  "Confirmed"
		);
		$this->ModelOrder->statusUpdate($data22,$order_id);
		$this->ModelOrder->deleteHisteryRow($order_id);
		$update_data = array('app_status'=>'');
		$this->ModelOrder->appStatusUpdate($update_data,$order_id);

	}

	public function sendToApp()
	{
		extract($_POST);
		$record =$this->ModelOrder->agentInfo($agent_id);
		$token = $record['firebase_id'];
		$agentName = $record['name'];
		$data11 = array(

			'delivery_agent' =>  $agentName

		);		
		$this->ModelOrder->deliveryAgentUpdate($data11,$order_id);

		$record2 = $this->ModelOrder->getStatus2($order_id);
		echo $status  = $record2['status'];

		$update_data = array('app_status'=>'unseen');
		$this->ModelOrder->appStatusUpdate($update_data,$order_id);

		$data22 = array(

			'status' =>  "Processing"

		);

		$this->ModelOrder->statusUpdate($data22,$order_id);

			// ADDED BY FARHAN 15 JUNE 2018


		$myData = array(

			'assign_time' => date('Y-m-d H:i:s')
		);

		$this->ModelOrder->statusUpdate2($myData,$order_id);


		$getNoOfOrders = $this->ModelOrder->getNoOfOrders($agentName);

		$order_status = array(

			'order_id' => $order_id

		);


		if($getNoOfOrders->order_status == '')
		{
			$order_status = implode(",",$order_status);

			$no_of_orders = array(

				"no_of_orders" => $getNoOfOrders->no_of_orders + 1,
				"order_status" => $order_status
			);



			$this->ModelOrder->updateNoOfOrders($no_of_orders,$agentName);
		}else{
			$target = array($order_id);
			$haystack = explode(",",$getNoOfOrders->order_status);


			if(!in_array($order_id, $haystack)) {

				$no_of_orders = array(

					"no_of_orders" => $getNoOfOrders->no_of_orders + 1,
					"order_status" => implode(",",$haystack).",".implode(',',$order_status)
				);
				$this->ModelOrder->updateNoOfOrders($no_of_orders,$agentName);

			}

		}


			// END BY FARHAN 15 JUNE 2018


		$timer = time();
		$cDate =  date('Y-m-d g:i:s');
		//for history record
		$historyData = array(
			'merchant_name' 	=> $merchant_name,
			'merchant_id'       => $record2['merchant_id'],
			'items' 			=> $items,
			'total' 			=> $total,
			'contact' 	       	=> $contact,
			'order_id' 		    => $order_id,
			'name' 	    		=> $name,
			'location' 	    	=> $location,
			'status' 		    => 'Processing',
			'agent_id' 		    => $agent_id,
			'create_date'       => $cDate,
			'payment_type'      => $payment_type
		);
		if ($app_status_flag == "new")
			$this->ModelOrder->saveHistory($historyData);
		else
			$this->ModelOrder->updateHistory($historyData,$order_id);

		define( 'API_ACCESS_KEY', 'AIzaSyB8JwFBtEyDOCi6cFvuTr26AG99h05dLMI' );
		$registrationIds = array( $token );
		$msg = array
		(
			'message' 	=> "You Have Receive a New Order",
			'title'		=> "Order Notification",
			'image' => "https://www.finalcheck.speedypixelgame.com//upload/1476254450-1475737169-kaka-logo-(1).png",
			'merchant_name' 	=> $merchant_name,
			'items' 			=> $items,
			'total' 			=> $total,
			'contact' 		=> $contact,
			'order_id' 		=> $order_id,
			'name' 			=> $name,
			'location' 		=> $location,
			'status' 			=> $status,
			'agent_id' 		=> $agent_id

		);
		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg

		); 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		); 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
	}
	public function addAgent()
	{
		$record =$this->ModelOrder->agentInfo($_POST['id']);
		$name = $record['name'];
		$data = array('name'=>$name,'agent_id'=>$_POST['id']);
		$this->ModelOrder->agentADD($data);	
	}
	public function removeAgent()
	{
		$this->ModelOrder->removeAgent($_POST['id']);
	}
	public function agents()
	{
		$user = $this->session->userdata('order_user');
		$roles = array('Super Admin','Manager');
		if (in_array($user->role,$roles)) {

			$data['agents'] =  $this->ModelOrder->getAgents();
			$this->load->view("view_agents.php",$data);
		}

		
	}
	public function showAgentOrders()
	{
		$agent = $_POST['agent'];
		$data['type'] = "showAgentOrders";
		$data['data'] = $this->ModelOrder->showAgentOrders($agent);
		$this->load->view('view_ajax_order_detail',$data);
	}
	public function showAgentHistory()
	{
		$agent = $_POST['agent'];
		$data['type'] = "showAgentHistory";
		$data['data'] = $this->ModelOrder->showAgentHistory($agent);
		$this->load->view('view_ajax_order_detail',$data);	
	}
	public function getAgentsData()

	{
		$data['agent_id'] = $_POST['agent_id'];
		$data['type'] = 'showProfile';
		$data['agent_data'] = $this->ModelOrder->getAgentProfile($data['agent_id']);
		$this->load->view('view_ajax_order_detail',$data);
	}
	public function updateAgentProfile()
	{
		$agent_id = $_POST['agent_id'];
		$data = array(

			'name' =>  $_POST['name'],
			'username' =>  $_POST['username'],
			'password' =>  $_POST['password'],
			'lati' =>  $_POST['lati'],
			'longi' =>  $_POST['longi'],
			'status' =>  $_POST['status'],
			'mobile' =>  $_POST['mobile']

		);
		$this->ModelOrder->updateAgent($data,$agent_id);
	}
	public function addNewAgent()
	{
		$data = array(

			'name' =>  $_POST['name'],
			'username' =>  $_POST['username'],
			'password' =>  $_POST['password'],
			'lati' =>  $_POST['lati'],
			'longi' =>  $_POST['longi'],
			'status' =>  $_POST['status'],
			'mobile' =>  $_POST['mobile']

		);
		$this->ModelOrder->addAgent($data);
	}
	public function delAgent()
	{
		$agent_id = $_POST['agent_id'];
		$this->ModelOrder->delAgent($agent_id);
	}
	public function getUserLocation()
	{
		$data['lati'] = $_POST['lat'];
		$data['longi'] = $_POST['long'];
		$data['type'] = 'showLocation';
		$this->load->view('view_ajax_order_detail',$data);
	}
//new work end







	public function workerA()
	{
		$password = $this->session->userdata('password');
		$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
		if (($password == "victor999") ||($password == "black999star!")){
			$this->load->view('workerA_view',$data);
		}
		
	}
	public function workerB()
	{
		$password = $this->session->userdata('password');
		
		$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
		if (($password == "charge777") ||($password == "black999star!")){
			$this->load->view('workerB_view',$data);
		}
	}
	public function logout()
	{
		$this->session->unset_userdata(array('username','password'));
		redirect(base_url().'login');
	}
	
	public function updateWorker()
	{
		$order_id = $_POST['order_id'];
		$worker = $_POST['worker_mark'];
		$this->ModelOrder->workerUpdate($order_id,$worker);
	}
	public function updateWorkerAgent()
	{
		$order_id = $_POST['agentID'];
		$worker = $_POST['worker_mark'];
		$this->ModelOrder->workerUpdateAgent($order_id,$worker);
	}
	public function ajaxStatusUpdate()
	{
		$order_id = $_POST['order_id'];

		// ADDED BY FARHAN 15 JUNE 2018

		$agentName = $_POST['agentName'];

		$getNoOfOrders = $this->ModelOrder->getNoOfOrders($agentName);
		
		if($_POST['status'] == 'Processing')
		{	
			


			$order_status = array(

				'order_id' => $order_id

			);

			$myData = array(

				'assign_time' => date('Y-m-d H:i:s')
			);
			if($getNoOfOrders->order_status == '')
			{	

				
				$this->ModelOrder->statusUpdate2($myData,$order_id);

				$order_status = implode(",",$order_status);

				$no_of_orders = array(

					"no_of_orders" => $getNoOfOrders->no_of_orders + 1,
					"order_status" => $order_status
				);
				
				$this->ModelOrder->updateNoOfOrders($no_of_orders,$agentName);
			}else{


				

				$target = array($order_id);
				$haystack = explode(",",$getNoOfOrders->order_status);


				if(!in_array($order_id, $haystack)) {
					$this->ModelOrder->statusUpdate2($myData,$order_id);
					$no_of_orders = array(

						"no_of_orders" => $getNoOfOrders->no_of_orders + 1,
						"order_status" => implode(",",$haystack).",".implode(',',$order_status)
					);
					$this->ModelOrder->updateNoOfOrders($no_of_orders,$agentName);

				}
				
			}



		}else{

			if($getNoOfOrders->no_of_orders != 0)
			{


				$target = array($order_id);
				$haystack = explode(",",$getNoOfOrders->order_status);
				$arrDiff = array_diff($haystack, $target);

				if(in_array($order_id, $haystack)) {

					$arrDiff = array_diff($haystack, $target);
					$no_of_orders = array(

						"no_of_orders" => $getNoOfOrders->no_of_orders - 1,
						"order_status" => implode(",",$arrDiff)
					);
					$this->ModelOrder->updateNoOfOrders($no_of_orders,$agentName);
				}
				
			}


		}

		// END BY FARHAN 15 JUNE 2018

		$data = array(

			'status' =>  $_POST['status']

		);

		$this->ModelOrder->statusUpdate($data,$order_id);
		echo allocateColor($data['status']);


		if($_POST['status'] == 'Delivered')
		{
			$data = array(

				'delivery_time2' =>  date('g:i'),
				'assign_time'	=> '0000-00-00 00:00:00'

			);

			$this->ModelOrder->deliveryTime2Update($data,$order_id);


			$agentName = $_POST['agentName'];

			$data = array(

				"status" => 1

			);

			$this->ModelOrder->agentStatusGreenByName($data,$agentName);
		}

		$checkSms = trim($_POST['checkSms']);
		if($checkSms == 'true')
		{

			// SEND SMS TO DELIVERY BOYS

			$username = urlencode("u_gearkartinfo"); 
			$msg_token = urlencode("JAM7a9"); 
		$sender_id = urlencode("ONKAKA"); // optional (compulsory in transactional sms) 
		$message = urlencode($_POST['sendSmsStatusMessage']); 
		$mobile = urlencode($_POST['mobile']); 

			// $user = "20082245";

			// $pass = "x66p4p";

			// $sender = "ONKAKA";

			// $phone = $mob;

			// $text = $text;

			// $priority = "dnd";

			// $stype = "0";

			// $msg_token = urlencode("JAM7a9");

		$data = "username=".$username."&msg_token=".$msg_token."&sender_id=".$sender_id."&message=".$message."&mobile=".$mobile;
		
		$ch = curl_init('http://103.255.100.34/api/send_transactional_sms.php?');


		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		echo $result = curl_exec($ch);

		$success = '<div class="alert alert-success">Message Sent</div>';

			//echo $success;

		curl_close($ch);
	}

	$data = array(

		'order_id' => $order_id,
		'status'   => $_POST['status'],
		'first_name' => $_POST['first_name'],
		'client_id'	=>$_POST['client_id']
	);

	$this->sendNotification($data);

}

public function ajaxPurchaseValueUpdate()
{
	$order_id = $_POST['order_id'];
	$data = array(

		'purchase_value' =>  $_POST['purchase_value']

	);

	$this->ModelOrder->purchaseValueUpdate($data,$order_id);
	echo 'Purchase Value Updated.';
}

public function ajaxTotalWTaxValueUpdate()
{
	$order_id = $_POST['order_id'];
	$data = array(

		'total_w_tax' =>  $_POST['total_w_tax']

	);

	$this->ModelOrder->totalWTaxValueUpdate($data,$order_id);
	echo 'Delivery Value Updated.';
} 

public function ajaxDeliveryChargeUpdate()
{
	$order_id = $_POST['order_id'];
	$data = array(

		'delivery_charge' =>  $_POST['delivery_charge']

	);

	$this->ModelOrder->deliveryChargeUpdate($data,$order_id);
	echo 'Delivery Value Updated.';
} 

public function ajaxDeliveryAgentUpdate()
{

	$order_id = $_POST['order_id'];
	$agentName = $_POST['delivery_agent'];

	if($this->ModelOrder->getAvailableAgentsByName($agentName))
	{
		$data = array(

			'delivery_agent' =>  $agentName

		);
		
		$this->ModelOrder->deliveryAgentUpdate($data,$order_id);
	}
	else
	{

		echo 'false';

	}
}

public function ajaxLocalityUpdate()
{

	$order_id = $_POST['order_id'];
	$data = array(

		'locality' =>  $_POST['locality']

	);

	$this->ModelOrder->localityUpdate($data,$order_id);
	echo 'Locality Updated.';
}

public function ajaxDeliveryTime2Update()
{
	$order_id = $_POST['order_id'];
	$data = array(

		'delivery_time2' =>  $_POST['delivery_time2']

	);

	$this->ModelOrder->deliveryTime2Update($data,$order_id);
	echo 'Delivery Agent Updated.';
}

public function ajaxCommentUpdate()
{
	$order_id = $_POST['order_id'];
	$data = array(

		'comment' =>  $_POST['comment']

	);

	$this->ModelOrder->commentUpdate($data,$order_id);
	echo 'Comment Updated.';
}

public function ajaxReminderUpdate()
{
	$order_id = $_POST['order_id'];
	
	if($_POST['reminder'] == '')
	{
		$data = array(

			'reminder' =>  NULL

		);

	}else{
	
		$data = array(

			'reminder' =>  date('Y-m-d '.$_POST['reminder'].':s')

		);

		
	}


	$this->ModelOrder->reminderUpdate($data,$order_id);
	echo 'Reminder Updated.';
}


public function ajaxOrder()
{	
	$from = $_POST['from'];

		// CHANGES BY FARHAN 6 JUNE 2018
	$deliveryAttendance = $this->ModelOrder->deliveryAttendance();

	if(sizeof($deliveryAttendance) > 0)
	{

		$boys_id = array_column($deliveryAttendance, 'id');
		$this->ModelOrder->updatedeliveryAttendance($boys_id);


	}

		$checkValues = $this->ModelOrder->checkValues($from); // CHECK RESTAURANT NAME IS PRESENT IN ORDERS

		if(count($checkValues) > 0) // IF YES THEN
		{

				$order_ids = array_column($checkValues, 'order_id'); // GET ORDDER IDS

				$merchantClient = $this->ModelOrder->getMerchantClient($order_ids); // GET MERCHANT CLIENT BY ORDER IDS

				$this->db->update_batch('mt_order_duplicate_farhan', $merchantClient, 'order_id'); 

				$data['getOrders'] = $this->ModelOrder->getOrder4($from); // GET ORDERS

			}else{	

				$data['getOrders'] = $this->ModelOrder->getOrder4($from);
			}

			$this->load->view('view_ajax_order',$data);

		// END BY FARHAN 6 JUNE 2018

}
		public function ajaxOrderWorkerA()
		{	
			$from = $_POST['from'];
			$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;


			$data['getOrders'] = $this->ModelOrder->getOrderA($from);

			if(sizeof($data['getOrders']) == 0 ){

				echo "NO Orders Yet.";

			}else{

				$last_id = $data['getOrders'][0]['order_id'];

				if($last_id > $first_order_id)
				{
					$this->decodeOrderItem($first_order_id);
				}

			}

			$this->load->view('view_ajax_orderA',$data);


		}
		public function ajaxOrderWorkerB()
		{	
			$from = $_POST['from'];
			$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;


			$data['getOrders'] = $this->ModelOrder->getOrderB($from);

			if(sizeof($data['getOrders']) == 0 ){

				echo "NO Orders Yet.";

			}else{

				$last_id = $data['getOrders'][0]['order_id'];

				if($last_id > $first_order_id)
				{
					$this->decodeOrderItem($first_order_id);
				}

			}

			$this->load->view('view_ajax_orderB',$data);


		}
		public function ajaxOrderByRange()
		{	
			$from = $_POST['from'];
			$to = $_POST['to'];
			$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
			$this->load->view('view_ajax_order',$data);
		}

		public function ajaxOrderByRangeWorkerA()
		{	
			$from = $_POST['from'];
			$to = $_POST['to'];
			$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
			$this->load->view('view_ajax_orderA',$data);
		}

		public function ajaxOrderByRangeWorkerB()
		{	
			$from = $_POST['from'];
			$to = $_POST['to'];
			$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
			$this->load->view('view_ajax_orderB',$data);
		}
	// public function reminderOrder()
	// {	
	// 	$date = $_POST['date'];

	// 	$data['getRemindOrders'] = $this->ModelOrder->getRemindOrder($date);
		
	// 	foreach($data['getRemindOrders'] as $reminderOrder)
	// 	{
	// 		echo $reminderOrder['order_id']."-";
	// 	}



	// 	//$this->load->view('view_ajax_order',$data);
	// }

		public function getReminder()
		{
			$data['order_id'] = $_POST['order_id'];
			$data['reminder'] = $_POST['reminder'];
			$data['mystatus'] =  $_POST['mystatus'];
			$data['type'] = 'reminder';
			$this->load->view('view_ajax_order_detail',$data);

		}


		public function getStatus()

		{

			$data['order_id'] = $_POST['order_id'];
			$data['status'] = $_POST['status'];
			$data['agentName'] = $_POST['agentName'];
			$data['mystatus'] =  $_POST['mystatus'];
			$data['type'] = 'status';
			$data['orderById'] = $this->ModelOrder->getOrderById2($data['order_id']);
			$this->load->view('view_ajax_order_detail',$data);

		}


	// CHANGES BY FARHAN 19 MAY 2018
		public function getPurchaseValue()
		{
			$data['order_id'] = $_POST['order_id'];
			$data['type'] = 'purchase_value';
		$data['itemPurchaseValue'] = $this->ModelOrder->getItemForPurchaseValue($data['order_id']); // CHANGES BY FARHAN
		$this->load->view('view_ajax_order_detail',$data);

	}
	// END BY FARHAN 19 MY 2018
	
	public function getDeliveryAgent()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['mystatus'] =  $_POST['mystatus'];
		$data['delivery_agent'] = $_POST['delivery_agent'];
		$data['type'] = 'delivery_agent';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getLocality()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['mystatus'] =  $_POST['mystatus'];
		// $data['locality'] = $_POST['locality'];
		$data['type'] = 'locality';
		$data['selected_locality'] = $_POST['locality'];
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getTotalWTax()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['mystatus'] =  $_POST['mystatus'];
		$data['total_w_tax'] = $_POST['total_w_tax'];
		$data['type'] = 'total_w_tax';
		$this->load->view('view_ajax_order_detail',$data);

	}
	
	public function getDeliveryCharge()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_charge'] = $_POST['delivery_charge'];
		$data['mystatus'] =  $_POST['mystatus'];
		$data['type'] = 'delivery_charge';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getDeliveryTime()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_time2'] = $_POST['delivery_time2'];
		$data['mystatus'] =  $_POST['mystatus'];
		$data['type'] = 'delivery_time2';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getComment()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['comment'] = $_POST['comment'];
		$data['mystatus'] =  $_POST['mystatus'];
		$data['type'] = 'comment';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getOrderById()
	{

		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'item';
			$data['orderById'] = $this->ModelOrder->getOrderByIdForItem($data['order_id']); // CHANGES BY FARHAN
		
			$this->load->view('view_ajax_order_detail',$data);

		}


		public function decodeOrderItem($first_order_id='')
		{

			$first_order_id = $first_order_id;

			$data['getOrders'] = $this->ModelOrder->getOrderForDecodeItem($first_order_id);

			foreach($data['getOrders'] as $getOrder)
			{	
				$order_id = $getOrder['order_id'];
			//$sizeOfItems = sizeof(json_decode($getOrder['json_details'],true));
				$Items = json_decode($getOrder['json_details'],true);
		// 	print_r($data['getOrders']);
		// die();
				$orderItem = [];

				foreach ($Items as $key => $item) {

				// $price = $item['price'];
				// $size = explode("|", $price);
				// $size = isset($size[1]) ? " (".$size[1].")" : " ";
					$orderItem[] = $item['qty']." - ".$this->ModelOrder->getItemById($item['item_id']);

				}

				$totalOrder = implode(",", $orderItem);
				$data = array(

					"decode_item" => $totalOrder

				);
				$this->ModelOrder->updateOrderItem($order_id,$data);

			}
			

		}


		public function ajaxDecodeItemUpdate()
		{
			$order_id = $_POST['order_id'];

			$data = array(

				'decode_item' =>  $_POST['decode_item']

			);

			$this->ModelOrder->decodeItemUpdate($data,$order_id);
		//echo 'Decode Item Updated.';
		}

		public function update_available_agents()
		{
			$id = $_POST['id'];

			$data = array(

				"name" => trim($_POST['agentName'])

			);


			$this->ModelOrder->agentUpdate($data,$id);
		}

	// UPDATE DELIVERY BOYS STATUS

		public function update_available_agents_status()
		{
			$id = $_POST['id'];

			$status = $_POST['status'];

			if($status == 1)
			{
				$statusValue = 0;

			}else{

				$statusValue = 1;
			}

			$data = array(

				"status" => trim($statusValue)

			);

			$this->ModelOrder->agentStatusUpdate($data,$id);


		}


		public function getOrderByIdInSms()

		{ 

			$data['order_id'] = $_POST['order_id'];
			$data['mystatus'] = $_POST['mystatus'];
			$data['type'] = 'sms';
			$data['orderById'] = $this->ModelOrder->getOrderById2($data['order_id']);

			$password = $this->session->userdata('password');
			if ($password == "black999star!")
				$data['worker_flag'] = "admin";
			elseif ($password == "victor999")
				$data['worker_flag'] = "A";
			elseif ($password == "charge777") 
				$data['worker_flag'] = "B";
			$this->load->view('view_ajax_order_detail',$data);

		}

	// SEND SMS TO DELIVERY BOYS

		public function smsApi()
		{

			$order_id = $_POST['order_id'];


			// AFTER SEND SMS INSERT DELIVERY BOYS NAME TO AGENT COLUMN

			$agentName = $_POST['agentName'];

			if($this->ModelOrder->getAvailableAgentsByName($agentName))
			{
				
				$data = array(

					'delivery_agent' =>  $agentName

				);
				
				$this->ModelOrder->deliveryAgentUpdate($data,$order_id);
			}
			else
			{

				echo 'false';
				exit();
			}

			// AFTER SEND SMS CHANGE STATUS TO PROCESSING

			$data = array(

				'status' =>  "Processing"

			);

			$this->ModelOrder->statusUpdate($data,$order_id);


			// SEND SMS TO DELIVERY BOYS

			$username = urlencode("u_gearkartinfo"); 
			$msg_token = urlencode("JAM7a9"); 
		$sender_id = urlencode("ONKAKA"); // optional (compulsory in transactional sms) 
		$message = urlencode($_POST['smsText']); 
		$mobile = urlencode($_POST['mobile']); 


		$data = "username=".$username."&msg_token=".$msg_token."&sender_id=".$sender_id."&message=".$message."&mobile=".$mobile;
		
		$ch = curl_init('http://103.255.100.34/api/send_transactional_sms.php?');

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);

		$success = '<div class="alert alert-success">Message Sent</div>';

			//echo $success;

		curl_close($ch);

		
	}


	public function export($cdate='')
	{
		$date = explode("%7C", $cdate);

		$from = $date[0];
		$to = $date[1];
		
		
		$getOrders = $this->ModelOrder->getOrder($from, $to);
// 		echo "<pre>";
// 		print_r($getOrders);
// 		echo "</pre>";
// 		die();

		foreach($getOrders as $getOrder) {
			// echo "<pre>";
			// print_r($this->ModelOrder->getClientById($getOrder['client_id']));
			// echo "</pre>";

			$time = strtotime($getOrder['date_created']);
			$con = date("g:i", $time);
			

			$data[] =array(


				"REF" 			 => $getOrder['order_id'],
				"MOP" 			 => strtoupper($getOrder['payment_type']),
				"STATUS" 		 => strtoupper($getOrder['status']),
				"NAME"			 => strtoupper($getOrder['first_name']." ".$getOrder['last_name']),
					"EMAIL"          => $getOrder['email_address'],	//ADDED BY FARHAN
					"MOBILE"  		 => isset($getOrder['contact_phone']) ? $getOrder['contact_phone'] : " ",
					"CON"			 => $con,
					"DEL"			 => $getOrder['delivery_time2'],
					"RESTAURANT"     => strtoupper($getOrder['restaurant_name']),
					"LOCALITY"		 => strtoupper($getOrder['locality']),
					"AGENT"			 => strtoupper($getOrder['delivery_agent']),
					"PV" 			 => $getOrder['purchase_value'],
					"DV"			 => $getOrder['total_w_tax'],
					"DF"			 => $getOrder['delivery_charge'],
					"deliveryTax"			 => $getOrder['delivery_charge']*$getOrder['delivery_tax'],
					"PROFIT"		 =>	$getOrder['total_w_tax'] - $getOrder['purchase_value'],
					"CHANGES"		 => $getOrder['comment']



				);

		}

		$this->db->insert_batch('mt_farhan_excel',$data);

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		$this->ModelOrder->export($cdate);

	}
	
	public function availableAgents()
	{
		$data['id'] = $_POST['id'];
		$data['agentName'] = $_POST['agentName'];
		$data['val'] = 'availabel_agents';


		$this->load->view('view_ajax_agent',$data);
	}

	public function updateDeliveryAgent()
	{
		$id = $_POST['id'];
		$data = array(

			'name' =>  $_POST['agentName']

		);
		
		$this->ModelOrder->updateDeliveryAgent($data,$id);
		echo 'Delivery Value Updated.';
	}

	// CODE ADDED BY FARHAN

	//FOR PASSWORD PROTECTION
	public function checkPassword()
	{
		$password = $_POST['password'];

		if($password == 'sam123!')
		{
			echo true;
			
		}else{

			echo false;
		}
	}

	// FOR FILTERS

	public function allFilters()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getFilteredLists'] = $this->ModelOrder->getFilteredListByDeliveryDate($from,$to);
		$this->load->view("view_ajax_filters",$data);
	}


	public function getSearhedData()
	{
		
		$from = $_POST['from'];
		$to = $_POST['to'];
		$values = $_POST['values'];

		$values = array_map('current', $values);

		$type = $_POST['type'];	

		$data['getOrders'] = $this->ModelOrder->getOrder2($from,$values,$type,$to);


		$this->load->view('view_ajax_order',$data);
	}


	public function getSearhedData2()
	{
		
		$from = $_POST['from'];
		$to = $_POST['to'];
		$values = $_POST['values'];

		//$res = call_user_func_array('array_merge', $values);

		$merchant = array_column($values, 'merchant');
		$status = array_column($values, 'status');
		$locality = array_column($values, 'locality');
		$payment_type = array_column($values, 'payment_type');
		$agent = array_column($values, 'agent');


		$type = $_POST['type'];	

		$data['getOrders'] = $this->ModelOrder->getOrder3($from,$to,$merchant,$status,$locality,$payment_type,$agent);


		$this->load->view('view_ajax_order',$data);
	}


	// FOR STOP REFRESH
	public function setSession()
	{
		$getSession = $this->session->userdata('refresh');

		if(!isset($getSession) || $getSession == 0)
		{
			$this->session->set_userdata('refresh',1);
			$refresh = $this->session->userdata('refresh');

		}else if($getSession == 1){

			$this->session->set_userdata('refresh',0);
			$refresh = $this->session->userdata('refresh');

		}

	}


	// END BY FARHAN
	
	public function updateClientStreet()
	{

		$data = array(

			"street" => $_POST['clientStreet']

		);
		$order_id = $_POST['order_id'];
		$this->ModelOrder->updateClientStreet($order_id,$data);

	}
	
	
	// ADDED BY FARHAN 29/04/2018
	public function guestEmail()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'guestCheckout';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);
		$this->load->view('view_ajax_order_detail',$data);
	}


	public function sendGuestEmail()
	{
		
		
		$this->load->library('email');
		$to = $_POST['guestEmail'];
		$subject = $_POST['guestSubject']; 
		$message = $_POST['guestMessage'];     
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->to($to);
		$this->email->from('Order@onlinekaka.com','Onlinekaka');
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();

		$data = array(

			"email_address" => trim($to)
		);

		$client_id = $_POST['client_id'];
		$order_id  = $_POST['order_id'];
		$this->ModelOrder->updateClientEmail($client_id,$data);
		$this->ModelOrder->updateSentEmail($order_id);
	}
	// END BY FARHAN 29/04/2018
	
	// ADDED BY FARHAN 18 JUNE 2018

	public function suggestedAgents()
	{	
		
		$data['order_id'] = $_POST['order_id'];

		$data['orderById'] = $this->ModelOrder->getOrderById2($data['order_id']);

		$data['type'] = 'suggestedAgents';

		$lat = $_POST['lat'];
		$lng = $_POST['lng'];

		$data['assign_time'] =$this->ModelOrder->assign_time();

		$data['suggestedAgents'] = $this->ModelOrder->getSuggestedAgents($lat,$lng);


		// $latLongs = array_column($data['suggestedAgents'],'longi','lati');

		$filterLatLongs = '';
		foreach($data['suggestedAgents'] as $suggestedAgents)
		{
			$filterLatLongs.= $suggestedAgents['lati'].','.$suggestedAgents['longi'].'|';
		}
		// echo $filterLatLongs;
		// die();


		// $filterLatLongs = implode('|', array_map(
		//     function ($v, $k) { return sprintf("%s,%s", $k, $v); },
		//     $latLongs,
		//     array_keys($latLongs)
		// ));


		$data['distancesTimes'] = file_get_contents_curl('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$lat.','.$lng.'&destinations='.$filterLatLongs.'&mode=driving&key=AIzaSyAPlDPWYkikUFdbFr0aZLx0DP1q16_MA68');
		$this->load->view('view_ajax_order_detail',$data);
	}

	// END BY FARHAN 18 JUNE 2018

	public function test($mob,$text)
	{
					// SEND SMS TO DELIVERY BOYS

		$username = urlencode("u_gearkartinfo"); 
		$msg_token = urlencode("JAM7a9"); 
		$sender_id = urlencode("ONKAKA"); // optional (compulsory in transactional sms) 
		$message = urlencode($text); 
		$mobile = urlencode($mob); 

			// $user = "20082245";

			// $pass = "x66p4p";

			// $sender = "ONKAKA";

			// $phone = $mob;

			// $text = $text;

			// $priority = "dnd";

			// $stype = "0";

			// $msg_token = urlencode("JAM7a9");

		$data = "username=".$username."&msg_token=".$msg_token."&sender_id=".$sender_id."&message=".$message."&mobile=".$mobile;
		
		$ch = curl_init('http://103.255.100.34/api/send_transactional_sms.php?');

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);

		$success = '<div class="alert alert-success">Message Sent</div>';

			//echo $success;

		curl_close($ch);

	}

	public function getMerchantId()
	{
		$options = $this->ModelOrder->getMerchantId();

		foreach($options as $option)
		{
			$mer_id = $option->merchant_id;
			$checkMerchantId = $this->ModelOrder->checkMerchantId($mer_id);

			if(count($checkMerchantId) == 0)
			{	


				$data = array(
					array(

						'merchant_id' => $mer_id,
						'option_name' => 'enabled_paypal',
						'option_value' => 'yes',
					
					),
				   array(

						'merchant_id' => $mer_id,
						'option_name' => 'paypal_mode',
						'option_value' => 'live',
					
					),
				   array(

						'merchant_id' => $mer_id,
						'option_name' => 'live_paypal_user',
						'option_value' => 'bilal_api1.onlinekaka.com',
					
					),
				   array(

						'merchant_id' => $mer_id,
						'option_name' => 'live_paypal_pass',
						'option_value' => 'F2U7PN4AGZ6UHQMB',
					
					),

				   array(

						'merchant_id' => $mer_id,
						'option_name' => 'live_paypal_signature',
						'option_value' => 'A21zB30vjel29DwiFoqVwmS2dbupAp0ie77N1X8Y8zRsaZwf-G2TLkQy',
					
					)
				);

				$this->db->insert_batch('mt_option',$data);

			}
		}

		// print_r($data);
		

	}


	public function onHoldOrders()
	{
		$from = $_POST['from'];
		$data['mystatus'] = isset($_POST['mystatus']) ? $_POST['mystatus'] : '';
		$data['getOrders'] = $this->ModelOrder->getonholdOrders($from);
		

		$this->load->view('view_ajax_order',$data);
	}
	
	public function getDelayedOrders()
	{
		$from = $_POST['from'];
		$data['mystatus'] = isset($_POST['mystatus']) ? $_POST['mystatus'] : '';
	    $data['getOrders'] = array_merge($this->ModelOrder->getDelayedOrders($from,$data['mystatus']),$this->ModelOrder->getDelayedOrdersOnHold($from,$data['mystatus']));
	    print_r($data['getOrders']);
	    die();
	  	$this->load->view('view_ajax_order',$data);
	}

	public function packing()
	{
		$order_id = $_POST['order_id'];

		if($_POST['packingValue'] == 0){

			
			$data = array(

				"packing" => 1

			);			
		}else{



			$data = array(

				"packing" => 0

			);	

		}

		
		$this->ModelOrder->updatePacking($order_id,$data);
	}

	public function removeProduct()
	{

		$item_id = $_POST['item_id'];
		$order_id = $_POST['order_id'];

		foreach($_SESSION["shopping_cart"] as $keys => $values)
			{
					if($values["item_id"] == $item_id)
					{
						unset($_SESSION['shopping_cart'][$keys]['qty']);
					}
			}
		shoppingCart($order_id);
		cartCal();
		
	}


	
	public function addItem()
	{	
		unset($_SESSION['shopping_cart']);
		unset($_SESSION['cartDelivery']);
		unset($_SESSION['finalTotal']);

		$data['order_id'] = $_POST['order_id'];
		$merchant_id = $_POST['merchant_id'];
		$data['type'] = 'addItem';

		

		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://www.speedypixelgame.com/kakatesting/store/getCategory");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "merchant_id=".$merchant_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		curl_close ($ch);

		$data['categories'] = json_decode($server_output,true);

			
		// echo "<pre>";
		// print_r($data['categories']);
		// die();

		$data['order'] = $this->ModelOrder->jsonDetails($data['order_id']);
	
		$json = json_decode($data['order']->json_details);
		
		
		

		foreach(json_decode($data['order']->json_details) as $keys => $values)
					{
							
	
							if(!empty($values->sub_item))
							{
								$sub_item = $values->sub_item;
								$addon_ids = $values->addon_ids;

							}else{

								$sub_item = "";
								$addon_ids = "";
							}



							$item_array = array(
								'qty'			=>	$values->qty,
								'item_id'		=>	$values->item_id,
								'price'			=>	$values->price,
								"discount"		=> $values->discount,
								"non_taxable"	=> $values->non_taxable,
								"sub_item"		=> $sub_item,
								"addon_ids"	=> $addon_ids
								
							);
							$_SESSION["shopping_cart"][] = $item_array;
					}
	
		$cartDelivery = array(

			
			"delivery_charge" => round($data['order']->delivery_charge),
			"delivery_tax" => round($data['order']->delivery_tax*100)/100 * round($data['order']->delivery_charge),
			"merchant_id" => $merchant_id

		);


		$_SESSION['cartDelivery'] = $cartDelivery;

		
 		$this->load->view('view_ajax_order_detail',$data);
	}

	public function addItemById()
	{	

		$price = $_POST['price'];
		
		$item_id = $_POST['item_id'];
		$order_id = $_POST['order_id'];

		$addon_item = $this->ModelOrder->getItemById2($item_id);

		$merchant_id = $addon_item->merchant_id;

		$discount = $addon_item->discount;
		$non_taxable = $addon_item->non_taxable;
		
		$sub_item = "";
	
		


		if(isset($_SESSION["shopping_cart"]))
		{
			$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
			
			
			if(!in_array($item_id, $item_array_id))
			{
					

					$item_array = array(
							
							"item_id"		=>	$item_id,
							"row"			=> "",
							"merchant_id"	=> $merchant_id,
							"discount"		=>  $discount,
							"currentController" => "store",
							"price"			=>	$price,
							"qty"			=>	1,
							"notes"			=> "",
							"sub_item"		=> "",
							"addon_ids"		=> "",
							"non_taxable"	=> $non_taxable,
						
						);
						
					$_SESSION["shopping_cart"][] = $item_array;
				
				

			}
			else
			{


				foreach($_SESSION["shopping_cart"] as $key => $cart)
				{	
					if($item_id == $cart['item_id']){
						


						$_SESSION['shopping_cart'][$key]['qty']++;
					}
				}

				
			}
		}
		else
		{
			$item_array = array(
							
							"item_id"		=>	$item_id,
							"row"			=> "",
							"merchant_id"	=> $merchant_id,
							"discount"		=>  $discount,
							"currentController" => "store",
							"price"			=>	$price,
							"qty"			=>	1,
							"notes"			=> "",
							"sub_item"		=> "",
							"addon_ids"		=> "",
							"non_taxable"	=> $non_taxable,
								

					
				);

			$_SESSION["shopping_cart"][] = $item_array;
		}


		 shoppingCart($order_id);
		 cartCal();
	}

	

	public function deleteCartProduct()
	{	
		$item_id = $_POST['item_id'];
		$order_id = $_POST['order_id'];
		$price = $_POST['price'];
		$json_details = $this->ModelOrder->jsonDetails($order_id);
		$addon = $_POST['addon_ids'];
		if(!empty($addon))
		{

			$addon_ids = explode("-",$_POST['addon_ids']);
		}
		
		

		foreach($_SESSION["shopping_cart"] as $keys => $values)
		{	
			$price2 = str_replace('"', '', $values['price']);
			if($values["item_id"] == $item_id && $price == $price2)
			{	
				
				if(empty($addon))
				{
					unset($_SESSION["shopping_cart"][$keys]);
					

				}else{

					$addonsDifference = array_diff($addon_ids, $values['addon_ids']);

					$addonsDifference = array_diff($addon_ids, $values['addon_ids']);

					if($addon_ids ===  $values['addon_ids']){
					
					  unset($_SESSION["shopping_cart"][$keys]);

					}
				} 
				
				
			}
		}

		shoppingCart($order_id,'addon');
		cartCal();
	}

	public function clearCart()
	{
		unset($_SESSION['shopping_cart']);

	}


	public function addToOrder()
	{

		if(count($_SESSION['shopping_cart']) < 1)
		{
			echo 'false';
			die();
		}

		$order_id = $_POST['order_id'];
		$client_id = $_POST['client_id'];
		// $checkJsonDetails = $this->ModelOrder->getOrderById($order_id);


		if(isset($_SESSION["shopping_cart"])){

			foreach($_SESSION["shopping_cart"] as $keys => $values)
			{
				$json_details[] = array(

					"currentController" => "store",
					"merchant_id" => $_SESSION['cartDelivery']['merchant_id'],
					"item_id"	  => $values['item_id'],
					"price"	  => $values['price'],
					"qty"	  => $values['qty'],
					"discount"	  => $values['discount'],
					"notes"	  => "",
					"row"	  => "",
					"two_flavors"	  => "",
					"non_taxable"	  => $values['non_taxable'],
					"sub_item"	  => $values['sub_item'],
					"addon_ids"	  => $values['addon_ids'],

				);


				if(!empty($values['addon_ids'])){
					$addons = [];
					foreach($values['addon_ids'] as $addon_id)
					{	
						$sub_item_array =  (array) $this->ModelOrder->subcategoryItemName($addon_id);
						$sub_item_cat = $this->ModelOrder->subcategoryName2($sub_item_array['category'])->subcategory_name;

						$addons[] = [

							"addon_name" => 	$sub_item_array['sub_item_name'],
							"addon_category" => $sub_item_cat,
							"addon_qty" =>	$values['qty'],
							"addon_price" => !empty($sub_item_cat['price']) ? $sub_item_cat['price'] : false

						];
					}
				// 	echo "<pre>";
				// print_r($addons);
				// die();
					$addons_json = json_encode($addons);

				}else{

					$addons_json = "";
				}



				$price = explode("|",$values['price']);

				$orderDetails[] = [

					"order_id" 		=> $order_id,
					"client_id"		=> $client_id,
					"item_id"	  => $values['item_id'],
					"item_name"	  => $this->ModelOrder->getItemById2($values['item_id'])->item_name,
					"order_notes" => "",
					"normal_price" => $price[0],
					"discounted_price" =>  $price[0],
					"size" => isset($price[1]) ? $price[1] : "",
					"qty"  => $values['qty'],
					"cooking_ref" => "",
					"addon"			=> $addons_json,
					"add_on_amount"  => 0.00,
					"ingredients"	=> "",
					"non_taxable"   => $values['non_taxable']
				];
			}

			// echo "<pre>";
			// print_r($orderDetails);
			// die();

			$data = array(

				"json_details" => json_encode($json_details),
				"sub_total"	   => $_SESSION['finalTotal']['sub_total'],
				"taxable_total" => $_SESSION['finalTotal']['taxable_total'],
				"total_w_tax"   => $_SESSION['finalTotal']['total_w_tax'],
				"status"		=> 'pending',
				"packaging"		=> $_SESSION['finalTotal']['packaging'],
				"is_edited"		=> 1
			);

			$this->ModelOrder->updateDetailsMtOrder($order_id,$data);

			$data2 = array(

				"json_details" => json_encode($json_details),
				"sub_total"	   => $_SESSION['finalTotal']['sub_total'],
				"taxable_total" => $_SESSION['finalTotal']['taxable_total'],
				"total_w_tax"   => $_SESSION['finalTotal']['total_w_tax'],
				"packaging"		=> $_SESSION['finalTotal']['packaging'],
				"status"		=> 'pending',
				"is_edited"		=> 1,
				"rechecked"		=> 1
			);
			
			
			$row = $this->ModelOrder->getOrderById2($order_id);

			if($row->order_history == null)
			{
				$json[] = array(

					"time_stamp" => date('Y-m-d h:i:s'),
					"user_id" => $this->session->userdata('order_user')->username,
					"total_w_tax" => $row->total_w_tax,
					"json_details" => json_decode($row->json_details,true)
				);

				$order_history = ["order_history" => json_encode($json)];
			
			}else{

				$json[] = array(

					"time_stamp" => date('Y-m-d h:i:s'),
					"user_id" => $this->session->userdata('order_user')->username,
					"total_w_tax" => $row->total_w_tax,
					"json_details" => json_decode($row->json_details,true)
				);
			
				$previousOrders = json_decode($row->order_history,true);
				$order_history = ["order_history" => json_encode(array_merge($json,$previousOrders))];
				
			}
			
		
			$data = array_merge($data2,$order_history);
			
			$this->ModelOrder->updateDetails($order_id,$data);
			
			$this->ModelOrder->deleteOrderDetails($order_id);
		

			$this->db->insert_batch('mt_order_details',$orderDetails);

			echo "Your Cart has been updated successfully";
			unset($_SESSION['shopping_cart']);
			unset($_SESSION['cartDelivery']);
			unset($_SESSION['finalTotal']);

		}else{


			echo "Something went wrong.";
		}

	}


		public function insertBatchOption()
	{
		$options = $this->ModelOrder->getMerchantId();

		foreach($options as $option)
		{
			$mer_id = $option->merchant_id;
			$checkMerchantId = $this->ModelOrder->checkMerchantIdOption($mer_id);
			
		

			if(count($checkMerchantId) == 0)
			{	


				$data[] = array(
					array(

						'merchant_id' => $mer_id,
						'option_name' => 'merchant_paytm_enabled',
						'option_value' => 'yes',
					
					)
				);

				

			}
		}

		$this->db->insert_batch('mt_option',array_column($data, 0));
	
		

	}

	public function updateMerchantTaxCharges()
	{
		$this->ModelOrder->updateMerchantTaxCharges();
	}

	public function sendNotification($data)
	{	
		$client_id = $data['client_id'];

		$firebase_ids = $this->ModelOrder->getFireBaseId($client_id);

		$firebase_ids = array_column($firebase_ids,"firebase_id");

		if(sizeof($firebase_ids) > 0)
		{

			$url = 'https://finalcheck.speedypixelgame.com/serviceclient/callback/sendNotifications';

			$title = "Order Status";

			$body = "Dear ".$data['first_name'].", Your order #".$data['order_id']." is ".$data['status'];

			$fields = array(
			            'title' => $title,
			            'body' => $body,
			            'fire_base_ids' => $firebase_ids,
			            'redirect_to'	=> '',
			            'id'			=> ''
			        );

			//url-ify the data for the POST
			$fields_string = http_build_query($fields);

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

			//execute post
			$result = curl_exec($ch);


			//close connection
			curl_close($ch);

		}

	}



	public function updateDistance()
	{
		


	   $this->ModelOrder->updateDistance();


		// print_r($data);
		

	}

		public function updateDistanceType()
	{
		


	   $this->ModelOrder->updateDistanceType();


		// print_r($data);
		

	}
	
	public function updateOption()
	{
		$option_name  = $_POST['option_name'];
		$option_value = $_POST['option_value'];
		updateOption($option_name,$option_value);

	}

	public function refresh()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$order_id = $_POST['order_id'];
		$data['getOrders'] = $this->ModelOrder->refresh($order_id,$from,$to);
		$this->load->view('view_ajax_order',$data);
	}



	public function addon()
	{
		$data['item_id'] = $_POST['item_id'];
		$data['merchant_id'] = $_POST['merchant_id'];
		$data['order_id'] = $_POST['order_id'];
		$data['addons'] = $this->ModelOrder->getItemById2($data['item_id']);
		
		$this->load->view('view_ajax_addon',$data);
	}

	public function addOndata()
	{	
		
	

		$searcharray = array();
		parse_str($_POST['data'], $searcharray);

		

		$addon_item = $this->ModelOrder->getItemById2($searcharray['item_id']);
		

		$discount = $addon_item->discount;
		$non_taxable = $addon_item->non_taxable;
		foreach(json_decode($addon_item->price,true) as $key => $price2)
			{	
				$size = $this->ModelOrder->sizeName($key);
				$aaa[] = $price2."|".$size;
			}
			// $item_array = array(
			// 		"item_id"		=>	$searcharray['item_id'],
			// 		"row"			=> "",
			// 		"merchant_id"	=> $searcharray['merchant_id'],
			// 		"discount"		=>  $discount,
			// 		"currentController" => "store",
			// 		"price"			=>	$searcharray['price'],
			// 		"qty"			=>	$searcharray['qty'],
			// 		"notes"			=> "",
			// 		"sub_item"		=> isset($searcharray['sub_item']) ? $searcharray['sub_item'] : '',
			// 		"non_taxable"	=> $non_taxable,
				
			// 		"addon_ids"		=> isset($_POST['addonIDs']) ? array_unique($_POST['addonIDs']) : '',

					
			// 	);

		 
		if(isset($_SESSION["shopping_cart"]))
		{
			$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
			$item_array_price = array_column($_SESSION["shopping_cart"], "price");
			
			if(!in_array($searcharray['item_id'], $item_array_id))
			{
					

					$item_array = array(
							"item_id"		=>	$searcharray['item_id'],
												"row"			=> "",
												"merchant_id"	=> $searcharray['merchant_id'],
												"discount"		=>  $discount,
												"currentController" => "store",
												"price"			=>	$searcharray['price'],
												"qty"			=>	$searcharray['qty'],
												"notes"			=> "",
												"sub_item"		=> isset($searcharray['sub_item']) ? $searcharray['sub_item'] : '',
												"non_taxable"	=> $non_taxable,
											
												"addon_ids"		=> isset($_POST['addonIDs']) ? array_unique($_POST['addonIDs']) : '',
						
						);
						
					$_SESSION["shopping_cart"][] = $item_array;
				
				

			}elseif (in_array($searcharray['price'], $aaa) && !in_array($searcharray['price'], $item_array_price) ) {
				
				
				$item_array = array(
							"item_id"		=>	$searcharray['item_id'],
					"row"			=> "",
					"merchant_id"	=> $searcharray['merchant_id'],
					"discount"		=>  $discount,
					"currentController" => "store",
					"price"			=>	$searcharray['price'],
					"qty"			=>	$searcharray['qty'],
					"notes"			=> "",
					"sub_item"		=> isset($searcharray['sub_item']) ? $searcharray['sub_item'] : '',
					"non_taxable"	=> $non_taxable,
				
					"addon_ids"		=> isset($_POST['addonIDs']) ? array_unique($_POST['addonIDs']) : '',
						
						);
						
					$_SESSION["shopping_cart"][] = $item_array;
			}
			else
			{



					foreach($_SESSION["shopping_cart"] as $key => $cart)
					{	
						if($searcharray['item_id'] == $cart['item_id'] && $cart['price'] == $searcharray['price']){
							
				

							if(isset($_POST['addonIDs']))
							{
								if($_POST['addonIDs'] ===  $cart['addon_ids']){
						
								   $_SESSION['shopping_cart'][$key]['qty'] = $_SESSION['shopping_cart'][$key]['qty']+ $searcharray['qty'];
								   $flag = true;
								   break;

								} 
								
							}else{

								$_SESSION['shopping_cart'][$key]['qty'] = $_SESSION['shopping_cart'][$key]['qty']+ $searcharray['qty'];
								$flag = true;
								 break;
							}
							

							
						}
					}

					if(!isset($flag))
					{	

								   $item_array = array(
								   					"item_id"		=>	$searcharray['item_id'],
								   					"row"			=> "",
								   					"merchant_id"	=> $searcharray['merchant_id'],
								   					"discount"		=>  $discount,
								   					"currentController" => "store",
								   					"price"			=>	$searcharray['price'],
								   					"qty"			=>	$searcharray['qty'],
								   					"notes"			=> "",
								   					"sub_item"		=> isset($searcharray['sub_item']) ? $searcharray['sub_item'] : '',
								   					"non_taxable"	=> $non_taxable,
								   				
								   					"addon_ids"		=> isset($_POST['addonIDs']) ? array_unique($_POST['addonIDs']) : ''
								   					
								   				);

								   			$_SESSION["shopping_cart"][] = $item_array;						
								   			 unset($flag);
					
					}

				
			}
		}
		else
		{
			$item_array = array(
					"item_id"		=>	$searcharray['item_id'],
					"row"			=> "",
					"merchant_id"	=> $searcharray['merchant_id'],
					"discount"		=>  $discount,
					"currentController" => "store",
					"price"			=>	$searcharray['price'],
					"qty"			=>	$searcharray['qty'],
					"notes"			=> "",
					"sub_item"		=>	isset($searcharray['sub_item']) ? $searcharray['sub_item'] : '',
					"non_taxable"	=> $non_taxable,
				
					"addon_ids"		=> isset($_POST['addonIDs']) ? array_unique($_POST['addonIDs']) : ''
					
				);

			$_SESSION["shopping_cart"][] = $item_array;
		}

		 shoppingCart($searcharray['order_id'], 'addon');
		 cartCal();
	}
	
	// 7 december 2018

	public function orderHistory()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'orderHistory';
		$data['orderHistory'] = $this->ModelOrder->getOrderById2($data['order_id'])->order_history;
		
		$this->load->view('view_ajax_order_detail',$data);
	}

	public function updateRechecked()
	{
		$order_id = $_POST['order_id'];

		$data = ['rechecked' => 0];

		$this->ModelOrder->updateDetails($order_id,$data);
	}

	
}