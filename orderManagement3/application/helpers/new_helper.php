<?php 



function allocateColor($status)
{
	switch ($status) {
		case 'Pending':
			return 'background-color: white';
			break;
		case 'Confirmed':
			return 'background-color: #00E5EE';
			break;
		case 'Processing':
			return 'background-color: yellow';
			break;
		case 'Delivered':
			return 'background-color: #00ff00';
			break;
		case 'Cancelled':
			return 'background-color: #ff211d';
			break;
		case 'On Hold':
			return 'background-color: pink';
			break;
		case 'Packed':
			return 'background-color: #cccecd';
			break;
		case 1:
			return 'background-color: #d60dd6';
			break;
			
	
	}
}



function deliveryBoysColor($no_of_orders)
{
	switch ($no_of_orders) {
		case '0':
			return '#00ff00';
			break;
		case '1':
			return '#FFFF33';
			break;
		case '2':
			return '#FF6600';
			break;
		case '3':
			return '#FF0000';
			break;
	
	}
}


function uniqueAssocArray($array, $uniqueKey) {
  if (!is_array($array)) {
    return array();
  }
  $uniqueKeys = array();
  foreach ($array as $key => $item) {
    if (!in_array($item[$uniqueKey], $uniqueKeys)) {
      $uniqueKeys[$item[$uniqueKey]] = $item;
    }
  }
  return $uniqueKeys;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

// ADDED BY FARHAN 15 JUNE 2018
// Sort Multi-dimensional Array by Value

function cmp($a, $b)
{
    if ($a["distance"] == $b["distance"]) {
        return 0;
    }
    return ($a["distance"] < $b["distance"]) ? -1 : 1;
}


function file_get_contents_curl($url) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);

    return $response_a;

}

// END BY FARHAN 18 JUNE 2018

function shoppingCart($order_id,$addon ='')
{
	$CI = get_instance();
	$CI->load->model('ModelOrder');
	
	?>
		<?php foreach($_SESSION["shopping_cart"] as $keys => $values) {?>
	<table class="table table-striped table-hover table-condensed" style="margin-bottom: 1px;">
		
		<tbody>
			<tr>
				<td><?php echo $values['qty']; ?></td>
				<td><?php echo $CI->ModelOrder->getItemById($values['item_id'],@$values['size']); ?></td>
				<td>₹ <?php echo $values['price']; ?></td>
				<td>
					<?php 
					if(!empty($addon))
					{
						$price = str_replace('"', '', $values['price']);
					}else{

						$price = str_replace('"', '', $values['price']);
					}

					 ?>
					<a href="javascript:void(0)" onclick="deleteCartProduct('<?php echo $values['item_id']; ?>','<?php echo $order_id; ?>','<?php echo $price; ?>',`<?php echo !empty($values['addon_ids']) ? implode("-",$values["addon_ids"]) : ""; ?>`)">Remove</a></td>

			</tr>
		
		</tbody>
	</table>
		<div>
			<?php 

										if(!empty($values['sub_item']))
											{
											foreach($values['sub_item'] as $key => $sub_item)
											{
												echo '<span style="margin-left:25px;"><b>'.trim($CI->ModelOrder->getSubCategory($key),'"').'</b></span>';
												echo '<ul style="margin-left:35px;">';
												foreach($sub_item as $sub)
												{
													$subArray = explode("|",$sub);

													$rupees = !empty($subArray[1]) ? " Rs.".$subArray[1] : "";

													echo '<li>'.$CI->ModelOrder->getSubCategoryItem($subArray[0]).$rupees.'</li>';

												}
												echo '</ul>';
											}
										} 

									 ?>
		</div>
		<?php } ?>
	<?php
}

function cartCal()
{	
	$CI = get_instance();
	$CI->load->model('ModelOrder');
	$sub_total = 0;
	$qty = 0;
	
	foreach($_SESSION['shopping_cart'] as $value)
	{	


		$sub_total += $value['qty']*$value['price'];

		if(!empty($value['sub_item']))
		{
		foreach($value['sub_item'] as $sub_item)
		{
			foreach($sub_item as $sub)
			{
				$subArray = explode("|",$sub);

				$sub_total += $value['qty']*$subArray[1];

			}
		}
	}

		$qty += $value['qty'];

	}

	$merchant_id =  $_SESSION['cartDelivery']['merchant_id'];

	$deliveryCharges =  $_SESSION['cartDelivery']['delivery_charge'];

	$packagingIncremental = $CI->ModelOrder->getOptionPackagingIncremental($merchant_id);

	$packagingCharge = $CI->ModelOrder->getOptionPackagingCharge($merchant_id);

	$checkDeliveryTax = $CI->ModelOrder->getOptionDeliveryChargesTax($merchant_id);

	$merchant_tax = $CI->ModelOrder->getOptionMerchantTax($merchant_id);

	$DeliveryTax = $CI->ModelOrder->getOptionDeliveryTax($merchant_id);

	if($packagingIncremental->option_value == '')
	{
		$packaging = $packagingCharge->option_value;

	}else{

		$packaging = $qty * $packagingCharge->option_value;
	}

	if($checkDeliveryTax->option_value == '')
	{	
		$deliveryTaxPercent = $deliveryCharges*$DeliveryTax->option_value/100;
		$taxable_total = ($deliveryCharges)*$DeliveryTax->option_value/100;
	}

	if($merchant_tax->option_value != '')
	{	
		$deliveryTaxPercent = 0;
		$taxable_total = ($sub_total + $packaging) * $merchant_tax->option_value/100;
	}	

	if($checkDeliveryTax->option_value == '' && $merchant_tax->option_value != '')
		{
			$deliveryTaxPercent = $deliveryCharges*$DeliveryTax->option_value/100;
			$taxable_total = ($sub_total+$packaging)*$merchant_tax->option_value/100 + $deliveryTaxPercent;
			$total_w_tax = $sub_total + $deliveryCharges + $packaging + $taxable_total;
			
		}

	$taxable_total = isset($taxable_total) ? $taxable_total : 0;

	$total_w_tax = $sub_total + $deliveryCharges + $packaging + $taxable_total;

	$_SESSION['finalTotal'] = array(

		"sub_total" => $sub_total,
		"delivery_charge" => $deliveryCharges,
		"packaging" =>$packaging,
		"taxable_total" => $taxable_total,
		"delivery_tax" => $_SESSION['cartDelivery']['delivery_tax'],
		"total_w_tax" => $total_w_tax,
		"merchant_id" => $merchant_id,

	);

	?>
	<div class="row">
		<div class="col-md-6 col-xs-6"><b>Sub Total:</b></div>
		<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo round($sub_total); ?></div>
		<div class="col-md-6 col-xs-6"><b>Delivery Fee:</b></div>
		<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo $deliveryCharges; ?></div>
		<div class="col-md-6 col-xs-6"><b>Packaging:</b></div>
		<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo !empty($packaging) ? $packaging: 0; ?></div>
		<div class="col-md-6 col-xs-6" style="cursor: pointer;" onclick="taxes()"><b>Total Tax: <i class="fa fa-caret-down"></i></b></div>
		<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo round($taxable_total); ?></div>
		<div style="display: none;" class="sub_tax">
			<?php if($checkDeliveryTax->option_value == ''){ ?>
			<div class="col-md-6 col-xs-6"><b>Delivery Tax:</b></div>
			<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo round($deliveryTaxPercent); ?></div>
			<?php } ?>
			<?php if($merchant_tax->option_value != ''){ ?>
			<div class="col-md-6 col-xs-6"><b>Restaurant Tax:</b></div>
			<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo round($taxable_total - $deliveryTaxPercent); ?></div>
			<?php } ?>
		</div>
		<div class="col-md-6 col-xs-6"><b>Total:</b></div>
		<div class="col-md-6 col-xs-6 text-right cart_subtotal">₹ <?php echo round($total_w_tax); ?></div>
	</div>
	<?php
}


function getOption($option_name)
{
	$CI = get_instance();
	$CI->load->model('ModelOrder');
	return $CI->ModelOrder->getOption($option_name);
}

function updateOption($option_name,$option_value)
{
	$CI = get_instance();
	$CI->load->model('ModelOrder');
	return $CI->ModelOrder->updateOption($option_name,$option_value);
}

 ?>