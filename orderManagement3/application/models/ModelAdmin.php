<?php 

/**
* 
*/
class ModelAdmin extends CI_Model
{

	public function login($data)
	{
		
		$this->db->where("username",$data['username']);
		$this->db->join("mt_farhan_roles","mt_farhan_roles.id=mt_farhan_users.role_id");
		$query = $this->db->get("mt_farhan_users");
		$user = $query->row();

		if(!empty($user))
		{
			if($data['password'] == $this->encryption->decrypt($user->password) && ($user->role == 'Super Admin' || $user->role == 'IPs Whitelisting'))
			{
				return $user;

			}else{

				return false;
			}
		}else{

			
			return false;
		}


	}

	public function orderManagementLogin($data)
	{
		
		$this->db->where("username",$data['username']);
		$this->db->join("mt_farhan_panels","mt_farhan_panels.id=mt_farhan_users.panel_id");
		$this->db->join("mt_farhan_roles","mt_farhan_roles.id=mt_farhan_users.role_id");
		$query = $this->db->get("mt_farhan_users");
		$user = $query->row();

		if(!empty($user))
		{
			if($data['password'] == $this->encryption->decrypt($user->password))
			{
				return $user;

			}else{

				return false;
			}
		}else{

			
			return false;
		}


	}

	public function getRoles()
	{

		$query = $this->db->get("mt_farhan_roles");
		 $this->db->order_by("id","desc");
		return $query->result();

	}

	public function userAdd($data)
	{
		$query = $this->db->insert('mt_farhan_users',$data);

		return $query;
	}

	public function getUsers()
	{	
		$this->db->select('mt_farhan_users.id,mt_farhan_panels.panel,mt_farhan_roles.role,mt_farhan_users.username,mt_farhan_users.email,
			mt_farhan_users.mobile');
    	$this->db->from('mt_farhan_users');
		$this->db->join('mt_farhan_panels', 'mt_farhan_panels.id = mt_farhan_users.panel_id');
		 $this->db->join('mt_farhan_roles', 'mt_farhan_roles.id = mt_farhan_users.role_id');

		$query = $this->db->get();
		return $query->result();

	}

	public function getUserById($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->get("mt_farhan_users");
		return $query->row();

	}

	public function updateUser($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("mt_farhan_users",$data);
		return $query;

	}

	public function userDelete($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->delete("mt_farhan_users");

		return $query;

	}

	public function roleAdd($data)
	{
		$query = $this->db->insert('mt_farhan_roles',$data);

		return $query;
	}

	public function getRoleById($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->get("mt_farhan_roles");
		return $query->row();

	}

	public function updateRole($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("mt_farhan_roles",$data);
		return $query;

	}

	public function roleDelete($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->delete("mt_farhan_roles");

		return $query;

	}

	public function updateWhitelisting($data)
	{	
		$this->db->where("id",1);
		$query = $this->db->update("mt_farhan_whitelisingIPs",$data);
		return $query;

	}

	public function getWhitelistingIps()
	{

		$this->db->where("id",1);
		$query = $this->db->get("mt_farhan_whitelisingIPs");
		return $query->row();

	}


	public function panelAdd($data)
	{
		$query = $this->db->insert('mt_farhan_panels',$data);

		return $query;
	}

	public function getPanels()
	{

		$query = $this->db->get("mt_farhan_panels");
		 $this->db->order_by("id","desc");
		return $query->result();

	}

	public function getPanelById($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->get("mt_farhan_panels");
		return $query->row();

	}


	public function updatePanel($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("mt_farhan_panels",$data);
		return $query;

	}


	public function panelDelete($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->delete("mt_farhan_panels");

		return $query;

	}

	public function updateOrders($order_ids)
	{
		$this->db->where_in('order_id',$order_ids);
		$data = array(

			'status' => 'Cancelled',
			'purchase_value' => 0,
			'delivery_agent' => '',
			'delivery_time2' => ''


		);

		return $this->db->update('mt_order_duplicate_farhan',$data);
	
	}

	public function updateMainOrders($order_ids)
	{
		$this->db->where_in('order_id',$order_ids);
		
		$data = array(
			'status' =>'Cancelled'
		);

		$this->db->update('mt_order',$data);
	
	}

	public function calculatePV($from,$to ='')
	{
		$this->db->select('order_id');
		$this->db->from('mt_order_duplicate_farhan');
		if($to != "")
		{
		    $this->db->where('delivery_date >=', $from);
		    $this->db->where('delivery_date <=', $to);
		}
		else
		{
		    $this->db->where('delivery_date',$from);
		}
		$this->db->where('purchase_value', 0);
		$result = $this->db->get();

		return $result->result();	
	}


	public function getFireBaseId()
	{
		// load 'anothercloudwaysdb'
		 
		$this->legacy_db = $this->load->database('otherdb', true);
		 
		$this->legacy_db->select ('firebase_id');

		$where = "firebase_id != '' AND firebase_id is NOT NULL";
		
		$this->legacy_db->where($where);

		$this->legacy_db->where('client_id',21400);


		$this->legacy_db->from ('mt_client');

		 
		$query = $this->legacy_db->get();
		 
		$result = $query->result_array();

		return $result;
	}
		
	public function getFromOrderTable()
	{	
		$this->db->select('order_id');
		$this->db->limit(1);
		$this->db->order_by('order_id','desc');
		$query = $this->db->get('mt_order');
		return $query->row('order_id');
	}

	public function getFromOrderDuplicateTable()
	{	
		$this->db->select('order_id');
		$this->db->limit(1);
		$this->db->order_by('order_id','desc');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row('order_id');
	}	


	public function fetchOrder($order_duplicate_id)
	{	
		$this->db->select('*');
		$this->db->where("order_id >",$order_duplicate_id);
		$query = $this->db->get('mt_order');
		return $query->result_array();
	}	

	public function insertOrders($order)
	{
		
		$this->db->insert('mt_order_duplicate_farhan',$order);

		
	}

	public function getAgents()
	{	
		
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result();

	}


	public function agentWalletReportAdd($data)
	{
		$query = $this->db->insert('agent_wallet_report',$data);

		return $query;
	}

	public function getAgentWallet($date='')
	{
		$this->db->where('date',$date);
		$query = $this->db->get("agent_wallet");
		$this->db->order_by("id","desc");
		return $query->result();

	}

	public function getAgentWalletReportById($agent_id)
	{
		$this->db->limit(1);
		$this->db->order_by('id','DESC');
		$this->db->where("agent_id",$agent_id);
		$query = $this->db->get("agent_wallet_report");
		return $query->row();

	}

	public function getAgentReportByIdDate($agent_id,$date)
	{
		
		$this->db->order_by('id','ASC');
		$this->db->where('DATE(order_creation_time)',$date);
		$this->db->where("agent_id",$agent_id);
		$query = $this->db->get("agent_wallet_report");
		return $query->result_array();

	}

	public function getAgentReportById($agent_id)
	{
		$this->db->limit(1);
		$this->db->order_by('id','DESC');
		$this->db->where("agent_id",$agent_id);
		$query = $this->db->get("agent_wallet_report");
		return $query->row();

	}

	public function updateAgentWallet($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("agent_wallet",$data);
		return $query;

	}

	public function getAgentById($agent_id)
	{	
		$this->db->where("id",$agent_id);
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->row();

	}

	public function getAgentReportFromTo($agent_id,$from,$to)
	{
		
		$this->db->order_by('id','ASC');
		if(!empty($to))
		{
			$this->db->where('DATE(order_creation_time) >=', $from);
			$this->db->where('DATE(order_creation_time) <=', $to);
		
		}else{

			$this->db->where('DATE(order_creation_time)', $from);
		}

	
		$this->db->where("agent_id",$agent_id);
		$query = $this->db->get("agent_wallet_report");
		return $query->result_array();

	}

}




 ?>