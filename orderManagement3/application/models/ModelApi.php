<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelApi extends CI_Model {

			public function getWalletReport($agent_id,$date)
			{	
								
				$query = $this->db->query("SELECT * FROM agent_wallet_report WHERE DATE(delivery_time) = '".$date."' AND agent_id = '".$agent_id."' ORDER BY delivery_time ASC");

				return $query->result();

			}

			public function getAgentWallet($agent_id)
			{
				$this->db->where('agent_id',$agent_id);

				$query = $this->db->get('agent_wallet');

				return $query->row('wallet');
			}

			public function updateAgentWallet($agentWallet,$agent_id)
			{
				$data = array(

							'wallet' => $agentWallet
						);

				$this->db->where('agent_id',$agent_id);

				$this->db->update('agent_wallet', $data);

			}

			public function insertWalletReport($data)
			{
		
				$this->db->insert('agent_wallet_report',$data);

			}

			public function getLastReportPetrol($agent_id,$date)
			{	
								
				$query = $this->db->query("SELECT petrol FROM agent_wallet_report WHERE DATE(delivery_time) = '".$date."' AND agent_id = '".$agent_id."' AND petrol != 0 ORDER BY delivery_time DESC LIMIT 1");

				return $query->row('petrol');

			}

}