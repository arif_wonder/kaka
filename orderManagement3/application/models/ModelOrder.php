<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelOrder extends CI_Model {

//new work

public function getAgentProfile($agent_id)
{
	$this->db->where('id',$agent_id);
	$query = $this->db->get('mt_delivery_boys_farhan');
	return $query->row_array();

}
public function getAgentOrderProcessing($agent)
{
	$this->db->where('delivery_agent',$agent);
	$this->db->where('status','Processing');
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->num_rows();
}
public function getAgentOrderDelivered($agent)
{
	$this->db->where('delivery_agent',$agent);
	$this->db->where('status','Delivered');
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->num_rows();
}
public function getAgentOrderAll($agent)
{
	$this->db->where('delivery_agent',$agent);
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->num_rows();
}
public function showAgentOrders($agent)
{
	$this->db->where('delivery_agent',$agent);
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->result_array();
}
public function showAgentHistory($agent)
{
	$this->db->where('agent_id',$agent);
	$query = $this->db->get('history');
	return $query->result_array();
}
public function updateAgent($data,$agent_id)
{	
	$this->db->where('id',$agent_id);
	$this->db->update('mt_delivery_boys_farhan', $data);
}
public function addAgent($data)
{
	$this->db->insert('mt_delivery_boys_farhan',$data);
}
public function delAgent($agent_id)
{
	$this->db->where('id',$agent_id);
	$this->db->delete('mt_delivery_boys_farhan');
}
public function deleteHisteryRow($order_id)
    {
       $this->db->where('order_id',$order_id);
       $this->db->delete('history');
    }
public function saveHistory($data)
{
	$this->db->insert('history',$data);
}
public function updateHistory($data,$order_id)
{
	$this->db->where("order_id",$order_id);
	$this->db->update('history',$data);
}
public function agentInfo($agent_id)
	{
		$this->db->where('id',$agent_id);
		$query = $this->db->get("mt_delivery_boys_farhan");
		return $query->row_array(); 
	}
public function getStatus2($order_id)
	{
		$this->db->where('order_id',$order_id);
		$query = $this->db->get("mt_order_duplicate_farhan");
		return $query->row_array(); 
	}

	public function appStatusUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}

//new rok edn

	public function getOrder($from, $to="")
	{
        if($to != "")
        {
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.email_address,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status,
						   mt_order_duplicate_farhan.sent_email,
						    mt_order_duplicate_farhan.delivery_tax,
						     mt_order_duplicate_farhan.request_from,
						     mt_order_duplicate_farhan.is_edited,
							mt_order_duplicate_farhan.rechecked,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderA($from, $to="")
	{
        if($to != "")
        {
        	//$this->db->where('workerType','A');
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
        	//$this->db->where('workerType','A');
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderB($from, $to="")
	{
        if($to != "")
        {
        	//$this->db->where('workerType','B');
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
        	//$this->db->where('workerType','B');
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderByRange($from,$to)
	{
		// ADDED BY FARHAN 9 OCT 2018
		if(getOption('Delivered') != '1')
		{
			$this->db->where('mt_order_duplicate_farhan.status !=','Delivered');
		}

		if(getOption('Cancelled') != '1')
		{
			$this->db->where('mt_order_duplicate_farhan.status !=','Cancelled');
		}

		// END BY FARHAN
		
		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
							mt_order_duplicate_farhan.latitude,
							mt_order_duplicate_farhan.lontitude,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status,
						     mt_order_duplicate_farhan.packing,
						     mt_order_duplicate_farhan.delivery_tax,
						      mt_order_duplicate_farhan.request_from,
						      mt_order_duplicate_farhan.is_edited,
							mt_order_duplicate_farhan.rechecked,


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}
	public function getOrderByRangeWorkerA($from,$to)
	{

		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);
		$this->db->where('workerType','A');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}
	public function getOrderByRangeWorkerB($from,$to)
	{

		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);
		$this->db->where('workerType','B');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}

	public function getMerchantById($merchant_id)
	{
		$this->db->where('merchant_id',$merchant_id);
		$query = $this->db->get('mt_merchant');
		return $query->row('restaurant_name');
	}

	public function getClientById($client_id)
	{	
		$this->db->select('first_name,last_name,contact_phone,street');
		$this->db->where('client_id',$client_id);
		$query = $this->db->get('mt_client');
		return $query->result_array();
	} 

	public function getClientAddressById($order_id,$client_id)
	{	
		$this->db->select('street,city,state,location_name');
		$this->db->where('client_id',$client_id);
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_order_delivery_address');
		return $query->row();
	}

	public function getItemById($item_id,$size_id="")
	{
		$this->db->where('item_id',$item_id);
		$query = $this->db->get('mt_item');
		if($size_id != ""){

			$db = $this->db->where('size_id',$size_id);
			$q = $db->get('mt_size');
			return $query->row('item_name')." ".$q->row('size_name');
		}
		else
			return $query->row('item_name');
	}

	public function statusUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);

		$this->db->where('order_id',$order_id);
		$this->db->update('history', $data);
		
			/* TO UPDATE ORDER STATUS IN mt_order TABLE - BY ALiGNWEBS 23-Mar-2018 4:31 PM */
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order', $data);
		/* END */
	}

	public function purchaseValueUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}

	public function totalWTaxValueUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}
	
		public function deliveryChargeUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}
    
	public function deliveryAgentUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function localityUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	} 

	public function deliveryTime2Update($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function commentUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function reminderUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}


	public function getRemindOrder($date)
	{
		$this->db->where('reminder !=', '');
		$this->db->like('date_created', $date);
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}

	public function getOrderById($order_id)
	{	
		$this->db->where('order_id',$order_id);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');




		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row();
	}

	// ADDED BY FARHAN
	public function getOrderByIdForItem($order_id)
	{	
		$this->db->select('*, mt_order_delivery_address.search_address AS search_address');
		$this->db->where('mt_order_duplicate_farhan.order_id',$order_id);
		$this->db->where('mt_order_delivery_address.order_id',$order_id);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_order_delivery_address', 'mt_order_delivery_address.client_id = mt_order_duplicate_farhan.client_id');

		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row();
	}

	// END BY FARHAN

	public function updateOrderItem($order_id,$data)
	{
		
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan',$data);
	}

	public function decodeItemUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function insertRow($data)
	{
		$this->db->insert_batch('mt_available_agents_farhan',$data);
	}
	public function agentADD($data)
	{
		$this->db->insert("mt_available_agents_farhan",$data);
	}
	public function removeAgent($id)
	{
		$this->db->where("id",$id);
		$this->db->delete("mt_available_agents_farhan");
	}
	public function getAvailableAgents()
	{
		$this->db->select("
			mt_delivery_boys_farhan.name,
			mt_available_agents_farhan.id as avail_id,
			mt_available_agents_farhan.wType,
			agent_id,
			mt_delivery_boys_farhan.status,
			mobile
			");
		$this->db->join('mt_available_agents_farhan', 'mt_available_agents_farhan.agent_id = mt_delivery_boys_farhan.id');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsAA()
	{
		$this->db->where("wType",'a');
		$this->db->select("
			mt_delivery_boys_farhan.name,
			mt_available_agents_farhan.id as avail_id,
			mt_available_agents_farhan.wType,
			agent_id,
			mt_delivery_boys_farhan.status,
			mobile
			");
		$this->db->join('mt_available_agents_farhan', 'mt_available_agents_farhan.agent_id = mt_delivery_boys_farhan.id');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsBB()
	{
		$this->db->where("wType",'b');
		$this->db->select("
			mt_delivery_boys_farhan.name,
			mt_available_agents_farhan.id as avail_id,
			mt_available_agents_farhan.wType,
			agent_id,
			mt_delivery_boys_farhan.status,
			mobile
			");
		$this->db->join('mt_available_agents_farhan', 'mt_available_agents_farhan.agent_id = mt_delivery_boys_farhan.id');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}

	public function getAgents()
	{
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getJoinAgents()
	{
		$this->db->join('mt_available_agents_farhan', 'mt_delivery_boys_farhan.id = mt_available_agents_farhan.agent_id ','left');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsA()
	{
		$this->db->where('wType','a');
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsB()
	{
		$this->db->where('wType','b');
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}


	public function agentUpdate($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function agentStatusUpdate($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function getDeliveryBoys()
	{
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}

	public function export($date)
	{
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = $date."- onlinekaka.csv";
        
     

		$this->db->select('*');
	
		$this->db->order_by('REF', 'asc'); 

		$query = $this->db->get('mt_farhan_excel');
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $this->db->truncate('mt_farhan_excel');
        force_download($filename, $data);
	}
	
	public function updateDeliveryAgent($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
		
	} 


	public function checkPaymentReference($order_id)
	{
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_payment_order');
		return $query->row('payment_reference');
		
		
	}

	// AFTER SEND SMS CHANGE DELIVERY BOYS COLOR TO RED

	public function agentStatusUpdateByName($data,$agentName)
	{
		$this->db->where('name',$agentName);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function getOrderForDecodeItem($first_order_id)
	{

		
		$this->db->where('delivery_date',date('Y-m-d'));
		$this->db->where('order_id >',$first_order_id);
		$this->db->order_by('order_id','desc');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();

	} 
	public function workerUpdate($order_id,$worker)
	{
		$this->db->set('workerType', $worker);  //Set the column name and which value to set..

		$this->db->where('order_id', $order_id); //set column_name and value in which row need to update

		$this->db->update('mt_order_duplicate_farhan'); //Set your table name
	}
	public function workerUpdateAgent($agentID,$worker)
	{
		$this->db->set('wType', $worker);  //Set the column name and which value to set..

		$this->db->where('id', $agentID); //set column_name and value in which row need to update

		$this->db->update('mt_available_agents_farhan'); //Set your table name
	}

	// AFTER DELIVER THE ORDER DELIVERY BOYS COLOR TO GREEN

	public function agentStatusGreenByName($data,$agentName)
	{	
		$this->db->where('delivery_date',date('Y-m-d'));
		$this->db->where('delivery_agent',$agentName);
		$this->db->where('status','Processing');
		$query = $this->db->get('mt_order_duplicate_farhan');
		$result = $query->row('delivery_agent');

		if(isset($result))
		{
			//print_r($data);

		}else{

		$this->db->where('name',$agentName);
		$this->db->update('mt_delivery_boys_farhan', $data);

		}
 
	}

	public function getOrdersOfDeliveryBoys($agentName)
	{	
         
		 $this->db->where('delivery_date',date('Y-m-d'));
		 $this->db->where('delivery_agent',$agentName);
		 $this->db->where('status','Processing');
		 $this->db->select('delivery_agent, COUNT(delivery_agent) as no_of_orders');
		 $this->db->group_by('delivery_agent'); 
		 $this->db->order_by('no_of_orders', 'desc'); 
		 $query = $this->db->get('mt_order_duplicate_farhan');
		 return $query->row('no_of_orders');


	}

	public function getAvailableAgentsByName($agentName)
	{
		$this->db->where('name',$agentName);
		$query = $this->db->get('mt_available_agents_farhan');
		$result = $query->result_array();

		if(sizeof($result) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getLocality()
	{
		$query = $this->db->get('mt_farhan_locality');
		return $query->result_array();
	}

		// CODE ADDED BY FARHAN

		// FOR FILTER
			public function getFilteredListByDeliveryDate($from,$to)
			{
				if($to == '')
				{

					$this->db->where('delivery_date',$from);
					
				}else{

					 $this->db->where('delivery_date >=', $from);
	           		 $this->db->where('delivery_date <=', $to);
				}

				$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');


				$this->db->select('mt_order_duplicate_farhan.merchant_id,
										mt_order_duplicate_farhan.status,
										mt_order_duplicate_farhan.locality,
										mt_order_duplicate_farhan.payment_type,
										mt_order_duplicate_farhan.delivery_agent,
				 					   mt_merchant.restaurant_name,');
				$query = $this->db->get('mt_order_duplicate_farhan');
				     return $query->result_array();
				 
			}

			// FOR FILTER
				public function getOrder2($from,$values,$type,$to)
				{	

			            if($to == '')
						{

							$this->db->where('delivery_date',$from);
							
						}else{

							 $this->db->where('delivery_date >=', $from);
			           		 $this->db->where('delivery_date <=', $to);
						}

			            switch ($type) {
			            	case 'agent':
			            		$this->db->where_in('delivery_agent', $values);
			            		break;
			            	case 'merchant':
			            		$this->db->where_in('mt_order_duplicate_farhan.merchant_id', $values);
			            		break;
			            	case 'payment_type':
			            		$this->db->where_in('mt_order_duplicate_farhan.payment_type', $values);
			            		break;
			            	case 'status':
			            		$this->db->where_in('mt_order_duplicate_farhan.status', $values);
			            		break;
			            	case 'locality':
			            		$this->db->where_in('mt_order_duplicate_farhan.locality', $values);
			            		break;
			            }
			        
					
					$this->db->order_by('order_id','desc');

					$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
					$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

					$this->db->select('mt_order_duplicate_farhan.order_id,
										mt_order_duplicate_farhan.decode_item,
									   mt_merchant.restaurant_name,
									   mt_client.first_name,
									   mt_client.last_name,
									   mt_client.contact_phone,
									   mt_order_duplicate_farhan.payment_type,
									   mt_order_duplicate_farhan.reminder,
									   mt_order_duplicate_farhan.purchase_value,
									   mt_order_duplicate_farhan.total_w_tax,
									   mt_order_duplicate_farhan.delivery_charge,
									   mt_order_duplicate_farhan.status,
									   mt_order_duplicate_farhan.delivery_agent,
									   mt_order_duplicate_farhan.locality,
									   mt_order_duplicate_farhan.date_created,
									   mt_order_duplicate_farhan.delivery_time,
									   mt_order_duplicate_farhan.delivery_time2,
									   mt_order_duplicate_farhan.comment,
									   mt_order_duplicate_farhan.workerType,
									   mt_order_duplicate_farhan.app_status,
									    mt_order_duplicate_farhan.packing,
									    mt_order_duplicate_farhan.delivery_tax,
									     mt_order_duplicate_farhan.request_from,
									     mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,

									  ');
					$query = $this->db->get('mt_order_duplicate_farhan');
				    return $query->result_array();
				
					
				}


				// FOR FILTER
				public function getOrder3($from,$to,$merchant,$status,$locality,$payment_type,$agent)
				{	

			            if($to == '')
						{

							$this->db->where('delivery_date',$from);
							
						}else{

							 $this->db->where('delivery_date >=', $from);
			           		 $this->db->where('delivery_date <=', $to);
						}

					$this->db->where_in('delivery_agent', $agent);
					$this->db->where_in("mt_order_duplicate_farhan.locality",$locality);
					$this->db->where_in("mt_order_duplicate_farhan.payment_type",$payment_type);
					$this->db->where_in("mt_order_duplicate_farhan.status",$status);
					$this->db->where_in("mt_order_duplicate_farhan.merchant_id",$merchant);


					$this->db->order_by('order_id','desc');

					$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
					$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

					$this->db->select('mt_order_duplicate_farhan.order_id,
										mt_order_duplicate_farhan.decode_item,
									   mt_merchant.restaurant_name,
									   mt_client.first_name,
									   mt_client.last_name,
									   mt_client.contact_phone,
									   mt_order_duplicate_farhan.payment_type,
									   mt_order_duplicate_farhan.reminder,
									   mt_order_duplicate_farhan.purchase_value,
									   mt_order_duplicate_farhan.total_w_tax,
									   mt_order_duplicate_farhan.delivery_charge,
									   mt_order_duplicate_farhan.status,
									   mt_order_duplicate_farhan.delivery_agent,
									   mt_order_duplicate_farhan.locality,
									   mt_order_duplicate_farhan.date_created,
									   mt_order_duplicate_farhan.delivery_time,
									   mt_order_duplicate_farhan.delivery_time2,
									   mt_order_duplicate_farhan.comment,
									   mt_order_duplicate_farhan.workerType,
									   mt_order_duplicate_farhan.app_status,
									     mt_order_duplicate_farhan.packing,
									     mt_order_duplicate_farhan.delivery_tax,
									      mt_order_duplicate_farhan.request_from,
									      mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,

									  ');
					$query = $this->db->get('mt_order_duplicate_farhan');
				    return $query->result_array();
				
					
				}

			public function getMerchantById2($merchant_id)
			{
				$this->db->where('merchant_id',$merchant_id);
				
				$query = $this->db->get('mt_merchant');
				return $query->row();

			}

				// END BY FARHAN
				
					public function updateClientStreet($order_id,$data)
			{	

				$this->db->where("order_id",$order_id);
				$this->db->update("mt_order_delivery_address",$data);

			}
			
				// ADDED BY FARHAN 29/04/2018
			public function updateClientEmail($client_id,$data)
			{	

				$this->db->where("client_id",$client_id);
				$this->db->update("mt_client",$data);

			}

			public function updateSentEmail($order_id)
			{	
				$data = array(

					"sent_email" => 1

				);
				$this->db->where("order_id",$order_id);
				$this->db->update("mt_order_duplicate_farhan",$data);

			}

			// END BY FARHAN 29/04/2018

			// ADDED BY FARHAN 19 MAY 2018
			public function getPurchaseValueFromItem($item_id)
			{
				
				$this->db->where("item_id",$item_id);
				return $this->db->get('mt_item')->row()->purchase_value;
			}

			
			public function getItemForPurchaseValue($order_id)
			{	
				$this->db->select('json_details,purchase_value,purchase_item,merchant_id');
				$this->db->from('mt_order_duplicate_farhan');
				$this->db->where("order_id",$order_id);
				return $this->db->get()->row();
			
			}

			// END BY FARHAN 19 MAY 2018

			// ADDED BY FARHAN 2 JUNE 2018

			public function getOptionPvStatus($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'pv_status');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			public function getOptionPvPercentage($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'pv_percentage');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			public function getOptionMerchantTax($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'merchant_tax');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			// END BY FARHAN 2 JUNE 2018

			// ADDED BY FARHAN 6 JUNE 2018

			public function getOrder4($from, $to="")

				{
				
					// ADDED BY FARHAN 9 OCT 2018
					if(getOption('Delivered') != '1')
					{
						$this->db->where('status !=','Delivered');
					}

					if(getOption('Cancelled') != '1')
					{
						$this->db->where('status !=','Cancelled');
					}

					// END BY FARHAN

			        if($to != "")

			        {

			            $this->db->where('delivery_date >=', $from);

			            $this->db->where('delivery_date <=', $to);

			        }

			        else

			        {

			            $this->db->where('delivery_date',$from);

			        }


					$this->db->order_by('order_id','desc');



					$this->db->select('mt_order_duplicate_farhan.order_id,
										
										mt_order_duplicate_farhan.latitude,
										
										mt_order_duplicate_farhan.lontitude,
										
									   mt_order_duplicate_farhan.restaurant_name,

									   mt_order_duplicate_farhan.first_name,

									   mt_order_duplicate_farhan.last_name,

									   mt_order_duplicate_farhan.email_address,

									   mt_order_duplicate_farhan.contact_phone,

									   mt_order_duplicate_farhan.payment_type,

									   mt_order_duplicate_farhan.reminder,

									   mt_order_duplicate_farhan.purchase_value,

									   mt_order_duplicate_farhan.total_w_tax,

									   mt_order_duplicate_farhan.delivery_charge,

									   mt_order_duplicate_farhan.status,

									   mt_order_duplicate_farhan.delivery_agent,

									   mt_order_duplicate_farhan.locality,

									   mt_order_duplicate_farhan.date_created,

									   mt_order_duplicate_farhan.delivery_time,

									   mt_order_duplicate_farhan.delivery_time2,

									   mt_order_duplicate_farhan.comment,

									   mt_order_duplicate_farhan.workerType,

									   mt_order_duplicate_farhan.app_status,

									   mt_order_duplicate_farhan.sent_email,
									    mt_order_duplicate_farhan.packing,
									    mt_order_duplicate_farhan.delivery_tax,
									     
									     mt_order_duplicate_farhan.request_from,
									      mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,




									  ');

					$query = $this->db->get('mt_order_duplicate_farhan');

				    return $query->result_array();

				

					

				}

				public function checkValues($from)
				{
					$this->db->where('delivery_date',$from);
					$this->db->where('restaurant_name',NULL);
					$this->db->select('order_id');
					return $this->db->get('mt_order_duplicate_farhan')->result_array();
				}

				public function getMerchantClient($order_ids)
				{
					
					$this->db->where_in('mt_order_duplicate_farhan.order_id', $order_ids);

					$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');

					$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

					$this->db->join('mt_view_merchant', 'mt_view_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');

					$this->db->select('mt_order_duplicate_farhan.order_id,
										mt_view_merchant.latitude,
									   mt_view_merchant.lontitude,
									   mt_merchant.restaurant_name,

									   mt_client.first_name,

									   mt_client.last_name,

									   mt_client.email_address,

									   mt_client.contact_phone

									  ');
						$query = $this->db->get('mt_order_duplicate_farhan');

	    				return $query->result_array();
				}

			// END BY FARHAN 6 JUNE 2018

			// ADDED BY FARHAN 15 JUNE 2018

			// GET NUMBER OF ORDERS BY DELIVERY AGENT

			public function getNoOfOrders($agentName)
			{	
				$this->db->select('name,order_status,no_of_orders');
				$this->db->where('name',$agentName);
				return $this->db->get('mt_available_agents_farhan')->row();
			}

			public function updateNoOfOrders($no_of_orders,$agentName)
			{
				$this->db->where('name',$agentName);
				$this->db->update('mt_available_agents_farhan', $no_of_orders);
			}

			public function statusUpdate2($data,$order_id)

			{

				$this->db->where('order_id',$order_id);

				$this->db->update('mt_order_duplicate_farhan', $data);

			}

			function file_get_contents_curl($url) {
			    $ch = curl_init();

			    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			    curl_setopt($ch, CURLOPT_HEADER, 0);
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			    curl_setopt($ch, CURLOPT_URL, $url);
			    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

			    $data = curl_exec($ch);
			    curl_close($ch);

			    return $data;
			}

			public function assign_time()
			{	

				$this->db->select('delivery_agent,MIN(assign_time) as myTime');
				$this->db->where('assign_time !=','0000-00-00 00:00:00');
				$this->db->where('assign_time < DATE_SUB(NOW(),INTERVAL 10 MINUTE)');
				$this->db->group_by('delivery_agent');
				$this->db->where(array('status'=>'Processing','delivery_date'=>date('Y-m-d'),'delivery_agent !=' => ''));
				$result = $this->db->get('mt_order_duplicate_farhan');
				return $result->result();

			}

			public function getSuggestedAgents($lat,$lng)
			{
				 
				$query = "SELECT a.agent_id,a.name,a.lati,a.longi, ( 6371 * acos( cos( radians($lat) ) * cos( radians( lati ) ) 
				* cos( radians( longi ) - radians($lng) ) 
				+ sin( radians($lat) ) * sin( radians( lati ) ) ) ) 
				AS distance
				FROM mt_available_agents_farhan a
				HAVING distance < 4";
				$result = $this->db->query($query);
				return $result->result_array();

			}

			public function getAgentByOrderId($order_id)
			{	
				$this->db->where('order_id',$order_id);
				return $this->db->get('mt_order_duplicate_farhan')->row()->delivery_agent;
			}

			public function getLocalityByAgent($agentName)
			{	
				$this->db->select('locality');
				$this->db->where('delivery_agent',$agentName);
				$this->db->where(array('status'=>'Processing','delivery_date'=>date('Y-m-d'),'locality !=' => ''));
				$query = $this->db->get('mt_order_duplicate_farhan');
				return $query->result_array();
			}

			public function deliveryAttendance()
			{	

				$this->db->select('id');
				$this->db->where('location_time !=','0000-00-00 00:00:00');
				$this->db->where('location_time < DATE_SUB(NOW(),INTERVAL 2 MINUTE)');
				$result = $this->db->get('mt_delivery_boys_farhan');
				return $result->result_array();

			}

			public function updatedeliveryAttendance($boys_id)
			{
				$this->db->where_in('id', $boys_id);
				$data = array('status'=>'false');
				$this->db->update('mt_delivery_boys_farhan', $data);
			}

			public function offer_percentage($merchant_id)
			{	
				$this->db->where('merchant_id', $merchant_id);
				return $this->db->get('mt_offers')->row();
			}

			public function getPackaging($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'merchant_packaging_charge');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			public function getAddonItemPrice($addon_id)
			{
				$this->db->select('sub_item_name,price');
				$this->db->from('mt_subcategory_item');
				$this->db->where('sub_item_id', $addon_id);
				return $this->db->get()->row();		
			}

			public function getMerchantId()
			{	
				$this->db->select('merchant_id');
				$this->db->from('mt_merchant');
				$query = $this->db->get();
				return $query->result();
			}

			public function checkMerchantId($id)
			{	
				$this->db->select('merchant_id,option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $id,'option_name' => 'paypal_mode');
				$this->db->where($array);
				return $this->db->get()->row();		

			}

				public function checkPaypalTransaction($order_id)
			{
				$this->db->where('order_id',$order_id);
				$query = $this->db->get('mt_paypal_payment');
				return $query->row('TRANSACTIONID');	
				
			}

			public function onhold_orders()
			{
				$this->db->where('status','On Hold');

				$this->db->where('delivery_date',date('Y-m-d'));
				$query = $this->db->get('mt_order_duplicate_farhan');
				return $query->num_rows();
			}
			
			public function getonholdOrders($from)

				{

			       	
					$this->db->where('status','On Hold');
			        $this->db->where('delivery_date',$from);

			      


					$this->db->order_by('order_id','desc');



					$this->db->select('mt_order_duplicate_farhan.order_id,
										
										mt_order_duplicate_farhan.latitude,
										
										mt_order_duplicate_farhan.lontitude,
										
									   mt_order_duplicate_farhan.restaurant_name,

									   mt_order_duplicate_farhan.first_name,

									   mt_order_duplicate_farhan.last_name,

									   mt_order_duplicate_farhan.email_address,

									   mt_order_duplicate_farhan.contact_phone,

									   mt_order_duplicate_farhan.payment_type,

									   mt_order_duplicate_farhan.reminder,

									   mt_order_duplicate_farhan.purchase_value,

									   mt_order_duplicate_farhan.total_w_tax,

									   mt_order_duplicate_farhan.delivery_charge,

									   mt_order_duplicate_farhan.status,

									   mt_order_duplicate_farhan.delivery_agent,

									   mt_order_duplicate_farhan.locality,

									   mt_order_duplicate_farhan.date_created,

									   mt_order_duplicate_farhan.delivery_time,

									   mt_order_duplicate_farhan.delivery_time2,

									   mt_order_duplicate_farhan.comment,

									   mt_order_duplicate_farhan.workerType,

									   mt_order_duplicate_farhan.app_status,

									   mt_order_duplicate_farhan.sent_email,

									    mt_order_duplicate_farhan.packing,
									    mt_order_duplicate_farhan.delivery_tax,
									     mt_order_duplicate_farhan.request_from,
									     mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,



									  ');

					$query = $this->db->get('mt_order_duplicate_farhan');

				    return $query->result_array();

				

					

				}
				
			public function delayed_orders($mystatus)
			{	
			   $this->db->select('order_id');
			   $this->db->from('mt_order_duplicate_farhan');
              
               $date = date('Y-m-d');

               if($mystatus == 'delayed')
               {

              		$this->db->where('date_created < date_sub(now(), interval 30 minute)');
               		
               }elseif($mystatus == 'veryDelayed')
               {
               		$this->db->where('date_created < date_sub(now(), interval 70 minute)');

               }

               $this->db->where('reminder', NULL);

          	   $where = "delivery_date='".$date."' AND (status='Processing' OR status='pending' OR status='Confirmed')";
               $this->db->where($where);
               
				$query = $this->db->get();
				return $query->num_rows();
				

			}

			
			public function delayed_orders_onhold($mystatus)
			{	
			   $this->db->select('order_id');
			   $this->db->from('mt_order_duplicate_farhan');
			    if($mystatus == 'delayed')
			    {

			   		$this->db->where('reminder < date_sub(now(), interval 30 minute)');
			    		
			    }elseif($mystatus == 'veryDelayed')
			    {
			    	 $this->db->where('reminder < date_sub(now(), interval 70 minute)');

			    }
             
               $date = date('Y-m-d');
             	$this->db->where('reminder !=',NULL);
          	   $where = "delivery_date='".$date."' AND (status='Processing' OR status='pending' OR status='Confirmed')";
               $this->db->where($where);
               
				$query = $this->db->get();
				return $query->num_rows();
				

			}




			public function getDelayedOrders($from,$mystatus)

				{
					// ADDED BY FARHAN 9 OCT 2018
					if(getOption('Delivered') != '1')
					{
						$this->db->where('mt_order_duplicate_farhan.status !=','Delivered');
					}

					if(getOption('Cancelled') != '1')
					{
						$this->db->where('mt_order_duplicate_farhan.status !=','Cancelled');
					}

					// END BY FARHAN
			       
		
					if($mystatus == 'delayed')
					{

			      		$this->db->where('date_created < date_sub(now(), interval 30 minute)');

					}elseif ($mystatus == 'veryDelayed') {
						
						$this->db->where('date_created < date_sub(now(), interval 70 minute)');
					}

					$this->db->where('reminder',NULL);
					$where = "delivery_date='".$from."' AND (status='Processing' OR status='pending' OR status='Confirmed')";
					$this->db->where($where);

					$this->db->order_by('order_id','desc');



					$this->db->select('mt_order_duplicate_farhan.order_id,
										
										mt_order_duplicate_farhan.latitude,
										
										mt_order_duplicate_farhan.lontitude,
										
									   mt_order_duplicate_farhan.restaurant_name,

									   mt_order_duplicate_farhan.first_name,

									   mt_order_duplicate_farhan.last_name,

									   mt_order_duplicate_farhan.email_address,

									   mt_order_duplicate_farhan.contact_phone,

									   mt_order_duplicate_farhan.payment_type,

									   mt_order_duplicate_farhan.reminder,

									   mt_order_duplicate_farhan.purchase_value,

									   mt_order_duplicate_farhan.total_w_tax,

									   mt_order_duplicate_farhan.delivery_charge,

									   mt_order_duplicate_farhan.status,

									   mt_order_duplicate_farhan.delivery_agent,

									   mt_order_duplicate_farhan.locality,

									   mt_order_duplicate_farhan.date_created,

									   mt_order_duplicate_farhan.delivery_time,

									   mt_order_duplicate_farhan.delivery_time2,

									   mt_order_duplicate_farhan.comment,

									   mt_order_duplicate_farhan.workerType,

									   mt_order_duplicate_farhan.app_status,

									   mt_order_duplicate_farhan.sent_email,
									     mt_order_duplicate_farhan.packing,
									     mt_order_duplicate_farhan.delivery_tax,
									      mt_order_duplicate_farhan.request_from,
									      mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,




									  ');

					$query = $this->db->get('mt_order_duplicate_farhan');

				    return $query->result_array();

				

					

				}

				public function getDelayedOrdersOnHold($from,$mystatus)

				{
					 if($mystatus == 'delayed')
		               {

		              		$this->db->where('reminder < date_sub(now(), interval 30 minute)');
		               		
		               }elseif($mystatus == 'veryDelayed')
		               {
		               		$this->db->where('reminder < date_sub(now(), interval 70 minute)');

		               }

		            $this->db->where('reminder !=',NULL);
					$where = "delivery_date='".$from."' AND (status='Processing' OR status='pending' OR status='Confirmed')";
					$this->db->where($where);
		

					$this->db->order_by('order_id','desc');



					$this->db->select('mt_order_duplicate_farhan.order_id,
										
										mt_order_duplicate_farhan.latitude,
										
										mt_order_duplicate_farhan.lontitude,
										
									   mt_order_duplicate_farhan.restaurant_name,

									   mt_order_duplicate_farhan.first_name,

									   mt_order_duplicate_farhan.last_name,

									   mt_order_duplicate_farhan.email_address,

									   mt_order_duplicate_farhan.contact_phone,

									   mt_order_duplicate_farhan.payment_type,

									   mt_order_duplicate_farhan.reminder,

									   mt_order_duplicate_farhan.purchase_value,

									   mt_order_duplicate_farhan.total_w_tax,

									   mt_order_duplicate_farhan.delivery_charge,

									   mt_order_duplicate_farhan.status,

									   mt_order_duplicate_farhan.delivery_agent,

									   mt_order_duplicate_farhan.locality,

									   mt_order_duplicate_farhan.date_created,

									   mt_order_duplicate_farhan.delivery_time,

									   mt_order_duplicate_farhan.delivery_time2,

									   mt_order_duplicate_farhan.comment,

									   mt_order_duplicate_farhan.workerType,

									   mt_order_duplicate_farhan.app_status,

									   mt_order_duplicate_farhan.sent_email,
									     mt_order_duplicate_farhan.packing,
									     mt_order_duplicate_farhan.delivery_tax,
									      mt_order_duplicate_farhan.request_from,
									      mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,




									  ');

					$query = $this->db->get('mt_order_duplicate_farhan');

				    return $query->result_array();

				

					

				}

				public function reminderOrders()
				{
					$this->db->select('order_id');
					$this->db->from('mt_order_duplicate_farhan');
					 $date = date('Y-m-d');
					$this->db->where('reminder < date_sub(now(), interval 1 SECOND)');
					$this->db->where('reminder !=', NULL);
		          	 $where = "delivery_date='".$date."' AND status='On Hold' ";
		          	 $this->db->where($where);

		          	 $query = $this->db->get();

		          	 return $query->num_rows();

				}


			public function updatePacking($order_id,$data)

			{

				$this->db->where('order_id',$order_id);

				$this->db->update('mt_order_duplicate_farhan', $data);

			}
				
			public function getAgentStatus($agent_name)
			{	
				$this->db->where('name',$agent_name);
				return $this->db->get('mt_delivery_boys_farhan')->row();
			}

			public function jsonDetails($order_id)
			{	
				$this->db->select('json_details,delivery_charge,tax,delivery_tax,taxable_total,sub_total,packaging,total_w_tax,client_id');
				$this->db->where('order_id',$order_id);
				return $this->db->get('mt_order_duplicate_farhan')->row();
			}

			public function updateDetails($order_id,$data)
			{
				$this->db->where('order_id',$order_id);

				$this->db->update('mt_order_duplicate_farhan', $data);

			}


			public function updateDetailsMtOrder($order_id,$data)
			{
				$this->db->where('order_id',$order_id);

				$this->db->update('mt_order', $data);
				
			}


			public function getOptionPackagingCharge($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'merchant_packaging_charge');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			public function getOptionPackagingIncremental($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'merchant_packaging_increment');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			// public function getOptionDeliveryCharges($merchant_id)
			// {
			// 	$this->db->select('option_name,option_value');
			// 	$this->db->from('mt_option');
			// 	$array = array('merchant_id' => $merchant_id,'option_name' => 'merchant_delivery_charges');
			// 	$this->db->where($array);
			// 	return $this->db->get()->row();				

			// }

			public function getOptionDeliveryChargesTax($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'merchant_tax_charges');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

		public function deliveryCharge($order_id)
		{	
			$this->db->select('delivery_charge');
			$this->db->where('order_id',$order_id);
			return $this->db->get('mt_order_duplicate_farhan')->row();
		}

		public function merchantItems($merchant_id)
		{	
			$this->db->select('item_id,item_name,price,addon_item');
			$this->db->from('mt_item');
			$this->db->where('merchant_id',$merchant_id);
			$query = $this->db->get();
			return $query->result();
		}

		public function merchantCategories($merchant_id)
		{	
			$this->db->select('cat_id,category_name');
			$this->db->order_by("date_created",'DESC');
			$this->db->from('mt_category');
			$this->db->where(['merchant_id'=> $merchant_id,'status'=>'publish']);
			$query = $this->db->get();
			return $query->result();
		}

		public function ItemByCategory($cat_id)
		{	
			$this->db->select('item_id,item_name,price,addon_item');
			$this->db->order_by("date_created",'DESC');
			$this->db->from('mt_item');
			$this->db->where(['category'=> $cat_id,'status'=>'publish']);
			$query = $this->db->get();
			return $query->result();
		}

		public function getOptionDeliveryTax($merchant_id)
			{
				$this->db->select('option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $merchant_id,'option_name' => 'delivery_tax');
				$this->db->where($array);
				return $this->db->get()->row();				

			}

			public function checkMerchantIdForTax($id)
			{	
				$this->db->select('merchant_id,option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $id,'option_name' => 'delivery_tax');
				$this->db->where($array);
				return $this->db->get()->row();		

			}

		public function updateMerchantTaxCharges()
			{	
				$data = array(

					'option_value' => ''
				);
				$this->db->where('option_name','merchant_tax_charges');

				$this->db->update('mt_option', $data);
			}

		public function getOrderById2($order_id)
	{	
		$this->db->select('json_details,restaurant_name,first_name,last_name,client_id,contact_phone,total_w_tax,payment_type,delivery_agent,status,date_created,order_history');
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row();
	}


	public function getFireBaseId($client_id)
	{
		// load 'anothercloudwaysdb'
		$this->db->select ('firebase_id');

		$where = "firebase_id != '' AND firebase_id is NOT NULL";
		
		$this->db->where($where);

		$this->db->where('client_id',$client_id);

		$this->db->from ('mt_client');
		 
		$query = $this->db->get();
		 
		$result = $query->result_array();

		return $result;
	}

	// ADDED BY FARHAN 4 OCT 2018

	public function getItemById2($item_id)
	{	
		$this->db->select('item_name,item_description,price,non_taxable,discount,addon_item,merchant_id,multi_option');
		$this->db->where('item_id',$item_id);
		$query = $this->db->get('mt_item');
		return $query->row();
	}

	public function checkProductDescription($id)
		{	

			$array = array('merchant_id' => $id,'option_name' => 'farhan_enable_description');
			$this->db->where($array);
			$query = $this->db->get('mt_option');
			return $query->row('option_value');	

		}

	public function updateDistance()
	{
		$data = array(

			'option_value' => '7'
		);
		$this->db->where('option_name','merchant_delivery_miles');

		$this->db->update('mt_option', $data);
	}

	public function updateDistanceType()
	{
		$data = array(

			'option_value' => 'km'
		);
		$this->db->where('option_name','merchant_distance_type');

		$this->db->update('mt_option', $data);
	}

	// ADDED BY FARHAN 9 OCT 2018

	public function getOption($option_name)
	{

		
		$query = $this->db->query('SELECT option_value FROM `mt_farhan_option` WHERE `option_name` = "'.$option_name.'"');
		return $query->row()->option_value;

	}

	public function updateOption($option_name,$option_value)
	{
		$this->db->set('option_value', $option_value); 
		$this->db->where('option_name', $option_name); 
		$this->db->update('mt_farhan_option');
	
	}


	public function refresh($order_id,$from,$to)
	{
					// ADDED BY FARHAN 9 OCT 2018
					if(getOption('Delivered') != '1')
					{
						$this->db->where('status !=','Delivered');
					}

					if(getOption('Cancelled') != '1')
					{
						$this->db->where('status !=','Cancelled');
					}

					// END BY FARHAN

			        if($to != "")

			        {

			            $this->db->where('delivery_date >=', $from);

			            $this->db->where('delivery_date <=', $to);

			        }

			        else

			        {

			            $this->db->where('delivery_date',$from);

			        }

			        $this->db->where('order_id',$order_id);

					$this->db->order_by('order_id','desc');



					$this->db->select('mt_order_duplicate_farhan.order_id,
										
										mt_order_duplicate_farhan.latitude,
										
										mt_order_duplicate_farhan.lontitude,
										
									   mt_order_duplicate_farhan.restaurant_name,

									   mt_order_duplicate_farhan.first_name,

									   mt_order_duplicate_farhan.last_name,

									   mt_order_duplicate_farhan.email_address,

									   mt_order_duplicate_farhan.contact_phone,

									   mt_order_duplicate_farhan.payment_type,

									   mt_order_duplicate_farhan.reminder,

									   mt_order_duplicate_farhan.purchase_value,

									   mt_order_duplicate_farhan.total_w_tax,

									   mt_order_duplicate_farhan.delivery_charge,

									   mt_order_duplicate_farhan.status,

									   mt_order_duplicate_farhan.delivery_agent,

									   mt_order_duplicate_farhan.locality,

									   mt_order_duplicate_farhan.date_created,

									   mt_order_duplicate_farhan.delivery_time,

									   mt_order_duplicate_farhan.delivery_time2,

									   mt_order_duplicate_farhan.comment,

									   mt_order_duplicate_farhan.workerType,

									   mt_order_duplicate_farhan.app_status,

									   mt_order_duplicate_farhan.sent_email,
									    mt_order_duplicate_farhan.packing,
									    mt_order_duplicate_farhan.delivery_tax,
									     mt_order_duplicate_farhan.request_from,
									     mt_order_duplicate_farhan.is_edited,
									      mt_order_duplicate_farhan.rechecked,




									  ');

					$query = $this->db->get('mt_order_duplicate_farhan');

				    return $query->result_array();
	}

		public function checkMerchantIdOption($id)
			{	
				$this->db->select('merchant_id,option_name,option_value');
				$this->db->from('mt_option');
				$array = array('merchant_id' => $id,'option_name' => 'merchant_paytm_enabled');
				$this->db->where($array);
				return $this->db->get()->row();		

			}

			public function sizeName($size_id)
			{
				$this->db->where("size_id",$size_id);
				$query = $this->db->get('mt_size');

				return $query->row('size_name');
			}

			public function subcategoryName($subcat_id)
			{	
				$this->db->select('subcategory_name,sequence');
				$this->db->where("subcat_id",$subcat_id);
				$query = $this->db->get('mt_subcategory');

				return $query->row();
			}

			public function subcategoryItemName($sub_item_id)
			{
				$this->db->select('sub_item_id, sub_item_name,price,category');
				$this->db->where("sub_item_id",$sub_item_id);
				$query = $this->db->get('mt_subcategory_item');
				return $query->row();
			}

			public function subcategoryName2($subcat_id)
			{	
				$subcat_id = json_decode($subcat_id, TRUE);
				$this->db->select('subcategory_name');
				$this->db->where_in("subcat_id",$subcat_id);
				$query = $this->db->get('mt_subcategory');

				return $query->row();
			}
	// END BY FARHAN



			public function getItemByCategory($cat_id,$merchant_id)
			{
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL,"https://www.speedypixelgame.com/kakatesting/store/getItemByCategory");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,
				            "cat_id=".$cat_id."&merchant_id=".$merchant_id);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				$server_output = curl_exec($ch);

				curl_close ($ch);

				$items = json_decode($server_output,true);

				return $items;

			}

			public function getSubCategory($subcat_id)
			{
				$this->db->where('subcat_id',$subcat_id);
				$query = $this->db->get('mt_subcategory');

				return $query->row('subcategory_name');
			}

			public function getSubCategoryItem($sub_item_id)
			{
				$this->db->where('sub_item_id',$sub_item_id);
				$query = $this->db->get('mt_subcategory_item');

				return $query->row('sub_item_name');
			}


			public function deleteOrderDetails($order_id)
			    {
			       $this->db->where('order_id',$order_id);
			       $this->db->delete('mt_order_details');
			    }

			// public function getItemById($item_id)
			// {
			// 	$ch = curl_init();

			// 	curl_setopt($ch, CURLOPT_URL,"https://www.speedypixelgame.com/kakatesting/store/getItemById");
			// 	curl_setopt($ch, CURLOPT_POST, 1);
			// 	curl_setopt($ch, CURLOPT_POSTFIELDS,
			// 	            "item_id=".$item_id);
			// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// 	$server_output = curl_exec($ch);

			// 	curl_close ($ch);

			// 	$items = json_decode($server_output,true);

			// 	return $items;

			// }
}	
