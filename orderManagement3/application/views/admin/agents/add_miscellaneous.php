<div class="container-fluid">
  <h4><?php echo $agent->name." #".$agent->id; ?></h4>

      <div class="row">
        <div class="col-lg-6">
         
    <div class="card card-register">
      <div class="card-header">Add Miscellaneous</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/storeMiscellaneous">
          <input type="hidden" name="agent_id" value="<?php echo $agent->id; ?>">
          <div class="form-group">
            <?php echo form_error('wallet','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="wallet" type="number" value="<?php echo set_value('wallet'); ?>" name="wallet" placeholder="Enter Balance">
          </div>

          <button type="submit" name="miscellaneousAdd" class="btn btn-primary btn-block">Add</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>


 