<div class="card-body">
<h4><?php echo $title; ?></h4>
<br>
          <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Date</th>
                  <th>Attendance</th>
       
                 
                </tr>
              </thead>
              <tbody>

                <?php foreach($agents as $agent) { ?>
                <tr>
                  <td><?php echo $agent->name; ?></td>
                  <td><?php echo $agent->mobile; ?></td>
                  <td><?php echo date('Y-m-d'); ?></td>
                  <td>  
                      <label class="radio-inline attendance"><input type="radio" checked date-id="<?php echo $agent->id; ?>" name="attendance<?php echo $agent->id ?>" value="present"> Present</label>
                      <label class="radio-inline attendance"><input type="radio" date-id="<?php echo $agent->id; ?>" name="attendance<?php echo $agent->id ?>" value="absent"> Absent</label>
                  </td>
  
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>