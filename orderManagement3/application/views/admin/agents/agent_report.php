<?php 

   $margin = array_column($agentReports, 'margin');
   $credits = array_sum(array_filter($margin, function ($num){
       return $num > 0;
   }));

   $debits = array_sum(array_filter($margin, function ($num){
       return $num < 0;
   }));

   $order_id = array_column($agentReports, 'order_id');
   $orders = array_sum(array_filter($order_id, function ($num){
       return $num != null;
   }));
?>

    <div class="container">
        <h4><?php echo $agent->name." #".$agent->id; ?></h4>
        <div class="card-header">
          <div class="row">
            <div class="col-6">
              <a href="<?php echo base_url(); ?>admin/addAgentWallet/<?php echo $agent->id ?>" class="btn btn-success">Add Balance in Wallet</a> <a href="<?php echo base_url(); ?>admin/addMiscellaneous/<?php echo $agent->id ?>" class="btn btn-danger">Add Miscellaneous</a>
            </div>
            <div class="col-6">
                <div class="row">
                  <div class="col-4">
                     <input type="hidden" id="agent_id" value="<?php echo $agent->id; ?>">
                  <input type="text" class="form-control" name="from" id="from" placeholder="From">
                  </div>
                  <div class="col-4">
                  <input type="text" class="form-control" name="to" id="to" placeholder="to">
                  </div>
                  <div class="col-4">
                    <button type="button" class="btn btn-primary" onclick="searchByDate()">Search</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
        <br>  
               <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                                    <h3 class=""><?php echo isset($lastReport->current_balance) ? $lastReport->current_balance : 0; ?></h3>
                                    <h6 class="card-subtitle">Available Balance</h6></div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->

                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                                    <h3 class=""><?php echo $credits; ?></h3>
                                    <h6 class="card-subtitle">Total Credits</h6></div>
                              
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                                    <h3 class=""><?php echo $debits; ?></h3>
                                    <h6 class="card-subtitle">Total Debits</h6></div>
                              
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                                    <h3 class=""><?php echo $orders; ?></h3>
                                    <h6 class="card-subtitle">Total Today's Orders</h6></div>
                               
                            </div>
                        </div>
                    </div>
                </div>
          <!-- Example Bar Chart Card-->
</div>
     <div id="display">
        <div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <?php if(sizeof($agentReports) > 0){ ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Order #</th>
                  <th>PV</th>
                  <th>DV</th>
                  <th>Margin</th>
                  <th>Current Balance</th>
                  <th>Order Status</th>
                  <th>Description</th>
                  <th>Petrol</th>
                  <th>Added By</th>
                  <th>Order Time</th>
                  <th>Delivery Time</th>
                    
                </tr>
              </thead>
              <tbody>
                <?php foreach($agentReports as $agentReport) { ?>
                <tr style="<?php if($agentReport['margin'] > 0){echo 'background:#00d230';}else{ echo 'background: #ff3d50';} ?>">
                  <td><?php echo $agentReport['order_id']; ?></td>
                  <td><?php echo $agentReport['pv']; ?></td>
                  <td><?php echo $agentReport['dv']; ?></td>
                  <td><?php echo $agentReport['margin']; ?></td>
                  <td><?php echo $agentReport['current_balance']; ?></td>
                  <td><?php echo $agentReport['order_status']; ?></td>
                  <td><?php echo $agentReport['description']; ?></td>
                  <td><?php echo $agentReport['petrol']; ?></td>
                  <td><?php echo $agentReport['added_by']; ?></td>
                  <td><?php echo $agentReport['order_creation_time']; ?></td>
                  <td><?php echo $agentReport['delivery_time']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php }else{ ?>

              No Report Found.

            <?php } ?>
          </div>
        </div>
 </div>