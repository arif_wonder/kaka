
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Edit an Agent Wallet</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/updateAgentWallet/<?php echo $agentWallet->id; ?>">

          <div class="form-group">
           <?php echo form_error('role_id','<div class="text-danger">', '</div>'); ?>
           <select class="form-control" name="agent_id" disabled="true">
             <option value="">Select Agent</option>
             <?php foreach($agents as $agent) { ?>

               <option value="<?php echo $agent->id ?>" <?php if($agent->id == $agentWallet->agent_id){ echo 'selected';} ?> <?php echo set_select('agent_id', $agent->id, False); ?>><?php echo $agent->name; ?></option>

             <?php } ?>
           </select>
         </div>
         <div class="form-group">
           <?php echo form_error('wallet','<div class="text-danger">', '</div>'); ?>
           <input class="form-control" id="wallet" type="number" value="<?php echo $agentWallet->wallet; ?>" name="wallet" placeholder="Enter Wallet">
         </div>
          <button type="submit" name="agentWalletEdit" class="btn btn-primary btn-block">Update</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
