       

          <!-- Example Bar Chart Card-->
       
      <div class="card-header">Agent Wallet List</div>
        <div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SL No.</th>
                  <th>Agent</th>
                   <th>Wallet</th>
                   <th>Date</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($agentWallets as $key => $agentWallet) { ?>
                <tr>
                  <td><?php echo $key + 1; ?></td>
                  <td><?php echo $agentWallet->agent_id; ?></td>
                  <td><?php echo $agentWallet->wallet; ?></td>
                  <td><?php echo $agentWallet->date; ?></td>
                  <td>
                    <a href="<?php echo base_url(); ?>admin/agentWalletEdit/<?php echo $agentWallet->id ?>" class="btn btn-primary btn-sm">Edit</a>
                   <!--  <a href="<?php echo base_url(); ?>admin/agentWalletDelete/<?php echo $agentWallet->id ?>" onclick="return confirm('Do you want to delete?');" class="btn btn-danger btn-sm">Delete</a> -->
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
 