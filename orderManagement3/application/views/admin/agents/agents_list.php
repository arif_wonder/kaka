       

          <!-- Example Bar Chart Card-->

      <div class="card-header">Agent List</div>
        <div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                   <th>Action</th>
                 
                </tr>
              </thead>
              <tbody>
                <?php foreach($agents as $key => $agent) { ?>
                <tr>
                  <td><?php echo $agent->id; ?></td>
                  <td><?php echo $agent->name; ?></td>
                  <td> <a href="<?php echo base_url(); ?>admin/agentReport/<?php echo $agent->id ?>" class="btn btn-primary btn-sm">Report</a></td>
               
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
 