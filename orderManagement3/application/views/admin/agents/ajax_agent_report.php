
        <div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <?php if(sizeof($agentReports) > 0){ ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Order #</th>
                  <th>PV</th>
                  <th>DV</th>
                  <th>Margin</th>
                  <th>Current Balance</th>
                  <th>Order Status</th>
                  <th>Description</th>
                  <th>Petrol</th>
                  <th>Added By</th>
                  <th>Order Time</th>
                  <th>Delivery Time</th>
                    
                </tr>
              </thead>
              <tbody>
                <?php foreach($agentReports as $agentReport) { ?>
                <tr style="<?php if($agentReport['margin'] > 0){echo 'background:#00d230';}else{ echo 'background: #ff3d50';} ?>">
                  <td><?php echo $agentReport['order_id']; ?></td>
                  <td><?php echo $agentReport['pv']; ?></td>
                  <td><?php echo $agentReport['dv']; ?></td>
                  <td><?php echo $agentReport['margin']; ?></td>
                  <td><?php echo $agentReport['current_balance']; ?></td>
                  <td><?php echo $agentReport['order_status']; ?></td>
                  <td><?php echo $agentReport['description']; ?></td>
                  <td><?php echo $agentReport['petrol']; ?></td>
                  <td><?php echo $agentReport['added_by']; ?></td>
                  <td><?php echo $agentReport['order_creation_time']; ?></td>
                  <td><?php echo $agentReport['delivery_time']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php }else{ ?>

              No Report Found.

            <?php } ?>
          </div>
        </div>
