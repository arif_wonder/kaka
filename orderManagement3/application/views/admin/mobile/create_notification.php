
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Send Notification</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/sendNotifcation">

          <div class="form-group">
            <?php echo form_error('title','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="title" type="text" value="<?php echo set_value('title'); ?>" name="title" placeholder="Enter title">
          </div>
          <div class="form-group">
             <?php echo form_error('body','<div class="text-danger">', '</div>'); ?>
            <textarea class="form-control" id="body" rows="6" name="body" placeholder="Enter Message"><?php echo set_value('body'); ?></textarea>
          </div>

          <button type="submit" name="sendNotification" class="btn btn-primary btn-block">Send</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
