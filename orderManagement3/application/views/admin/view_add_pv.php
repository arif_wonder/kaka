
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Calculate PV</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>

        <?php if(isset($_GET['success'])){ ?>

          <div class="alert alert-success">PV Calculated Successfully.</div>

        <?php } ?>
        <form method="post" action="<?php echo base_url(); ?>admin/storePV">
          <div class="row">
          <div class="col-md-6">
          <div class="form-group">
            <?php echo form_error('from','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="from" type="text" value="<?php echo set_value('from'); ?>" name="from" placeholder="FROM">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <?php echo form_error('to','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="to" type="text" value="<?php echo set_value('to'); ?>" name="to" placeholder="TO">
          </div>
        </div>
      </div>
          <button type="submit" name="pvAdd" class="btn btn-primary btn-block">Calculate</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
