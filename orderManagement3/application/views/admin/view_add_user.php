
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Add a User</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/storeUser">
          <div class="form-group">
            <?php echo form_error('panel_id','<div class="text-danger">', '</div>'); ?>
            <select class="form-control" name="panel_id">
              <option value="">Select Panel</option>
              <?php foreach($panels as $panel) { ?>

                <option value="<?php echo $panel->id ?>" <?php echo set_select('panel_id', $panel->id, False); ?>><?php echo $panel->panel; ?></option>

              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <?php echo form_error('role_id','<div class="text-danger">', '</div>'); ?>
            <select class="form-control" name="role_id">
              <option value="">Select Role</option>
              <?php foreach($roles as $role) { ?>

                <option value="<?php echo $role->id ?>" <?php echo set_select('role_id', $role->id, False); ?>><?php echo $role->role; ?></option>

              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <?php echo form_error('username','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="username" type="text" value="<?php echo set_value('username'); ?>" name="username" placeholder="Enter Username">
          </div>
          <div class="form-group">
             <?php echo form_error('email','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="exampleInputEmail1" type="email" value="<?php echo set_value('email'); ?>" name="email" placeholder="Enter email">
          </div>
          <div class="form-group">
           
               <?php echo form_error('password','<div class="text-danger">', '</div>'); ?>
                <input class="form-control"  type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password">
          </div>
          <div class="form-group">   
              
              <?php echo form_error('confirmPassword','<div class="text-danger">', '</div>'); ?>
                <input class="form-control"  type="password" name="confirmPassword" value="<?php echo set_value('confirmPassword'); ?>" placeholder="Confirm password">
              
          </div>
          <div class="form-group">
            <?php echo form_error('mobile','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="mobile" type="text" name="mobile" value="<?php echo set_value('mobile'); ?>" placeholder="Enter Mobile">
          </div>
          <button type="submit" name="userAdd" class="btn btn-primary btn-block">Add</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
