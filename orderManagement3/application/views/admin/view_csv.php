
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Upload CSV</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/storeCsv" enctype="multipart/form-data" onsubmit="return confirm('Are you sure you want to update the orders?');">
          <div class="form-group">
            <?php echo form_error('csv','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="csv" type="file" name="csv" required="" accept=".csv" >
          </div>
   
          <button type="submit" name="csvAdd" class="btn btn-primary btn-block">Upload</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
