
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Edit a Role</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/updateRole/<?php echo $role->id; ?>">

          <div class="form-group">
            <?php echo form_error('role','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="role" type="text" value="<?php echo $role->role; ?>" name="role" placeholder="Enter Role">
          </div>

          <button type="submit" name="roleEdit" class="btn btn-primary btn-block">Update</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
