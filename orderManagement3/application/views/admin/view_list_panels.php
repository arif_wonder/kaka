        <div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SL No.</th>
                  <th>Panel</th>
                   <th>Action</th>
                 
                </tr>
              </thead>
              <tbody>
                <?php foreach($panels as $key => $panel) { ?>
                <tr>
                  <td><?php echo $key + 1; ?></td>
                  <td><?php echo $panel->panel; ?></td>

                  <td>
                    <a href="<?php echo base_url(); ?>admin/panelEdit/<?php echo $panel->id ?>" class="btn btn-primary btn-sm">Edit</a>
                    <a href="<?php echo base_url(); ?>admin/panelDelete/<?php echo $panel->id ?>" onclick="return confirm('Do you want to delete?');" class="btn btn-danger btn-sm">Delete</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>