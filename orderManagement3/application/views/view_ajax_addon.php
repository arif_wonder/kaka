<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

 ?>
<form method="post" action="" id="addToCart3"  onSubmit="return addToCart3()">
<div class="row">
			<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
			<input type="hidden" name="merchant_id" value="<?php echo $merchant_id; ?>">
			<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
			<label>Price</label>
			
			<?php 
			$i = 0;
			foreach(json_decode($addons->price,true) as $key => $price)
			{	
				$size = $CI->ModelOrder->sizeName($key); 
				if($size != "")
				{
					$size = "|".$size;

				}else{

					$size = "";
				}

				?>
				<div class="col-lg-6">
					<div class="form-group">
				<label><input name='price' id="price"  type='radio' value='<?php echo $price.$size; ?>' <?php if($i == 0){echo "checked";} ?>> <?php echo $price." ".$size; ?></label>
					</div>
				</div>
			<?php 
			$i++;
			} ?>
			


	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="form-group">
			<label>Quantity</label>
		<input class="uk-form-width-mini numeric_only qty" maxlength="5" min="1" step="1" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" type="text" value="1" name="qty" id="qty">
	</div>
	</div>

</div>

<div class="row">
	<div class="col-lg-12">
		
		<?php 
		$sizeofAddons = count(json_decode($addons->addon_item,true));
		if($sizeofAddons > 0)
		{
		$multi_option = json_decode($addons->multi_option,true);
		foreach(json_decode($addons->addon_item,true) as $key => $addon) { 

				$subcategoryName = $CI->ModelOrder->subcategoryName($key);

			?>
		<label><?php echo $subcategoryName->subcategory_name; ?></label>

			
					<?php
						if($multi_option[$key][0] == 'one')
						
						{
							foreach($addon as $key2=>$val)
						{ 
							$sub_cat = $CI->ModelOrder->subcategoryItemName($val);
					?>		
							<div><input value="<?php echo $sub_cat->sub_item_id ?>|<?php echo $sub_cat->price ?>|<?php echo $sub_cat->sub_item_name; ?>|right" data-id="<?php echo $sub_cat->sub_item_id ?>" rel="one" data-id="<?php echo $key; ?>" class="sub_item sub_item_name_<?php echo $key; ?>" type="radio" name="sub_item[<?php echo $key ?>][]" id="sub_item_<?php echo $key;?>"> <?php echo $sub_cat->sub_item_name." ".$sub_cat->price; ?></div>
					
					<?php	} 
						}elseif($multi_option[$key][0] == 'custom'){

						foreach($addon as $key2 => $val)
						{ 
							$sub_cat = $CI->ModelOrder->subcategoryItemName($val);
						?>
						
							<div><input value="<?php echo $sub_cat->sub_item_id ?>|<?php echo $sub_cat->price ?>|<?php echo $sub_cat->sub_item_name; ?>|" data-id="<?php echo $sub_cat->sub_item_id ?>" data-option="" rel="custom" class="sub_item_name sub_item_name_<?php echo $key; ?>" type="checkbox" name="sub_item[<?php echo $key; ?>][<?php echo $key2 ?>]" id="sub_item_<?php echo $key; ?>_<?php echo $key2 ?>"><?php echo $sub_cat->sub_item_name."       ".$sub_cat->price; ?></div>
					
					<?php	} 
						}

					  ?>		

		
		<?php } 

			}
		?>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
	<button type="submit" class="btn btn-success">Add to Cart</button>
</div>
</div>
		</form>