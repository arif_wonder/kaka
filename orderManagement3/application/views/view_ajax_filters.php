				<div class="row filter-block-agent" style="display: none">
					<div class="col-md-12">
						<div class="filter-box-agent">
							<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
								<div class="filter_by_values">Filter by values...</div>
								<div class="select_clear_values">
									<a href="javascript:void(0)" onclick="selectAll('agent')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('agent')">Clear</a>
								</div>
								<div class="searh_fields">

									<input type="text" class="form-control" name="filter_search" id="filter_search_agent" onkeyup="searchFilter('agent')"><span class="fa fa-search search_filter" onclick="searchFocus('agent')"></span>
								</div>
								<div class="list_of_fields">
									<ul id="filter_options_agent" class="filter_options">
										<!-- GET DELVIERY AGENTS -->
										<?php foreach(uniqueAssocArray($getFilteredLists, 'delivery_agent') as $get_available_agent) { ?>



										<li>
											<a href="#">
												<label><input type="checkbox" name="agent[]" checked value="<?php echo $get_available_agent['delivery_agent'] ?>" class="checkBoxClass checkBox-agent"> <?php echo $get_available_agent['delivery_agent']; ?></label>
											</a>
										</li>


										<?php } ?>
									</ul>
								</div>
							</div>
							<hr>
							<div class="filter-footer">
								<button type="button" class="btn btn-primary" onclick="getSearchResult('agent')">OK</button>
								<button type="button" class="btn btn-default" onclick ="cancelBlock('agent')">Cancel</button>
							</div>
						</div>

					</div>
				</div>
				<!-- END AGENTS -->

				<!-- MERCHANTS -->
				<div class="row filter-block-merchant" style="display: none">
					<div class="col-md-12">
						<div class="filter-box-merchant">
							<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
								<div class="filter_by_values">Filter by values...</div>
								<div class="select_clear_values">
									<a href="javascript:void(0)" onclick="selectAll('merchant')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('merchant')">Clear</a>
								</div>
								<div class="searh_fields">

									<input type="text" class="form-control" name="filter_search" id="filter_search_merchant" onkeyup="searchFilter('merchant')"><span class="fa fa-search search_filter" onclick="searchFocus('merchant')"></span>
								</div>
								<div class="list_of_fields">
									<ul id="filter_options_merchant" class="filter_options">
										<!-- GET DELVIERY AGENTS -->
										<?php foreach(uniqueAssocArray($getFilteredLists, 'restaurant_name') as $getMerchant) { ?>



										<li>
											<a href="#">
												<label><input type="checkbox" name="merchant[]" checked value="<?php echo $getMerchant['merchant_id'] ?>" class="checkBoxClass checkBox-merchant"> <?php echo $getMerchant['restaurant_name']; ?></label>
											</a>
										</li>


										<?php } ?>
									</ul>
								</div>
							</div>
							<hr>
							<div class="filter-footer">
								<button type="button" class="btn btn-primary" onclick="getSearchResult('merchant')">OK</button>
								<button type="button" class="btn btn-default" onclick ="cancelBlock('merchant')">Cancel</button>
							</div>
						</div>

					</div>
				</div>
				<!-- END MERCHANT -->
				<!-- PAYMENT TYPE -->
				<div class="row filter-block-payment_type" style="display: none">
					<div class="col-md-12">
						<div class="filter-box-payment_type">
							<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
								<div class="filter_by_values">Filter by values...</div>
								<div class="select_clear_values">
									<a href="javascript:void(0)" onclick="selectAll('payment_type')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('payment_type')">Clear</a>
								</div>
								<div class="searh_fields">

									<input type="text" class="form-control" name="filter_search" id="filter_search_payment_type" onkeyup="searchFilter('payment_type')"><span class="fa fa-search search_filter" onclick="searchFocus('payment_type')"></span>
								</div>
								<div class="list_of_fields">
									<ul id="filter_options_payment_type" class="filter_options">
										<!-- GET DELVIERY AGENTS -->
										<?php foreach(uniqueAssocArray($getFilteredLists, 'payment_type') as $payment_type) { ?>



										<li>
											<a href="#">
												<label><input type="checkbox" name="payment_type[]" checked  value="<?php echo $payment_type['payment_type'] ?>" class="checkBoxClass checkBox-payment_type"> <?php echo $payment_type['payment_type']; ?></label>
											</a>
										</li>


										<?php } ?>
									</ul>
								</div>
							</div>
							<hr>
							<div class="filter-footer">
								<button type="button" class="btn btn-primary" onclick="getSearchResult('payment_type')">OK</button>
								<button type="button" class="btn btn-default" onclick ="cancelBlock('payment_type')">Cancel</button>
							</div>
						</div>

					</div>
				</div>
				<!-- END PAYMENT TYPE -->
				<!-- LOCALITY -->
				<div class="row filter-block-locality" style="display: none">
					<div class="col-md-12">
						<div class="filter-box-locality">
							<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
								<div class="filter_by_values">Filter by values...</div>
								<div class="select_clear_values">
									<a href="javascript:void(0)" onclick="selectAll('locality')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('locality')">Clear</a>
								</div>
								<div class="searh_fields">

									<input type="text" class="form-control" name="filter_search" id="filter_search_locality" onkeyup="searchFilter('locality')"><span class="fa fa-search search_filter" onclick="searchFocus('locality')"></span>
								</div>
								<div class="list_of_fields">
									<ul id="filter_options_locality" class="filter_options">
										<!-- GET DELVIERY AGENTS -->
										<?php foreach(uniqueAssocArray($getFilteredLists, 'locality') as $locality) { ?>



										<li>
											<a href="#">
												<label><input type="checkbox" name="locality[]" checked value="<?php echo $locality['locality'] ?>" class="checkBoxClass checkBox-locality"> <?php echo $locality['locality']; ?></label>
											</a>
										</li>


										<?php } ?>
									</ul>
								</div>
							</div>
							<hr>
							<div class="filter-footer">
								<button type="button" class="btn btn-primary" onclick="getSearchResult('locality')">OK</button>
								<button type="button" class="btn btn-default" onclick ="cancelBlock('locality')">Cancel</button>
							</div>
						</div>

					</div>
				</div>
				<!-- END LOCALITY -->
				<!-- STATUS-->
				<div class="row filter-block-status" style="display: none">
					<div class="col-md-12">
						<div class="filter-box-status">
							<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
								<div class="filter_by_values">Filter by values...</div>
								<div class="select_clear_values">
									<a href="javascript:void(0)" onclick="selectAll('status')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('status')">Clear</a>
								</div>
								<div class="searh_fields">

									<input type="text" class="form-control" name="filter_search" id="filter_search_status" onkeyup="searchFilter('status')"><span class="fa fa-search search_filter" onclick="searchFocus('status')"></span>
								</div>
								<div class="list_of_fields">
									<ul id="filter_options_status" class="filter_options">
										<!-- GET DELVIERY AGENTS -->
										<?php foreach(uniqueAssocArray($getFilteredLists, 'status') as $status) { ?>

										
										
										<li>
											<a href="#">
												<label><input type="checkbox" name="status[]" checked value="<?php echo $status['status'] ?>" class="checkBoxClass checkBox-status"> <?php echo $status['status']; ?></label>
											</a>
										</li>
										
										
										<?php } ?>
									</ul>
								</div>
							</div>
							<hr>
							<div class="filter-footer">
								<button type="button" class="btn btn-primary" onclick="getSearchResult('status')">OK</button>
								<button type="button" class="btn btn-default" onclick ="cancelBlock('status')">Cancel</button>
							</div>
						</div>
						
					</div>
				</div>
				<!-- END STATUS -->