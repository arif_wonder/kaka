	<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

$sequence_number =sizeof($getOrders);
$i = 1;

?>
<input type="hidden" value="<?php echo $sequence_number ?>" id='newOrder'>
				  
					<?php foreach($getOrders as $getOrder) { 

						if($getOrder['reminder'] == NULL)
							{
								$mytime = '';
							}else{

								$mytime =  date("H:i",strtotime($getOrder['reminder']));
							}

						?>
					<tr data-time="<?php echo $mytime; ?>" data-currenttime="<?php echo date("H:i");  ?>" data-status="<?php echo $getOrder['status'] ?>" data-rowid="allocateColor<?php echo $getOrder['order_id']; ?>"  id="allocateColor<?php echo $getOrder['order_id']; ?>" style="<?php echo allocateColor($getOrder['status']); ?> <?php echo allocateColor($getOrder['rechecked']); ?>">
						<?php 
							$cellClr = "";
							if ($getOrder['workerType'] == 'A')
							{
								$cellClr = "background:yellow;";
							}
							elseif ($getOrder['workerType'] == 'B') {
								$cellClr = "background:blue;";

							}elseif ($getOrder['is_edited'] == 1) {
								$cellClr = "background:#d60dd6;";
							}
							else
							{
								if ($getOrder['order_id'] % 2 == 0)
									$cellClr = "background:yellow;";
								else
								$cellClr = "background:blue;";
									
							}

								

						 ?>
						<td id="refresh<?php echo $getOrder['order_id']; ?>" style="<?php echo $cellClr ?>"><?php echo $sequence_number--; ?> 
							<?php if($getOrder['rechecked'] == 1) : ?>
								<button onclick="rechecked(<?php echo $getOrder['order_id']; ?>)" class="btn btn-xs" style="margin: 2px -4px;">RC</button>
							<?php endif; ?>
						</td>
						<td><?php echo $getOrder['order_id']; ?></td>
						<td><?php echo strtoupper($getOrder['restaurant_name']); ?></td>
						<td><?php echo strtoupper($getOrder['first_name']." ".$getOrder['last_name']); ?></td>
						<td><?php echo $getOrder['contact_phone'];?></td>
						<td>
							
							<button type="button" onclick="viewOrderAddress(<?php echo $getOrder['order_id']; ?>)" id="viewOrderAddress" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xs">view</button>
							<!-- ADDED BY FARHAN 29/04/2018 -->
							<?php 
								if(isset($getOrder['email_address'])){

								$email = explode("@",$getOrder['email_address']);
								if(count($email) > 1){
									if($email[1] == 'www.onlinekaka.com'){
								 ?>

								<button style="margin-top: 4px;" type="button" onclick="guestEmail(<?php echo $getOrder['order_id']; ?>)" id="viewOrderAddress" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-xs">email</button>
								<?php 
										} 
								}
								$sent_email = $getOrder['sent_email'];
								if($sent_email == 1){ ?>

									<button style="margin-top: 4px;" type="button" onclick="guestEmail(<?php echo $getOrder['order_id']; ?>)" id="viewOrderAddress" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs">E-sent</button>

								<?php }
								}

								?>
 							<!-- END BY FARHAN 29/04/2018 -->
 					
						</td>
						 
						<td><?php echo strtoupper($getOrder['payment_type']); ?></td>
				
						<td contenteditable="true" id="reminder<?php echo $getOrder['order_id']; ?>" onclick="reminder(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')"><?php echo $mytime; ?></td>
						<!-- CHANGES BY FARHAN 19 MAY 2018 -->
						
						<td class="text-center">
							<?php if($getOrder['purchase_value'] == 0) { ?>
							<button type="button" onclick="purchaseValue(<?php echo $getOrder['order_id']; ?>)" id="purchase_value<?php echo $getOrder['order_id']; ?>" class="btn btn-default btn-xs">cal</button>
							<?php }else{ ?>

							<div  contenteditable="true" id="purchase_value<?php echo $getOrder['order_id']; ?>" onclick="purchaseValue(<?php echo $getOrder['order_id']; ?>)">
							<?php echo $getOrder['purchase_value']; ?>

							 </div>
							 <?php } ?>
							<div class="text-center">
								<input style="width: 25px; height:25px;" type="checkbox" id="packing" <?php if($getOrder['packing'] == 1){ echo 'checked'; } ?> value="<?php echo $getOrder['packing']; ?>" onclick="packing(<?php echo $getOrder['order_id']; ?>,<?php echo $getOrder['packing']; ?>)">
							</div>
						</td>
							


						
						<!-- END BY FARHAN 19 MAY 2018 -->
						<td contenteditable="true" id="total_w_tax<?php echo $getOrder['order_id']; ?>" onclick="total_w_tax(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')" ><?php echo round($getOrder['total_w_tax']); ?></td>
						
						<td contenteditable="true" id="delivery_charge<?php echo $getOrder['order_id']; ?>" onclick="delivery_charge(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')" >
							
							<?php 	
									$delivery_charge = $getOrder['delivery_charge'] + $getOrder['delivery_charge']*$getOrder['delivery_tax'];
									echo round($delivery_charge);

							?>
								
						</td>
						
						<td>
							<?php 
								$checkPaymentReference  = $CI->ModelOrder->checkPaymentReference($getOrder['order_id']);
								$paypal = $CI->ModelOrder->checkPaypalTransaction($getOrder['order_id']);
								if($paypal != '')
								{
									$checkPaymentReference = $paypal;
								}

							?>

							<p id="status<?php echo $getOrder['order_id'];?>"><?php echo isset($checkPaymentReference) ?  $getOrder['status'].'(Paid)' : $getOrder['status']; ?></p>
					
						<button type="button" class="btn btn-xs btn-primary" onclick="editStatus('<?php echo $getOrder['order_id']; ?>','<?php echo $getOrder['status']; ?>','<?php echo $getOrder['delivery_agent']; ?>','<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')">edit</button>
						</td>
						
						<td>
							<?php 
							if ($getOrder['request_from'] == 'web')
							{
								echo 'web';

							}elseif ($getOrder['request_from'] == 'AndroidApp') {
								
								echo 'Android';

							}else{

								echo 'N/A';
							}
													
							?>
							
							

						</td>
						<td contenteditable="true" id="delivery_agent<?php echo $getOrder['order_id']; ?>" onclick="deliveryAgent(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')"><?php echo strtoupper($getOrder['delivery_agent']); ?></td>

						<td contenteditable="true" id="locality<?php echo $getOrder['order_id']; ?>" onclick="locality(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')"><?php echo strtoupper($getOrder['locality']); ?></td>

						<?php if(date('g:i') != $getOrder['reminder']) { ?>
<!-- 
						<td contenteditable="true" id="reminder<?php echo $getOrder['order_id']; ?>" onblur="reminder(<?php echo $getOrder['order_id']; ?>)"><?php echo $getOrder['reminder']; ?></td> -->
						<?php } ?>
						<?php 
							$time = strtotime($getOrder['date_created']);
							$myFormatedTime = date("g:i", $time);
						 ?>
						<td><?php echo $myFormatedTime; ?></td>
						<td contenteditable="true" id="delivery_time2<?php echo $getOrder['order_id']; ?>" onclick="deliveryTime2(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')"><?php echo !empty($getOrder['delivery_time']) ? $getOrder['delivery_time'] : $getOrder['delivery_time2']; ?></td>

						<td contenteditable="true" id="comment<?php echo $getOrder['order_id']; ?>" onclick="comment(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')">
							<?php echo "<p style='width: 100px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'>".strtoupper($getOrder['comment'])."<p>"; ?>
						</td>
						<td>
							<?php echo $getOrder['app_status'];
							//$latitude = isset($getOrder['latitude']) ? $getOrder['latitude'] : '';
							//$lontitude = isset($getOrder['lontitude']) ? $getOrder['lontitude'] : '';
							 ?>
							<button class='btn btn-xs btn-primary' onclick="suggestedAgents(<?php echo $getOrder['latitude']; ?>,<?php echo $getOrder['lontitude']; ?>,<?php echo $getOrder['order_id']; ?>)">Assign</button>
						
							
						</td>
						<td>
							<button type="button" class="btn btn-primary btn-xs" onclick="sendSms(<?php echo $getOrder['order_id']; ?>,'<?php echo isset($mystatus) ? $mystatus : 'all'; ?>')">sms</button>
							<?php if($getOrder['is_edited'] == 1) { ?>
							<button type="button" class="btn btn-primary btn-xs" style="margin: 4px 6px;" onclick="order_history(<?php echo $getOrder['order_id']; ?>)">H</button>
							<?php } ?>
						</td>
					   <?php $i++; ?>
					</tr>
					<?php } ?>
