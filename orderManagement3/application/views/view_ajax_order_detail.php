<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');



?>
<div id="success"></div>

<?php if ($type == "showAgentHistory"): ?>
	
			<!-- Data Tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bootstrap.min.js"></script>					<!-- Custom Data tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/custom/custom-datatables2.js"></script>
			<table id="basicExample3" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="1%">#</th>
													<th>Merchant_name</th>
													<th>Items</th>
													<th>Total</th>
													<th>Contact</th>
													<th width="1%">order_id</th>
													<th>Name</th>
													<th>Location</th>
													<th width="1%">Status</th>
													<th>Create Date</th>
													
												</tr>
											</thead>
											<tbody>

												<?php $i =0; foreach ($data as $row): $i++;?>
												
													<tr>
														<td><?=$i?></td>
														<td><?=$row['merchant_name']?></td>
														<td><?=$row['items']?></td>
														<td><?=$row['total']?></td>
														<td><?=$row['contact']?></td>
														<td><?=$row['order_id']?></td>
														<td><?=$row['name']?></td>
														<td><?=$row['location']?></td>
														<td><?=$row['status']?></td>
														<td><?=$row['create_date']?></td>
														
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
<?php endif ?>
<?php if ($type == "showAgentOrders"): ?>

		
			<!-- Data Tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bootstrap.min.js"></script>					<!-- Custom Data tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/custom/custom-datatables2.js"></script>
		

		
								
										<table id="basicExample2" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="1%">#</th>
													<th>Order ID</th>
													<th>total_w_tax</th>
													<th>Status</th>
													<th>Delivery time</th>
													<th>Delivery date</th>
													
												</tr>
											</thead>
											<tbody>

												<?php $i =0; foreach ($data as $row): $i++;?>
												
													<tr>
														<td><?=$i?></td>
														<td><?=$row['order_id']?></td>
														<td><?=$row['total_w_tax']?></td>
														<td><?=$row['status']?></td>
														<td><?=$row['delivery_time2']?></td>
														<td><?=$row['delivery_date']?></td>
														
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
					
<?php endif ?>
<?php if ($type == "showProfile"): ?>
<!-- <pre>
	<?php print_r($agent_data) ;?>
</pre> -->

	<form>
		<div class="form-group row">
			<div class="col-md-12">
				<input type="hidden" id="agent_id" value="<?=$agent_data['id']?>">
				<label class="col-form-label">Name</label>
				<input type="text" class="form-control input-sm" id="name" value="<?=$agent_data['name']?>">
			</div>
				
							
		</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Username</label>
								<input type="text" id="username" value="<?=$agent_data['username']?>" class="input-sm form-control"></div>
							<div class="col-md-6">
								<label class="col-form-label">Password</label>
								<input type="password" value="<?=$agent_data['password']?>" id="password" class="input-sm form-control">
							</div>
						</div>	
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Latitude</label>
								<input type="text" value="<?=$agent_data['lati']?>" id="lati" class="input-sm form-control"></div>
							<div class="col-md-6">
								<label class="col-form-label">Longitude</label>
								<input type="text" value="<?=$agent_data['longi']?>" id="longi" class="input-sm form-control">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Mobile</label>
								<input type="number"  value="<?=$agent_data['mobile']?>" id="mobile" class="input-sm form-control">
							</div>
							<div class="col-md-6">
			<label class="col-form-label">Status</label>
				<select id="status" class="form-control input-sm">
						<option value="true" <?php if($agent_data['status'] == 'true') echo " selected" ?> >Active</option>
						<option value="false" <?php if($agent_data['status'] == 'false') echo "selected" ?> >Disable</option>
				</select>
			</div>
						</div>
							<button type="button" class="btn btn-block btn-outline-success btn-sm mx-auto myBtn" onclick="updateProfile()">Update Profile</button>	
														
	</form>
<?php endif ?>
<script type="text/javascript">
	function updateProfile()
	{
		var agent_id = $("#agent_id").val();
		var name = $("#name").val().trim();
		var username= $("#username").val().trim();
		var password= $("#password").val().trim();
		var status = $("#status").val();
		var lati = $("#lati").val().trim();
		var longi = $("#longi").val().trim();
		var mobile = $("#mobile").val().trim();

		$.post("<?php echo base_url(); ?>order/updateAgentProfile",{agent_id,name,username,password,status,lati,longi,mobile}, 
			function(data){$('#myModal').modal('hide');window.location.href=window.location.pathname; });
		
	}
</script>
							<?php if($type == 'sms') { ?>
							<div class="row">
								<div class="col-md-8">
						         	 <div class="form-group text-center">
						         	<textarea rows="15" cols="15" class="form-control" id="smsText"><?php 
										$merchant_name = $orderById->restaurant_name;
										echo trim($merchant_name)."\r\n"; 
										
						         		$totalOrders = json_decode($orderById->json_details);
						         		$orders2 ='';

						         		foreach($totalOrders as $totalOrder)

						         		{
						         			$item = $CI->ModelOrder->getItemById2($totalOrder->item_id);

						         			$orders2 .= $totalOrder->qty." - ".$item->item_name."\r\n";
						         			
						         			$order_descriptions = $item->item_description;

						         			$checkProductDescription = $CI->ModelOrder->checkProductDescription($totalOrder->merchant_id);

							         		if($order_descriptions != '' && $checkProductDescription == '1')
							         		{
							         			$description = explode(",",$order_descriptions);

							         			foreach ($description as $des) {
							         				
							         				$orders2 .= "   - ".$des."\r\n";
							         			}
						         			}


						         		}

						         		echo $orders2;

						         		echo "\r\n"."Order Id - $order_id"."\r\n".$orderById->first_name." ".$orderById->last_name."\r\n";
						         		
						         		$name = $orderById->first_name." ".$orderById->last_name;

						         		$address = $CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id);
						         		
						         		
						        		$location =  trim($address->street." ".$address->city." ".$address->state." ".$address->location_name)."\r\n"."\n";
						        		echo $location;
						         		$contact = $orderById->contact_phone."\r\n";

						         		echo str_replace("+91","",$contact);
						         		$total = $orderById->total_w_tax;
						         		echo "Total - Rs. ".round($total);

						         		//$orders2 = implode(',', $orders2);

						         	?></textarea>	
						<input type="hidden" id="merchant_name" value="<?=$merchant_name?>">
						<input type="hidden" id="items" value="<?=$orders2?>">
						<input type="hidden" id="total" value="<?=$total?>">
						<input type="hidden" id="contact" value="<?=$contact?>">
						<input type="hidden" id="order_id" value="<?=$order_id?>">
						<input type="hidden" id="name" value="<?=$name?>">
						<input type="hidden" id="location" value="<?=$location?>">
						<input type="hidden" id="payment_type" value="<?=$orderById->payment_type?>">
						
						         	</div>
						         	</div>
						         	<div class="col-md-4">
							         	<div class="form-group" style="display:none">
											<select id="mobile" class="form-control" name="mobile">
											<?php 
											if ($worker_flag == "A") 
												$getDeliveryBoys = $this->ModelOrder->getAvailableAgentsAA();
											elseif($worker_flag == "B") 
												$getDeliveryBoys = $this->ModelOrder->getAvailableAgentsBB();
											else 
												$getDeliveryBoys = $this->ModelOrder->getAvailableAgents();

											?>

											
											
											
											<?php foreach(sort($getDeliveryBoys) as $getDeliveryBoy) { ?>
												<option value="<?php echo $getDeliveryBoy['mobile']; ?>"><?php echo $getDeliveryBoy['name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group text-center" style="display:none">
											<button type="button" class="btn btn-primary" onclick="sendSmsApi(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')" id="sendButton">Send</button>
										</div>
									<?php
									 $app_status = $this->ModelOrder->getStatus2($order_id); 
									 if($app_status['app_status'] != 'seen')
									 {
									 	?>
									 	<?php if ($app_status['app_status'] == 'unseen') 
									 		$app_status_flag = "old";
									 		else
									 		$app_status_flag = "new";	
									 	 ?>
									 	 <input type="hidden" id="app_status_flag" value="<?=$app_status_flag;?>">
									 	<div class="form-group">
											<select id="agent" class="form-control" name="agent">
											<?php 
												// $getDeliveryBoys = $CI->ModelOrder->getAvailableAgents(); ?>

												<?php foreach($getDeliveryBoys as $getDeliveryBoy) { ?>
												<option value="<?php echo $getDeliveryBoy['agent_id']; ?>"><?php echo $getDeliveryBoy['name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group text-center">
											<button type="button" class="btn btn-primary" onclick="sentToApp()" id="sendButton">Send to App</button>
										</div>
									 	<?php
									 }
									 else
									 {
									     ?><div class='alert alert-sm alert-success'>Order received and seen</div>
									     <button type="button" class="btn btn-danger" onclick="unAssign()">Re-Assign</button>
									     <?php
									 }
									?>
										
									</div>
								
						
							</div>
							<?php } ?>



							<?php if($type == 'comment') { ?>
								
							  	<div class="row">
									<div class="col-md-6 col-md-offset-3">	
										<div class="form-group">
										<textarea id="comment" class="form-control" ><?php echo trim($comment); ?></textarea>
										<!-- <input type="text" id="comment" class="form-control" value="<?php echo $comment; ?>"> -->
									</div>
									</div>
								
								
									<div class="col-md-6 col-md-offset-3">	
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="commentUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
								</div>
							
							<?php } ?>

							<?php if($type == 'delivery_time2') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="delivery_time2" class="form-control" value="<?php echo trim($delivery_time2); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryTimeUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'reminder') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<input type="text" id="reminder" class="form-control" value="<?php echo trim($reminder); ?>">
									</div>
									<div class="form-group text-center">
										<button type="button" class="btn btn-success myBtn" onclick="reminderUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'total_w_tax') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="total_w_tax" class="form-control" value="<?php echo trim($total_w_tax); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="totalWTaxUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>
							
							<?php if($type == 'delivery_charge') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="delivery_charge" class="form-control" value="<?php echo trim($delivery_charge); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryChargeUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'delivery_agent') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<select id="delivery_agent" class="form-control" name="delivery_agent">
							<?php 


		//$password = $this->session->userdata('password');
		// if ($password == "black999star!") {
		// 	$agents = $CI->ModelOrder->getAvailableAgents();
		// }
		// elseif ($password == "victor999") {
			
		// 	$agents = $CI->ModelOrder->getAvailableAgentsA();
		// }elseif ($password == "charge777") {
		// 	$agents = $CI->ModelOrder->getAvailableAgentsB();
		// }
		//
		$user = $this->session->userdata('order_user');
		$roles = array('Super Admin','Manager','Junior Manager');
		if ($user->panel == 'Super Panel' && in_array($user->role,$roles)) {
				$agents = $CI->ModelOrder->getAvailableAgents();
		}elseif($user->panel == 'Panel A' && in_array($user->role,$roles)){

				$agents = $CI->ModelOrder->getAvailableAgentsA();
		}elseif($user->panel == 'Panel B' && in_array($user->role,$roles)){
			$agents = $CI->ModelOrder->getAvailableAgentsB();

		}					
		?>
							


									<?php foreach($agents as $agent) { ?>
												<option value="<?php echo $agent['name']; ?>" <?php if($agent['name'] == $delivery_agent){ echo 'selected';} ?>><?php echo $agent['name']; ?></option>
									<?php } ?>
									</select>
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryAgentUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'locality') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<select id="locality" class="form-control" name="locality">
									<?php $localities = $CI->ModelOrder->getLocality(); ?>	

									<option></option>
									<?php foreach($localities as $locality) { ?>
												<option value="<?php echo $locality['locality_name']; ?>" <?php if($locality['locality_name'] == $selected_locality){ echo 'selected';} ?>><?php echo $locality['locality_name']; ?></option>
									<?php } ?>
									</select>
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="localityUpdate(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>')">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<!-- CHANGES BY FARHAN 19 MAY 2018 -->
							<?php if($type == 'purchase_value') {



								if($itemPurchaseValue->purchase_value == 0){
									$orders2 ='';
										$items = json_decode($itemPurchaseValue->json_details);

								$total = 0;

								$pv_status = $CI->ModelOrder->getOptionPvStatus($itemPurchaseValue->merchant_id);

										?>
									<table class="table table-striped">
												<thead>
													<tr>
													<th>ITEM</th>
													<th>PV</th>
													</tr>
												</thead>
												<tbody>
									<?php
									$myarray =array();
								if($pv_status->option_value == '1')
								{

								
											$merchant_tax = $CI->ModelOrder->getOptionMerchantTax($itemPurchaseValue->merchant_id);
											
											if($merchant_tax->option_value == '')
											{
										
											
												foreach($items as $key => $item) {


														
														$pv_percentage = $CI->ModelOrder->getOptionPvPercentage($itemPurchaseValue->merchant_id);
														$purchase_value = $item->price - ($item->price*$pv_percentage->option_value/100);
														$item_name = $item->qty." x ".$CI->ModelOrder->getItemById($item->item_id);
														$purchaseArray = array(

															"item_price" => $item->qty*$purchase_value,
															"item_name"  => $item_name
														);
														array_push($myarray, $purchaseArray);

														?>

															<tr>

																<td><b><?php echo $item_name; ?></b></td>
																<td>₹ <b><?php echo $item->qty*$purchase_value; ?></b></td>
															</tr>

														<?php
														if(isset($item->addon_ids))
														{	
															$subItemArray =[];
															$addon_price = 0;

															foreach($item->addon_ids as $addon_id)
															{
																// $val_subs=explode('|',$item);
																
																$addon = $CI->ModelOrder->getAddonItemPrice($addon_id);
																$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
																$addon_price +=  $item->qty*intval($addon_price2);
																$subItemArray[] = array(

																	"sub_item_price" => $item->qty*$addon_price2,
																	"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
																);
																?>
																<tr>
																	<td>&nbsp;&nbsp;&nbsp;<?php echo  $item->qty." x ".$addon->sub_item_name; ?></td>
																	<td>₹ <?php echo  $item->qty*$addon_price2; ?></td>
																</tr>
																<?php

															$myarray[$key]['sub_items'] = $subItemArray;
															}
															
															
														$total = $total + $addon_price;
														
														}

														$total += $item->qty*$purchase_value;

														// $purchase[] = $item->qty*$purchase_value;
														
														// $orders2 .= $item->qty." - ".$CI->ModelOrder->getItemById($item->item_id).",";
														
												}
												
											

											}else{

												foreach($items as $item) {

														
														$pv_percentage = $CI->ModelOrder->getOptionPvPercentage($itemPurchaseValue->merchant_id);
														$purchase_value = $item->price - ($item->price*$pv_percentage->option_value/100);
														$purchase_value = $purchase_value + ($purchase_value*$merchant_tax->option_value/100);
														$item_name = $item->qty." x ".$CI->ModelOrder->getItemById($item->item_id);
														$purchaseArray = array(

															"item_price" => $item->qty*$purchase_value,
															"item_name"  => $item_name
														);
														array_push($myarray, $purchaseArray);

														?>

															<tr>
																<td><b><?php echo $item_name; ?></b></td>
																<td>₹ <b><?php echo $item->qty*$purchase_value; ?></b></td>
															</tr>

														<?php
														if(isset($item->addon_ids))
														{	
															$subItemArray =[];
															$addon_price = 0;
															foreach($item->addon_ids as $addon_id)
															{														
																
																$addon = $CI->ModelOrder->getAddonItemPrice($addon_id);
																$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
																$addon_price2 = $addon_price2 + ($addon_price2*$merchant_tax->option_value/100);
																$addon_price += intval($addon_price2);
																$subItemArray[] = array(

																	"sub_item_price" => $item->qty*$addon_price2,
																	"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
																);
																?>
																<tr>
																	<td>&nbsp;&nbsp;&nbsp;<?php echo  $item->qty." x ".$addon->sub_item_name; ?></td>
																	<td>₹ <?php echo $item->qty*$addon_price2; ?></td>
																</tr>
																<?php
																$myarray[$key]['sub_items'] = $subItemArray;
															
															}
															$total = $total + $addon_price;
													
														}
														$total += $item->qty*$purchase_value;

														// $purchase[] = $item->qty*$purchase_value;
														// $orders2 .= $item->qty." - ".$CI->ModelOrder->getItemById($item->item_id).",";
														

												}

											}



								}else{


										foreach($items as $key => $item) {
											$purchase_value = $CI->ModelOrder->getPurchaseValueFromItem($item->item_id);
										
											$item_name = $item->qty." x ".$CI->ModelOrder->getItemById($item->item_id);
											$purchaseArray = array(

															"item_price" => $item->qty*$purchase_value,
															"item_name"  => $item_name
														);
											array_push($myarray, $purchaseArray);
												?>
												<tr>

																<td><?php echo $item_name; ?></td>
																<td>₹ <?php echo $item->qty*$purchase_value; ?></td>
															</tr>
												<?php
												
												
												if(isset($item->addon_ids))
														{	
															$subItemArray =[];
															$addon_price = 0;
															foreach($item->addon_ids as $addon_id)
															{
																// $val_subs=explode('|',$item);
																
																$addon = $CI->ModelOrder->getAddonItemPrice($addon_id);
																$addon_price2 = $addon->price - ($addon->price*$pv_percentage->option_value/100);
																$addon_price += $item->qty*intval($addon_price2);
																$subItemArray[] = array(

																	"sub_item_price" => $item->qty*$addon_price2,
																	"sub_item_name"  => $item->qty." x ".$addon->sub_item_name
																);
																?>
																<tr>
																	<td>&nbsp;&nbsp;&nbsp;<?php echo  $item->qty." x ".$addon->sub_item_name; ?></td>
																	<td>₹ <?php echo $item->qty*$addon_price2; ?></td>
																</tr>
																<?php
																$myarray[$key]['sub_items'] = $subItemArray;
															
															}
															$total = $total + $addon_price;
													
														}
												$total += $item->qty*$purchase_value;

												// $purchase[] = $item->qty*$purchase_value;
												// $orders2 .= $item->qty." - ".$CI->ModelOrder->getItemById($item->item_id).",";
												

										}			

								}

												?>
												</tbody>
												<tfoot>
													<tr>
														<td>Total</td>
														<td>₹ <b><?php echo round($total); ?></b></td>
													</tr>
												</tfoot>
												</table>
												<?php	
								// $orders2 = rtrim($orders2,',');
								// $decode_item = explode(",",$orders2);
								// $purchase_items = array_combine($decode_item, $purchase);

								// echo "<pre>";
								// print_r($purchase_items);
								// die();

								$data = array(


									"purchase_item"  => json_encode($myarray),
  									"purchase_value" => $total

								);

								$CI->ModelOrder->purchaseValueUpdate($data,$order_id);
							
								}else {


										$total = $itemPurchaseValue->purchase_value;

										$items = json_decode($itemPurchaseValue->purchase_item);
			

										?>

										<table class="table table-striped">
												<thead>
													<tr>
													<th>ITEM</th>
													<th>PV</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach($items as $item) { ?>
														<tr>
															<td><b><?php echo  $item->item_name; ?></b></td>
															<td>₹ <b><?php echo $item->item_price; ?></b></td>
														</tr>
														<?php 
															if(isset($item->sub_items))
																	{	
																		
																		
																		foreach($item->sub_items as $sub_item)
																		{
																			// $val_subs=explode('|',$item);
																			
																			
																			?>
																			<tr>
																				<td>&nbsp;&nbsp;&nbsp;<?php echo  $sub_item->sub_item_name; ?></td>
																				<td>₹ <?php echo $sub_item->sub_item_price; ?></td>
																			</tr>
																			<?php
																			
																		
																		}
																		
																
																	}

														 ?>

													<?php } ?>
												</tbody>
												<tfoot>
													<tr>
														<td><b>Total</b></td>
														<td>₹ <b><?php echo round($total); ?></b></td>
													</tr>
												</tfoot>
												</table>
										<?php
										
								}

								?>

											

												

									<div class="row">

									<div class="col-md-6 col-md-offset-3">

									<div class="form-group">

									<input type="text" id="purchase_value" class="form-control" value="<?php echo round($total); ?>">

									</div>

									<div class="form-group text-center">

									<button type="button" class="btn btn-success myBtn" onclick="purchaseValueUpdate(<?php echo $order_id; ?>)">Submit</button>

									</div>

									</div>

									</div>
<!-- 									<div class="row">
										<div class="col-md-8 col-md-offset-2">
											<table class="table table-striped">
												<thead>
													<tr>
													<th>ITEM</th>
													<th>PV</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$x = 0; 
													foreach($purchase_items as $item => $value){
													$x+= $value; 
														?>

													<tr>
														<td><?php echo $item; ?></td>
														<td><?php echo "₹ ".$value; ?></td>
													</tr>

													<?php } ?>
													
												</tbody>
												<tfoot>
													<tr>
														<td><b>Total</b></td>
														<td><?php echo "₹ ".round($x); ?></td>
													</tr>
												</tfoot>
											</table>
											
										</div>

									</div> -->
							<?php } ?>
							<!-- END BY FARHAN 19 MAY 2018 -->

							<!-- ADDED BY FARHAN 18 JUNE 2018 -->


							<?php if($type == 'suggestedAgents'){ ?>
							<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.min.js"></script>
							<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bootstrap.min.js"></script>	
							<script type="text/javascript">
								
									    $('#assignTable').DataTable({

									    	 "order": [[ 3, "asc" ]],
									    	 bFilter: false,
									    	 "bPaginate": false
									    });
									    
								
							</script>
							<?php 
							$totalOrders = json_decode($orderById->json_details);
							$orders2 ='';
							foreach($totalOrders as $totalOrder)

						         		{
						         			$item = $CI->ModelOrder->getItemById2($totalOrder->item_id);

						         			$orders2 .= $totalOrder->qty." - ".$item->item_name."\r\n";
						         			
						         			$order_descriptions = $item->item_description;

							         		if($order_descriptions != '')
							         		{
							         			$description = explode(",",$order_descriptions);

							         			foreach ($description as $des) {
							         				
							         				$orders2 .= "   - ".$des."\r\n";
							         			}
						         			}


						         		}
							$merchant_name = $orderById->restaurant_name;
							$total = $orderById->total_w_tax;
							$contact = $orderById->contact_phone."\r\n";
							$name = $orderById->first_name." ".$orderById->last_name;
							
							$address = $CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id);

							$location =  trim($address->street." ".$address->city." ".$address->state." ".$address->location_name)."\r\n"."\n";
							?>
							<input type="hidden" id="merchant_name" value="<?=$merchant_name?>">

							<input type="hidden" id="items" value="<?=$orders2?>">

							<input type="hidden" id="total" value="<?=$total?>">

							<input type="hidden" id="contact" value="<?=$contact?>">

							<input type="hidden" id="order_id" value="<?=$order_id?>">

							<input type="hidden" id="name" value="<?=$name?>">

							<input type="hidden" id="location" value="<?=$location?>">

							<input type="hidden" id="payment_type" value="<?=$orderById->payment_type?>">
							<?php $app_status = $this->ModelOrder->getStatus2($order_id); 

							if($app_status['app_status'] != 'seen')

							{

								?>

								<?php if ($app_status['app_status'] == 'unseen') 

								$app_status_flag = "old";

								else

									$app_status_flag = "new";	

								?>

								<input type="hidden" id="app_status_flag" value="<?=$app_status_flag;?>">

								<?php } ?>


								<?php 
								
								foreach($assign_time as $time)
								{	

									$deliveryAgent[] = $time->delivery_agent;


								}
									
								?>

								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped table-hover" id="assignTable">
											<thead>
												<tr>
													<th>Sr No</th>
													<th width="125">Agent Name</th>
													<td></td>
													<th>Action</th>
													<th>Distance</th>
													<th>Time</th>
													<th>Address</th>
													<th>Destination</th>
												</tr>
											</thead>
											<tbody>

												<?php 
												$i = 0;


							
												foreach($suggestedAgents as $key => $agents) { 
													$no_of_orders = $CI->ModelOrder->getNoOfOrders($agents['name']);

													$no_of_orders = $no_of_orders->no_of_orders;
													

													if(!empty($deliveryAgent)){
														if(in_array($agents['name'], $deliveryAgent))
														{
															continue;
														}
													}

												$agentStatus = @$CI->ModelOrder->getAgentStatus($agents['name'])->status;

												if($agentStatus == 'true'){
													$as = 'On';
													$statusBg = "background:green";}
													else{
													$as ='Off';
													$statusBg = "background:red";}

											
													?>

													<tr>
														<td><?php echo $key + 1; ?></td>
														<td style="border: 3px solid black;color:black;padding:6px; padding-top:12px;;background-color: <?php echo deliveryBoysColor($no_of_orders); ?>"><?php echo $agents['name']; ?></td>
														 <td style="border: 3px solid black;color:white;<?=$statusBg?>;padding: 12px 12px 12px 12px;border-right: 5px solid black;"><?=$as?></td>
														<td><?php  if($app_status['app_status'] != 'seen') { ?>

															<button type="button" class="btn btn-primary" onclick="sentToApp2(<?php echo $agents['agent_id'] ?>)" id="sendButton">Send to App</button>

															<?php }else { 
																if($agents['name'] == $app_status['delivery_agent'])
																{ ?>
														<!-- 	<div class='alert alert-sm alert-success'>Order received and seen</div> -->
															<button type="button" class="btn btn-danger" onclick="unAssign()">Re-Assign</button>
															<?php }} ?></td>
															<td style="vertical-align: middle;">
																<?php echo $distancesTimes['rows'][0]['elements'][$key]['distance']['text']*1.6." Km"; ?>
															</td>
															<td style="vertical-align: middle;">
																<?php echo $distancesTimes['rows'][0]['elements'][$key]['duration']['text']; ?>
															</td>
															<td style="vertical-align: middle;">
																<?php 
																$variable = $distancesTimes['destination_addresses'][$key];
																echo substr($variable, 0, strpos($variable, ", Lucknow")); 
																?>
															</td>
															<td>
																<?php 
																$locality = array_column($CI->ModelOrder->getLocalityByAgent($agents['name']),'locality'); 
																if($no_of_orders != 0)
																{

																	echo implode(', ', $locality);
																}
																				
																?>
															</td>
									
												</tr>
												<?php $i++; ?>
												<?php } ?>

											</tbody>

										</table>
										<input type="hidden" id="getLoops" value="<?php echo $i; ?>">

									</div>

								</div>


								<?php } ?>



							<!-- END BY FARHAN 18 JUNE 2018 -->

<!-- ADDED BY FARHAN -->
<style type="text/css">
	#itemTable {  
    border-collapse: collapse;
}
#itemTable th, #itemTable td {
    padding: 3px;
}
.highlight{

	background: #00ff00;
}
.highlight2{

	background: #7FFFD4;
}

</style>
<!-- END BY FARHAN -->

							<?php if($type == 'item') { 
								

								if($CI->ModelOrder->getPackaging($orderById->merchant_id)->option_value != '')
								{
									$packaging = $orderById->packaging;
								}
								$offer_percentage = $CI->ModelOrder->offer_percentage($orderById->merchant_id);
								if($offer_percentage != '')
								{
									$discount = round($offer_percentage->offer_percentage);
									
									$offer_status = $offer_percentage->status;

									$valid_to = $offer_percentage->valid_to;


									$todays_date = date("Y-m-d");

									$today = strtotime($todays_date);
									$expiration_date = strtotime($valid_to);

									if ($expiration_date > $today && $offer_status =='publish' ) {

									    $sub_total = 0;
										$items = json_decode($orderById->json_details);
												foreach($items as $item) {

													$sub_total = $sub_total + $item->price*$item->qty;

												}
										$discountValue = ($sub_total)*$discount/100; 

									} else {
									    
										$discountValue = 'no';
									}
								

									
								
								}
								
								?>
							<div class="row">
								<div class="col-md-12">
								<!-- ADDED BY FARHAN -->
									<table id="itemTable" class="table">
										<tr>
											<th>Name:</th>
											<td><?php echo $CI->ModelOrder->getClientById($orderById->client_id)[0]['first_name']." ".$CI->ModelOrder->getClientById($orderById->client_id)[0]['last_name']; ?></td>
										</tr>
										<tr>
											<th>Merchant Name:</th>
											<td><?php echo $orderById->restaurant_name; ?></td>
										</tr>
										<tr>
											<?php 
												$merchantAddress = $CI->ModelOrder->getMerchantById2($orderById->merchant_id);
											 ?>
											<th>Merchant Address:</th>
											<td><?php echo $merchantAddress->street." ".$merchantAddress->city." ".$merchantAddress->state." ".$merchantAddress->post_code; ?></td>
										</tr>
										<tr class="highlight2">
											<th>Search Address:</th>
											<td><?php echo $orderById->search_address; ?></td>
										</tr>
										<tr class="highlight">
											<th>Deliver to:</th>
											<td><?php echo '<span contenteditable="true" onblur="updateClientStreet('.$orderById->order_id.')" id="clientStreet'.$orderById->order_id.'">'.$orderById->street.'</span> '.$orderById->city.' '.$orderById->state.' '.$orderById->post_code; ?></td>
										</tr>
										<tr class="highlight">
											<th>Landmark:</th>
											<td><?php echo $orderById->location_name; ?></td>
										</tr>
										<tr class="highlight">
											<th>Contact Number:</th>
											<td><?php echo $orderById->contact_phone; ?></td>
										</tr>
										<tr class="highlight">
											<th>Delivery Instruction:</th>
											<td><?php echo $orderById->delivery_instruction; ?></td>
										</tr>
										<tr>
											<th>Payment Type:</th>
											<td><?php echo $orderById->payment_type; ?></td>
										</tr>
										<tr>
											<th>TRN Date:</th>
											<td><?php echo $orderById->delivery_date." ".$orderById->delivery_time; ?></td>
										</tr>
										<tr>
											<th>Delivery Date:</th>
											<td><?php echo $orderById->delivery_date; ?></td>
										</tr>
										<?php 
											$items = json_decode($orderById->json_details);
											foreach($items as $item) {
												$merchant_id = $item->merchant_id;

												
										?>
										<tr id="<?php echo $item->item_id; ?>">
											<td width="50%">
												<?php echo $item->qty." ".$CI->ModelOrder->getItemById($item->item_id)."<br>"; ?>
												<?php 

												if(!empty($item->sub_item))
													{
													foreach($item->sub_item as $key => $sub_item)
													{
														echo '<span style="margin-left:25px;"><b>'.trim($CI->ModelOrder->getSubCategory($key),'"').'</b></span>';
														echo '<ul style="margin-left:35px;">';
														foreach($sub_item as $sub)
														{
															$subArray = explode("|",$sub);

															$rupees = !empty($subArray[1]) ? " Rs.".$subArray[1] : "";

															echo '<li>'.$CI->ModelOrder->getSubCategoryItem($subArray[0]).$rupees.'</li>';

														}
														echo '</ul>';
													}
												} 

											 ?>	
											</td>
											<td>
												<?php echo "₹ ".$item->price*$item->qty; ?> 
											</td>
										</tr>
										
										<?php } ?>
										<tr>
											<th>Item</th>
											<td><a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="addItem(<?php echo $orderById->order_id; ?>,<?php echo $merchant_id; ?>)" style="float: right;    margin-right: 5px;">Changes in Item</a></td>
										</tr>
										<?php
										if(isset($discountValue) && $discountValue != 'no'){?>
										<tr>
											<th>Discount <?php echo isset($discount) ? $discount."%" : '0%'; ?></th>
											<td><?php echo isset($discountValue) ? '₹ '.round($discountValue) : '₹ 0'; ?></td>
										</tr>
										<?php } ?>
										<?php if(!empty($orderById->voucher_code)): ?>
											<tr>
											<th>Voucher Discount (<?php echo $orderById->voucher_code; ?>)</th>
											<td><?php echo "₹ ".round($orderById->voucher_amount); ?></td>
										</tr>
										<?php endif; ?>
										<tr>
											<th>Sub Total:</th>
											<td id="subtotal<?php echo $orderById->order_id; ?>"><?php echo "₹ ".round($orderById->sub_total); ?></td>
										</tr>
										<tr>
											<th>Delivery Fee:</th>
											<td id="deliveryFee<?php echo $orderById->order_id; ?>"><?php echo "₹ ".round($orderById->delivery_charge); ?></td>
										</tr>
										<tr>
											<th>Packaging:</th>
											<td id="packaging<?php echo $orderById->order_id; ?>"><?php echo isset($packaging) ? '₹ '.round($packaging) : '₹ 0'; ?></td>
										</tr>
										<tr>
											<th style="cursor: pointer;" onclick="taxes()">Taxes: <i class="fa fa-caret-down"></i></th>
											<td id="taxable_total<?php echo $orderById->order_id; ?>"><?php echo "₹ ".round($orderById->taxable_total); ?></td>
										</tr>
										<tr class="sub_tax" style="display: none;">
											<td style="cursor: pointer;">Restaurant Tax:</td>
											<td id="taxable_total<?php echo $orderById->order_id; ?>">

												<?php 
												$delivery_tax = $CI->ModelOrder->getOptionDeliveryTax($merchant_id)->option_value;
													if(!empty($delivery_tax))
													{	
														$delivery_tax2 = $delivery_tax/100*$orderById->delivery_charge;
														echo round($orderById->taxable_total - $delivery_tax2);
													}else{

														echo $orderById->taxable_total;
													}
												 ?>
													
											</td>
										</tr>
										<?php 
									
										if(!empty($delivery_tax))
											{
										 ?>
										<tr class="sub_tax" style="display: none;">
											<td style="cursor: pointer;">Delivery Tax:</td>
											<td id="taxable_total<?php echo $orderById->order_id; ?>">
												<?php
													
													$delivery_tax2 = $delivery_tax/100*$orderById->delivery_charge;
												
													echo round($delivery_tax2);

												?>
											</td>
										</tr>
										<?php } ?>
										<tr>
											<th>Total:</th>
											<td id="total_w_tax<?php echo $orderById->order_id; ?>"><?php echo "₹ ".round($orderById->total_w_tax); ?></td>
										</tr>
									</table>
									<!-- END BY FARHAN -->
								</div>
					
							</div>
							<?php } ?>

							<?php if($type == 'status') { ?>
							<div class="row">
									<div class="col-md-5 col-md-offset-1">
									<div class="form-group">
										<input type="hidden" id="agentName<?php echo $order_id; ?>" value="<?php echo $agentName; ?>">
										<input type="hidden" id="delivery_agent_color" value="<?php echo trim($orderById->delivery_agent) ?>">
										<select id="status" class="form-control" name="status">
											<option value="Pending" <?php if($status == 'Pending'){ echo 'selected';} ?>>Pending</option>
											<option value="Confirmed" <?php if($status == 'Confirmed'){ echo 'selected';} ?>>Confirmed</option>
											<option value="Processing" <?php if($status == 'Processing'){ echo 'selected';} ?>>Processing</option>
											<option value="Delivered" <?php if($status == 'Delivered'){ echo 'selected';} ?>>Delivered</option>
											<option value="Cancelled" <?php if($status == 'Cancelled'){ echo 'selected';} ?>>Cancelled</option>
											<option value="On Hold" <?php if($status == 'On Hold'){ echo 'selected';} ?>>On Hold</option>
										</select>
									</div>
									</div>
									<div class="col-md-5">
									<div class="form-group">
									<input type="text" class="form-control" name="sms_contact_phone" id="sms_contact_phone" value="<?php echo $orderById->contact_phone; ?>">
									</div>
									</div>
									
									<div class="col-md-10 col-md-offset-1">
										<div class="form-group">
											<textarea class="form-control" id="sendSmsStatusMessage" rows="4" placeholder="Enter Message...">Dear <?php echo $orderById->first_name; ?>, your order <?php echo "#".$order_id; ?> is confirmed. It will be delivered to you in approximately 60 minutes</textarea>
												<label><input type="checkbox" name="checkSms" id="checkSms" value="yes"> Check if you want to send sms.</label>
										</div>
									</div>

									<div class="col-md-12 text-center">
								
									<div class="form-group">
										<button type="button" class="btn btn-success" onclick="orderStatus(<?php echo $order_id; ?>,'<?php echo $mystatus; ?>','<?php echo $orderById->first_name; ?>',<?php echo $orderById->client_id; ?>)">Submit</button>
										
									</div>
									</div>
							</div>
							<?php } ?>
	<!-- ADDED BY FARHAN 29/04/2018 -->
							<?php if ($type == "guestCheckout"): ?>
				
								<form>
									
									<div class="form-group">
										<input type="hidden" id="client_id<?php echo $order_id?>" value="<?php echo $orderById->client_id; ?>">
										<label>Subject</label>
										<input type="text" value="Thank you for your order!"  id="guestSubject<?php echo $order_id?>" class="form-control" required="">
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="email"   id="guestEmail<?php echo $order_id?>" class="form-control" placeholder="Enter Email" required="">
									</div>
									<div class="form-group">
										<label>Message</label>
										<textarea required="" id="guestMessage<?php echo $order_id?>" class="guestMessage"><div style="text-align: center;"><img src="https://www.onlinekaka.com/upload/1476254450-1475737169-kaka-logo-(1).png" width="200"></div><p>Dear, <?php echo $orderById->first_name." ".$orderById->last_name; ?></p><p> Thank you for placing your order at OnlineKaka. We hope you enjoy your food! Your order number is <?php echo $order_id ?>. We have included your order receipt and delivery details below:</p><p><b>Order Details</b></p><table style="text-align: left">
										<tr>
											<th>Customer Name:</th>
											<td><?php echo $CI->ModelOrder->getClientById($orderById->client_id)[0]['first_name']." ".$CI->ModelOrder->getClientById($orderById->client_id)[0]['last_name']; ?></td>
										</tr>
										<tr>
											<th>Merchant Name:</th>
											<td><?php echo $orderById->restaurant_name; ?></td>
										</tr>
										<tr>
											<th>Telephone:</th>
											<td><?php echo $orderById->restaurant_phone; ?></td>
										</tr>
										<tr>
											<?php 
												$merchantAddress = $CI->ModelOrder->getMerchantById2($orderById->merchant_id);
											 ?>
											<th>Merchant Address:</th>
											<td><?php echo $merchantAddress->street." ".$merchantAddress->city." ".$merchantAddress->state." ".$merchantAddress->post_code; ?></td>
										</tr>
										<tr>
											<th>TRN Type:</th>
											<td><?php echo $orderById->trans_type; ?></td>
										</tr>
										<tr>
											<th>Payment Type:</th>
											<td><?php echo $orderById->payment_type; ?></td>
										</tr>
										<tr>
											<th>Reference#:</th>
											<td><?php echo $orderById->order_id; ?></td>
										</tr>
										<tr>
											<th>TRN Date:</th>
											<td><?php echo $orderById->delivery_date." ".$orderById->delivery_time; ?></td>
										</tr>
										<tr>
											<th>Delivery Date:</th>
											<td><?php echo $orderById->delivery_date; ?></td>
										</tr>
										<tr>
											<th>Deliver to:</th>
											<td><?php echo '<span contenteditable="true" onblur="updateClientStreet('.$orderById->order_id.')" id="clientStreet'.$orderById->order_id.'">'.$orderById->street.'</span> '.$orderById->city.' '.$orderById->state.' '.$orderById->post_code; ?></td>
										</tr>
										<tr>
											<th>Landmark:</th>
											<td><?php echo $orderById->location_name; ?></td>
										</tr>
										<tr>
											<th>Delivery Instruction:</th>
											<td><?php echo $orderById->delivery_instruction; ?></td>
										</tr>
										<tr>
											<th>Contact Number:</th>
											<td><?php echo $orderById->contact_phone; ?></td>
										</tr>
										
										<?php 
											$items = json_decode($orderById->json_details);
											foreach($items as $item) {
										?>
										<tr>
											<th width="50%"><?php echo $item->qty." ".$CI->ModelOrder->getItemById($item->item_id); ?></th>
											<td><?php echo "₹ ".$item->price*$item->qty; ?></td>
										</tr>
										<?php } ?>
										<tr>
											<th>Sub Total:</th>
											<td><?php echo "₹ ".round($orderById->sub_total,2); ?></td>
										</tr>
										<tr>
											<th>Delivery Fee:</th>
											<td><?php echo "₹ ".round($orderById->delivery_charge,2); ?></td>
										</tr>
										<tr>
											<th>Tax <?php echo ($orderById->tax*100)."%" ?>:</th>
											<td><?php echo "₹ ".round($orderById->taxable_total,2); ?></td>
										</tr>
										<tr>
											<th>Total:</th>
											<td><?php echo "₹ ".round($orderById->total_w_tax,2); ?></td>
										</tr>
									</table><p> Best Regards<br>Onlinekaka</p></textarea>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-success" onclick="sendGuestEmail('<?php echo $order_id; ?>')">Send</button>
									</div>													
								</form>
							<?php endif ?>
							<!-- END BY FARHAN 29/04/2018 -->
  						<?php if($type == 'addItem'){ ?>
  							<style type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.min.css"></style>
  							<div class="row" id="addItemById">
  								<div class="col-md-8">

  											<?php foreach($categories as $category) { ?>
  												<h4><?php echo $category['category_name'] ?></h4>
  									<table class="table table-striped table-hover table-condensed" id="addItemTable">
  										<thead>
  											<tr>
  												<th>Item Name</th>

  												<th>Price</th>
  												<th>Add</th>
  												
  											</tr>
  										</thead>
  										<tbody>


  												
  												<?php 

  													$items = $CI->ModelOrder->getItemByCategory($category['cat_id'],$category['merchant_id']);
  												

  												?>
  												<?php
  													
  													foreach($items as $item) { ?>

  													<tr>
	  												<td><?php echo $item['item_name']; ?></td>
	  												<td>₹ <?php echo $item['prices'][0]['price']; ?></td>
	  												<td>
	  												<?php if($item['not_available'] == 2) { ?>
	  														

	  														<a href="javascript:vodi(0)" onclick="outOfStock()"><i class="fa fa-plus-circle" style="font-size: 26px;color: green;" ></i></a>
	  												<?php }else{ ?>

	  													<?php if($item['single_item'] == 1) { ?>

	  													<a href="javascript:vodi(0)" onclick="addItemByIdwithAddon(<?php echo $item['item_id']; ?>,<?php echo $order_id; ?>,<?php echo $category['merchant_id']; ?>)"><i class="fa fa-plus-circle" style="font-size: 26px;color: green;"></i></a>

	  													<?php } ?>

	  													<?php if($item['single_item'] == 2) { ?>

	  													<a href="javascript:vodi(0)" onclick="addItemById(<?php echo $item['prices'][0]['price']; ?>,<?php echo $item['item_id']; ?>,<?php echo $order_id; ?>)"><i class="fa fa-plus-circle" style="font-size: 26px;color: green;"></i></a>

	  													<?php } 

	  												}?>
	  												</td>
	  												</tr>

  													<?php }


  												 ?>		


  										</tbody>
  									</table>
  											<?php } 

  											
  											?>
  								</div>
  								<div class="col-md-4">
  									<!-- <a href="javascript:void(0)" onclick="clearCart(<?php echo $order_id; ?>)">Clear Cart</a>  -->
  									
  									<span><b>Cart Details </b></span><a href="javascript:void(0)" class="btn btn-success btn-sm pull-right" onclick="addToOrder('<?php echo $order_id; ?>','<?php echo $order->client_id; ?>')">Update cart</a>
  									<div id="cart">
  										

  										<?php
  										shoppingCart($order_id);
  										echo "<hr>";
  										cartCal();
  										 ?>
  									</div>
  									
  								</div>
  								
  							</div>

  							  											 <!-- Modal -->
  						    <div id="addAddonModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
  						   
  						      <div class="modal-dialog modal-lg">
  						        <!-- Modal content-->
  						        <div class="modal-content" style="overflow: auto; height: 500px">
  						          <div class="modal-header">
  						            <button type="button" class="close" onclick="closeModal()">&times;</button>
  						            <h4 class="modal-title"></h4>
  						          </div>
  						          <div class="modal-body">
  						            <div class="container" id="addonBody">


  						            </div>
  						          </div>
  						          <div class="modal-footer">
  						            <button type="button" class="btn btn-default" onclick="closeModal()">Close</button>
  						          </div>
  						        </div>

  						      </div>
  						  
  						    </div>
  						    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
  						<?php } ?>
  						   
  						   <!-- // 7 dec 2018  -->
  						   			<?php if($type == 'orderHistory'){ ?>
  						   			
  							<div class="row">
  								<div class="col-md-12">

  											
  									<?php foreach(json_decode($orderHistory) as $history) { ?>	
  									<div class="panel panel-default">
  									  <div class="panel-heading">Edit By: <?php echo $history->user_id." and time: ".$history->time_stamp; ?></div>
  									  <div class="panel-body">
  									  	
  									 
  									<table class="table table-striped table-hover table-condensed" id="addItemTable">
  										<thead>
  											<tr>
  												<th>Item Name</th>
  												<th>Price</th>
  												<th>Qty</th>
  												
  											</tr>
  										</thead>
  										<tbody>
  													
  													<?php foreach($history->json_details as $json_details){ ?>
  													<tr>
	  												<td><?php echo $CI->ModelOrder->getItemById2($json_details->item_id)->item_name; ?></td>
	  												<td>₹ <?php echo $json_details->price; ?></td>
	  												<td><?php echo $json_details->qty; ?></td>
	  												</tr>			
	  												<?php } ?>
	  								</tbody>
	  								<tfoot>
	  								    <tr>
	  								      <th style="text-align: right;" colspan="1">Total :</th>
	  								      <td>₹ <?php echo round($history->total_w_tax); ?></td>
	  								    </tr>
	  								</tfoot>
	  								</table>
	  								  </div>
	  								</div>
	  								<?php } ?>
	  						</div>
	  					</div>

  						<?php } ?>
							<script type="text/javascript">
								$('td').click(function () {
								    var clickedCell = $(this).text();
								   
								});
						$(".modal input, .modal textarea").focus();

							function orderStatus(order_id,mystatus,first_name,client_id)
							{

								var agentName = $("#agentName"+order_id).val();
								var status = $("#status").val();

								if(agentName == '' && (status == "Processing" || status == 'Delivered'))
								{
									alert("Please first select delivery agent.");
									
								}else{

								    var checkSms = $("#checkSms").prop('checked');
								    var sendSmsStatusMessage = $("#sendSmsStatusMessage").val();
								    var mobile = $("#sms_contact_phone").val();
	                                 
									 var agentName = $("#delivery_agent_color").val();

								
									 $.post("<?php echo base_url(); ?>order/ajaxStatusUpdate",{order_id:order_id,status:status,agentName:agentName,checkSms:checkSms,sendSmsStatusMessage:sendSmsStatusMessage,mobile:mobile,first_name:first_name,client_id:client_id}, 
									 	function(data){
									 		
									 		//$("#success").html('<div class="alert alert alert-success">Updated</div>');
								  		  	$('#myModal').modal('hide');
								  		  	var from = $("#from").val();var to2 = $("#to").val();
						
						refresh(order_id,from,to2,mystatus);

								    });
								}
							}
						function unAssign(){
						    
        						var  order_id 	= $("#order_id").val();
        						
        					

								 $.post("<?php echo base_url(); ?>order/unAssign",{order_id}, 
								 	function(data){ 
                                    $("#msg").html("<h2>Successfully undo</h2>");
							  		$('#myModal').modal('hide');  
							    });
							
						}
							function sentToApp() 
							{
								
						var  merchant_name= $("#merchant_name").val();
						var  items		= $("#items").val();
						var  total 		= $("#total").val();
						var  contact 	= $("#contact").val();
						var  order_id 	= $("#order_id").val();
						var  name 		= $("#name").val();
						var  location 	= $("#location").val();
						var  agent_id 	= $("#agent").val();
						var payment_type = $("#payment_type").val();
						var app_status_flag = $("#app_status_flag").val();

								 $.post("<?php echo base_url(); ?>order/sendToApp",{merchant_name,items,total,contact,order_id,name,location,agent_id,app_status_flag,payment_type}, 
								 	function(data){ 
                                    //alert(data);
							  		$('#myModal').modal('hide');  
							    });
							}
							function purchaseValueUpdate(order_id)
							{
								 var purchase_value = $("#purchase_value").val();

								// alert(purchase_value);

							
								 $.post("<?php echo base_url(); ?>order/ajaxPurchaseValueUpdate",{order_id:order_id,purchase_value:purchase_value}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');  
 
							    });
							}

							function deliveryAgentUpdate(order_id,mystatus)
							{
								 var delivery_agent = $("#delivery_agent").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryAgentUpdate",{order_id:order_id,delivery_agent:delivery_agent}, 
								 	function(data){

								 	if(data == 'false')
								 	{
								 		alert('Agent Name is not Available in the list');
								 	}
								 	else
								 	{

								 		$('#myModal').modal('hide');
								 		var from = $("#from").val();var to2 = $("#to").val();
					
						refresh(order_id,from,to2,mystatus);  
								 	}

								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		

							    });
							}

							function localityUpdate(order_id,mystatus)
							{
								 var locality = $("#locality").val();

								//alert(delivery_agent);
								 $.post("<?php echo base_url(); ?>order/ajaxLocalityUpdate",{order_id:order_id,locality:locality}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');  
							  		var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2,mystatus);

							    });
							}

							function totalWTaxUpdate(order_id,mystatus)
							{
								 var total_w_tax = $("#total_w_tax").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxTotalWTaxValueUpdate",{order_id:order_id,total_w_tax:total_w_tax}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 
							  		var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2,mystatus);

							    });
							}
							
								function deliveryChargeUpdate(order_id,mystatus)
							{
								 var delivery_charge = $("#delivery_charge").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryChargeUpdate",{order_id:order_id,delivery_charge:delivery_charge}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 
							  		var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2,mystatus);

							    });
							}

							function deliveryTimeUpdate(order_id,mystatus)
							{
								 var delivery_time2 = $("#delivery_time2").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryTime2Update",{order_id:order_id,delivery_time2:delivery_time2}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		 $('#myModal').modal('hide');
							  		 var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2,mystatus);

							    });
							}

							function reminderUpdate(order_id,mystatus)
							{
								 var reminder = $("#reminder").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxReminderUpdate",{order_id:order_id,reminder:reminder}, 
								 	function(data){

								 

								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		 $('#myModal').modal('hide');
							  		 var from = $("#from").val();var to2 = $("#to").val();
					
				
					refresh(order_id,from,to2,mystatus);

							    });
							}

							function commentUpdate(order_id,mystatus)
							{
								 var comment = $("#comment").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxCommentUpdate",{order_id:order_id,comment:comment}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');
							  		var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2,mystatus);

							    });
							}

							function editOrderItem(order_id)
							{
								 var decode_item = $("#decode_item").val().trim();
							
								 $.post("<?php echo base_url(); ?>order/ajaxDecodeItemUpdate",{order_id:order_id,decode_item:decode_item}, 
								 	function(data){

								 	
								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');
							  		var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2);

							    });
								
							} 

							function sendSmsApi(order_id,mystatus)
							{
								var smsText = $("#smsText").val().trim();
								var mobile = $("#mobile").val().trim();

								var agentName = $("#mobile option:selected").text();
								
								 $.post("<?php echo base_url(); ?>order/smsApi",{order_id:order_id,smsText:smsText,mobile:mobile,agentName:agentName}, 
								 	function(data){

								 	
								 	if(data == 'false')
								 	{
								 		alert('Agent Name is not Available in the list');
								 	}
								 	else
								 	{

								 		$('#myModal').modal('hide'); 
								 		var from = $("#from").val();var to2 = $("#to").val();
					
					refresh(order_id,from,to2,mystatus); 
								 	}
								 
							  		
 
							    });
							}

							$("#comment,#delivery_time2,#delivery_charge,#total_w_tax,#locality,#delivery_agent,#purchase_value").keyup(function(event){
							    if(event.keyCode == 13){
							        $(".myBtn").click();
							    }
							});

							var selectized = $('#locality,#delivery_agent').selectize({

								create: true,
								sortField: 'text'

							});
						//	selectized[0].selectize.focus();

							
							 $('#reminder').timepicker({ 'timeFormat': 'H:i' });
							 
							 // ADDED BY FARHAN	

							 function updateClientStreet(order_id)
							 {
							 	var clientStreet = $("#clientStreet"+order_id).text();

							 	
								 $.post("<?php echo base_url(); ?>order/updateClientStreet",{order_id:order_id,clientStreet:clientStreet}, 
								 	function(data){

								 	//	 $('#myModal').modal('hide');

								 	});

							 }
							 
							 
							 	// ADDED BY FARHAN 29/04/2018
								function sendGuestEmail(order_id)
								{

								 var client_id = $("#client_id"+order_id).val();
								 var guestEmail = $("#guestEmail"+order_id).val();
								 if(guestEmail == ''){

								 	alert("Please Enter Email");

								 }else{

									 var guestSubject = $("#guestSubject"+order_id).val();
									 var guestMessage = $("#guestMessage"+order_id).val();
									
									 $.post("<?php echo base_url(); ?>order/sendGuestEmail",{order_id:order_id,guestEmail:guestEmail,guestSubject:guestSubject,guestMessage:guestMessage,client_id:client_id}, 
									 	function(data){
									 		$('#myModal').modal('hide');

									 	//	desktopNotification("Email Sent Successfully.");

									 	});
								 	
								 }
								}
								

							 	

				
						
							 	$('.guestMessage').richText({
									  // text formatting
									  bold: true,
									  italic: true,
									  underline: true,

									  // text alignment
									  leftAlign: true,
									  centerAlign: true,
									  rightAlign: true,

									  // lists
									  ol: false,
									  ul: false,

									  // title
									  heading: false,

									  // fonts
									  fonts: false,
									  fontList: ["Arial", 
									    "Arial Black", 
									    "Comic Sans MS", 
									    "Courier New", 
									    "Geneva", 
									    "Georgia", 
									    "Helvetica", 
									    "Impact", 
									    "Lucida Console", 
									    "Tahoma", 
									    "Times New Roman",
									    "Verdana"
									    ],
									  fontColor: false,
									  fontSize: true,

									  // uploads
									  imageUpload: false,
									  fileUpload: false,

									  // media
									  Embed: false,

									  // link
									  urls: false,

									  // tables
									  table: false,

									  // code
									  removeStyles: false,
									  code: false,

						

									  // dev settings
									  useSingleQuotes: false,
									  height: 0,
									  heightPercentage: 70,
									  id: "",
									  class: "",
									  useParagraph: false
									  
									});
							 	// END BY FARHAN 29/04/2018

							 	// ADDED BY FARHAN 18 JUNE 2018


							 	function sentToApp2(agent_id) 

							 	{



							 		var  merchant_name= $("#merchant_name").val();

							 		var  items		= $("#items").val();

							 		var  total 		= $("#total").val();

							 		var  contact 	= $("#contact").val();

							 		var  order_id 	= $("#order_id").val();

							 		var  name 		= $("#name").val();

							 		var  location 	= $("#location").val();
							 		alert(location);
							 		var payment_type = $("#payment_type").val();

							 		var app_status_flag = $("#app_status_flag").val();



							 		$.post("<?php echo base_url(); ?>order/sendToApp",{merchant_name,items,total,contact,order_id,name,location,agent_id,app_status_flag,payment_type}, 

							 			function(data){ 

							 				$('#myModal').modal('hide');  

							 			});

							 	}


							 	function removeProduct(item_id,order_id) 

							 	{

							 		if (confirm("Are you sure?")) {
							 		       
							 		  $.post("<?php echo base_url(); ?>order/removeProduct",{item_id:item_id,order_id:order_id}, 

							 		  	function(data){ 
							 		  		$("#cart").html(data);
							 		  		// console.log(data);
							 		  		// $('#'+item_id).fadeOut(300, function(){ 
							 		  		//     $(this).remove();
							 		  		// });
							 		  		
							 		  		// var result = $.parseJSON(data);
							 		  		
							 		  		// var order_id = result[0];

							 		  		// $("#subtotal"+order_id).text("₹ "+result[1]);
							 		  		// $("#packaging"+order_id).text("₹ "+result[2]);
							 		  		// $("#taxable_total"+order_id).text("₹ "+result[3]);
							 		  		// $("#total_w_tax"+order_id).text("₹ "+result[4]);
							 		  	});


							 		}
							 	
							 		
							 	}

							 	function closeModal()
							 	{

							 		$("#addAddonModal").modal('hide');
							 	}

							 	function addToCart3()
							 	{	
							 		
							 		
							 		var addonIDs = $("input:checkbox:checked,input:radio:checked").map(function(){
									     return $(this).attr('data-id');
									    }).get(); 

							 		var data =$("#addToCart3").serialize();
							 		$.post("<?php echo base_url() ?>order/addOndata", {data:data,addonIDs:addonIDs}, function(data){

							 			notify("Food Item added to cart");
							 			$('#addAddonModal').modal('hide');
							 			$('#addItem').modal('show');
							 			$("#cart").html(data);
							 			$("#addItem").css('overflow-y','auto');
							 		});
							 		return false;
							 	}
							 		

							 		
							 	function notify(text)
							 	{
							 		$.notify(
							 				  text, 
							 				  { 
							 				  	position:"top right",
							 				  	className: 'success',
							 				  	autoHideDelay: 2000,	
							 				  }
							 				);
							 	}
							 		
							 	function addItemById(price,item_id,order_id)
							 	{
							 		
							 		// $('#addItem').modal('hide');
							 		
							 		$.post("<?php echo base_url(); ?>order/addItemById",{price:price,item_id:item_id,order_id:order_id}, function(data){

							 				notify("Food Item added to cart");
							 				$("#cart").html(data);

							 				
							 		});
							 		

							 	}
							 

							 	function addItemByIdwithAddon(item_id,order_id,merchant_id)
							 	{
							 		
							 		// $('#addItem').modal('hide');

							 		$('#addItem').on('shown.bs.modal', function () {
							 		$('#addItem').animate({ scrollTop: 0 }, 'slow');
							 		});
							 		
							 		$("#addAddonModal").modal('show');
							 		$("#addonBody").html('Loading...');
							 		$.post("<?php echo base_url(); ?>order/addon",{item_id:item_id,merchant_id:merchant_id,order_id:order_id}, function(data){

							 				
							 				$("#addonBody").html(data);
							 				
							 				$("#addItem").css('overflow-y','auto');
							 				
							 		});
							 		

							 	}



							 	function taxes()
							 	{
							 		$(".sub_tax").slideToggle();
							 	}
							 	
							 	function clearCart(order_id)
							 	{
							 		$.post("<?php echo base_url(); ?>order/clearCart",{order_id:order_id},

							 			function(data){ 
							 				
							 				$("#cart").html('Loading...');
							 				$("#cart").html(data);
							 				
							 			});
							 	}

							 	function deleteCartProduct(item_id,order_id,price,addon_ids)
							 	{	
							 		
							 		$.post("<?php echo base_url(); ?>order/deleteCartProduct",{item_id:item_id,order_id:order_id,price:price,addon_ids:addon_ids}, 

							 			function(data){ 
							 					
							 				//console.log(data);

							 				$("#cart").html('Loading...');
							 				$("#cart").html(data);
							 				
							 			});


							 	}

							 	function addToOrder(order_id,client_id)
							 	{
							 		$.post("<?php echo base_url(); ?>order/addToOrder",{order_id:order_id,client_id:client_id}, 

							 			function(data){ 
							 				
							 				if(data == 'false')
							 				{	
							 					alert('Cart can not be empty');

							 				}else{

								 				//console.log(data);

								 				$("#addItem").modal('hide');
								 				viewOrderAddress(order_id);
							 					
							 				}
							 				
							 				
							 			});
							 	}

							 	function size(id)
							 	{
							 		$("#size"+id).slideToggle();
							 	}

							 	function outOfStock()
							 	{
							 		alert("Item is out of stock");
							 	}
							 // END BY FARHAN
							</script>