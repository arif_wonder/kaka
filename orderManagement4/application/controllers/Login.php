<?php
header('Access-Control-Allow-Origin: *');
class Login extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		$this->load->model('ModelAdmin');
		$whitelist = $this->ModelAdmin->getWhitelistingIps();
	
		$whitelist = explode(",",$whitelist->ips);

		if (in_array(get_client_ip(), $whitelist)) {

		   
			$this->load->library('form_validation');    

		} else {
		    //Action for all other IP Addresses
		    echo '<h1 style="text-align:center;">Failed! You are not authorized to access this page.</h1>'; 
		 
		    exit;
		}
		
		
	}


	public function index()
	{	

		// $password = $this->session->userdata('password');

		
		// if($password == 'admin123')
		// {
		// 	redirect(base_url());

		// }
	
		$this->load->view('view_login');
	}


	public function login_save()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['login'])) {
   			
   			
   			 $this->form_validation->set_rules('username', 'Username', 'required');
   			 $this->form_validation->set_rules('password', 'Password', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                      
                        $this->load->view('view_login');
                     
                }
                else
                {

                       $data = array(

                       	"username" => $_POST['username'],
                       	"password" => $_POST['password']

                       );


                       $user = $this->ModelAdmin->orderManagementLogin($data);

                       if(!empty($user))
                       {
                       		 $this->session->set_userdata('order_user', $user);
                       		 redirect(base_url());

                       }else{

                       		$this->session->set_flashdata('failure', '<div class="alert alert-danger"><strong>Failed!</strong> Invalid Login.</div>');
                       		redirect(base_url().'login');
                       }
                }
			
		




		}


	}

	// public function login_save()
	// {	
	// 	$password = $this->session->userdata('password');

		
	// 	if($password == 'admin123')
	// 	{
	// 		redirect(base_url());

	// 	}

	// 	if(isset($_POST['submit']))
	// 	{
	// 		$username = $this->input->post('username');
	// 		$password = $this->input->post('password');

	// 		if(($username == 'wonderpillars' && $password == 'admin123') || ($username == 'worker12a' && $password == 'worker12pass') || ($username == 'worker43b' && $password == 'workerbpass') )
	// 		{
	// 			$this->session->set_userdata('username',$username);
	// 			$this->session->set_userdata('password',$password);

	// 			redirect(base_url());

	// 		}else
	// 		{
	// 			$this->session->set_flashdata('failure', 'Your Username or Password is invalid');
	// 			redirect(base_url().'login');

	// 		}


	// 	}

	// }

	public function logout()
	{
		$this->session->unset_userdata('order_user');
		
		redirect(base_url().'login');
	}

}
