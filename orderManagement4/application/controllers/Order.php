<?php
date_default_timezone_set('Asia/Kolkata');
header('Access-Control-Allow-Origin: *');
class Order extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		
		if($this->session->userdata('order_user'))
		{

			$this->load->model('ModelOrder');
		}else{

			redirect(base_url().'login');
			exit;
		}
		
		
	}

	public function index()
	{	

		$password = $this->session->userdata('password');
		$user = $this->session->userdata('order_user');

		$data['role'] = $user->role;
		$roles = array('Super Admin','Manager','Junior Manager');
		if ($user->panel == 'Super Panel' && in_array($user->role,$roles)) {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();


			// CODE ADDED BY FARHAN
			// FOR FILTERS
			$from = date('Y-m-d');  
			$data['getFilteredLists'] = $this->ModelOrder->getFilteredListByDeliveryDate($from,$to='');

			// FOR STOP REFRESH
			$data['refresh'] = $this->session->userdata('refresh');
			// END BY FARHAN
		
			$this->load->view('view_order',$data);
		}
		elseif ($user->panel == 'Panel A' && in_array($user->role,$roles)) {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgentsAA();

			$this->load->view('view_panelA.php',$data);
			
		}elseif ($user->panel == 'Panel B' && in_array($user->role,$roles)) {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgentsBB();

			$this->load->view('view_panelB.php',$data);
		}


	}

// new work
public function unAssign()
{
    extract($_POST);
	
	$data11 = array(
					'delivery_agent' =>  ''
				);		
		$this->ModelOrder->deliveryAgentUpdate($data11,$order_id);
	$data22 = array(
					'status' =>  "Confirmed"
				);
   $this->ModelOrder->statusUpdate($data22,$order_id);
   $this->ModelOrder->deleteHisteryRow($order_id);
   $update_data = array('app_status'=>'');
	$this->ModelOrder->appStatusUpdate($update_data,$order_id);
			
}
	public function sendToApp()
	{
		extract($_POST);
		$record =$this->ModelOrder->agentInfo($agent_id);
		$token = $record['firebase_id'];
		$agentName = $record['name'];
		$data11 = array(

					'delivery_agent' =>  $agentName

				);		
		$this->ModelOrder->deliveryAgentUpdate($data11,$order_id);

		$record2 = $this->ModelOrder->getStatus2($order_id);
		echo $status  = $record2['status'];

		$update_data = array('app_status'=>'unseen');
		$this->ModelOrder->appStatusUpdate($update_data,$order_id);

			$data22 = array(

					'status' =>  "Processing"

				);

			$this->ModelOrder->statusUpdate($data22,$order_id);
			$timer = time();
			 $cDate =  date('Y-m-d g:i:s');
		//for history record
		$historyData = array(
					'merchant_name' 	=> $merchant_name,
					'items' 			=> $items,
					'total' 			=> $total,
					'contact' 		=> $contact,
					'order_id' 		=> $order_id,
					'name' 			=> $name,
					'location' 		=> $location,
					'status' 		=> 'Processing',
					'agent_id' 		=> $agent_id,
					'create_date'   => $cDate,
					'payment_type' => $payment_type
		);
		if ($app_status_flag == "new")
			$this->ModelOrder->saveHistory($historyData);
		else
			$this->ModelOrder->updateHistory($historyData,$order_id);

		define( 'API_ACCESS_KEY', 'AIzaSyB8JwFBtEyDOCi6cFvuTr26AG99h05dLMI' );
                       $registrationIds = array( $token );
                       $msg = array
                       (
	                      'message' 	=> "You Have Receive a New Order",
	                      'title'		=> "Order Notification",
						  'image' => "https://www.finalcheck.speedypixelgame.com//upload/1476254450-1475737169-kaka-logo-(1).png",
						  'merchant_name' 	=> $merchant_name,
						  'items' 			=> $items,
						  'total' 			=> $total,
						  'contact' 		=> $contact,
						  'order_id' 		=> $order_id,
						  'name' 			=> $name,
						  'location' 		=> $location,
						  'status' 			=> $status,
						  'agent_id' 		=> $agent_id

                        );
                       $fields = array
                       (
	                      'registration_ids' 	=> $registrationIds,
	                      'data'			=> $msg

                       ); 
                       $headers = array
                       (
        	               'Authorization: key=' . API_ACCESS_KEY,
        	               'Content-Type: application/json'
                       ); 
                       $ch = curl_init();
                       curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                       curl_setopt( $ch,CURLOPT_POST, true );
                       curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                       curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                       curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                       curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                       $result = curl_exec($ch );
                       curl_close( $ch );
	}
	public function addAgent()
	{
		$record =$this->ModelOrder->agentInfo($_POST['id']);
		$name = $record['name'];
		$data = array('name'=>$name,'agent_id'=>$_POST['id']);
		$this->ModelOrder->agentADD($data);	
	}
	public function removeAgent()
	{
		$this->ModelOrder->removeAgent($_POST['id']);
	}
	public function agents()
	{
		$user = $this->session->userdata('order_user');
		$roles = array('Super Admin','Manager');
		if (in_array($user->role,$roles)) {

			$data['agents'] =  $this->ModelOrder->getAgents();
			$this->load->view("view_agents.php",$data);
		}

		
	}
public function showAgentOrders()
{
	$agent = $_POST['agent'];
	$data['type'] = "showAgentOrders";
	$data['data'] = $this->ModelOrder->showAgentOrders($agent);
	$this->load->view('view_ajax_order_detail',$data);
}
public function showAgentHistory()
{
	$agent = $_POST['agent'];
	$data['type'] = "showAgentHistory";
	$data['data'] = $this->ModelOrder->showAgentHistory($agent);
	$this->load->view('view_ajax_order_detail',$data);	
}
public function getAgentsData()

	{
		$data['agent_id'] = $_POST['agent_id'];
		$data['type'] = 'showProfile';
		$data['agent_data'] = $this->ModelOrder->getAgentProfile($data['agent_id']);
		$this->load->view('view_ajax_order_detail',$data);
	}
public function updateAgentProfile()
{
	$agent_id = $_POST['agent_id'];
	$data = array(

			'name' =>  $_POST['name'],
			'username' =>  $_POST['username'],
			'password' =>  $_POST['password'],
			'lati' =>  $_POST['lati'],
			'longi' =>  $_POST['longi'],
			'status' =>  $_POST['status'],
			'mobile' =>  $_POST['mobile']

			);
	$this->ModelOrder->updateAgent($data,$agent_id);
}
public function addNewAgent()
{
	$data = array(

			'name' =>  $_POST['name'],
			'username' =>  $_POST['username'],
			'password' =>  $_POST['password'],
			'lati' =>  $_POST['lati'],
			'longi' =>  $_POST['longi'],
			'status' =>  $_POST['status'],
			'mobile' =>  $_POST['mobile']

			);
	$this->ModelOrder->addAgent($data);
}
public function delAgent()
{
	$agent_id = $_POST['agent_id'];
	$this->ModelOrder->delAgent($agent_id);
}
public function getUserLocation()
{
	$data['lati'] = $_POST['lat'];
	$data['longi'] = $_POST['long'];
	$data['type'] = 'showLocation';
	$this->load->view('view_ajax_order_detail',$data);
}
//new work end







	public function workerA()
	{
		$password = $this->session->userdata('password');
		$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
		if (($password == "victor999") ||($password == "black999star!")){
			$this->load->view('workerA_view',$data);
			}
		
	}
	public function workerB()
	{
		$password = $this->session->userdata('password');
		
		$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
		if (($password == "charge777") ||($password == "black999star!")){
			$this->load->view('workerB_view',$data);
			}
	}
	public function logout()
	{
		$this->session->unset_userdata(array('username','password'));
		redirect(base_url().'login');
	}
	
	public function updateWorker()
	{
		$order_id = $_POST['order_id'];
		$worker = $_POST['worker_mark'];
		$this->ModelOrder->workerUpdate($order_id,$worker);
	}
	public function updateWorkerAgent()
	{
		$order_id = $_POST['agentID'];
		$worker = $_POST['worker_mark'];
		$this->ModelOrder->workerUpdateAgent($order_id,$worker);
	}
	public function ajaxStatusUpdate()
	{
		$order_id = $_POST['order_id'];



		$data = array(

				'status' =>  $_POST['status']

		);

		$this->ModelOrder->statusUpdate($data,$order_id);
		echo allocateColor($data['status']);


		if($_POST['status'] == 'Delivered')
		{
			$data = array(

				'delivery_time2' =>  date('g:i')

			);
		
			$this->ModelOrder->deliveryTime2Update($data,$order_id);


			$agentName = $_POST['agentName'];

			$data = array(

			"status" => 1

			);

			$this->ModelOrder->agentStatusGreenByName($data,$agentName);
		}

		$checkSms = trim($_POST['checkSms']);
		if($checkSms == 'true')
		{

			// SEND SMS TO DELIVERY BOYS

			$user = "20082245";

			$pass = "x66p4p";

			$sender = "ONKAKA";

			$phone = $_POST['mobile'];

			$text = $_POST['sendSmsStatusMessage'];

			$priority = "dnd";

			$stype = "0";


			$data = "user=".$user."&pwd=".$pass."&senderid=".$sender."&mobileno=".$phone."&msgtext=".$text."&smstype=".$stype;
			
			$ch = curl_init('http://bulksmsindia.mobi/sendurlcomma.aspx?');

			curl_setopt($ch, CURLOPT_POST, true);

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);

			$success = '<div class="alert alert-success">Message Sent</div>';

			//echo $success;

			curl_close($ch);
		}
		
	}

	public function ajaxPurchaseValueUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'purchase_value' =>  $_POST['purchase_value']

			);
		
		$this->ModelOrder->purchaseValueUpdate($data,$order_id);
		echo 'Purchase Value Updated.';
	}

	public function ajaxTotalWTaxValueUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'total_w_tax' =>  $_POST['total_w_tax']

			);
		
		$this->ModelOrder->totalWTaxValueUpdate($data,$order_id);
		echo 'Delivery Value Updated.';
	} 
	
		public function ajaxDeliveryChargeUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'delivery_charge' =>  $_POST['delivery_charge']

			);
		
		$this->ModelOrder->deliveryChargeUpdate($data,$order_id);
		echo 'Delivery Value Updated.';
	} 

	public function ajaxDeliveryAgentUpdate()
	{

		$order_id = $_POST['order_id'];
		$agentName = $_POST['delivery_agent'];

		if($this->ModelOrder->getAvailableAgentsByName($agentName))
		{
			$data = array(

				'delivery_agent' =>  $agentName

			);
		
			$this->ModelOrder->deliveryAgentUpdate($data,$order_id);
		}
		else
		{

			echo 'false';

		}
	}

	public function ajaxLocalityUpdate()
	{

		$order_id = $_POST['order_id'];
		$data = array(

				'locality' =>  $_POST['locality']

			);
		
		$this->ModelOrder->localityUpdate($data,$order_id);
		echo 'Locality Updated.';
	}

	public function ajaxDeliveryTime2Update()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'delivery_time2' =>  $_POST['delivery_time2']

			);
		
		$this->ModelOrder->deliveryTime2Update($data,$order_id);
		echo 'Delivery Agent Updated.';
	}

	public function ajaxCommentUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'comment' =>  $_POST['comment']

			);
		
		$this->ModelOrder->commentUpdate($data,$order_id);
		echo 'Comment Updated.';
	}

	public function ajaxReminderUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'reminder' =>  $_POST['reminder']

			);
		
		$this->ModelOrder->reminderUpdate($data,$order_id);
		echo 'Reminder Updated.';
	}


	public function ajaxOrder()
	{	
		$from = $_POST['from'];
		$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;

		
		$data['getOrders'] = $this->ModelOrder->getOrder($from);
	
		if(sizeof($data['getOrders']) == 0 ){

		 	echo "NO Orders Yet.";

		 }else{

			$last_id = $data['getOrders'][0]['order_id'];
			
			if($last_id > $first_order_id)
			{
				$this->decodeOrderItem($first_order_id);
			}

		}
		
		$this->load->view('view_ajax_order',$data);
		
		
	}
	public function ajaxOrderWorkerA()
	{	
		$from = $_POST['from'];
		$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;

		
		$data['getOrders'] = $this->ModelOrder->getOrderA($from);
	
		if(sizeof($data['getOrders']) == 0 ){

		 	echo "NO Orders Yet.";

		 }else{

			$last_id = $data['getOrders'][0]['order_id'];
			
			if($last_id > $first_order_id)
			{
				$this->decodeOrderItem($first_order_id);
			}

		}
		
		$this->load->view('view_ajax_orderA',$data);
		
		
	}
public function ajaxOrderWorkerB()
	{	
		$from = $_POST['from'];
		$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;

		
		$data['getOrders'] = $this->ModelOrder->getOrderB($from);
	
		if(sizeof($data['getOrders']) == 0 ){

		 	echo "NO Orders Yet.";

		 }else{

			$last_id = $data['getOrders'][0]['order_id'];
			
			if($last_id > $first_order_id)
			{
				$this->decodeOrderItem($first_order_id);
			}

		}
		
		$this->load->view('view_ajax_orderB',$data);
		
		
	}
	public function ajaxOrderByRange()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
		$this->load->view('view_ajax_order',$data);
	}
	
	public function ajaxOrderByRangeWorkerA()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
		$this->load->view('view_ajax_orderA',$data);
	}

	public function ajaxOrderByRangeWorkerB()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
		$this->load->view('view_ajax_orderB',$data);
	}
	// public function reminderOrder()
	// {	
	// 	$date = $_POST['date'];

	// 	$data['getRemindOrders'] = $this->ModelOrder->getRemindOrder($date);
		
	// 	foreach($data['getRemindOrders'] as $reminderOrder)
	// 	{
	// 		echo $reminderOrder['order_id']."-";
	// 	}

	
	 
	// 	//$this->load->view('view_ajax_order',$data);
	// }

	public function getReminder()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['reminder'] = $_POST['reminder'];
		$data['type'] = 'reminder';
		$this->load->view('view_ajax_order_detail',$data);

	}


	public function getStatus()

	{

		$data['order_id'] = $_POST['order_id'];
		$data['status'] = $_POST['status'];
		$data['type'] = 'status';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);
		$this->load->view('view_ajax_order_detail',$data);

	}

	// CHANGES BY FARHAN 19 MAY 2018
	public function getPurchaseValue()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'purchase_value';
		$data['itemPurchaseValue'] = $this->ModelOrder->getItemForPurchaseValue($data['order_id']); // CHANGES BY FARHAN
		$this->load->view('view_ajax_order_detail',$data);

	}
	// END BY FARHAN 19 MY 2018
	public function getDeliveryAgent()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_agent'] = $_POST['delivery_agent'];
		$data['type'] = 'delivery_agent';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getLocality()
	{
		$data['order_id'] = $_POST['order_id'];
		// $data['locality'] = $_POST['locality'];
		$data['type'] = 'locality';
		$data['selected_locality'] = $_POST['locality'];
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getTotalWTax()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['total_w_tax'] = $_POST['total_w_tax'];
		$data['type'] = 'total_w_tax';
		$this->load->view('view_ajax_order_detail',$data);

	}
	
		public function getDeliveryCharge()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_charge'] = $_POST['delivery_charge'];
		$data['type'] = 'delivery_charge';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getDeliveryTime()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_time2'] = $_POST['delivery_time2'];
		$data['type'] = 'delivery_time2';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getComment()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['comment'] = $_POST['comment'];
		$data['type'] = 'comment';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getOrderById()
	{

		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'item';
		$client_id = $this->ModelOrder->getClientId($_POST['contact_phone']); // CHANGES BY FARHAN
		$data['noOfOrders'] = $this->ModelOrder->getNumberOfOrders($client_id); // CHANGES BY FARHAN
		$data['orderById'] = $this->ModelOrder->getOrderByIdForItem($data['order_id']); // CHANGES BY FARHAN
		$this->load->view('view_ajax_order_detail',$data);

	}


	public function decodeOrderItem($first_order_id='')
	{

		$first_order_id = $first_order_id;

		$data['getOrders'] = $this->ModelOrder->getOrderForDecodeItem($first_order_id);
		
		foreach($data['getOrders'] as $getOrder)
		{	
			$order_id = $getOrder['order_id'];
			//$sizeOfItems = sizeof(json_decode($getOrder['json_details'],true));
			$Items = json_decode($getOrder['json_details'],true);
		// 	print_r($data['getOrders']);
		// die();
			$orderItem = [];
			
			foreach ($Items as $key => $item) {

				// $price = $item['price'];
				// $size = explode("|", $price);
				// $size = isset($size[1]) ? " (".$size[1].")" : " ";
				$orderItem[] = $item['qty']." - ".$this->ModelOrder->getItemById($item['item_id']);

			}

			$totalOrder = implode(",", $orderItem);
			$data = array(

				"decode_item" => $totalOrder

				);
			$this->ModelOrder->updateOrderItem($order_id,$data);
			
		}
			
		
	}


	public function ajaxDecodeItemUpdate()
	{
		$order_id = $_POST['order_id'];

		$data = array(

				'decode_item' =>  $_POST['decode_item']

			);

		$this->ModelOrder->decodeItemUpdate($data,$order_id);
		//echo 'Decode Item Updated.';
	}

	public function update_available_agents()
	{
		$id = $_POST['id'];

		$data = array(

			"name" => trim($_POST['agentName'])

			);
		

		$this->ModelOrder->agentUpdate($data,$id);
	}

	// UPDATE DELIVERY BOYS STATUS

	public function update_available_agents_status()
	{
		$id = $_POST['id'];

		$status = $_POST['status'];

		if($status == 1)
		{
			$statusValue = 0;

		}else{

			$statusValue = 1;
		}

		$data = array(

			"status" => trim($statusValue)

			);

		$this->ModelOrder->agentStatusUpdate($data,$id);


	}


	public function getOrderByIdInSms()

	{ 

		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'sms';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);
	    
		$password = $this->session->userdata('password');
		if ($password == "black999star!")
			$data['worker_flag'] = "admin";
		elseif ($password == "victor999")
			$data['worker_flag'] = "A";
		elseif ($password == "charge777") 
			$data['worker_flag'] = "B";
		$this->load->view('view_ajax_order_detail',$data);

	}

	// SEND SMS TO DELIVERY BOYS

	public function smsApi()
	{
 
		$order_id = $_POST['order_id'];


			// AFTER SEND SMS INSERT DELIVERY BOYS NAME TO AGENT COLUMN

			$agentName = $_POST['agentName'];

			if($this->ModelOrder->getAvailableAgentsByName($agentName))
			{
				
				$data = array(

					'delivery_agent' =>  $agentName

				);
				
				$this->ModelOrder->deliveryAgentUpdate($data,$order_id);
			}
			else
			{

				echo 'false';
				exit();
			}

			// AFTER SEND SMS CHANGE STATUS TO PROCESSING

			$data = array(

					'status' =>  "Processing"

				);

			$this->ModelOrder->statusUpdate($data,$order_id);


			// SEND SMS TO DELIVERY BOYS

			$user = "20082245";

			$pass = "x66p4p";

			$sender = "ONKAKA";

			$phone = $_POST['mobile'];

			$text = $_POST['smsText'];

			$priority = "dnd";

			$stype = "0";


			$data = "user=".$user."&pwd=".$pass."&senderid=".$sender."&mobileno=".$phone."&msgtext=".$text."&smstype=".$stype;
		
			$ch = curl_init('http://bulksmsindia.mobi/sendurlcomma.aspx?');

			curl_setopt($ch, CURLOPT_POST, true);

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);

			$success = '<div class="alert alert-success">Message Sent</div>';

			//echo $success;

			curl_close($ch);

		
	}


		public function export($cdate='')
	{
	    $date = explode("%7C", $cdate);
	
		$from = $date[0];
		$to = $date[1];
		
		
		$getOrders = $this->ModelOrder->getOrder($from, $to);
// 		echo "<pre>";
// 		print_r($getOrders);
// 		echo "</pre>";
// 		die();

		foreach($getOrders as $getOrder) {
			// echo "<pre>";
			// print_r($this->ModelOrder->getClientById($getOrder['client_id']));
			// echo "</pre>";
		
			$time = strtotime($getOrder['date_created']);
			$con = date("g:i", $time);
			

			$data[] =array(


					"REF" 			 => $getOrder['order_id'],
					"MOP" 			 => strtoupper($getOrder['payment_type']),
					"STATUS" 		 => strtoupper($getOrder['status']),
					"NAME"			 => strtoupper($getOrder['first_name']." ".$getOrder['last_name']),
					"EMAIL"          => $getOrder['email_address'],	//ADDED BY FARHAN
					"MOBILE"  		 => isset($getOrder['contact_phone']) ? $getOrder['contact_phone'] : " ",
					"CON"			 => $con,
					"DEL"			 => $getOrder['delivery_time2'],
					"RESTAURANT"     => strtoupper($getOrder['restaurant_name']),
					"LOCALITY"		 => strtoupper($getOrder['locality']),
					"AGENT"			 => strtoupper($getOrder['delivery_agent']),
					"PV" 			 => $getOrder['purchase_value'],
					"DV"			 => $getOrder['total_w_tax'],
					"DF"			 => $getOrder['delivery_charge'],
					"PROFIT"		 =>	$getOrder['total_w_tax'] - $getOrder['purchase_value'],
					"CHANGES"		 => $getOrder['comment']



				);

		}

		$this->db->insert_batch('mt_farhan_excel',$data);

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		 $this->ModelOrder->export($cdate);

	}
	
	public function availableAgents()
	{
		$data['id'] = $_POST['id'];
		$data['agentName'] = $_POST['agentName'];
		$data['val'] = 'availabel_agents';


		$this->load->view('view_ajax_agent',$data);
	}

	public function updateDeliveryAgent()
	{
		$id = $_POST['id'];
		$data = array(

				'name' =>  $_POST['agentName']

			);
		
		$this->ModelOrder->updateDeliveryAgent($data,$id);
		echo 'Delivery Value Updated.';
	}

	// CODE ADDED BY FARHAN

	//FOR PASSWORD PROTECTION
	public function checkPassword()
	{
		$password = $_POST['password'];

		if($password == 'sam123!')
		{
			echo true;
			
		}else{

			echo false;
		}
	}

	// FOR FILTERS

	public function allFilters()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getFilteredLists'] = $this->ModelOrder->getFilteredListByDeliveryDate($from,$to);
		$this->load->view("view_ajax_filters",$data);
	}


	public function getSearhedData()
	{
		
		$from = $_POST['from'];
		$to = $_POST['to'];
		$values = $_POST['values'];

		$values = array_map('current', $values);

		$type = $_POST['type'];	

		$data['getOrders'] = $this->ModelOrder->getOrder2($from,$values,$type,$to);


		$this->load->view('view_ajax_order',$data);
	}


		public function getSearhedData2()
	{
		
		$from = $_POST['from'];
		$to = $_POST['to'];
		$values = $_POST['values'];

		//$res = call_user_func_array('array_merge', $values);

		$merchant = array_column($values, 'merchant');
		$status = array_column($values, 'status');
		$locality = array_column($values, 'locality');
		$payment_type = array_column($values, 'payment_type');
		$agent = array_column($values, 'agent');


		$type = $_POST['type'];	

		$data['getOrders'] = $this->ModelOrder->getOrder3($from,$to,$merchant,$status,$locality,$payment_type,$agent);


		$this->load->view('view_ajax_order',$data);
	}


	// FOR STOP REFRESH
	public function setSession()
	{
		$getSession = $this->session->userdata('refresh');

		if(!isset($getSession) || $getSession == 0)
		{
			$this->session->set_userdata('refresh',1);
			$refresh = $this->session->userdata('refresh');

		}else if($getSession == 1){

			$this->session->set_userdata('refresh',0);
			$refresh = $this->session->userdata('refresh');

		}

	}


	// END BY FARHAN
	
		public function updateClientStreet()
	{

		$data = array(

			"street" => $_POST['clientStreet']

		);
		$order_id = $_POST['order_id'];
		$this->ModelOrder->updateClientStreet($order_id,$data);

	}
	
	
	// ADDED BY FARHAN 29/04/2018
	public function guestEmail()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'guestCheckout';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);
		$this->load->view('view_ajax_order_detail',$data);
	}


	public function sendGuestEmail()
	{
		
		
		$this->load->library('email');
		$to = $_POST['guestEmail'];
		$subject = $_POST['guestSubject']; 
		$message = $_POST['guestMessage'];     
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->to($to);
		$this->email->from('Order@onlinekaka.com','Onlinekaka');
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();

		$data = array(

			"email_address" => trim($to)
		);

		$client_id = $_POST['client_id'];
		$order_id  = $_POST['order_id'];
		$this->ModelOrder->updateClientEmail($client_id,$data);
		$this->ModelOrder->updateSentEmail($order_id);
	}

	// ADDED BY FARHN 13 MAY 2018

	// END BY FARHN 13 MAY 2018
	// END BY FARHAN 29/04/2018
	
}