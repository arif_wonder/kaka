<?php 

/**
* 
*/
class ModelAdmin extends CI_Model
{

	public function login($data)
	{
		
		$this->db->where("username",$data['username']);
		$this->db->join("mt_farhan_roles","mt_farhan_roles.id=mt_farhan_users.role_id");
		$query = $this->db->get("mt_farhan_users");
		$user = $query->row();

		if(!empty($user))
		{
			if($data['password'] == $this->encryption->decrypt($user->password) && ($user->role == 'Super Admin' || $user->role == 'IPs Whitelisting'))
			{
				return $user;

			}else{

				return false;
			}
		}else{

			
			return false;
		}


	}

	public function orderManagementLogin($data)
	{
		$this->db->select('*');
		$this->db->from('mt_farhan_users');
		$this->db->where("username",$data['username']);
		$this->db->join("mt_farhan_panels","mt_farhan_panels.id=mt_farhan_users.panel_id");
		$this->db->join("mt_farhan_roles","mt_farhan_roles.id=mt_farhan_users.role_id");
		$query = $this->db->get();
		$user = $query->row();

		if(!empty($user))
		{
			if($data['password'] == $this->encryption->decrypt($user->password))
			{
				return $user;

			}else{

				return false;
			}
		}else{

			
			return false;
		}


	}

	public function getRoles()
	{

		$query = $this->db->get("mt_farhan_roles");
		 $this->db->order_by("id","desc");
		return $query->result();

	}

	public function userAdd($data)
	{
		$query = $this->db->insert('mt_farhan_users',$data);

		return $query;
	}

	public function getUsers()
	{	
		$this->db->select('mt_farhan_users.id,mt_farhan_panels.panel,mt_farhan_roles.role,mt_farhan_users.username,mt_farhan_users.email,
			mt_farhan_users.mobile');
    	$this->db->from('mt_farhan_users');
		$this->db->join('mt_farhan_panels', 'mt_farhan_panels.id = mt_farhan_users.panel_id');
		 $this->db->join('mt_farhan_roles', 'mt_farhan_roles.id = mt_farhan_users.role_id');

		$query = $this->db->get();
		return $query->result();

	}

	public function getUserById($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->get("mt_farhan_users");
		return $query->row();

	}

	public function updateUser($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("mt_farhan_users",$data);
		return $query;

	}

	public function userDelete($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->delete("mt_farhan_users");

		return $query;

	}

	public function roleAdd($data)
	{
		$query = $this->db->insert('mt_farhan_roles',$data);

		return $query;
	}

	public function getRoleById($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->get("mt_farhan_roles");
		return $query->row();

	}

	public function updateRole($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("mt_farhan_roles",$data);
		return $query;

	}

	public function roleDelete($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->delete("mt_farhan_roles");

		return $query;

	}

	public function updateWhitelisting($data)
	{	
		$this->db->where("id",1);
		$query = $this->db->update("mt_farhan_whitelisingIPs",$data);
		return $query;

	}

	public function getWhitelistingIps()
	{

		$this->db->where("id",1);
		$query = $this->db->get("mt_farhan_whitelisingIPs");
		return $query->row();

	}


	public function panelAdd($data)
	{
		$query = $this->db->insert('mt_farhan_panels',$data);

		return $query;
	}

	public function getPanels()
	{

		$query = $this->db->get("mt_farhan_panels");
		 $this->db->order_by("id","desc");
		return $query->result();

	}

	public function getPanelById($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->get("mt_farhan_panels");
		return $query->row();

	}


	public function updatePanel($data,$id)
	{	
		$this->db->where("id",$id);
		$query = $this->db->update("mt_farhan_panels",$data);
		return $query;

	}


	public function panelDelete($id)
	{

		$this->db->where("id",$id);
		$query = $this->db->delete("mt_farhan_panels");

		return $query;

	}
}




 ?>