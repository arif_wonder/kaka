<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelOrder extends CI_Model {

//new work

public function getAgentProfile($agent_id)
{
	$this->db->where('id',$agent_id);
	$query = $this->db->get('mt_delivery_boys_farhan');
	return $query->row_array();

}
public function getAgentOrderProcessing($agent)
{
	$this->db->where('delivery_agent',$agent);
	$this->db->where('status','Processing');
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->num_rows();
}
public function getAgentOrderDelivered($agent)
{
	$this->db->where('delivery_agent',$agent);
	$this->db->where('status','Delivered');
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->num_rows();
}
public function getAgentOrderAll($agent)
{
	$this->db->where('delivery_agent',$agent);
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->num_rows();
}
public function showAgentOrders($agent)
{
	$this->db->where('delivery_agent',$agent);
	$query = $this->db->get('mt_order_duplicate_farhan');
	return $query->result_array();
}
public function showAgentHistory($agent)
{
	$this->db->where('agent_id',$agent);
	$query = $this->db->get('history');
	return $query->result_array();
}
public function updateAgent($data,$agent_id)
{	
	$this->db->where('id',$agent_id);
	$this->db->update('mt_delivery_boys_farhan', $data);
}
public function addAgent($data)
{
	$this->db->insert('mt_delivery_boys_farhan',$data);
}
public function delAgent($agent_id)
{
	$this->db->where('id',$agent_id);
	$this->db->delete('mt_delivery_boys_farhan');
}
public function deleteHisteryRow($order_id)
    {
       $this->db->where('order_id',$order_id);
       $this->db->delete('history');
    }
public function saveHistory($data)
{
	$this->db->insert('history',$data);
}
public function updateHistory($data,$order_id)
{
	$this->db->where("order_id",$order_id);
	$this->db->update('history',$data);
}
public function agentInfo($agent_id)
	{
		$this->db->where('id',$agent_id);
		$query = $this->db->get("mt_delivery_boys_farhan");
		return $query->row_array(); 
	}
public function getStatus2($order_id)
	{
		$this->db->where('order_id',$order_id);
		$query = $this->db->get("mt_order_duplicate_farhan");
		return $query->row_array(); 
	}

	public function appStatusUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}

//new rok edn

	public function getOrder($from, $to="")
	{
        if($to != "")
        {
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.email_address,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status,
						   mt_order_duplicate_farhan.sent_email,
						   mt_order_duplicate_farhan.client_id

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderA($from, $to="")
	{
        if($to != "")
        {
        	//$this->db->where('workerType','A');
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
        	//$this->db->where('workerType','A');
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderB($from, $to="")
	{
        if($to != "")
        {
        	//$this->db->where('workerType','B');
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
        	//$this->db->where('workerType','B');
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderByRange($from,$to)
	{

		
		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}
	public function getOrderByRangeWorkerA($from,$to)
	{

		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);
		$this->db->where('workerType','A');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}
	public function getOrderByRangeWorkerB($from,$to)
	{

		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);
		$this->db->where('workerType','B');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,
						   mt_order_duplicate_farhan.app_status


						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}

	public function getMerchantById($merchant_id)
	{
		$this->db->where('merchant_id',$merchant_id);
		$query = $this->db->get('mt_merchant');
		return $query->row('restaurant_name');
	}

	public function getClientById($client_id)
	{	
		$this->db->select('first_name,last_name,contact_phone,street');
		$this->db->where('client_id',$client_id);
		$query = $this->db->get('mt_client');
		return $query->result_array();
	} 

	public function getClientAddressById($order_id,$client_id)
	{	
		$this->db->select('street,city,state,location_name');
		$this->db->where('client_id',$client_id);
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_order_delivery_address');
		return $query->row();
	}

	public function getItemById($item_id)
	{
		$this->db->where('item_id',$item_id);
		$query = $this->db->get('mt_item');
		return $query->row('item_name');
	}

	public function statusUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);

		$this->db->where('order_id',$order_id);
		$this->db->update('history', $data);
		
			/* TO UPDATE ORDER STATUS IN mt_order TABLE - BY ALiGNWEBS 23-Mar-2018 4:31 PM */
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order', $data);
		/* END */
	}

	public function purchaseValueUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}

	public function totalWTaxValueUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}
	
		public function deliveryChargeUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}
    
	public function deliveryAgentUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function localityUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	} 

	public function deliveryTime2Update($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function commentUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function reminderUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}


	public function getRemindOrder($date)
	{
		$this->db->where('reminder !=', '');
		$this->db->like('date_created', $date);
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}

	public function getOrderById($order_id)
	{	
		$this->db->where('order_id',$order_id);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');




		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row();
	}

	// ADDED BY FARHAN
	public function getOrderByIdForItem($order_id)
	{	
		$this->db->where('mt_order_duplicate_farhan.order_id',$order_id);
		$this->db->where('mt_order_delivery_address.order_id',$order_id);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_order_delivery_address', 'mt_order_delivery_address.client_id = mt_order_duplicate_farhan.client_id');




		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row();
	}

	// END BY FARHAN

	public function updateOrderItem($order_id,$data)
	{
		
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan',$data);
	}

	public function decodeItemUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function insertRow($data)
	{
		$this->db->insert_batch('mt_available_agents_farhan',$data);
	}
	public function agentADD($data)
	{
		$this->db->insert("mt_available_agents_farhan",$data);
	}
	public function removeAgent($id)
	{
		$this->db->where("id",$id);
		$this->db->delete("mt_available_agents_farhan");
	}
	public function getAvailableAgents()
	{
		$this->db->select("
			mt_delivery_boys_farhan.name,
			mt_available_agents_farhan.id as avail_id,
			mt_available_agents_farhan.wType,
			agent_id,
			mt_delivery_boys_farhan.status,
			mobile
			");
		$this->db->join('mt_available_agents_farhan', 'mt_available_agents_farhan.agent_id = mt_delivery_boys_farhan.id');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsAA()
	{
		$this->db->where("wType",'a');
		$this->db->select("
			mt_delivery_boys_farhan.name,
			mt_available_agents_farhan.id as avail_id,
			mt_available_agents_farhan.wType,
			agent_id,
			mt_delivery_boys_farhan.status,
			mobile
			");
		$this->db->join('mt_available_agents_farhan', 'mt_available_agents_farhan.agent_id = mt_delivery_boys_farhan.id');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsBB()
	{
		$this->db->where("wType",'b');
		$this->db->select("
			mt_delivery_boys_farhan.name,
			mt_available_agents_farhan.id as avail_id,
			mt_available_agents_farhan.wType,
			agent_id,
			mt_delivery_boys_farhan.status,
			mobile
			");
		$this->db->join('mt_available_agents_farhan', 'mt_available_agents_farhan.agent_id = mt_delivery_boys_farhan.id');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}

	public function getAgents()
	{
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getJoinAgents()
	{
		$this->db->join('mt_available_agents_farhan', 'mt_delivery_boys_farhan.id = mt_available_agents_farhan.agent_id ','left');
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsA()
	{
		$this->db->where('wType','a');
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsB()
	{
		$this->db->where('wType','b');
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}


	public function agentUpdate($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function agentStatusUpdate($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function getDeliveryBoys()
	{
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}

	public function export($date)
	{
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = $date."- onlinekaka.csv";
        
     

		$this->db->select('*');
	
		$this->db->order_by('REF', 'asc'); 

		$query = $this->db->get('mt_farhan_excel');
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $this->db->truncate('mt_farhan_excel');
        force_download($filename, $data);
	}
	
	public function updateDeliveryAgent($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
		
	} 


	public function checkPaymentReference($order_id)
	{
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_payment_order');
		return $query->row('payment_reference');
		
		
	}

	// AFTER SEND SMS CHANGE DELIVERY BOYS COLOR TO RED

	public function agentStatusUpdateByName($data,$agentName)
	{
		$this->db->where('name',$agentName);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function getOrderForDecodeItem($first_order_id)
	{

		
		$this->db->where('delivery_date',date('Y-m-d'));
		$this->db->where('order_id >',$first_order_id);
		$this->db->order_by('order_id','desc');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();

	} 
	public function workerUpdate($order_id,$worker)
	{
		$this->db->set('workerType', $worker);  //Set the column name and which value to set..

		$this->db->where('order_id', $order_id); //set column_name and value in which row need to update

		$this->db->update('mt_order_duplicate_farhan'); //Set your table name
	}
	public function workerUpdateAgent($agentID,$worker)
	{
		$this->db->set('wType', $worker);  //Set the column name and which value to set..

		$this->db->where('id', $agentID); //set column_name and value in which row need to update

		$this->db->update('mt_available_agents_farhan'); //Set your table name
	}

	// AFTER DELIVER THE ORDER DELIVERY BOYS COLOR TO GREEN

	public function agentStatusGreenByName($data,$agentName)
	{	
		$this->db->where('delivery_date',date('Y-m-d'));
		$this->db->where('delivery_agent',$agentName);
		$this->db->where('status','Processing');
		$query = $this->db->get('mt_order_duplicate_farhan');
		$result = $query->row('delivery_agent');

		if(isset($result))
		{
			//print_r($data);

		}else{

		$this->db->where('name',$agentName);
		$this->db->update('mt_delivery_boys_farhan', $data);

		}
 
	}

	public function getOrdersOfDeliveryBoys($agentName)
	{	
         
		 $this->db->where('delivery_date',date('Y-m-d'));
		 $this->db->where('delivery_agent',$agentName);
		 $this->db->where('status','Processing');
		 $this->db->select('delivery_agent, COUNT(delivery_agent) as no_of_orders');
		 $this->db->group_by('delivery_agent'); 
		 $this->db->order_by('no_of_orders', 'desc'); 
		 $query = $this->db->get('mt_order_duplicate_farhan');
		 return $query->row('no_of_orders');


	}

	public function getAvailableAgentsByName($agentName)
	{
		$this->db->where('name',$agentName);
		$query = $this->db->get('mt_available_agents_farhan');
		$result = $query->result_array();

		if(sizeof($result) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getLocality()
	{
		$query = $this->db->get('mt_farhan_locality');
		return $query->result_array();
	}

		// CODE ADDED BY FARHAN

		// FOR FILTER
			public function getFilteredListByDeliveryDate($from,$to)
			{
				if($to == '')
				{

					$this->db->where('delivery_date',$from);
					
				}else{

					 $this->db->where('delivery_date >=', $from);
	           		 $this->db->where('delivery_date <=', $to);
				}

				$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');


				$this->db->select('mt_order_duplicate_farhan.merchant_id,
										mt_order_duplicate_farhan.status,
										mt_order_duplicate_farhan.locality,
										mt_order_duplicate_farhan.payment_type,
										mt_order_duplicate_farhan.delivery_agent,
				 					   mt_merchant.restaurant_name,');
				$query = $this->db->get('mt_order_duplicate_farhan');
				     return $query->result_array();
				 
			}

			// FOR FILTER
				public function getOrder2($from,$values,$type,$to)
				{	

			            if($to == '')
						{

							$this->db->where('delivery_date',$from);
							
						}else{

							 $this->db->where('delivery_date >=', $from);
			           		 $this->db->where('delivery_date <=', $to);
						}

			            switch ($type) {
			            	case 'agent':
			            		$this->db->where_in('delivery_agent', $values);
			            		break;
			            	case 'merchant':
			            		$this->db->where_in('mt_order_duplicate_farhan.merchant_id', $values);
			            		break;
			            	case 'payment_type':
			            		$this->db->where_in('mt_order_duplicate_farhan.payment_type', $values);
			            		break;
			            	case 'status':
			            		$this->db->where_in('mt_order_duplicate_farhan.status', $values);
			            		break;
			            	case 'locality':
			            		$this->db->where_in('mt_order_duplicate_farhan.locality', $values);
			            		break;
			            }
			        
					
					$this->db->order_by('order_id','desc');

					$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
					$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

					$this->db->select('mt_order_duplicate_farhan.order_id,
										mt_order_duplicate_farhan.decode_item,
									   mt_merchant.restaurant_name,
									   mt_client.first_name,
									   mt_client.last_name,
									   mt_client.contact_phone,
									   mt_order_duplicate_farhan.payment_type,
									   mt_order_duplicate_farhan.reminder,
									   mt_order_duplicate_farhan.purchase_value,
									   mt_order_duplicate_farhan.total_w_tax,
									   mt_order_duplicate_farhan.delivery_charge,
									   mt_order_duplicate_farhan.status,
									   mt_order_duplicate_farhan.delivery_agent,
									   mt_order_duplicate_farhan.locality,
									   mt_order_duplicate_farhan.date_created,
									   mt_order_duplicate_farhan.delivery_time,
									   mt_order_duplicate_farhan.delivery_time2,
									   mt_order_duplicate_farhan.comment,
									   mt_order_duplicate_farhan.workerType,
									   mt_order_duplicate_farhan.app_status

									  ');
					$query = $this->db->get('mt_order_duplicate_farhan');
				    return $query->result_array();
				
					
				}


				// FOR FILTER
				public function getOrder3($from,$to,$merchant,$status,$locality,$payment_type,$agent)
				{	

			            if($to == '')
						{

							$this->db->where('delivery_date',$from);
							
						}else{

							 $this->db->where('delivery_date >=', $from);
			           		 $this->db->where('delivery_date <=', $to);
						}

					$this->db->where_in('delivery_agent', $agent);
					$this->db->where_in("mt_order_duplicate_farhan.locality",$locality);
					$this->db->where_in("mt_order_duplicate_farhan.payment_type",$payment_type);
					$this->db->where_in("mt_order_duplicate_farhan.status",$status);
					$this->db->where_in("mt_order_duplicate_farhan.merchant_id",$merchant);


					$this->db->order_by('order_id','desc');

					$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
					$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

					$this->db->select('mt_order_duplicate_farhan.order_id,
										mt_order_duplicate_farhan.decode_item,
									   mt_merchant.restaurant_name,
									   mt_client.first_name,
									   mt_client.last_name,
									   mt_client.contact_phone,
									   mt_order_duplicate_farhan.payment_type,
									   mt_order_duplicate_farhan.reminder,
									   mt_order_duplicate_farhan.purchase_value,
									   mt_order_duplicate_farhan.total_w_tax,
									   mt_order_duplicate_farhan.delivery_charge,
									   mt_order_duplicate_farhan.status,
									   mt_order_duplicate_farhan.delivery_agent,
									   mt_order_duplicate_farhan.locality,
									   mt_order_duplicate_farhan.date_created,
									   mt_order_duplicate_farhan.delivery_time,
									   mt_order_duplicate_farhan.delivery_time2,
									   mt_order_duplicate_farhan.comment,
									   mt_order_duplicate_farhan.workerType,
									   mt_order_duplicate_farhan.app_status

									  ');
					$query = $this->db->get('mt_order_duplicate_farhan');
				    return $query->result_array();
				
					
				}

			public function getMerchantById2($merchant_id)
			{
				$this->db->where('merchant_id',$merchant_id);
				
				$query = $this->db->get('mt_merchant');
				return $query->row();

			}

				// END BY FARHAN
				
					public function updateClientStreet($order_id,$data)
			{	

				$this->db->where("order_id",$order_id);
				$this->db->update("mt_order_delivery_address",$data);

			}
			
				// ADDED BY FARHAN 29/04/2018
			public function updateClientEmail($client_id,$data)
			{	

				$this->db->where("client_id",$client_id);
				$this->db->update("mt_client",$data);

			}

			public function updateSentEmail($order_id)
			{	
				$data = array(

					"sent_email" => 1

				);
				$this->db->where("order_id",$order_id);
				$this->db->update("mt_order_duplicate_farhan",$data);

			}

			// ADDED BY FARHN 13 MAY 2018

			public function getClientId($contact_phone){

				$this->db->select('client_id');
				$this->db->where('contact_phone !=','');
				$this->db->where('contact_phone',$contact_phone);
				$query = $this->db->get('mt_client');
				$client_id = array_column($query->result_array(), 'client_id');
				
				return $client_id;

			}

			public function getNumberOfOrders($client_id){

				if(sizeof($client_id) > 0)
				{
				$this->db->where_in('client_id',$client_id);
				
				$num = $this->db->count_all_results('mt_order_duplicate_farhan');
				
				return $num;
				
				}else{

					return 0;
				}
			}
			
			// ADDED BY FARHAN 19 MAY 2018
			public function getPurchaseValueFromItem($item_id)
			{
				
				$this->db->where("item_id",$item_id);
				return $this->db->get('mt_item')->row()->purchase_value;
			}

		
			public function getItemForPurchaseValue($order_id)
			{	
				$this->db->select('json_details,purchase_value');
				$this->db->from('mt_order_duplicate_farhan');
				$this->db->where("order_id",$order_id);
				return $this->db->get()->row();
			
			}

			// END BY FARHAN 19 MAY 2018

			// public function getOrderByNumber($client_id){

			// 			$client_id = explode(",", $client_id);
					
			// 			$this->db->where_in('mt_order_duplicate_farhan.client_id',$client_id);
			// 			$this->db->order_by('order_id','desc');

			// 			$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
			// 			$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

			// 			$this->db->select('mt_order_duplicate_farhan.order_id,
			// 							   mt_merchant.restaurant_name,
			// 							   mt_client.first_name,
			// 							   mt_client.last_name,
			// 							   mt_client.contact_phone,
			// 							   mt_order_duplicate_farhan.payment_type,
			// 							   mt_order_duplicate_farhan.reminder,
			// 							   mt_order_duplicate_farhan.purchase_value,
			// 							   mt_order_duplicate_farhan.total_w_tax,
			// 							   mt_order_duplicate_farhan.delivery_charge,
			// 							   mt_order_duplicate_farhan.status,
			// 							   mt_order_duplicate_farhan.delivery_agent,
			// 							   mt_order_duplicate_farhan.locality,
			// 							   mt_order_duplicate_farhan.date_created,
			// 							   mt_order_duplicate_farhan.delivery_time,
			// 							   mt_order_duplicate_farhan.delivery_time2,
			// 							   mt_order_duplicate_farhan.comment,
			// 							   mt_order_duplicate_farhan.workerType,
			// 							   mt_order_duplicate_farhan.app_status,
			// 							    mt_order_duplicate_farhan.date_created,



			// 							  ');
			// 			$query = $this->db->get('mt_order_duplicate_farhan');
			// 		    return $query->result();

			// }

			// END BY FARHAN 13 MAY 2018

			// END BY FARHAN 29/04/2018
}
