
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Add a User</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url(); ?>admin/storeRole">

          <div class="form-group">
            <?php echo form_error('role','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="role" type="text" value="<?php echo set_value('role'); ?>" name="role" placeholder="Enter Role">
          </div>

          <button type="submit" name="roleAdd" class="btn btn-primary btn-block">Add</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
