        <div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SL No.</th>
                  <th>Role</th>
                   <th>Action</th>
                 
                </tr>
              </thead>
              <tbody>
                <?php foreach($roles as $key => $role) { ?>
                <tr>
                  <td><?php echo $key + 1; ?></td>
                  <td><?php echo $role->role; ?></td>

                  <td>
                    <a href="<?php echo base_url(); ?>admin/roleEdit/<?php echo $role->id ?>" class="btn btn-primary btn-sm">Edit</a>
                    <a href="<?php echo base_url(); ?>admin/roleDelete/<?php echo $role->id ?>" onclick="return confirm('Do you want to delete?');" class="btn btn-danger btn-sm">Delete</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>