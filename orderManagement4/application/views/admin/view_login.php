<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin - Order Management</title>
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>assets/css/admin/bootstrap.min.css" rel="stylesheet" type="text/css">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>assets/css/admin/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>assets/css/admin/sb-admin.min.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Admin</div>
      <div class="card-body">
        <?php echo $this->session->flashdata('credential');?>
        <form method="post" action="<?php echo base_url() ?>admin/login">
          <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <?php echo form_error('username','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="exampleInputEmail1" type="text" value="<?php echo set_value('username'); ?>" name="username" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <?php echo form_error('password','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="exampleInputPassword1" type="password" value="<?php echo set_value('password'); ?>" name="password" placeholder="Password">
          </div>
  
          <button type="submit" name="login" class="btn btn-primary btn-block" href="index.html">Login</button>
        </form>
        <div class="text-center">
        <!--   <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a> -->
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/admin/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url() ?>assets/js/admin/jquery.easing.min.js"></script>
</body>

</html>