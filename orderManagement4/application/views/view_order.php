<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

$date = date('Y-m-d');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Order Management System</title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/admin.ico">

	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datepick.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/selectize.bootstrap3.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/richtext.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style type="text/css">

	.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border: 1px solid #2d2424;}

	tr.reminder { 
		animation: blinker 1s linear infinite;
	}

	@keyframes blinker {  
	  50% { opacity: 0.6; }
	}
	.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 4px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding:10px 5px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}

	/*ADD CODE FARHAN*/

	.farhan-filter{

		cursor: pointer;
	}
	.filter-box-agent, .filter-box-merchant, .filter-box-payment_type, .filter-box-status, .filter-box-locality{

		background: #fff;
		position: absolute;
		top: 100px;
		padding: 20px;
		box-shadow: 1px px solid black;
		-webkit-box-shadow: 0 0 10px 0 #08040F;
		box-shadow: 0 0 18px 0 #08040F;
		border: 1px solid #968d8d;
	}

	.filter-box-agent{

		left: 875px;
	}

	.filter-box-merchant{

		left: 100px;
	}

	.filter-box-payment_type{

		left: 300px;

	}

    .filter-box-status{

		left:500px;

	}

	.filter-box-locality{

		left:700px;

	}
	.search_filter {
		float: right;
		margin-right: 10px;
		margin-top: -23px;
		position: relative;
		z-index: 2;
		color: black;
	}
	.filter_options {
		padding: 4px;
		background: #c1c1c1;
		margin: 2px;
	}
	label {
		display: block;
		padding-left: 15px;
		text-indent: -15px;
	}
	.checkBoxClass {
		width: 25px;
		height: 25px;
		padding: 0;
		margin:0;
		vertical-align: bottom;
		position: relative;
		top: -1px;
		*overflow: hidden;
	}
	.searh_fields{

		margin: 10px 0px;

	}
	.filter_options {
		/* Remove default list styling */
		list-style-type: none;
		padding: 0;
		margin: 0;
	}

	.filter_options li a {
		border: 1px solid #ddd; /* Add a border to all links */
		margin-top: -1px; /* Prevent double borders */
		background-color: #f6f6f6; /* Grey background color */
		padding: 12px; /* Add some padding */
		text-decoration: none; /* Remove default text underline */
		font-size: 18px; /* Increase the font-size */
		color: black; /* Add a black text color */
		display: block; /* Make it into a block element to fill the whole list */
	}

	.filter_options li a:hover:not(.header) {
		background-color: #eee; /* Add a hover effect to all links, except for headers */
	}

	/*END CODE FARHAN*/
</style>
</head>
<body>

	<div class="row">
		<div class="col-md-11">
			<div class="panel panel-default">
  				<div class="panel-heading" style="   position: fixed;
    height: 60px;
    top: 0;
    width: 100%;
    z-index: 100;">
  					<div class="row">
  						<div class="col-md-1">
  						<img src="https://www.onlinekaka.com/upload/1476254450-1475737169-kaka-logo-(1).png" width="84">
  						</div>
  						<div id="header" style="display: none;">
					  	<div class="col-md-2">
							<div class="form-group">
							
									<input type="text" name="from" id="from" class="form-control" placeholder="From" readonly="readonly">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
							
									<input type="text" name="to" id="to" class="form-control" placeholder="To" readonly="readonly">
							</div>
						</div>
						<div class="col-md-1">
						<!-- CHANGES BY FARHAN -->
						<!-- FOR PASSWORD PROTECTION -->
						<a href="javascript:void(0)" class="btn btn-default" onclick="commonBox('search')">Search</a>
						</div>

						<div class="col-md-1">
							<a class="btn btn-default btn-sm" target="_blank" onclick="commonBox('export')" href="javascript:void(0)">Export(CSV)</a>
					    </div>
					    <!-- END BY FARHAN -->
					 </div>
					 <div class="col-md-1">
					 	<button class='btn btn-default btn-sm' id='bellDiv'><i class="fa fa-fw fa-bell"></i></button>
					 	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-gear"></i>
     
    </button>

    <ul class="dropdown-menu">
      <li><a onclick="setBell(1)"><i class="fa fa-bell fa-fw"></i>On</a></li>
      <li><a onclick="setBell(0)"><i class="fa fa-bell-slash fa-fw"></i>Off</a></li>
      <li><a onclick="setBellTime();"><i class="fa fa-bell-slash fa-fw"></i>Temporary Off</a></li>
    </ul>
    <script type="text/javascript">
    	function setBell(flag)
    	{
    		$("#bellSetting").val(flag);
    		if (flag == 0) $("#bellDiv").html("<i class='fa fa-bell-slash'></i>");
    		if (flag == 1) $("#bellDiv").html("<i class='fa fa-bell'></i>");
    	}
    	function setBellTime()
    	{
    		//console.log("bell timer is called");
    		$("#bellSetting").val("0");
    		$("#bellDiv").html("<i class='fa fa-bell-slash'></i>")
    		var sec = 0;
    		var myVar2;
			myVar2 = setInterval(function(){
				sec = sec + 60;
				console.log($("#bellSetting").val());
				if (sec > '300') 
				{
					clearInterval(myVar2);
					$("#bellDiv").html("<i class='fa fa-bell'></i>")
					$("#bellSetting").val("1");
				}	
			}, 60000);
			 //myVar = setTimeout(setBellOn(), 3000);
    	}
    </script>
					 </div>
					<div class="col-md-1">
					 	<p>Panel<br> <button style="background:yellow;border:1px solid blue">A</button>&nbsp;<button style="background:blue;color:white;border:1px solid black">B</button></p>
					</div>
					<div class="col-md-3 pull-right">
						<!-- ADDED BY FARHAN  6 MAY 2018 -->
						<?php if($role != 'Junior Manager') { ?>
							<a class="btn btn-default text-left" id="showSearch" href="javascript:void(0)">SHOW</a><a class="btn btn-default " target='_blank' href="<?php echo base_url()?>order/agents">Agents</a>
							<?php } ?>
							<a class="btn btn-default text-left"  href="<?php echo base_url().'login/logout' ?>"><i class="fa fa-sign-out"></i></a>
						<!-- END BY FARHAN -->
							<!-- ADDED BY FARHAN -->
							<a class="btn btn-default text-left" id="pause" onclick="pause()" href="javascript:void(0)">
								<?php if($refresh == '' || $refresh == 0) { ?>

									PAUSE

								<?php }else { ?>

									REFRESH

								<?php }?>
							</a>
							<!-- END BY FARHAN -->
					</div>
					
					<!--     <div class="col-md-1 pull-right">
							<a class="btn btn-default text-left"  id="refresh" href="javascript:void(0)">REFRESH</a>
					    </div> -->
					</div>
					 <div class="col-md-1">
					 	<div id='loadingmessage' style='display:none;'>
						  Please Wait
						</div>
					</div>

				</div>
			</div>

 			<div class="panel-body" style="padding: 0px;">

 			<!-- ADD CODE BY FARHAN -->

 			<input type="hidden" value="<?php echo isset($refresh) ? $refresh : 0; ?>" id="refresh">
 			<!-- AGENTS -->
 			<div id="allFilters">
 			<div class="row filter-block-agent" style="display: none">
 				<div class="col-md-12">
 					<div class="filter-box-agent">
 						<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
 							<div class="filter_by_values">Filter by values...</div>
 							<div class="select_clear_values">
 								<a href="javascript:void(0)" onclick="selectAll('agent')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('agent')">Clear</a>
 							</div>
 							<div class="searh_fields">

 								<input type="text" class="form-control" name="filter_search" id="filter_search_agent" onkeyup="searchFilter('agent')"><span class="fa fa-search search_filter" onclick="searchFocus('agent')"></span>
 							</div>
 							<div class="list_of_fields">
 								<ul id="filter_options_agent" class="filter_options">
 									<!-- GET DELVIERY AGENTS -->
 									<?php foreach(uniqueAssocArray($getFilteredLists, 'delivery_agent') as $get_available_agent) { ?>

 									

 									<li>
 										<a href="#">
 											<label><input type="checkbox" name="agent[]" checked value="<?php echo $get_available_agent['delivery_agent'] ?>" class="checkBoxClass checkBox-agent"> <?php echo $get_available_agent['delivery_agent']; ?></label>
 										</a>
 									</li>


 									<?php } ?>
 								</ul>
 							</div>
 						</div>
 						<hr>
 						<div class="filter-footer">
 							<button type="button" class="btn btn-primary" onclick="getSearchResult('agent')">OK</button>
 							<button type="button" class="btn btn-default" onclick ="cancelBlock('agent')">Cancel</button>
 						</div>
 					</div>

 				</div>
 			</div>
 			<!-- END AGENTS -->

 			<!-- MERCHANTS -->
 			<div class="row filter-block-merchant" style="display: none">
 				<div class="col-md-12">
 					<div class="filter-box-merchant">
 						<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
 							<div class="filter_by_values">Filter by values...</div>
 							<div class="select_clear_values">
 								<a href="javascript:void(0)" onclick="selectAll('merchant')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('merchant')">Clear</a>
 							</div>
 							<div class="searh_fields">

 								<input type="text" class="form-control" name="filter_search" id="filter_search_merchant" onkeyup="searchFilter('merchant')"><span class="fa fa-search search_filter" onclick="searchFocus('merchant')"></span>
 							</div>
 							<div class="list_of_fields">
 								<ul id="filter_options_merchant" class="filter_options">
 									<!-- GET DELVIERY AGENTS -->
 									<?php foreach(uniqueAssocArray($getFilteredLists, 'restaurant_name') as $getMerchant) { ?>



 									<li>
 										<a href="#">
 											<label><input type="checkbox" name="merchant[]" checked value="<?php echo $getMerchant['merchant_id'] ?>" class="checkBoxClass checkBox-merchant"> <?php echo $getMerchant['restaurant_name']; ?></label>
 										</a>
 									</li>


 									<?php } ?>
 								</ul>
 							</div>
 						</div>
 						<hr>
 						<div class="filter-footer">
 							<button type="button" class="btn btn-primary" onclick="getSearchResult('merchant')">OK</button>
 							<button type="button" class="btn btn-default" onclick ="cancelBlock('merchant')">Cancel</button>
 						</div>
 					</div>

 				</div>
 			</div>
 			<!-- END MERCHANT -->
 			<!-- PAYMENT TYPE -->
 			<div class="row filter-block-payment_type" style="display: none">
 				<div class="col-md-12">
 					<div class="filter-box-payment_type">
 						<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
 							<div class="filter_by_values">Filter by values...</div>
 							<div class="select_clear_values">
 								<a href="javascript:void(0)" onclick="selectAll('payment_type')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('payment_type')">Clear</a>
 							</div>
 							<div class="searh_fields">

 								<input type="text" class="form-control" name="filter_search" id="filter_search_payment_type" onkeyup="searchFilter('payment_type')"><span class="fa fa-search search_filter" onclick="searchFocus('payment_type')"></span>
 							</div>
 							<div class="list_of_fields">
 								<ul id="filter_options_payment_type" class="filter_options">
 									<!-- GET DELVIERY AGENTS -->
 									<?php foreach(uniqueAssocArray($getFilteredLists, 'payment_type') as $payment_type) { ?>



 									<li>
 										<a href="#">
 											<label><input type="checkbox" name="payment_type[]" checked value="<?php echo $payment_type['payment_type'] ?>" class="checkBoxClass checkBox-payment_type"> <?php echo $payment_type['payment_type']; ?></label>
 										</a>
 									</li>


 									<?php } ?>
 								</ul>
 							</div>
 						</div>
 						<hr>
 						<div class="filter-footer">
 							<button type="button" class="btn btn-primary" onclick="getSearchResult('payment_type')">OK</button>
 							<button type="button" class="btn btn-default" onclick ="cancelBlock('payment_type')">Cancel</button>
 						</div>
 					</div>

 				</div>
 			</div>
 			<!-- END PAYMENT TYPE -->
 			<!-- LOCALITY -->
 			<div class="row filter-block-locality" style="display: none">
 				<div class="col-md-12">
 					<div class="filter-box-locality">
 						<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
 							<div class="filter_by_values">Filter by values...</div>
 							<div class="select_clear_values">
 								<a href="javascript:void(0)" onclick="selectAll('locality')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('locality')">Clear</a>
 							</div>
 							<div class="searh_fields">

 								<input type="text" class="form-control" name="filter_search" id="filter_search_locality" onkeyup="searchFilter('locality')"><span class="fa fa-search search_filter" onclick="searchFocus('locality')"></span>
 							</div>
 							<div class="list_of_fields">
 								<ul id="filter_options_locality" class="filter_options">
 									<!-- GET DELVIERY AGENTS -->
 									<?php foreach(uniqueAssocArray($getFilteredLists, 'locality') as $locality) { ?>

 									
 									<li>
 										<a href="#">
 											<label><input type="checkbox" name="locality[]" checked value="<?php echo $locality['locality'] ?>" class="checkBoxClass checkBox-locality"> <?php echo $locality['locality']; ?></label>
 										</a>
 									</li>

 									
 									<?php }?>
 								</ul>
 							</div>
 						</div>
 						<hr>
 						<div class="filter-footer">
 							<button type="button" class="btn btn-primary" onclick="getSearchResult('locality')">OK</button>
 							<button type="button" class="btn btn-default" onclick ="cancelBlock('locality')">Cancel</button>
 						</div>
 					</div>

 				</div>
 			</div>
 			<!-- END LOCALITY -->
 			<!-- STATUS-->
 			<div class="row filter-block-status" style="display: none">
 				<div class="col-md-12">
 					<div class="filter-box-status">
 						<div style="overflow-y: scroll; overflow-x: scroll;  height:300px; width: 300px;">
 							<div class="filter_by_values">Filter by values...</div>
 							<div class="select_clear_values">
 								<a href="javascript:void(0)" onclick="selectAll('status')">Select all</a> - <a href="javascript:void(0)" onclick="clearCheckBox('status')">Clear</a>
 							</div>
 							<div class="searh_fields">

 								<input type="text" class="form-control" name="filter_search" id="filter_search_status" onkeyup="searchFilter('status')"><span class="fa fa-search search_filter" onclick="searchFocus('status')"></span>
 							</div>
 							<div class="list_of_fields">
 								<ul id="filter_options_status" class="filter_options">
 									<!-- GET DELVIERY AGENTS -->
 									<?php foreach(uniqueAssocArray($getFilteredLists, 'status') as $status) { ?>

 									
 									
 									<li>
 										<a href="#">
 											<label><input type="checkbox" name="status[]" checked value="<?php echo $status['status'] ?>" class="checkBoxClass checkBox-status"> <?php echo $status['status']; ?></label>
 										</a>
 									</li>
 									
 									
 									<?php } ?>
 								</ul>
 							</div>
 						</div>
 						<hr>
 						<div class="filter-footer">
 							<button type="button" class="btn btn-primary" onclick="getSearchResult('status')">OK</button>
 							<button type="button" class="btn btn-default" onclick ="cancelBlock('status')">Cancel</button>
 						</div>
 					</div>
 					
 				</div>
 			</div>
 			<!-- END STATUS -->
 			</div>
 			<!-- END CODE FARHAN -->		
			<table class="table table-bordered" id="mainTable" style="font-weight: bold; margin-top: 37px;
">
				<thead>
					<tr style="background-color: black; color: #fff;">
						<th width="1%">ID</th>
						<th width="1%">REF#</th>
						<th class="th-merchant" width="1%">MERCHANT NAME <i class="fa fa-filter farhan-filter" onclick="display('merchant')"></i></th> <!--  CHANGES BY FARHAN    -->    
						<th width="1%">NAME</th>
						<th width="1%">MOBILE</th>
						<th width="1%">ITEM</th>            

						<th class="th-payment_type" width="1%">PT <i class="fa fa-filter farhan-filter" onclick="display('payment_type')"></i></th> <!--  CHANGES BY FARHAN    --> 
			           <!--  <th width="1%">Total</th>
			           	<th width="1%">Tax</th> -->
			           	<th width="1%">R</th>
			           	<th width="1%">PV</th>  
			           	<th width="1%">DV</th>
			           	<th width="1%">DF</th>
			           	<th class="th-status" width="1%">STATUS <i class="fa fa-filter farhan-filter" onclick="display('status')"></i></th>	<!--  CHANGES BY FARHAN    --> 
			           	<th class="th-agent" width="1%">AGENT <i class="fa fa-filter farhan-filter" onclick="display('agent')"></i></th> <!--  CHANGES BY FARHAN    --> 
			           	<th class="th-locality" width="1%">Locality <i class="fa fa-filter farhan-filter" onclick="display('locality')"></i></th> <!--  CHANGES BY FARHAN    --> 
			            <th width="1%">CON</th>
			            <th width="1%">DEL</th>

			            <th width="1%">COMMENT</th>
			            <th width="1%">SMS</th>  
			         
					</tr>
				</thead>
				<tbody id="myData" style="color:#000000;">
					
				</tbody>
			</table>
		<div id="loader">Please Wait...</div>
		</div>
	</div>
	<div class="col-md-1" id="agentsData">

	<table class="table table-bordered"  style="font-weight: bold;;margin-top: 58px; margin-left: -40px;">
		<thead>
		<tr style="background-color: black; color: #fff;">
			<th></th>
			<th>
				Delivery Boys	
			</th>
			<th></th>
			
		<tr>
		</thead>
		<tbody>
			<?php foreach($get_available_agents as $get_available_agent) { ?>
				<?php $no_of_orders = $CI->ModelOrder->getOrdersOfDeliveryBoys($get_available_agent['name']); ?>
				<tr>
					<?php 
							$bgClr = "";
							if (empty($get_available_agent['wType']) && !empty($get_available_agent['name']))
							{
								$bgClr = "";
							}
							elseif ($get_available_agent['wType'] == 'a' && !empty($get_available_agent['name'])) {
								$bgClr = "yellow";

							}elseif (!empty($get_available_agent['name']))
							{
								$bgClr = "blue";
							}
						 ?>
					<td style="background: <?php echo $bgClr ?>;padding-top:6px">
						 <span class="dropdown">
      						<div class="dropdown">
							  <button class="dropbtn" style="font-size: 10pt;margin:0"><i class="fa fa-arrow-down"></i></button>
							  <div class="dropdown-content">
							    <?php 
							if (empty($get_available_agent['wType']) && !empty($get_available_agent['name']))
							{
							?>
							
						
								<a  href="#" id="span<?php echo $get_available_agent['avail_id']; ?>" onclick="assignWorker('a','<?php echo $get_available_agent['avail_id']; ?>')"  >Assign to "A"</a>
								<a id="span<?php echo $get_available_agent['avail_id']; ?>"  onclick="assignWorker('b','<?php echo $get_available_agent['avail_id']; ?>')"  >Assign to "B"</a>
							
							
							<?php
								
							}
							elseif ($get_available_agent['wType'] == 'a' && !empty($get_available_agent['name'])) {
							?>
							
							
							<a id="span<?php echo $get_available_agent['avail_id']; ?>"  href="#" onclick="assignWorker('b','<?php echo $get_available_agent['avail_id']; ?>')"  >Assign to "B"</a>
							
							<?php
							}elseif (!empty($get_available_agent['name']))
							{
							?>
							<span >
							<a id="span<?php echo $get_available_agent['avail_id']; ?>"   href="#" onclick="assignWorker('a','<?php echo $get_available_agent['avail_id']; ?>')"  >Assign to "A"</a>
							</span>
							<?php
							}
						 ?>

<!-- 
							    <a href="#" onclick="available_agents(<?php echo $get_available_agent['id']; ?>)">Edit    </a> -->
							  </div>
							</div>
     					<span>
					</td>
					<td style="color:black;padding:6px; padding-top:12px;;background-color: <?php echo deliveryBoysColor($no_of_orders); ?>" id="name<?php echo $get_available_agent['avail_id']; ?>" onclick="available_agents(<?php echo $get_available_agent['avail_id']; ?>)" contenteditable="true">
							<?php echo $get_available_agent['name'];  ?>
							 <?php //echo $no_of_orders ?> 
					</td>
					<?php if($get_available_agent['status'] == 'true')
						 {$statusBg = "background:green";$status = "on";} 
						 else
						 {$statusBg = "background:red";$status="off";}
					 ?>
					 <td style="color:white;<?=$statusBg?>"><?=$status?></td>
			   
			
			     </tr>
			<?php } ?>
				<tr>
					<td></td>
					<td><button class="btn btn-sm btn-primary brad0" onclick="available_agents('0')">Add New</button></td>
					<td></td>
				</tr>
		</tbody>
	</table>

	</div>
</div>
</div>
<div class="alert alert-success alert-sm" id="showNotice" style="box-shadow: 2px 2px 11px black;position: fixed;top:49px;right:14px;z-index: 5555;display: none;"></div>
<input type="hidden" id="oldOrder" value="0">
<input type="hidden" id="bellSetting" value="1">
<input type="hidden" value="<?php echo date('Y-m-d',time())?>" id="myTime" >
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        
        </div>
      <!--   <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
  <!-- CODE ADDED BY FARHAN -->
  <!--FOR PASSWORD PROTECTION -->
  <!-- Modal -->
  <div class="modal fade" id="passwordModal" role="dialog">
  	<div class="modal-dialog modal-md">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title"></h4>
  			</div>
  			<div class="modal-body2" style="position: relative;padding: 15px;">
  				<div class="row">
  					
  					<div class="col-md-12">
  						<div id="error"></div>
  						<form id="passwordForm">
  							<input type="hidden" id="checkType">
  							<div class="form-group">
  								<label>Password</label>
  								<input type="password" name="password" id="password" class="form-control" placeholder="Enter Password" required="">
  							</div>
  							<div class="form-group">
  								<button type="submit" class="btn btn-success">Submit</button>
  							</div>
  						</form>
  					</div>
  				</div>
  			</div>
        <!--   <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
    </div>
  </div>
  </div>
  <!-- END BY FARHAN -->
<script src="<?php echo base_url();  ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url();  ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.timepicker.js"></script>
<script src='https://cdn.rawgit.com/admsev/jquery-play-sound/master/jquery.playSound.js'></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/selectize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.richtext.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">


	var myInterval;
	
	$(document).ready(function () {

	

		//$('#oldOrder').val($('#newOrder').val());
		console.log("old:"+$("#oldOrder").val());
		
		// ADDED BY FARHAN
		var refresh = $("#refresh").val();
		
		if(refresh == 0)
		{

			ajaxRefresh();
			
		}else{

			OnetimeAjaxRefresh();

		}
		
		// END BY FARHAN
	})

	function OnetimeAjaxRefresh()
	{

						$('#loadingmessage').show();

						var from2 = $("#from").val();
						var to2 = $("#to").val();
						var date = '<?php echo $date; ?>';

						var order_id = $("#mainTable").find('tbody td:eq(1)').html();


						if((from2 ==='') || (from2 === date)){


							$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date,order_id:order_id},
								function(data){


									$('#loadingmessage').hide(); 
									$('#loader').hide(); 

										  		// REFRESH TABLE DATA

										  		$("#myData").html(data);

											 	// REFRESH DELIVERY AGENT

											 	$("#agentsData").load(location.href + " #agentsData");
											 	var bell = $("#bellSetting").val();
											 	var newOrder = $("#newOrder").val();
											 	var oldOrder = $("#oldOrder").val();

											 	if (newOrder > oldOrder) 
											 	{
											 		var count = -1;
											 		var diff = eval(newOrder-oldOrder);
											 		$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();

											 		if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");console.log('play bell:'+bell);}
											 		console.log(eval(newOrder-oldOrder));
											 		$( "tr" ).each(function( index, element ) {
											 			if (count != -1) { $( element ).css( "border", "2px solid blue" );}

											 			count++;
											 			if (count == diff) {console.log("stopped at"+count);return false;}
											 		});
									// console.log("yes new order found click the bell");
									$("#oldOrder").val(newOrder);
								}


			 									// REMINDER

			 									reminderHighlight();

			 								});

						}

	}

	function deliveryAgent(order_id)
		{	
			
			$(".modal-title").html('Edit Delivery Agent');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			var delivery_agent = $("#delivery_agent"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryAgent",{order_id:order_id,delivery_agent:delivery_agent}, 
			 	function(data){
   	
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	

    function ajaxRefresh()
		{

				 	myInterval = setInterval(function(){	

				 	$('#loadingmessage').show();
							 	
					var from2 = $("#from").val();
					var to2 = $("#to").val();
					var date = '<?php echo $date; ?>';
					
					var order_id = $("#mainTable").find('tbody td:eq(1)').html();

  
						 if((from2 ==='') || (from2 === date)){
								
									
								$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date,order_id:order_id},
								 	function(data){

								 
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
								 		var bell = $("#bellSetting").val();
					var newOrder = $("#newOrder").val();
					var oldOrder = $("#oldOrder").val();
					
					if (newOrder > oldOrder) 
					{
						var count = -1;
						var diff = eval(newOrder-oldOrder);
						$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();
						
						if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");console.log('play bell:'+bell);}
						console.log(eval(newOrder-oldOrder));
						$( "tr" ).each(function( index, element ) {
						    if (count != -1) { $( element ).css( "border", "2px solid blue" );}
						   
						    count++;
						    if (count == diff) {console.log("stopped at"+count);return false;}
						  });
						// console.log("yes new order found click the bell");
						$("#oldOrder").val(newOrder);
					}

 									
 									// REMINDER

 									reminderHighlight();

							    });

							}
					}, 5000);
		}
// 		function ajaxRefresh()
// 		{
			

// 				 	myInterval = setInterval(function(){	
// 					// console.log("new:"+$("#newOrder").val());
// 					// console.log("old:"+$("#oldOrder").val());

					
// 				 	$('#loadingmessage').show();
							 	
// 					var from = $("#from").val();
// 					 if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
// 					 var today =  $("#myTime").val();
// 					 if(today != from) return;
// 					var to2 = $("#to").val();
//   					if (to2.length === 0 ) {to2 = from;}

// 					var date = '<?php echo $date; ?>';
					
// 					var order_id = $("#mainTable").find('tbody td:eq(1)').html();
					
// 					//var order_id = undefined;
//   					console.log(order_id);
// 						 if((from !='') && (to2 != '') ){
								
// 									refresh2(order_id,from,to2);
// 								// $.post("<?php //echo base_url(); ?>order/ajaxOrder",{from:from2,order_id:order_id},	
								
// 							}
// 					}, 10000);
// 		}
		function refresh2(order_id,from,to2)
		{

			var from = $("#from").val();
		
			 if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
		
					var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}
			$.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to2,order_id:order_id},
								 	function(data){

								 	// console.log(data);
								 	
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);
								 	var bell = $("#bellSetting").val();
					var newOrder = $("#newOrder").val();
					var oldOrder = $("#oldOrder").val();
					
					if (newOrder > oldOrder) 
					{
						var count = -1;
						var diff = eval(newOrder-oldOrder);
						$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();
						
						if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");}
						console.log(eval(newOrder-oldOrder));
						$( "tr" ).each(function( index, element ) {
						    if (count != -1) { $( element ).css( "border", "2px solid blue" );}
						   
						    count++;
						    if (count == diff) {console.log("stopped at"+count);return false;}
						  });
						// console.log("yes new order found click the bell");
						$("#oldOrder").val(newOrder);
					}

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
 									
 									// REMINDER

 									reminderHighlight();

							    });

		}

		function refresh(order_id,from,to2)
		{

			var from = $("#from").val();
			 if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
					var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}
			$.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to2,order_id:order_id},
								 	function(data){

								 	// console.log(data);
								 	
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);
								 	var bell = $("#bellSetting").val();
					var newOrder = $("#newOrder").val();
					var oldOrder = $("#oldOrder").val();
					
					if (newOrder > oldOrder) 
					{
						var count = -1;
						var diff = eval(newOrder-oldOrder);
						$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();
						
						if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");}
						console.log(eval(newOrder-oldOrder));
						$( "tr" ).each(function( index, element ) {
						    if (count != -1) { $( element ).css( "border", "2px solid blue" );}
						   
						    count++;
						    if (count == diff) {console.log("stopped at"+count);return false;}
						  });
						// console.log("yes new order found click the bell");
						$("#oldOrder").val(newOrder);
					}

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
 									
 									// REMINDER

 									reminderHighlight();

							    });

		}

		function reminderHighlight()
		{
			$('#myData tr').each(function () {

				var time = $(this).attr('data-time');

				var currenttime = $(this).attr('data-currenttime');
				
				//alert(currenttime);
				//reminderTime = currenttime - time;
	
				//var rowid = $(this).attr('data-rowid');
				var status = $(this).attr('data-status');

				// var diff = time - currenttime;

				if(time == currenttime || time < currenttime && time != '' && status == 'On Hold')
				{
					$(this).addClass('reminder')
				}	
			})
		}	

		// $(document).ready(function(){

		// 	 $('#loadingmessage').show();
		// 	var date = '<?php echo $date; ?>';


		// 	$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date},
		// 	 	function(data){

			 	

		// 	 	$("#myData").html(data);
		//   		$('#loadingmessage').hide();  	

		//     });

		// });
	
		function orderStatus(order_id)
		{

			 var status = $("#status"+order_id).val();
			 $.post("<?php echo base_url(); ?>order/ajaxStatusUpdate",{order_id:order_id,status:status}, 
			 	function(data){


			 		$("#allocateColor"+order_id).css("background-color", data);
		  		  

		    });
	

		}


		function purchaseValue(order_id)
		{	
			$(".modal-title").html('Edit Purchase Value #'+order_id);
			 $("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var purchase_value = $("#purchase_value"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getPurchaseValue",{order_id:order_id,purchase_value:purchase_value}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	

        function delivery_charge(order_id)
		{
			$(".modal-title").html('Edit Delivery Charge');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var delivery_charge = $("#delivery_charge"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryCharge",{order_id:order_id,delivery_charge:delivery_charge}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });
		}
		
		function total_w_tax(order_id)
		{
			$(".modal-title").html('Edit Delivery Value');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var total_w_tax = $("#total_w_tax"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getTotalWTax",{order_id:order_id,total_w_tax:total_w_tax}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

	

		}		  

	

		function deliveryTime2(order_id)
		{

			 $(".modal-title").html('Edit Delivery Time');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var delivery_time2 = $("#delivery_time2"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryTime",{order_id:order_id,delivery_time2:delivery_time2}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });


		}	

		function assignWork(order_id,worker_mark)
		{
			console.log(order_id);
			console.log(worker_mark);
					
			
			
			 $.post("<?php echo base_url(); ?>order/updateWorker",{order_id:order_id,worker_mark:worker_mark}, 
			 function(data)
			 {	$("#workerType"+order_id).html("");
			 	var from = $("#from").val();var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}

					var order_id = undefined;
					refresh(order_id,from,to2);
   					console.log(data);
   				
   					
		   	 });
		}
		function assignWorker(worker_mark,agentID)
		{

			console.log(agentID);
			console.log(worker_mark);
			 $.post("<?php echo base_url(); ?>order/updateWorkerAgent",{agentID:agentID,worker_mark:worker_mark}, 
			 function(data)
			 {
   					console.log(data);
   					$("#span"+agentID).html("done");
   					var from = $("#from").val();
   					var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}

   					
					var order_id = undefined;
					refresh(order_id,from,to2);
		   	 });
		}
		function comment(order_id)
		{

			$(".modal-title").html('Edit Comment');
			$("#myModal").modal();

			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var comment = $("#comment"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getComment",{order_id:order_id,comment:comment}, 
			 	function(data){
   		
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}

			function reminder(order_id)
		{


			 $(".modal-title").html('Edit Reminder Time');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var reminder = $("#reminder"+order_id).text();

			 $.post("<?php echo base_url(); ?>order/getReminder",{order_id:order_id,reminder:reminder}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

			
			
			
			
		}	


		 function changeText(param)
          { 
           
               if ($.trim($(param).text()) === 'more') {
                    $(param).text('less');
                } else {
                    $(param).text('more');        
                }
          }

        function convert(str) {
		    var date = new Date(str),
		        mnth = ("0" + (date.getMonth()+1)).slice(-2),
		        day  = ("0" + date.getDate()).slice(-2);
		    return [ date.getFullYear(), mnth, day ].join("-");
		}


	



  //         $(function(){
		//    $('#from').datepick({
		//    	dateFormat: 'yyyy-mm-dd',
		//       onSelect: function (dateText) {

		//       	  var date = convert(dateText);
		//           $.post("<?php echo base_url(); ?>order/ajaxOrder",{date:date}, 
		// 	 	  function(data){
	  	
		// 	 	  	$("#myData").html(data);
		//     	});
		//       }
		//    });
		// });

      	function exportCsv()
      	{
      		var from = $('#from').val();
      		var to = $('#to').val();

      		var today = '<?php echo $date; ?>';
      		
      		var cdate = "";

      		if(from ==='')
      		{
      			from = today
      		}
      		
      		cdate = from+"|"+to;
      		window.location.href = "<?php echo base_url(); ?>order/export/"+cdate;
      	}
		 

			 // CHANGES BY FARHAN 6 MAY 2018
	      	 var role = '<?php echo $role; ?>';

	      	 if(role == 'Manager')
	      	 {

				 $('#from').datepick({
				 	dateFormat: 'yyyy-mm-dd',
				 	minDate: -3

				 });
				 $('#to').datepick({
				 	dateFormat: 'yyyy-mm-dd',
				 	minDate: -3
				 });
	      	 	
	      	 }else{


	      	 	 $('#from').datepick({
				 	dateFormat: 'yyyy-mm-dd'
				 });

				 $('#to').datepick({
				 	dateFormat: 'yyyy-mm-dd'
				 });

	      	 }
			// END BY FARHAN
		function searchOrder()
		{
			var from = $("#from").val();
		    if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
			var to = $("#to").val();
		    $('#loadingmessage').show();
		    var order_id = $("table").find('tbody td:eq(1)').html();

			if(to ===''){

		          $.post("<?php echo base_url(); ?>order/ajaxOrder",{from:from,order_id:order_id}, 
			 	  function(data){
	  				filters(from,to);  //ADDED BY FARHAN
			 	  	$("#myData").html(data);
			 	  	$('#loadingmessage').hide();
		    });}else{

		          $.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to,order_id:order_id}, 
			 	  function(data){
	  				filters(from,to); // ADDED BY FARHAN
			 	  	$("#myData").html(data);
			 	  	$('#loadingmessage').hide();
		    });

		          }


		}

	 $("#showSearch").click(function () {

	         $("#header").toggle();
	    });


	function editStatus(order_id,status)
	{
			
			$(".modal-title").html('Edit Status');

            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            
           // var status = $("td #status"+order_id).val();
            //alert(status);
		  $.ajax({
              type: "POST",
              data: {order_id:order_id,status:status},
              url: "<?php echo base_url(); ?>order/getStatus",
              success:function(response){

              
            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);
              

            }
            });
	}


// 	function viewOrderAddress(order_id)
// 	{
	    
		 
// 			$(".modal-title").html('Order Details');
			
//             $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

// 		  	$.ajax({
//               type: "POST",
//               data: {order_id:order_id},
//               url: "<?php echo base_url(); ?>order/getOrderById",
//               success:function(response){

              
//             // $('.modal-body').css('background-color','white');
//               $(".modal-body").html(response);

//             }
//             });
// 	}


	function viewOrderAddress(order_id,contact_phone)
	{

			
            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

		  	$.ajax({
              type: "POST",
              data: {order_id:order_id,contact_phone:contact_phone},
              url: "<?php echo base_url(); ?>order/getOrderById",
              success:function(response){

            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);


                var noOfOrders = $("#noOfOrders").val();
				$(".modal-title").html('<h4 style="display:inline;">Order Details #'+order_id+'</h4><button style="float: right;margin-right:16px;margin-top: -4px;font-size: 16px;" type="button" class="btn btn-primary">Orders <span class="badge">'+noOfOrders+'</span></button>');

            }
            });
	}
	function locality(order_id)
		{	
			
			$(".modal-title").html('Edit Locality');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			var locality = $("#locality"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getLocality",{order_id:order_id,locality:locality}, 
			 	function(data){
   	
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	 

		function available_agents(id)
		{	
			$(".modal-title").html('Add Agent');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var agentName = $("#name"+id).text();
			 $.post("<?php echo base_url(); ?>order/availableAgents",{id:id,agentName:agentName}, 
			 	function(data){
   			

			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}

		// CHANGE COLOR OF DELIVERY BOYS AND CHANGE STATUS OF DELIVERY BOYS
		
		function changeColor(id)
		{	

			var color = $('#name'+id).css("background-color");
			if(color == 'rgb(0, 255, 0)')
			{
				$("#name"+id).css("background-color", 'rgb(255,0,0)');
			}else{

				$("#name"+id).css("background-color", 'rgb(0,255,0)');
			}

				var status = $("#status"+id).val().trim();

			
			
			$.post("<?php echo base_url(); ?>order/update_available_agents_status",{id:id,status:status}, 
			 function(data){
   					
   					//alert(data);

			 		

		    });
		}

		function sendSms(order_id)
		{

			$(".modal-title").html('SMS');
			$("#myModal").modal();
			$(".modal-body").html('<center><h4>Loading...</h4></center>');
							
			$.post("<?php echo base_url(); ?>order/getOrderByIdInSms",{order_id:order_id}, 
				function(data){

					$(".modal-body").html(data);
								 	
					//$('#myModal').modal('hide');

			});
		}

		// CODE ADDED BY FARHAN

		//FOR PASSWORD PROTECTION
		function commonBox(param)
		{
			$("#checkType").val(param);
			$(".modal-title").html('Password');
			$("#passwordModal").modal();
		}	

		//FOR PASSWORD PROTECTION
		$("#passwordForm").submit(function(e){
			e.preventDefault();
			var password = $("#password").val();
			var checkType = $("#checkType").val();

			$.post("<?php echo base_url(); ?>order/checkPassword",{password:password},function(data){

				if(data == true)
				{
					$('#passwordModal').modal('hide');
					$("#password").val("");
					
					$("#error").hide();

					if(checkType == 'search')
					{
						searchOrder();

					}else if(checkType == 'export')
					{
						exportCsv();
					}
					
				}else{

					$("#error").show();
					$("#error").html('<div class="alert alert-danger">Invalid Password</div>');

				}
				
			});

			
		});



		function filters(from,to)
		{	

			$.post("<?php echo base_url(); ?>order/allFilters",{from:from,to:to},function(data){

				$("#allFilters").html(data);

			});
		}


		// SELECT ALL CHECKBOX
		function selectAll(param) {

			$(".checkBox-"+param).prop('checked', true);
		}

		// CLEAR ALL CHECK BOX	
		function clearCheckBox(param)
		{
			$(".checkBox-"+param).prop('checked', false);
		}

		// SEARCH AGENTS USING INPUT

		function searchFilter(param)
		{

			var input, filter, ul, li,i;

			input = document.getElementById('filter_search_'+param);
			filter = input.value.toUpperCase();
			ul = document.getElementById("filter_options_"+param);
			li = ul.getElementsByTagName('li');

			for (i = 0; i < li.length; i++) {
				a = li[i].getElementsByTagName("a")[0];
				if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
					li[i].style.display = "";
				} else {
					li[i].style.display = "none";
				}
			}
		}

		// SHOW AGENT FILTER

		function display(param)
		{
			

			var check = $(".filter-block-"+param).css('display');


			if(check == 'none')
			{
				$(".filter-block-"+param).css('display','block');
			}else{

				$(".filter-block-"+param).css('display','none');
			}

			

		}

		// CANCEL BUTTON OF FILTER

		function cancelBlock(param)
		{

			$(".filter-block-"+param).hide();

		}

		// CLICK ON SEARCH ICON AND FOCUS ON INPUT

		function searchFocus(param)
		{
			$("#filter_search_"+param).focus();
		}


		// 

		function getSearchResult(param)
		{	
			var from = $("#from").val();
			if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
			var to = $("#to").val();
			if(to == '')
			{
				to = '';
			}
			
			var values = new Array();
			$.each($("input[name='"+param+"[]']:checked"), function() {
				values.push({value:$(this).val()});

			});

				

		  var unchecked = $("input[name='merchant[]']:not(:checked),input[name='agent[]']:not(:checked),input[name='locality[]']:not(:checked),input[name='payment_type[]']:not(:checked),input[name='status[]']:not(:checked)");

		
			if(unchecked.length != 0)
			{
				values = [];
				$.each($("input[name='merchant[]']:checked"), function() {
					values.push({merchant:$(this).val()});

				});

				$.each($("input[name='agent[]']:checked"), function() {
					values.push({agent:$(this).val()});

				});
				$.each($("input[name='status[]']:checked"), function() {
					values.push({status:$(this).val()});

				});
				$.each($("input[name='locality[]']:checked"), function() {
					values.push({locality:$(this).val()});

				});
				$.each($("input[name='payment_type[]']:checked"), function() {
					values.push({payment_type:$(this).val()});

				});

				$.post("<?php echo base_url(); ?>order/getSearhedData2",{"values":values,"type":param,"from":from,"to":to}, 
							function(data){
								
								var uncheckedColor = $("input[name='"+param+"[]']:not(:checked)");
								if(uncheckedColor.length != 0)
								{	
									
									$("#mainTable th.th-"+param).css({"background-color":'#d63031'});
								}else{

									$("#mainTable th.th-"+param).css("background-color",'#000000');
								}

								$(".filter-block-"+param).css('display','none');
								$("#myData").html(data);
							});		



			}else{



					if(values.length > 0)
					{

						$.post("<?php echo base_url(); ?>order/getSearhedData",{"values":values,"type":param,"from":from,"to":to}, 
							function(data){
								console.log(data);
								var uncheckedColor = $("input[name='"+param+"[]']:not(:checked)");
								if(uncheckedColor.length != 0)
								{	
									$("th.th-"+param).css("background",'#d63031');
								}else{

									$("th.th-"+param).css("background",'#000000');
								}
								$(".filter-block-"+param).css('display','none');
								$("#myData").html(data);
							});				
					}else{

						alert("Select atleast one.")
					}
				

				
			}



		}


		function pause(){

			$.post("<?php echo base_url(); ?>order/setSession",function(data){

						alert("Updated");

						location.reload();
					});		

		}

		// END BY FARHAN



// ADDED BY FARHAN 29/04/2018
	function guestEmail(order_id)
	{
		
			$(".modal-title").html('Guest Email');
			
            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

		  	$.ajax({
              type: "POST",
              data: {order_id:order_id},
              url: "<?php echo base_url(); ?>order/guestEmail",
              success:function(response){

              
            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);

            }
            });
	}
	// ADDED BY FARHN 13 MAY 2018
	function getOrderByNumber(mobile,client_id)
	{	

			$("#myModal").modal();
			$(".modal-title").html('No of Orders by Mobile Number #' +mobile);
			
            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

		  	$.ajax({
              type: "POST",
              data: {client_id:client_id},
              url: "<?php echo base_url(); ?>order/getOrderByNumber",
              success:function(response){


            // $('.modal-body').css('background-color','white');
             $(".modal-body").html(response);

            }
            });
	}

	// END BY FARHAN 13 MAY 2018


	// END BY FARHAN 29/04/2018

</script>
</body>
</html>