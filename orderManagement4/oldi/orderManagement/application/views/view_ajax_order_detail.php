	<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');



?>
							<div id="success"></div>



							<?php if($type == 'sms') { ?>
							<div class="row">
								<div class="col-md-8">
						         	 <div class="form-group text-center">
						         	<textarea rows="15" cols="15" class="form-control" id="smsText"><?php 
										$merchant_name = $orderById->restaurant_name;
										echo trim($merchant_name)."\r\n"; 

						         		$totalOrders = explode(",",$orderById->decode_item);
						         		foreach($totalOrders as $totalOrder)
						         		{
						         			echo trim($totalOrder).","."\r\n";
						         		}

						         		echo "\r\n"."Order Id - $order_id"."\r\n".$orderById->first_name." ".$orderById->last_name."\r\n";

						         		echo trim($CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->street." ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->location_name)."\r\n"."\n";

						         		$contact = $CI->ModelOrder->getClientById($orderById->client_id)[0]['contact_phone']."\r\n";

						         		echo str_replace("+91","",$contact);

						         		echo "Total - Rs. ".round($orderById->total_w_tax);

						         	?></textarea>	
						         	
						         	</div>
						         	</div>
						         	<div class="col-md-4">
							         	<div class="form-group">
											<select id="mobile" class="form-control" name="mobile">
											<?php $getDeliveryBoys = $CI->ModelOrder->getDeliveryBoys(); ?>

												<?php foreach($getDeliveryBoys as $getDeliveryBoy) { ?>
												<option value="<?php echo $getDeliveryBoy['mobile']; ?>"><?php echo $getDeliveryBoy['name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group text-center">
											<button type="button" class="btn btn-primary" onclick="sendSmsApi(<?php echo $order_id; ?>)" id="sendButton">Send</button>
										</div>
									</div>
								
						
							</div>
							<?php } ?>



							<?php if($type == 'comment') { ?>
								
							  	<div class="row">
									<div class="col-md-6 col-md-offset-3">	
										<div class="form-group">
										<textarea id="comment" class="form-control" ><?php echo trim($comment); ?></textarea>
										<!-- <input type="text" id="comment" class="form-control" value="<?php echo $comment; ?>"> -->
									</div>
									</div>
								
								
									<div class="col-md-6 col-md-offset-3">	
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="commentUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
								</div>
							
							<?php } ?>

							<?php if($type == 'delivery_time2') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="delivery_time2" class="form-control" value="<?php echo trim($delivery_time2); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryTimeUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'reminder') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<input type="text" id="reminder" class="form-control" value="<?php echo trim($reminder); ?>">
									</div>
									<div class="form-group text-center">
										<button type="button" class="btn btn-success myBtn" onclick="reminderUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'total_w_tax') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="total_w_tax" class="form-control" value="<?php echo trim($total_w_tax); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="totalWTaxUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>
							
							<?php if($type == 'delivery_charge') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="delivery_charge" class="form-control" value="<?php echo trim($delivery_charge); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryChargeUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'delivery_agent') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<select id="delivery_agent" class="form-control" name="delivery_agent">
									<?php $agents = $CI->ModelOrder->getAvailableAgents(); ?>	


									<?php foreach($agents as $agent) { ?>
												<option value="<?php echo $agent['name']; ?>" <?php if($agent['name'] == $delivery_agent){ echo 'selected';} ?>><?php echo $agent['name']; ?></option>
									<?php } ?>
									</select>
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryAgentUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'locality') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<select id="locality" class="form-control" name="locality">
									<?php $localities = $CI->ModelOrder->getLocality(); ?>	

									<option></option>
									<?php foreach($localities as $locality) { ?>
												<option value="<?php echo $locality['locality_name']; ?>" <?php if($locality['locality_name'] == $selected_locality){ echo 'selected';} ?>><?php echo $locality['locality_name']; ?></option>
									<?php } ?>
									</select>
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="localityUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'purchase_value') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="purchase_value" class="form-control" value="<?php echo trim($purchase_value); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="purchaseValueUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>



							<?php if($type == 'item') { ?>
							<div class="row">
								<div class="col-md-8">
						         	 <div class="form-group text-center">
							         	<textarea rows="15" class="form-control" id="decode_item"><?php 
							         		$totalOrders = explode(",",$orderById->decode_item);
							         		foreach($totalOrders as $totalOrder)
							         		{
							         			echo trim($totalOrder).","."\r\n";
							         		}
							         	?></textarea>
						         	</div>
						         	<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="editOrderItem(<?php echo $order_id; ?>)">Submit</button>
									</div>
								</div>
								<div class="col-md-4">
									<?php 
											
											echo "<p><b>Address:</b></p> ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->street."</br></br><b> Landmark: </b> ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->location_name."</br></br> ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->city." ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->state."</p><b>Delivery Instruction:</b><p>".$orderById->delivery_instruction."</p>";


									?>
								</div>
							</div>
							<?php } ?>

							<?php if($type == 'status') { ?>
							<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<input type="hidden" id="delivery_agent_color" value="<?php echo trim($orderById->delivery_agent) ?>">
										<select id="status" class="form-control" name="status">
											<option value="Pending" <?php if($status == 'Pending'){ echo 'selected';} ?>>Pending</option>
											<option value="Confirmed" <?php if($status == 'Confirmed'){ echo 'selected';} ?>>Confirmed</option>
											<option value="Processing" <?php if($status == 'Processing'){ echo 'selected';} ?>>Processing</option>
											<option value="Delivered" <?php if($status == 'Delivered'){ echo 'selected';} ?>>Delivered</option>
											<option value="Cancelled" <?php if($status == 'Cancelled'){ echo 'selected';} ?>>Cancelled</option>
											<option value="On Hold" <?php if($status == 'On Hold'){ echo 'selected';} ?>>On Hold</option>
										</select>
									</div>
									</div>
									
									
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<button type="button" class="btn btn-success" onclick="orderStatus(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
							</div>
							<?php } ?>



							<script type="text/javascript">
						$(".modal input, .modal textarea").focus();

							function orderStatus(order_id)
							{
								 var status = $("#status").val();

								 var agentName = $("#delivery_agent_color").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxStatusUpdate",{order_id:order_id,status:status,agentName:agentName}, 
								 	function(data){

								 		
								 		//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		  	$('#myModal').modal('hide');

							    });
							}

							function purchaseValueUpdate(order_id)
							{
								 var purchase_value = $("#purchase_value").val();

								// alert(purchase_value);

							
								 $.post("<?php echo base_url(); ?>order/ajaxPurchaseValueUpdate",{order_id:order_id,purchase_value:purchase_value}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');  
 
							    });
							}

							function deliveryAgentUpdate(order_id)
							{
								 var delivery_agent = $("#delivery_agent").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryAgentUpdate",{order_id:order_id,delivery_agent:delivery_agent}, 
								 	function(data){

								 	if(data == 'false')
								 	{
								 		alert('Agent Name is not Available in the list');
								 	}
								 	else
								 	{

								 		$('#myModal').modal('hide');  
								 	}

								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		

							    });
							}

							function localityUpdate(order_id)
							{
								 var locality = $("#locality").val();

								//alert(delivery_agent);
								 $.post("<?php echo base_url(); ?>order/ajaxLocalityUpdate",{order_id:order_id,locality:locality}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');  

							    });
							}

							function totalWTaxUpdate(order_id)
							{
								 var total_w_tax = $("#total_w_tax").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxTotalWTaxValueUpdate",{order_id:order_id,total_w_tax:total_w_tax}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 

							    });
							}
							
								function deliveryChargeUpdate(order_id)
							{
								 var delivery_charge = $("#delivery_charge").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryChargeUpdate",{order_id:order_id,delivery_charge:delivery_charge}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 

							    });
							}

							function deliveryTimeUpdate(order_id)
							{
								 var delivery_time2 = $("#delivery_time2").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryTime2Update",{order_id:order_id,delivery_time2:delivery_time2}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		 $('#myModal').modal('hide');

							    });
							}

							function reminderUpdate(order_id)
							{
								 var reminder = $("#reminder").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxReminderUpdate",{order_id:order_id,reminder:reminder}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		 $('#myModal').modal('hide');

							    });
							}

							function commentUpdate(order_id)
							{
								 var comment = $("#comment").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxCommentUpdate",{order_id:order_id,comment:comment}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');

							    });
							}

							function editOrderItem(order_id)
							{
								 var decode_item = $("#decode_item").val().trim();
							
								 $.post("<?php echo base_url(); ?>order/ajaxDecodeItemUpdate",{order_id:order_id,decode_item:decode_item}, 
								 	function(data){

								 	
								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');

							    });
								
							} 

							function sendSmsApi(order_id)
							{
								var smsText = $("#smsText").val().trim();
								var mobile = $("#mobile").val().trim();

								var agentName = $("#mobile option:selected").text();
								
								 $.post("<?php echo base_url(); ?>order/smsApi",{order_id:order_id,smsText:smsText,mobile:mobile,agentName:agentName}, 
								 	function(data){

								 	
								 	if(data == 'false')
								 	{
								 		alert('Agent Name is not Available in the list');
								 	}
								 	else
								 	{

								 		$('#myModal').modal('hide');  
								 	}
								 
							  		
 
							    });
							}

							$("#comment,#delivery_time2,#delivery_charge,#total_w_tax,#locality,#delivery_agent,#purchase_value").keyup(function(event){
							    if(event.keyCode == 13){
							        $(".myBtn").click();
							    }
							});

							var selectized = $('#locality,#delivery_agent').selectize({

								create: true,
								sortField: 'text'

							});
							selectized[0].selectize.focus();

							
							 $('#reminder').timepicker({ 'timeFormat': 'g.i' });
							</script>