<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

$date = date('Y-m-d');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Order Management System</title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/admin.ico">

	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datepick.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/selectize.bootstrap3.css">
<style type="text/css">
	.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border: 1px solid #2d2424;}

	tr.reminder { 
		animation: blinker 1s linear infinite;
	}

	@keyframes blinker {  
	  50% { opacity: 0.6; }
	}
</style>
</head>
<body>

	<div class="row">
		<div class="col-md-11">
			<div class="panel panel-default">
  				<div class="panel-heading" style="   position: fixed;
    height: 60px;
    top: 0;
    width: 100%;
    z-index: 100;">
  					<div class="row">
  						<div class="col-md-1">
  						<img src="https://www.onlinekaka.com/upload/1476254450-1475737169-kaka-logo-(1).png" width="84">
  						</div>
  						<div id="header" style="display: none;">
					  	<div class="col-md-2">
							<div class="form-group">
							
									<input type="text" name="from" id="from" class="form-control" placeholder="From">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
							
									<input type="text" name="to" id="to" class="form-control" placeholder="To">
							</div>
						</div>
						<div class="col-md-1">
						<a href="javascript:void(0)" class="btn btn-default" onclick="searchOrder()">Search</a>
						</div>

						<div class="col-md-2">
							<a class="btn btn-default" target="_blank" onclick="exportCsv()" href="javascript:void(0)">Export into CSV</a>
					    </div>
					    
					 </div>
					 <div class="col-md-2">
					 	<div id='loadingmessage' style='display:none;'>
						  Please Wait...
						</div>
					</div>
					<div class="col-md-1 pull-right">
							<a class="btn btn-default text-left"  href="<?php echo base_url().'login/logout' ?>">LOGOUT</a>
					</div>
					<div class="col-md-1 pull-right">
							<a class="btn btn-default text-left" id="showSearch" href="javascript:void(0)">SHOW</a>
					</div>
					<!--     <div class="col-md-1 pull-right">
							<a class="btn btn-default text-left"  id="refresh" href="javascript:void(0)">REFRESH</a>
					    </div> -->
					</div>
				</div>
			</div>

 			<div class="panel-body" style="padding: 0px;">
			<table class="table table-bordered" id="mainTable" style="font-weight: bold; margin-top: 37px;
">
				<thead>
					<tr style="background-color: black; color: #fff;">
						<th width="1%">ID</th>
						<th width="1%">REF#</th>
			            <th width="1%">MERCHANT NAME</th>           
			            <th width="1%">NAME</th>
			            <th width="1%">MOBILE</th>
			            <th width="1%">ITEM</th>            
			            
			            <th width="1%">PT</th>
			           <!--  <th width="1%">Total</th>
			            <th width="1%">Tax</th> -->
			            <th width="1%">R</th>
			            <th width="1%">PV</th>  
			            <th width="1%">DV</th>
			            <th width="1%">DF</th>
			            <th width="1%">STATUS</th>
			            <th width="1%">AGENT</th>
			            <th width="1%">Locality</th>
			            <th width="1%">CON</th>
			            <th width="1%">DEL</th>
			            <th width="1%">COMMENT</th>
			            <th width="1%">SMS</th>  
			         
					</tr>
				</thead>
				<tbody id="myData" style="color:#000000;">
						
				</tbody>
			</table>
		<div id="loader">Please Wait...</div>
		</div>
	</div>
	<div class="col-md-1" id="agentsData">

	<table class="table table-bordered"  style="font-weight: bold;;margin-top: 58px; margin-left: -40px;">
		<thead>
		<tr style="background-color: black; color: #fff;">
			<th>
				Delivery Boys	
			</th>
			
		<tr>
		</thead>
		<tbody>
			<?php foreach($get_available_agents as $get_available_agent) { ?>
				<?php $no_of_orders = $CI->ModelOrder->getOrdersOfDeliveryBoys($get_available_agent['name']); ?>
				<tr>
					<td style="color:black; background-color: <?php echo deliveryBoysColor($no_of_orders); ?>" id="name<?php echo $get_available_agent['id']; ?>" onclick="available_agents(<?php echo $get_available_agent['id']; ?>)" contenteditable="true">
						
						<?php echo $get_available_agent['name']; ?>

					</td>
			   
			
			     </tr>
			<?php } ?>
		</tbody>
	</table>

	</div>
</div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        
        </div>
      <!--   <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
<script src="<?php echo base_url();  ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url();  ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/selectize.min.js"></script>
<script type="text/javascript">
	var myInterval;
	
	$(document).ready(function () {
		ajaxRefresh();
	})



	function deliveryAgent(order_id)
		{	
			
			$(".modal-title").html('Edit Delivery Agent');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			var delivery_agent = $("#delivery_agent"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryAgent",{order_id:order_id,delivery_agent:delivery_agent}, 
			 	function(data){
   	
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	

		function ajaxRefresh()
		{

				 	myInterval = setInterval(function(){	

				 	$('#loadingmessage').show();
							 	
					var from2 = $("#from").val();
					var to2 = $("#to").val();
					var date = '<?php echo $date; ?>';
					
					var order_id = $("#mainTable").find('tbody td:eq(1)').html();

  
						 if((from2 ==='') || (from2 === date)){
								
									
								$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date,order_id:order_id},
								 	function(data){

								 
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
 									
 									// REMINDER

 									reminderHighlight();

							    });

							}
					}, 10000);
		}
		

		function reminderHighlight()
		{
			$('#myData tr').each(function () {

				var time = $(this).attr('data-time');

				var currenttime = $(this).attr('data-currenttime');
				
				//alert(currenttime);
				//reminderTime = currenttime - time;
	
				//var rowid = $(this).attr('data-rowid');
				var status = $(this).attr('data-status');

				// var diff = time - currenttime;

				if(time == currenttime || time < currenttime && time != '' && status == 'On Hold')
				{
					$(this).addClass('reminder')
				}	
			})
		}	

		// $(document).ready(function(){

		// 	 $('#loadingmessage').show();
		// 	var date = '<?php echo $date; ?>';


		// 	$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date},
		// 	 	function(data){

			 	

		// 	 	$("#myData").html(data);
		//   		$('#loadingmessage').hide();  	

		//     });

		// });
	
		function orderStatus(order_id)
		{

			 var status = $("#status"+order_id).val();
			 $.post("<?php echo base_url(); ?>order/ajaxStatusUpdate",{order_id:order_id,status:status}, 
			 	function(data){


			 		$("#allocateColor"+order_id).css("background-color", data);
		  		  

		    });
	

		}


		function purchaseValue(order_id)
		{	
			$(".modal-title").html('Edit Purchase Value');
			 $("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var purchase_value = $("#purchase_value"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getPurchaseValue",{order_id:order_id,purchase_value:purchase_value}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	

        function delivery_charge(order_id)
		{
			$(".modal-title").html('Edit Delivery Charge');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var delivery_charge = $("#delivery_charge"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryCharge",{order_id:order_id,delivery_charge:delivery_charge}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });
		}
		
		function total_w_tax(order_id)
		{
			$(".modal-title").html('Edit Delivery Value');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var total_w_tax = $("#total_w_tax"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getTotalWTax",{order_id:order_id,total_w_tax:total_w_tax}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

	

		}		  

	

		function deliveryTime2(order_id)
		{

			 $(".modal-title").html('Edit Delivery Time');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var delivery_time2 = $("#delivery_time2"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryTime",{order_id:order_id,delivery_time2:delivery_time2}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });


		}	

		function comment(order_id)
		{

			$(".modal-title").html('Edit Comment');
			$("#myModal").modal();

			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var comment = $("#comment"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getComment",{order_id:order_id,comment:comment}, 
			 	function(data){
   		
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}

			function reminder(order_id)
		{


			 $(".modal-title").html('Edit Reminder Time');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var reminder = $("#reminder"+order_id).text();

			 $.post("<?php echo base_url(); ?>order/getReminder",{order_id:order_id,reminder:reminder}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

			
			
			
			
		}	


		 function changeText(param)
          { 
           
               if ($.trim($(param).text()) === 'more') {
                    $(param).text('less');
                } else {
                    $(param).text('more');        
                }
          }

        function convert(str) {
		    var date = new Date(str),
		        mnth = ("0" + (date.getMonth()+1)).slice(-2),
		        day  = ("0" + date.getDate()).slice(-2);
		    return [ date.getFullYear(), mnth, day ].join("-");
		}


	



  //         $(function(){
		//    $('#from').datepick({
		//    	dateFormat: 'yyyy-mm-dd',
		//       onSelect: function (dateText) {

		//       	  var date = convert(dateText);
		//           $.post("<?php echo base_url(); ?>order/ajaxOrder",{date:date}, 
		// 	 	  function(data){
	  	
		// 	 	  	$("#myData").html(data);
		//     	});
		//       }
		//    });
		// });

      	function exportCsv()
      	{
      		var from = $('#from').val();
      		var to = $('#to').val();

      		var today = '<?php echo $date; ?>';
      		
      		var cdate = "";

      		if(from ==='')
      		{
      			from = today
      		}
      		
      		cdate = from+"|"+to;
      		window.location.href = "<?php echo base_url(); ?>order/export/"+cdate;
      	}
		 

		 $('#from').datepick({dateFormat: 'yyyy-mm-dd'});
		 $('#to').datepick({dateFormat: 'yyyy-mm-dd'});
		
		function searchOrder()
		{
			var from = $("#from").val();
			var to = $("#to").val();
		    $('#loadingmessage').show();
		    var order_id = $("table").find('tbody td:eq(1)').html();

			if(to ===''){

		          $.post("<?php echo base_url(); ?>order/ajaxOrder",{from:from,order_id:order_id}, 
			 	  function(data){
	  	
			 	  	$("#myData").html(data);
			 	  	$('#loadingmessage').hide();
		    });}else{

		          $.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to,order_id:order_id}, 
			 	  function(data){
	  	
			 	  	$("#myData").html(data);
			 	  	$('#loadingmessage').hide();
		    });

		          }


		}

	 $("#showSearch").click(function () {

	         $("#header").toggle();
	    });


	function editStatus(order_id,status)
	{
			
			$(".modal-title").html('Edit Status');

            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            
           // var status = $("td #status"+order_id).val();
            //alert(status);
		  $.ajax({
              type: "POST",
              data: {order_id:order_id,status:status},
              url: "<?php echo base_url(); ?>order/getStatus",
              success:function(response){

              
            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);

            }
            });
	}


	function viewOrderAddress(order_id)
	{
		 
			$(".modal-title").html('Order Details');
			
            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

		  	$.ajax({
              type: "POST",
              data: {order_id:order_id},
              url: "<?php echo base_url(); ?>order/getOrderById",
              success:function(response){

              
            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);

            }
            });
	}

	function locality(order_id)
		{	
			
			$(".modal-title").html('Edit Locality');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			var locality = $("#locality"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getLocality",{order_id:order_id,locality:locality}, 
			 	function(data){
   	
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	 

		function available_agents(id)
		{	
			$(".modal-title").html('Add Agent');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var agentName = $("#name"+id).text();
			 $.post("<?php echo base_url(); ?>order/availableAgents",{id:id,agentName:agentName}, 
			 	function(data){
   			

			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}

		// CHANGE COLOR OF DELIVERY BOYS AND CHANGE STATUS OF DELIVERY BOYS
		
		function changeColor(id)
		{	

			var color = $('#name'+id).css("background-color");
			if(color == 'rgb(0, 255, 0)')
			{
				$("#name"+id).css("background-color", 'rgb(255,0,0)');
			}else{

				$("#name"+id).css("background-color", 'rgb(0,255,0)');
			}

				var status = $("#status"+id).val().trim();

			
			
			$.post("<?php echo base_url(); ?>order/update_available_agents_status",{id:id,status:status}, 
			 function(data){
   					
   					//alert(data);

			 		

		    });
		}

		function sendSms(order_id)
		{

			$(".modal-title").html('SMS');
			$("#myModal").modal();
			$(".modal-body").html('<center><h4>Loading...</h4></center>');
							
			$.post("<?php echo base_url(); ?>order/getOrderByIdInSms",{order_id:order_id}, 
				function(data){

					$(".modal-body").html(data);
								 	
					//$('#myModal').modal('hide');

			});
		}

</script>
</body>
</html>