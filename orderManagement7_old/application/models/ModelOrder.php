<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelOrder extends CI_Model {


	public function getOrder($from, $to="")
	{
        if($to != "")
        {
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderA($from, $to="")
	{
        if($to != "")
        {
        	$this->db->where('workerType','A');
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
        	$this->db->where('workerType','A');
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderB($from, $to="")
	{
        if($to != "")
        {
        	$this->db->where('workerType','B');
            $this->db->where('delivery_date >=', $from);
            $this->db->where('delivery_date <=', $to);
        }
        else
        {
        	$this->db->where('workerType','B');
            $this->db->where('delivery_date',$from);
        }
		
		$this->db->order_by('order_id','desc');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();
	
		
	}
	public function getOrderByRange($from,$to)
	{

		
		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}
	public function getOrderByRangeWorkerA($from,$to)
	{

		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);
		$this->db->where('workerType','A');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}
	public function getOrderByRangeWorkerB($from,$to)
	{

		$this->db->order_by('order_id','desc');
		$this->db->where('delivery_date <=',$to);
		$this->db->where('delivery_date >=',$from);
		$this->db->where('workerType','B');

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');
		
		$this->db->select('mt_order_duplicate_farhan.order_id,
						   mt_merchant.restaurant_name,
						   mt_client.first_name,
						   mt_client.last_name,
						   mt_client.contact_phone,
						   mt_order_duplicate_farhan.payment_type,
						   mt_order_duplicate_farhan.reminder,
						   mt_order_duplicate_farhan.purchase_value,
						   mt_order_duplicate_farhan.total_w_tax,
						   mt_order_duplicate_farhan.delivery_charge,
						   mt_order_duplicate_farhan.status,
						   mt_order_duplicate_farhan.delivery_agent,
						   mt_order_duplicate_farhan.locality,
						   mt_order_duplicate_farhan.date_created,
						   mt_order_duplicate_farhan.delivery_time,
						   mt_order_duplicate_farhan.delivery_time2,
						   mt_order_duplicate_farhan.comment,
						   mt_order_duplicate_farhan.workerType,

						  ');
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}

	public function getMerchantById($merchant_id)
	{
		$this->db->where('merchant_id',$merchant_id);
		$query = $this->db->get('mt_merchant');
		return $query->row('restaurant_name');
	}

	public function getClientById($client_id)
	{	
		$this->db->select('first_name,last_name,contact_phone,street');
		$this->db->where('client_id',$client_id);
		$query = $this->db->get('mt_client');
		return $query->result_array();
	} 

	public function getClientAddressById($order_id,$client_id)
	{	
		$this->db->select('street,city,state,location_name');
		$this->db->where('client_id',$client_id);
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_order_delivery_address');
		return $query->row();
	}

	public function getItemById($item_id)
	{
		$this->db->where('item_id',$item_id);
		$query = $this->db->get('mt_item');
		return $query->row('item_name');
	}

	public function statusUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);

		/* TO UPDATE ORDER STATUS IN mt_order TABLE - BY ALiGNWEBS 23-Mar-2018 4:31 PM */
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order', $data);
		/* END */
	}

	public function purchaseValueUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}

	public function totalWTaxValueUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}
	
		public function deliveryChargeUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
		
	}

	public function deliveryAgentUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function localityUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	} 

	public function deliveryTime2Update($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function commentUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function reminderUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}


	public function getRemindOrder($date)
	{
		$this->db->where('reminder !=', '');
		$this->db->like('date_created', $date);
		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->result_array();
	}

	public function getOrderById($order_id)
	{	
		$this->db->where('order_id',$order_id);

		$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
		$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');




		$query = $this->db->get('mt_order_duplicate_farhan');
		return $query->row();
	}

	public function updateOrderItem($order_id,$data)
	{
		
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan',$data);
	}

	public function decodeItemUpdate($data,$order_id)
	{
		$this->db->where('order_id',$order_id);
		$this->db->update('mt_order_duplicate_farhan', $data);
	}

	public function insertRow($data)
	{
		$this->db->insert_batch('mt_available_agents_farhan',$data);
	}

	public function getAvailableAgents()
	{
		
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsA()
	{
		$this->db->where('wType','a');
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}
	public function getAvailableAgentsB()
	{
		$this->db->where('wType','b');
		$query = $this->db->get('mt_available_agents_farhan');
		return $query->result_array();
	}


	public function agentUpdate($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function agentStatusUpdate($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function getDeliveryBoys()
	{
		$query = $this->db->get('mt_delivery_boys_farhan');
		return $query->result_array();
	}

	public function export($date)
	{
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = $date."- onlinekaka.csv";
        
     

		$this->db->select('*');
	
		$this->db->order_by('REF', 'asc'); 

		$query = $this->db->get('mt_farhan_excel');
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $this->db->truncate('mt_farhan_excel');
        force_download($filename, $data);
	}
	
	public function updateDeliveryAgent($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('mt_available_agents_farhan', $data);
		
	} 


	public function checkPaymentReference($order_id)
	{
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('mt_payment_order');
		return $query->row('payment_reference');
		
		
	}

	// AFTER SEND SMS CHANGE DELIVERY BOYS COLOR TO RED

	public function agentStatusUpdateByName($data,$agentName)
	{
		$this->db->where('name',$agentName);
		$this->db->update('mt_available_agents_farhan', $data);
	}

	public function getOrderForDecodeItem($first_order_id)
	{

		
		$this->db->where('delivery_date',date('Y-m-d'));
		$this->db->where('order_id >',$first_order_id);
		$this->db->order_by('order_id','desc');
		$query = $this->db->get('mt_order_duplicate_farhan');
	    return $query->result_array();

	} 
	public function workerUpdate($order_id,$worker)
	{
		$this->db->set('workerType', $worker);  //Set the column name and which value to set..

		$this->db->where('order_id', $order_id); //set column_name and value in which row need to update

		$this->db->update('mt_order_duplicate_farhan'); //Set your table name
	}
	public function workerUpdateAgent($agentID,$worker)
	{
		$this->db->set('wType', $worker);  //Set the column name and which value to set..

		$this->db->where('id', $agentID); //set column_name and value in which row need to update

		$this->db->update('mt_available_agents_farhan'); //Set your table name
	}

	// AFTER DELIVER THE ORDER DELIVERY BOYS COLOR TO GREEN

	public function agentStatusGreenByName($data,$agentName)
	{	
		$this->db->where('delivery_date',date('Y-m-d'));
		$this->db->where('delivery_agent',$agentName);
		$this->db->where('status','Processing');
		$query = $this->db->get('mt_order_duplicate_farhan');
		$result = $query->row('delivery_agent');

		if(isset($result))
		{
			print_r($data);

		}else{

		$this->db->where('name',$agentName);
		$this->db->update('mt_available_agents_farhan', $data);

		}
 
	}

	public function getOrdersOfDeliveryBoys($agentName)
	{	

		 $this->db->where('delivery_date',date('Y-m-d'));
		 $this->db->where('delivery_agent',$agentName);
		 $this->db->where('status','Processing');
		 $this->db->select('delivery_agent, COUNT(delivery_agent) as no_of_orders');
		 $this->db->group_by('delivery_agent'); 
		 $this->db->order_by('no_of_orders', 'desc'); 
		 $query = $this->db->get('mt_order_duplicate_farhan');
		 return $query->row('no_of_orders');


	}

	public function getAvailableAgentsByName($agentName)
	{
		$this->db->where('name',$agentName);
		$query = $this->db->get('mt_available_agents_farhan');
		$result = $query->result_array();

		if(sizeof($result) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getLocality()
	{
		$query = $this->db->get('mt_farhan_locality');
		return $query->result_array();
	}
}
