<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

$date = date('Y-m-d');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Order Management System</title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/admin.ico">

	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datepick.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.timepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/selectize.bootstrap3.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- ADDED BY ALiGNWEBS - 24-03-2018 9:53 P -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/sumoselect.min.css" />
<!-- DONE -->
<style type="text/css">
	.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border: 1px solid #2d2424;}

	tr.reminder { 
		animation: blinker 1s linear infinite;
	}

	@keyframes blinker {  
	  50% { opacity: 0.6; }
	}
	.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 4px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 5px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
</style>


<!-- BY ALIGNWEBS 27-3-2018 -->
<style>
	.aw-filter-visible { display: table-row; }
.SumoSelect {
    width: 89px;
    zoom: 70%;
}
	.SumoSelect.open>.optWrapper {
	    top: 35px;
	    display: block;
	    width: 300px;
	}
</style>
<!-- END -->
</head>
<body>

	<div class="row">
		<div class="col-md-11">
			<div class="panel panel-default">
  				<div class="panel-heading" style="   position: fixed;
    height: 60px;
    top: 0;
    width: 100%;
    z-index: 100;">
  					<div class="row">
  						<div class="col-md-1">
  						<img src="https://www.onlinekaka.com/upload/1476254450-1475737169-kaka-logo-(1).png" width="84">
  						</div>
  						<div id="header" style="display: none;">
					  	<div class="col-md-2">
							<div class="form-group">
							
									<input type="text" name="from" id="from" class="form-control" placeholder="From">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
							
									<input type="text" name="to" id="to" class="form-control" placeholder="To">
							</div>
						</div>
						<div class="col-md-1">
						<a href="javascript:void(0)" class="btn btn-default" onclick="searchOrder()">Search</a>
						</div>

						<div class="col-md-1">
							<a class="btn btn-default btn-sm" target="_blank" onclick="exportCsv()" href="javascript:void(0)">Export(CSV)</a>
					    </div>
					    
					 </div>
					 <div class="col-md-1">
					 	<button class='btn btn-default btn-sm' id='bellDiv'><i class="fa fa-fw fa-bell"></i></button>
					 	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-gear"></i>
     
    </button>

    <ul class="dropdown-menu">
      <li><a onclick="setBell(1)"><i class="fa fa-bell fa-fw"></i>On</a></li>
      <li><a onclick="setBell(0)"><i class="fa fa-bell-slash fa-fw"></i>Off</a></li>
      <li><a onclick="setBellTime();"><i class="fa fa-bell-slash fa-fw"></i>Temporary Off</a></li>
    </ul>
    <script type="text/javascript">
    	function setBell(flag)
    	{
    		$("#bellSetting").val(flag);
    		if (flag == 0) $("#bellDiv").html("<i class='fa fa-bell-slash'></i>");
    		if (flag == 1) $("#bellDiv").html("<i class='fa fa-bell'></i>");
    	}
    	function setBellTime()
    	{
    		//console.log("bell timer is called");
    		$("#bellSetting").val("0");
    		$("#bellDiv").html("<i class='fa fa-bell-slash'></i>")
    		var sec = 0;
    		var myVar2;
			myVar2 = setInterval(function(){
				sec = sec + 60;
				console.log($("#bellSetting").val());
				if (sec > '300') 
				{
					clearInterval(myVar2);
					$("#bellDiv").html("<i class='fa fa-bell'></i>")
					$("#bellSetting").val("1");
				}	
			}, 60000);
			 //myVar = setTimeout(setBellOn(), 3000);
    	}
    </script>
					 </div>
					 <div class="col-md-1">
					 	<div id='loadingmessage' style='display:none;'>
						  Please Wait
						</div>
					</div>
					 <div class="col-md-1">
					 	<p>Panel<br> <button style="background:yellow;border:1px solid blue">A</button>&nbsp;<button style="background:blue;color:white;border:1px solid black">B</button></p>
					</div>
					<div class="col-md-1 pull-right">
							<a class="btn btn-default text-left"  href="<?php echo base_url().'login/logout' ?>">LOGOUT</a>
					</div>
					<div class="col-md-1 pull-right">
							<a class="btn btn-default text-left" id="showSearch" href="javascript:void(0)">SHOW</a>
					</div>
					<!--     <div class="col-md-1 pull-right">
							<a class="btn btn-default text-left"  id="refresh" href="javascript:void(0)">REFRESH</a>
					    </div> -->
					</div>
				</div>
			</div>

 			<div class="panel-body" style="padding: 0px;">
			<table class="table table-bordered" id="mainTable" style="font-weight: bold; margin-top: 37px;
">
				<thead>
					<tr style="background-color: black; color: #fff;">
						<th width="1%">ID</th>
						<th width="1%">REF#</th>
			            <th width="1%">MERCHANT NAME</th>           
			            <th width="1%">NAME</th>
			            <th width="1%">MOBILE</th>
			            <th width="1%">ITEM</th>            
			            
			            <th width="1%">PT</th>
			           <!--  <th width="1%">Total</th>
			            <th width="1%">Tax</th> -->
			            <th width="1%">R</th>
			            <th width="1%">PV</th>  
			            <th width="1%">DV</th>
			            <th width="1%">DF</th>
			            <th width="1%">STATUS</th>			            
			            <th width="1%">Assign</th>
			            <th width="1%">AGENT</th>
			            <th width="1%">Locality</th>
			            <th width="1%">CON</th>
			            <th width="1%">DEL</th>

			            <th width="1%">COMMENT</th>
			            <th width="1%">SMS</th>  
			         
					</tr>
					<tr>
						<th width="1%"><a href="javascript:void(0)" onclick="selectAllFilters()">select all</a></th>
						<th width="1%"><a href="javascript:void(0)" onclick="clearFilters()">clear</a></th>
			            <th width="1%">
			            	<select name="merchant" class="SumoUnder" tabindex="-1" multiple="multiple"></select>
						</th>           
			            <th width="1%">NAME</th>
			            <th width="1%">MOBILE</th>
			            <th width="1%">ITEM</th>            
			            
			            <th width="1%"><select name="pt" class="SumoUnder" tabindex="-1" multiple="multiple"></select></th>
			           <!--  <th width="1%">Total</th>
			            <th width="1%">Tax</th> -->
			            <th width="1%">R</th>
			            <th width="1%">PV</th>  
			            <th width="1%">DV</th>
			            <th width="1%">DF</th>
			            <th width="1%"><select name="status" class="SumoUnder" tabindex="-1" multiple="multiple"></select></th>			            
			            <th width="1%">Assign</th>
			            <th width="1%"><select name="agent" class="SumoUnder" tabindex="-1" multiple="multiple"></select></th>
			            <th width="1%"><select name="locality" class="SumoUnder" tabindex="-1" multiple="multiple"></select></th>
			            <th width="1%">CON</th>
			            <th width="1%">DEL</th>

			            <th width="1%">COMMENT</th>
			            <th width="1%">SMS</th>  
			         
					</tr>
				</thead>
				<tbody id="myData" style="color:#000000;">
						
				</tbody>
			</table>
		<div id="loader">Please Wait...</div>
		</div>
	</div>
	<div class="col-md-1" id="agentsData">

	<table class="table table-bordered"  style="font-weight: bold;;margin-top: 58px; margin-left: -40px;">
		<thead>
		<tr style="background-color: black; color: #fff;">
			<th></th>
			<th>
				Delivery Boys	
			</th>
			
		<tr>
		</thead>
		<tbody>
			<?php foreach($get_available_agents as $get_available_agent) { ?>
				<?php $no_of_orders = $CI->ModelOrder->getOrdersOfDeliveryBoys($get_available_agent['name']); ?>
				<tr>
					<?php 
							$bgClr = "";
							if (empty($get_available_agent['wType']) && !empty($get_available_agent['name']))
							{
								$bgClr = "";
							}
							elseif ($get_available_agent['wType'] == 'a' && !empty($get_available_agent['name'])) {
								$bgClr = "yellow";

							}elseif (!empty($get_available_agent['name']))
							{
								$bgClr = "blue";
							}
						 ?>
					<td style="background: <?php echo $bgClr ?>">
						 <span class="dropdown">
      						<div class="dropdown">
							  <button class="dropbtn" style="font-size: 6pt"><i class="fa fa-arrow-down"></i></button>
							  <div class="dropdown-content">
							    <?php 
							if (empty($get_available_agent['wType']) && !empty($get_available_agent['name']))
							{
							?>
							
						
								<a  href="#" id="span<?php echo $get_available_agent['id']; ?>" onclick="assignWorker('a','<?php echo $get_available_agent['id']; ?>')"  >A</a>
								<a id="span<?php echo $get_available_agent['id']; ?>"  onclick="assignWorker('b','<?php echo $get_available_agent['id']; ?>')"  >B</a>
							
							
							<?php
								
							}
							elseif ($get_available_agent['wType'] == 'a' && !empty($get_available_agent['name'])) {
							?>
							
							
							<a id="span<?php echo $get_available_agent['id']; ?>"  href="#" onclick="assignWorker('b','<?php echo $get_available_agent['id']; ?>')"  >B</a>
							
							<?php
							}elseif (!empty($get_available_agent['name']))
							{
							?>
							<span >
							<a id="span<?php echo $get_available_agent['id']; ?>"   href="#" onclick="assignWorker('a','<?php echo $get_available_agent['id']; ?>')"  >A</a>
							</span>
							<?php
							}
						 ?>

<!-- 
							    <a href="#" onclick="available_agents(<?php echo $get_available_agent['id']; ?>)">Edit    </a> -->
							  </div>
							</div>
     					<span>
					</td>
					<td style="color:black; background-color: <?php echo deliveryBoysColor($no_of_orders); ?>" id="name<?php echo $get_available_agent['id']; ?>" onclick="available_agents(<?php echo $get_available_agent['id']; ?>)" contenteditable="true">
							<?php echo $get_available_agent['name'];  ?>
							 <?php //echo $no_of_orders ?> 
						
						
						
						
					</td>
			   
			
			     </tr>
			<?php } ?>
		</tbody>
	</table>

	</div>
</div>
</div>
<div class="alert alert-success alert-sm" id="showNotice" style="box-shadow: 2px 2px 11px black;position: fixed;top:49px;right:14px;z-index: 5555;display: none;"></div>
<input type="hidden" id="oldOrder" value="0">
<input type="hidden" id="bellSetting" value="1">
<input type="hidden" value="<?php echo date('Y-m-d',time())?>" id="myTime" >
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        
        </div>
      <!--   <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>
<script src="<?php echo base_url();  ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();  ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url();  ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/jquery.timepicker.js"></script>
<script src='https://cdn.rawgit.com/admsev/jquery-play-sound/master/jquery.playSound.js'></script>
<script type="text/javascript" src="<?php echo base_url();  ?>assets/js/selectize.min.js"></script>
<!-- ADDED BY ALiGNWEBS - 24-03-2018 9:53 P -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/jquery.sumoselect.min.js"></script>
<!-- END -->
<script type="text/javascript">
	var myInterval;
	
	$(document).ready(function () {
		//$('#oldOrder').val($('#newOrder').val());
		console.log("old:"+$("#oldOrder").val());
			
		ajaxRefresh();
	
	})



	function deliveryAgent(order_id)
		{	
			
			$(".modal-title").html('Edit Delivery Agent');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			var delivery_agent = $("#delivery_agent"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryAgent",{order_id:order_id,delivery_agent:delivery_agent}, 
			 	function(data){
   	
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	

    function ajaxRefresh()
		{

				 	myInterval = setInterval(function(){	

				 	$('#loadingmessage').show();
							 	
					var from2 = $("#from").val();
					var to2 = $("#to").val();
					var date = '<?php echo $date; ?>';
					
					var order_id = $("#mainTable").find('tbody td:eq(1)').html();

  
						 if((from2 ==='') || (from2 === date)){
								
									
								$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date,order_id:order_id},
								 	function(data){

								 
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
								 		var bell = $("#bellSetting").val();
					var newOrder = $("#newOrder").val();
					var oldOrder = $("#oldOrder").val();
					
					if (newOrder > oldOrder) 
					{
						var count = -1;
						var diff = eval(newOrder-oldOrder);
						$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();
						
						if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");console.log('play bell:'+bell);}
						console.log(eval(newOrder-oldOrder));
						$( "tr" ).each(function( index, element ) {
						    if (count != -1) { $( element ).css( "border", "2px solid blue" );}
						   
						    count++;
						    if (count == diff) {console.log("stopped at"+count);return false;}
						  });
						// console.log("yes new order found click the bell");
						$("#oldOrder").val(newOrder);
					}

 									
 									// REMINDER

 									reminderHighlight();

							    });

							}
					}, 10000);
		}
// 		function ajaxRefresh()
// 		{
			

// 				 	myInterval = setInterval(function(){	
// 					// console.log("new:"+$("#newOrder").val());
// 					// console.log("old:"+$("#oldOrder").val());

					
// 				 	$('#loadingmessage').show();
							 	
// 					var from = $("#from").val();
// 					 if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
// 					 var today =  $("#myTime").val();
// 					 if(today != from) return;
// 					var to2 = $("#to").val();
//   					if (to2.length === 0 ) {to2 = from;}

// 					var date = '<?php echo $date; ?>';
					
// 					var order_id = $("#mainTable").find('tbody td:eq(1)').html();
					
// 					//var order_id = undefined;
//   					console.log(order_id);
// 						 if((from !='') && (to2 != '') ){
								
// 									refresh2(order_id,from,to2);
// 								// $.post("<?php //echo base_url(); ?>order/ajaxOrder",{from:from2,order_id:order_id},	
								
// 							}
// 					}, 10000);
// 		}
		function refresh2(order_id,from,to2)
		{

			var from = $("#from").val();
		
			 if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
		
					var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}
			$.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to2,order_id:order_id},
								 	function(data){

								 	// console.log(data);
								 	
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);
								 	var bell = $("#bellSetting").val();
					var newOrder = $("#newOrder").val();
					var oldOrder = $("#oldOrder").val();
					
					if (newOrder > oldOrder) 
					{
						var count = -1;
						var diff = eval(newOrder-oldOrder);
						$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();
						
						if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");}
						console.log(eval(newOrder-oldOrder));
						$( "tr" ).each(function( index, element ) {
						    if (count != -1) { $( element ).css( "border", "2px solid blue" );}
						   
						    count++;
						    if (count == diff) {console.log("stopped at"+count);return false;}
						  });
						// console.log("yes new order found click the bell");
						$("#oldOrder").val(newOrder);
					}

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
 									
 									// REMINDER

 									reminderHighlight();

							    });

		}

		function refresh(order_id,from,to2)
		{

			var from = $("#from").val();
			 if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
					var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}
			$.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to2,order_id:order_id},
								 	function(data){

								 	// console.log(data);
								 	
							  		$('#loadingmessage').hide(); 
							  		$('#loader').hide(); 
							  		
							  		// REFRESH TABLE DATA

								 	$("#myData").html(data);
								 	var bell = $("#bellSetting").val();
					var newOrder = $("#newOrder").val();
					var oldOrder = $("#oldOrder").val();
					
					if (newOrder > oldOrder) 
					{
						var count = -1;
						var diff = eval(newOrder-oldOrder);
						$("#showNotice").fadeIn().html(diff+" New Orders").delay(5000).fadeOut();
						
						if (bell == 1) {$.playSound("https://notificationsounds.com/message-tones/chime-351/download/mp3");}
						console.log(eval(newOrder-oldOrder));
						$( "tr" ).each(function( index, element ) {
						    if (count != -1) { $( element ).css( "border", "2px solid blue" );}
						   
						    count++;
						    if (count == diff) {console.log("stopped at"+count);return false;}
						  });
						// console.log("yes new order found click the bell");
						$("#oldOrder").val(newOrder);
					}

								 	// REFRESH DELIVERY AGENT

								 	$("#agentsData").load(location.href + " #agentsData");
 									
 									// REMINDER

 									reminderHighlight();

							    });

		}

		function reminderHighlight()
		{
			$('#myData tr').each(function () {

				var time = $(this).attr('data-time');

				var currenttime = $(this).attr('data-currenttime');
				
				//alert(currenttime);
				//reminderTime = currenttime - time;
	
				//var rowid = $(this).attr('data-rowid');
				var status = $(this).attr('data-status');

				// var diff = time - currenttime;

				if(time == currenttime || time < currenttime && time != '' && status == 'On Hold')
				{
					$(this).addClass('reminder')
				}	
			})
		}	

		// $(document).ready(function(){

		// 	 $('#loadingmessage').show();
		// 	var date = '<?php echo $date; ?>';


		// 	$.post("<?php echo base_url(); ?>order/ajaxOrder",{from:date},
		// 	 	function(data){

			 	

		// 	 	$("#myData").html(data);
		//   		$('#loadingmessage').hide();  	

		//     });

		// });
	
		function orderStatus(order_id)
		{

			 var status = $("#status"+order_id).val();
			 $.post("<?php echo base_url(); ?>order/ajaxStatusUpdate",{order_id:order_id,status:status}, 
			 	function(data){


			 		$("#allocateColor"+order_id).css("background-color", data);
		  		  

		    });
	

		}


		function purchaseValue(order_id)
		{	
			$(".modal-title").html('Edit Purchase Value');
			 $("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var purchase_value = $("#purchase_value"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getPurchaseValue",{order_id:order_id,purchase_value:purchase_value}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	

        function delivery_charge(order_id)
		{
			$(".modal-title").html('Edit Delivery Charge');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var delivery_charge = $("#delivery_charge"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryCharge",{order_id:order_id,delivery_charge:delivery_charge}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });
		}
		
		function total_w_tax(order_id)
		{
			$(".modal-title").html('Edit Delivery Value');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var total_w_tax = $("#total_w_tax"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getTotalWTax",{order_id:order_id,total_w_tax:total_w_tax}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

	

		}		  

	

		function deliveryTime2(order_id)
		{

			 $(".modal-title").html('Edit Delivery Time');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var delivery_time2 = $("#delivery_time2"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getDeliveryTime",{order_id:order_id,delivery_time2:delivery_time2}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });


		}	

		function assignWork(order_id,worker_mark)
		{
			console.log(order_id);
			console.log(worker_mark);
					
			
			
			 $.post("<?php echo base_url(); ?>order/updateWorker",{order_id:order_id,worker_mark:worker_mark}, 
			 function(data)
			 {	$("#workerType"+order_id).html("");
			 	var from = $("#from").val();var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}

					var order_id = undefined;
					refresh(order_id,from,to2);
   					console.log(data);
   				
   					
		   	 });
		}
		function assignWorker(worker_mark,agentID)
		{

			console.log(agentID);
			console.log(worker_mark);
			 $.post("<?php echo base_url(); ?>order/updateWorkerAgent",{agentID:agentID,worker_mark:worker_mark}, 
			 function(data)
			 {
   					console.log(data);
   					$("#span"+agentID).html("done");
   					var from = $("#from").val();
   					var to2 = $("#to").val();
   					if (to2.length === 0 ) {to2 = from;}

   					
					var order_id = undefined;
					refresh(order_id,from,to2);
		   	 });
		}
		function comment(order_id)
		{

			$(".modal-title").html('Edit Comment');
			$("#myModal").modal();

			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var comment = $("#comment"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getComment",{order_id:order_id,comment:comment}, 
			 	function(data){
   		
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}

			function reminder(order_id)
		{


			 $(".modal-title").html('Edit Reminder Time');
			 $("#myModal").modal();
	
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var reminder = $("#reminder"+order_id).text();

			 $.post("<?php echo base_url(); ?>order/getReminder",{order_id:order_id,reminder:reminder}, 
			 	function(data){
   
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

			
			
			
			
		}	


		 function changeText(param)
          { 
           
               if ($.trim($(param).text()) === 'more') {
                    $(param).text('less');
                } else {
                    $(param).text('more');        
                }
          }

        function convert(str) {
		    var date = new Date(str),
		        mnth = ("0" + (date.getMonth()+1)).slice(-2),
		        day  = ("0" + date.getDate()).slice(-2);
		    return [ date.getFullYear(), mnth, day ].join("-");
		}


	



  //         $(function(){
		//    $('#from').datepick({
		//    	dateFormat: 'yyyy-mm-dd',
		//       onSelect: function (dateText) {

		//       	  var date = convert(dateText);
		//           $.post("<?php echo base_url(); ?>order/ajaxOrder",{date:date}, 
		// 	 	  function(data){
	  	
		// 	 	  	$("#myData").html(data);
		//     	});
		//       }
		//    });
		// });

      	function exportCsv()
      	{
      		var from = $('#from').val();
      		var to = $('#to').val();

      		var today = '<?php echo $date; ?>';
      		
      		var cdate = "";

      		if(from ==='')
      		{
      			from = today
      		}
      		
      		cdate = from+"|"+to;
      		window.location.href = "<?php echo base_url(); ?>order/export/"+cdate;
      	}
		 

		 $('#from').datepick({dateFormat: 'yyyy-mm-dd'});
		 $('#to').datepick({dateFormat: 'yyyy-mm-dd'});
		
		function searchOrder()
		{
			var from = $("#from").val();
		    if (from.length === 0 ) {from = $("#myTime").val();$("#from").val(from);}
			var to = $("#to").val();
		    $('#loadingmessage').show();
		    var order_id = $("table").find('tbody td:eq(1)').html();

			if(to ===''){

		          $.post("<?php echo base_url(); ?>order/ajaxOrder",{from:from,order_id:order_id}, 
			 	  function(data){
	  	
			 	  	$("#myData").html(data);
			 	  	$('#loadingmessage').hide();
			 	  	initFilters();

		    });}else{

		          $.post("<?php echo base_url(); ?>order/ajaxOrderByRange",{from:from,to:to,order_id:order_id}, 
			 	  function(data){
	  	
			 	  	$("#myData").html(data);
			 	  	$('#loadingmessage').hide();
			 	  	initFilters();
		    });

		          }


		}

	 $("#showSearch").click(function () {

	         $("#header").toggle();
	    });


	function editStatus(order_id,status)
	{
			
			$(".modal-title").html('Edit Status');

            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            
           // var status = $("td #status"+order_id).val();
            //alert(status);
		  $.ajax({
              type: "POST",
              data: {order_id:order_id,status:status},
              url: "<?php echo base_url(); ?>order/getStatus",
              success:function(response){

              
            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);
              

            }
            });
	}


// 	function viewOrderAddress(order_id)
// 	{
	    
		 
// 			$(".modal-title").html('Order Details');
			
//             $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

// 		  	$.ajax({
//               type: "POST",
//               data: {order_id:order_id},
//               url: "<?php echo base_url(); ?>order/getOrderById",
//               success:function(response){

              
//             // $('.modal-body').css('background-color','white');
//               $(".modal-body").html(response);

//             }
//             });
// 	}


	function viewOrderAddress(order_id)
	{
		 
			$(".modal-title").html('Order Details');
			
            $(".modal-body").html('<center><h4>Loading...</h4></center>');
            

		  	$.ajax({
              type: "POST",
              data: {order_id:order_id},
              url: "<?php echo base_url(); ?>order/getOrderById",
              success:function(response){

              
            // $('.modal-body').css('background-color','white');
              $(".modal-body").html(response);

            }
            });
	}
	function locality(order_id)
		{	
			
			$(".modal-title").html('Edit Locality');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			var locality = $("#locality"+order_id).text();
			
			 $.post("<?php echo base_url(); ?>order/getLocality",{order_id:order_id,locality:locality}, 
			 	function(data){
   	
			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}	 

		function available_agents(id)
		{	
			$(".modal-title").html('Add Agent');
			$("#myModal").modal();
			 $(".modal-body").html('<center><h4>Loading...</h4></center>');
			 var agentName = $("#name"+id).text();
			 $.post("<?php echo base_url(); ?>order/availableAgents",{id:id,agentName:agentName}, 
			 	function(data){
   			

			 		
   					$(".modal-body").html(data);	//alert(data);
		  		  

		    });

		}

		// CHANGE COLOR OF DELIVERY BOYS AND CHANGE STATUS OF DELIVERY BOYS
		
		function changeColor(id)
		{	

			var color = $('#name'+id).css("background-color");
			if(color == 'rgb(0, 255, 0)')
			{
				$("#name"+id).css("background-color", 'rgb(255,0,0)');
			}else{

				$("#name"+id).css("background-color", 'rgb(0,255,0)');
			}

				var status = $("#status"+id).val().trim();

			
			
			$.post("<?php echo base_url(); ?>order/update_available_agents_status",{id:id,status:status}, 
			 function(data){
   					
   					//alert(data);

			 		

		    });
		}

		function sendSms(order_id)
		{

			$(".modal-title").html('SMS');
			$("#myModal").modal();
			$(".modal-body").html('<center><h4>Loading...</h4></center>');
							
			$.post("<?php echo base_url(); ?>order/getOrderByIdInSms",{order_id:order_id}, 
				function(data){

					$(".modal-body").html(data);
								 	
					//$('#myModal').modal('hide');

			});
		}

</script>

<!-- ADDED BY ALIGNWEBS 24-03-2018 -->
<script>
	function initFilters()
	{
		$(document).find('.SumoUnder').each(function(index, el) {
			
			var $select = $(this)
			var type = $select.attr('name');
			/*console.log(type)*/
			$('#myData tr').each(function () {
				var type_value = $(this).attr('data-'+type);
				

				if($select.find('option[value="'+type_value+'"]').length < 1 && type_value != "")
						$('<option value="'+type_value+'" selected>'+type_value+'</option>').appendTo($select);

			})

			$select.SumoSelect()
		});
	}

	function clearFilters()
	{
		$(document).find('.SumoUnder').each(function(index, el) {
			
			var $select = $(this)
			/*console.log(type)*/
			$(this).find('option').each(function () {
				
				$(this).removeAttr('selected','selected');

			})

			$select[0].sumo.reload();
		});
	}

	function selectAllFilters()
	{
		searchOrder()
	}

	$('.SumoUnder').change(function () { 

		var name = $(this).attr('name');

		getFiltersValues()

	})

	function getFiltersValues()
	{
		var filters = ["merchant","agent","locality","pt","status"];

		// var merchant = $('select[name="merchant"]').val();
		// var agent = $('select[name="agent"]').val();
		// var locality = $('select[name="locality"]').val();
		// var pt = $('select[name="pt"]').val();
		// var status = $('select[name="status"]').val();

		$('#myData tr').each(function () {
				var $row = $(this);
				$row.removeClass('aw-filter-visible')

				var matched = true;

				$(filters).each(function (index, key) {

					if($('select[name="'+key+'"] option:selected').length > 0)
					{
				
						if(matched == true)
						{

							if($.inArray($row.attr('data-'+key), $('select[name="'+key+'"]').val()) >= 0)
							{
								$row.addClass('aw-filter-visible');								
							}
							else{
								matched = false;
								$row.removeClass('aw-filter-visible');
							}

						} else {
								matched = false;
								$row.removeClass('aw-filter-visible');
							}
					}

				})
		})

		$('#myData tr').show();

		if($('#myData tr.aw-filter-visible').length > 0)
		{
			$('#myData tr').not('.aw-filter-visible').hide();
		}
		else
			$('#myData tr').hide()

	}
</script>
<!-- END -->
</body>
</html>