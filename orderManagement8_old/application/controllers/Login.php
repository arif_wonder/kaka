<?php
header('Access-Control-Allow-Origin: *');
class Login extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();

		
		$this->load->model('ModelOrder');
	}


	public function index()
	{	

		$password = $this->session->userdata('password');

		
		if($password == 'admin123')
		{
			redirect(base_url());

		}
	
		$this->load->view('view_login');
	}

	public function login_save()
	{	
		$password = $this->session->userdata('password');

		
		if($password == 'admin123')
		{
			redirect(base_url());

		}

		if(isset($_POST['submit']))
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if(($username == 'wonderpillars' && $password == 'admin123') || ($username == 'worker12a' && $password == 'worker12pass') || ($username == 'worker43b' && $password == 'workerbpass') )
			{
				$this->session->set_userdata('username',$username);
				$this->session->set_userdata('password',$password);

				redirect(base_url());

			}else
			{
				$this->session->set_flashdata('failure', 'Your Username or Password is invalid');
				redirect(base_url().'login');

			}


		}

	}

	public function logout()
	{
		$this->session->unset_userdata('password');
		
		redirect(base_url().'login');
	}

}
