<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set("Asia/Kolkata");
class Order extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		
		$password = $this->session->userdata('password');
		if(($password == 'admin123') || ($password == 'worker12pass') || ($password == 'workerbpass'))
		{
			$this->load->model('ModelOrder');
		}
		else
		redirect(base_url().'login');
		
	}

	public function index()
	{
		$password = $this->session->userdata('password');

		
		if ($password == "admin123") {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
			$this->load->view('view_order',$data);
		}
		elseif ($password == "worker12pass") {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgentsA();

			$this->load->view('workerA_view',$data);
			
		}elseif ($password == "workerbpass") {
			$data['get_available_agents'] = $this->ModelOrder->getAvailableAgentsB();

			$this->load->view('workerB_view',$data);
		}

	}

// new work

	public function sendToApp()
	{
		extract($_POST);
		$record =$this->ModelOrder->agentInfo($agent_id);
		$token = $record['firebase_id'];

		$record2 = $this->ModelOrder->getStatus2($order_id);
		 $status  = $record2['status'];
		$agentName = $record['name'];
		$data11 = array(

					'delivery_agent' =>  $agentName

				);		
		$this->ModelOrder->deliveryAgentUpdate($data11,$order_id);


		$update_data = array('app_status'=>'unseen');
		$this->ModelOrder->appStatusUpdate($update_data,$order_id);
// 		$data22 = array(

// 					'status' =>  "Processing"

// 				);

// 			$this->ModelOrder->statusUpdate($data22,$order_id);
		//for history record
		date_default_timezone_set("Asia/Kolkata");
		$historyData = array(
					'merchant_name' 	=> $merchant_name,
					'items' 			=> $items,
					'total' 			=> $total,
					'contact' 		=> $contact,
					'order_id' 		=> $order_id,
					'name' 			=> $name,
					'location' 		=> $location,
					'status' 		=> $status,
					'agent_id' 		=> $agent_id,
					'create_date'   => date('Y-m-d h:i:s', time())
		);

		if ($app_status_flag == "new")
			$this->ModelOrder->saveHistory($historyData);
		else
			$this->ModelOrder->updateHistory($historyData,$order_id);

		define( 'API_ACCESS_KEY', 'AIzaSyB8JwFBtEyDOCi6cFvuTr26AG99h05dLMI' );
                       $registrationIds = array( $token );
                       $msg = array
                       (
	                      'message' 	=> "You Have Receive a New Order",
	                      'title'		=> "Order Notify",
						  'image' => "https://www.finalcheck.speedypixelgame.com//upload/1476254450-1475737169-kaka-logo-(1).png",
						  'merchant_name' 	=> $merchant_name,
						  'items' 			=> $items,
						  'total' 			=> $total,
						  'contact' 		=> $contact,
						  'order_id' 		=> $order_id,
						  'name' 			=> $name,
						  'location' 		=> $location,
						  'status' 			=> $status,
						  'agent_id' 		=> $agent_id

                        );
                       $fields = array
                       (
	                      'registration_ids' 	=> $registrationIds,
	                      'data'			=> $msg

                       ); 
                       $headers = array
                       (
        	               'Authorization: key=' . API_ACCESS_KEY,
        	               'Content-Type: application/json'
                       ); 
                       $ch = curl_init();
                       curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                       curl_setopt( $ch,CURLOPT_POST, true );
                       curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                       curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                       curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                       curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                       $result = curl_exec($ch );
                       curl_close( $ch );
	}
	public function addAgent()
	{
		$record =$this->ModelOrder->agentInfo($_POST['id']);
		$name = $record['name'];
		$data = array('name'=>$name,'agent_id'=>$_POST['id']);
		$this->ModelOrder->agentADD($data);	
	}
	public function removeAgent()
	{
		$this->ModelOrder->removeAgent($_POST['id']);
	}
	public function agents()
	{
		$password = $this->session->userdata('password');

		if ($password == "admin123")
		{
			$data['agents'] =  $this->ModelOrder->getAgents();
			$this->load->view("view_agents.php",$data);
		}

		
	}
public function showAgentOrders()
{
	$agent = $_POST['agent'];
	$data['type'] = "showAgentOrders";
	$data['data'] = $this->ModelOrder->showAgentOrders($agent);
	$this->load->view('view_ajax_order_detail',$data);
}
public function showAgentHistory()
{
	$agent = $_POST['agent'];
	$data['type'] = "showAgentHistory";
	$data['data'] = $this->ModelOrder->showAgentHistory($agent);
	$this->load->view('view_ajax_order_detail',$data);	
}
public function getAgentsData()

	{
		$data['agent_id'] = $_POST['agent_id'];
		$data['type'] = 'showProfile';
		$data['agent_data'] = $this->ModelOrder->getAgentProfile($data['agent_id']);
		$this->load->view('view_ajax_order_detail',$data);
	}
public function updateAgentProfile()
{
	$agent_id = $_POST['agent_id'];
	$data = array(

			'name' =>  $_POST['name'],
			'username' =>  $_POST['username'],
			'password' =>  $_POST['password'],
			'lati' =>  $_POST['lati'],
			'longi' =>  $_POST['longi'],
			'status' =>  $_POST['status'],
			'mobile' =>  $_POST['mobile']

			);
	$this->ModelOrder->updateAgent($data,$agent_id);
}
public function addNewAgent()
{
	$data = array(

			'name' =>  $_POST['name'],
			'username' =>  $_POST['username'],
			'password' =>  $_POST['password'],
			'lati' =>  $_POST['lati'],
			'longi' =>  $_POST['longi'],
			'status' =>  $_POST['status'],
			'mobile' =>  $_POST['mobile']

			);
	$this->ModelOrder->addAgent($data);
}
public function delAgent()
{
	$agent_id = $_POST['agent_id'];
	$this->ModelOrder->delAgent($agent_id);
}
public function getUserLocation()
{
	$data['lati'] = $_POST['lat'];
	$data['longi'] = $_POST['long'];
	$data['type'] = 'showLocation';
	$this->load->view('view_ajax_order_detail',$data);
}
//new work end







	public function workerA()
	{
		$password = $this->session->userdata('password');
		$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
		if (($password == "worker12pass") ||($password == "admin123")){
			$this->load->view('workerA_view',$data);
			}
		
	}
	public function workerB()
	{
		$password = $this->session->userdata('password');
		
		$data['get_available_agents'] = $this->ModelOrder->getAvailableAgents();
		if (($password == "workerbpass") ||($password == "admin123")){
			$this->load->view('workerB_view',$data);
			}
	}
	public function logout()
	{
		$this->session->unset_userdata(array('username','password'));
		redirect(base_url().'login');
	}
	
	public function updateWorker()
	{
		$order_id = $_POST['order_id'];
		$worker = $_POST['worker_mark'];
		$this->ModelOrder->workerUpdate($order_id,$worker);
	}
	public function updateWorkerAgent()
	{
		$order_id = $_POST['agentID'];
		$worker = $_POST['worker_mark'];
		$this->ModelOrder->workerUpdateAgent($order_id,$worker);
	}
	public function ajaxStatusUpdate()
	{
		$order_id = $_POST['order_id'];


		$data = array(

				'status' =>  $_POST['status']

		);

		$this->ModelOrder->statusUpdate($data,$order_id);
		echo allocateColor($data['status']);


		if($_POST['status'] == 'Delivered')
		{
			$data = array(

				'delivery_time2' =>  date('g:i')

			);
		
			$this->ModelOrder->deliveryTime2Update($data,$order_id);


			$agentName = $_POST['agentName'];

			$data = array(

			"status" => 1

			);

			$this->ModelOrder->agentStatusGreenByName($data,$agentName);
		}
	}

	public function ajaxPurchaseValueUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'purchase_value' =>  $_POST['purchase_value']

			);
		
		$this->ModelOrder->purchaseValueUpdate($data,$order_id);
		echo 'Purchase Value Updated.';
	}

	public function ajaxTotalWTaxValueUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'total_w_tax' =>  $_POST['total_w_tax']

			);
		
		$this->ModelOrder->totalWTaxValueUpdate($data,$order_id);
		echo 'Delivery Value Updated.';
	} 
	
		public function ajaxDeliveryChargeUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'delivery_charge' =>  $_POST['delivery_charge']

			);
		
		$this->ModelOrder->deliveryChargeUpdate($data,$order_id);
		echo 'Delivery Value Updated.';
	} 

	public function ajaxDeliveryAgentUpdate()
	{

		$order_id = $_POST['order_id'];
		$agentName = $_POST['delivery_agent'];

		if($this->ModelOrder->getAvailableAgentsByName($agentName))
		{
			$data = array(

				'delivery_agent' =>  $agentName

			);
		
			$this->ModelOrder->deliveryAgentUpdate($data,$order_id);
		}
		else
		{

			echo 'false';

		}
	}

	public function ajaxLocalityUpdate()
	{

		$order_id = $_POST['order_id'];
		$data = array(

				'locality' =>  $_POST['locality']

			);
		
		$this->ModelOrder->localityUpdate($data,$order_id);
		echo 'Locality Updated.';
	}

	public function ajaxDeliveryTime2Update()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'delivery_time2' =>  $_POST['delivery_time2']

			);
		
		$this->ModelOrder->deliveryTime2Update($data,$order_id);
		echo 'Delivery Agent Updated.';
	}

	public function ajaxCommentUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'comment' =>  $_POST['comment']

			);
		
		$this->ModelOrder->commentUpdate($data,$order_id);
		echo 'Comment Updated.';
	}

	public function ajaxReminderUpdate()
	{
		$order_id = $_POST['order_id'];
		$data = array(

				'reminder' =>  $_POST['reminder']

			);
		
		$this->ModelOrder->reminderUpdate($data,$order_id);
		echo 'Reminder Updated.';
	}


	public function ajaxOrder()
	{	
		$from = $_POST['from'];
		$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;

		
		$data['getOrders'] = $this->ModelOrder->getOrder($from);
	
		if(sizeof($data['getOrders']) == 0 ){

		 	echo "NO Orders Yet.";

		 }else{

			$last_id = $data['getOrders'][0]['order_id'];
			
			if($last_id > $first_order_id)
			{
				$this->decodeOrderItem($first_order_id);
			}

		}
		
		$this->load->view('view_ajax_order',$data);
		
		
	}
	public function ajaxOrderWorkerA()
	{	
		$from = $_POST['from'];
		$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;

		
		$data['getOrders'] = $this->ModelOrder->getOrderA($from);
	
		if(sizeof($data['getOrders']) == 0 ){

		 	echo "NO Orders Yet.";

		 }else{

			$last_id = $data['getOrders'][0]['order_id'];
			
			if($last_id > $first_order_id)
			{
				$this->decodeOrderItem($first_order_id);
			}

		}
		
		$this->load->view('view_ajax_orderA',$data);
		
		
	}
public function ajaxOrderWorkerB()
	{	
		$from = $_POST['from'];
		$first_order_id = isset($_POST['order_id']) ? $_POST['order_id'] : 1;

		
		$data['getOrders'] = $this->ModelOrder->getOrderB($from);
	
		if(sizeof($data['getOrders']) == 0 ){

		 	echo "NO Orders Yet.";

		 }else{

			$last_id = $data['getOrders'][0]['order_id'];
			
			if($last_id > $first_order_id)
			{
				$this->decodeOrderItem($first_order_id);
			}

		}
		
		$this->load->view('view_ajax_orderB',$data);
		
		
	}
	public function ajaxOrderByRange()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
		$this->load->view('view_ajax_order',$data);
	}
	
	public function ajaxOrderByRangeWorkerA()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
		$this->load->view('view_ajax_orderA',$data);
	}

	public function ajaxOrderByRangeWorkerB()
	{	
		$from = $_POST['from'];
		$to = $_POST['to'];
		$data['getOrders'] = $this->ModelOrder->getOrderByRange($from,$to);
		$this->load->view('view_ajax_orderB',$data);
	}
	// public function reminderOrder()
	// {	
	// 	$date = $_POST['date'];

	// 	$data['getRemindOrders'] = $this->ModelOrder->getRemindOrder($date);
		
	// 	foreach($data['getRemindOrders'] as $reminderOrder)
	// 	{
	// 		echo $reminderOrder['order_id']."-";
	// 	}

	
	 
	// 	//$this->load->view('view_ajax_order',$data);
	// }

	public function getReminder()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['reminder'] = $_POST['reminder'];
		$data['type'] = 'reminder';
		$this->load->view('view_ajax_order_detail',$data);

	}


	public function getStatus()

	{

		$data['order_id'] = $_POST['order_id'];
		$data['status'] = $_POST['status'];
		$data['type'] = 'status';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);
		$this->load->view('view_ajax_order_detail',$data);

	}


	public function getPurchaseValue()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['purchase_value'] = $_POST['purchase_value'];
		$data['type'] = 'purchase_value';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getDeliveryAgent()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_agent'] = $_POST['delivery_agent'];
		$data['type'] = 'delivery_agent';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getLocality()
	{
		$data['order_id'] = $_POST['order_id'];
		// $data['locality'] = $_POST['locality'];
		$data['type'] = 'locality';
		$data['selected_locality'] = $_POST['locality'];
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getTotalWTax()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['total_w_tax'] = $_POST['total_w_tax'];
		$data['type'] = 'total_w_tax';
		$this->load->view('view_ajax_order_detail',$data);

	}
	
		public function getDeliveryCharge()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_charge'] = $_POST['delivery_charge'];
		$data['type'] = 'delivery_charge';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getDeliveryTime()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['delivery_time2'] = $_POST['delivery_time2'];
		$data['type'] = 'delivery_time2';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getComment()
	{
		$data['order_id'] = $_POST['order_id'];
		$data['comment'] = $_POST['comment'];
		$data['type'] = 'comment';
		$this->load->view('view_ajax_order_detail',$data);

	}

	public function getOrderById()
	{

		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'item';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);
		$this->load->view('view_ajax_order_detail',$data);

	}


	public function decodeOrderItem($first_order_id='')
	{

		$first_order_id = $first_order_id;

		$data['getOrders'] = $this->ModelOrder->getOrderForDecodeItem($first_order_id);
		
		foreach($data['getOrders'] as $getOrder)
		{	
			$order_id = $getOrder['order_id'];
			//$sizeOfItems = sizeof(json_decode($getOrder['json_details'],true));
			$Items = json_decode($getOrder['json_details'],true);
		// 	print_r($data['getOrders']);
		// die();
			$orderItem = [];
			
			foreach ($Items as $key => $item) {

				// $price = $item['price'];
				// $size = explode("|", $price);
				// $size = isset($size[1]) ? " (".$size[1].")" : " ";
				$orderItem[] = $item['qty']." - ".$this->ModelOrder->getItemById($item['item_id']);

			}

			$totalOrder = implode(",", $orderItem);
			$data = array(

				"decode_item" => $totalOrder

				);
			$this->ModelOrder->updateOrderItem($order_id,$data);
			
		}
			
		
	}


	public function ajaxDecodeItemUpdate()
	{
		$order_id = $_POST['order_id'];

		$data = array(

				'decode_item' =>  $_POST['decode_item']

			);

		$this->ModelOrder->decodeItemUpdate($data,$order_id);
		//echo 'Decode Item Updated.';
	}

	public function update_available_agents()
	{
		$id = $_POST['id'];

		$data = array(

			"name" => trim($_POST['agentName'])

			);
		

		$this->ModelOrder->agentUpdate($data,$id);
	}

	// UPDATE DELIVERY BOYS STATUS

	public function update_available_agents_status()
	{
		$id = $_POST['id'];

		$status = $_POST['status'];

		if($status == 1)
		{
			$statusValue = 0;

		}else{

			$statusValue = 1;
		}

		$data = array(

			"status" => trim($statusValue)

			);

		$this->ModelOrder->agentStatusUpdate($data,$id);


	}


	public function getOrderByIdInSms()

	{ 

		$data['order_id'] = $_POST['order_id'];
		$data['type'] = 'sms';
		$data['orderById'] = $this->ModelOrder->getOrderById($data['order_id']);

		$this->load->view('view_ajax_order_detail',$data);

	}

	// SEND SMS TO DELIVERY BOYS

	public function smsApi()
	{
 
		$order_id = $_POST['order_id'];


			// AFTER SEND SMS INSERT DELIVERY BOYS NAME TO AGENT COLUMN

			$agentName = $_POST['agentName'];

			if($this->ModelOrder->getAvailableAgentsByName($agentName))
			{
				
				$data = array(

					'delivery_agent' =>  $agentName

				);
				
				$this->ModelOrder->deliveryAgentUpdate($data,$order_id);
			}
			else
			{

				echo 'false';
				exit();
			}

			// AFTER SEND SMS CHANGE STATUS TO PROCESSING

			$data = array(

					'status' =>  "Processing"

				);

			$this->ModelOrder->statusUpdate($data,$order_id);


			// SEND SMS TO DELIVERY BOYS

			$user = "20082245";

			$pass = "x66p4p";

			$sender = "ONKAKA";

			$phone = $_POST['mobile'];

			$text = $_POST['smsText'];

			$priority = "dnd";

			$stype = "0";


			$data = "user=".$user."&pwd=".$pass."&senderid=".$sender."&mobileno=".$phone."&msgtext=".$text."&smstype=".$stype;
		
			$ch = curl_init('http://bulksmsindia.mobi/sendurlcomma.aspx?');

			curl_setopt($ch, CURLOPT_POST, true);

			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);

			$success = '<div class="alert alert-success">Message Sent</div>';

			//echo $success;

			curl_close($ch);

		
	}


		public function export($cdate='')
	{
	    $date = explode("%7C", $cdate);
	
		$from = $date[0];
		$to = $date[1];
		
		
		$getOrders = $this->ModelOrder->getOrder($from, $to);
// 		echo "<pre>";
// 		print_r($getOrders);
// 		echo "</pre>";
// 		die();

		foreach($getOrders as $getOrder) {
			// echo "<pre>";
			// print_r($this->ModelOrder->getClientById($getOrder['client_id']));
			// echo "</pre>";
		
			$time = strtotime($getOrder['date_created']);
			$con = date("g:i", $time);
			

			$data[] =array(


					"REF" 			 => $getOrder['order_id'],
					"MOP" 			 => strtoupper($getOrder['payment_type']),
					"STATUS" 		 => strtoupper($getOrder['status']),
					"NAME"			 => strtoupper($getOrder['first_name']." ".$getOrder['last_name']),
					"MOBILE"  		 => isset($getOrder['contact_phone']) ? $getOrder['contact_phone'] : " ",
					"CON"			 => $con,
					"DEL"			 => $getOrder['delivery_time2'],
					"RESTAURANT"     => strtoupper($getOrder['restaurant_name']),
					"LOCALITY"		 => strtoupper($getOrder['locality']),
					"AGENT"			 => strtoupper($getOrder['delivery_agent']),
					"PV" 			 => $getOrder['purchase_value'],
					"DV"			 => $getOrder['total_w_tax'],
					"DF"			 => $getOrder['delivery_charge'],
					"PROFIT"		 =>	$getOrder['total_w_tax'] - $getOrder['purchase_value'],
					"CHANGES"		 => $getOrder['comment']



				);

		}

		$this->db->insert_batch('mt_farhan_excel',$data);

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		 $this->ModelOrder->export($cdate);

	}
	
	public function availableAgents()
	{
		$data['id'] = $_POST['id'];
		$data['agentName'] = $_POST['agentName'];
		$data['val'] = 'availabel_agents';
		$this->load->view('view_ajax_agent',$data);

	}

	public function updateDeliveryAgent()
	{
		$id = $_POST['id'];
		$data = array(

				'name' =>  $_POST['agentName']

			);
		
		$this->ModelOrder->updateDeliveryAgent($data,$id);
		echo 'Delivery Value Updated.';
	} 
}
