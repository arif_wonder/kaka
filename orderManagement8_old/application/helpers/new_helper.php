<?php 



function allocateColor($status)
{
	switch ($status) {
		case 'Pending':
			return 'background-color: white';
			break;
		case 'Confirmed':
			return 'background-color: #00E5EE';
			break;
		case 'Processing':
			return 'background-color: yellow';
			break;
		case 'Delivered':
			return 'background-color: #00ff00';
			break;
		case 'Cancelled':
			return 'background-color: #ff211d';
			break;
		case 'On Hold':
			return 'background-color: pink';
			break;
		case 'Packed':
			return 'background-color: #cccecd';
			break;
			
	
	}
}



function deliveryBoysColor($no_of_orders)
{
	switch ($no_of_orders) {
		case '':
			return '#00ff00';
			break;
		case '1':
			return '#FFFF33';
			break;
		case '2':
			return '#FF6600';
			break;
		case '3':
			return '#FF0000';
			break;
	
	}
}


function uniqueAssocArray($array, $uniqueKey) {
  if (!is_array($array)) {
    return array();
  }
  $uniqueKeys = array();
  foreach ($array as $key => $item) {
    if (!in_array($item[$uniqueKey], $uniqueKeys)) {
      $uniqueKeys[$item[$uniqueKey]] = $item;
    }
  }
  return $uniqueKeys;
}


 ?>