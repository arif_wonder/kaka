<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelFilter extends CI_Model {


		public function getOrder($from,$values,$type,$to)
		{	

	            if($to == '')
				{

					$this->db->where('delivery_date',$from);
					
				}else{

					 $this->db->where('delivery_date >=', $from);
	           		 $this->db->where('delivery_date <=', $to);
				}

	            switch ($type) {
	            	case 'agent':
	            		$this->db->where_in('delivery_agent', $values);
	            		break;
	            	case 'merchant':
	            		$this->db->where_in('mt_order_duplicate_farhan.merchant_id', $values);
	            		break;
	            	case 'payment_type':
	            		$this->db->where_in('mt_order_duplicate_farhan.payment_type', $values);
	            		break;
	            	case 'status':
	            		$this->db->where_in('mt_order_duplicate_farhan.status', $values);
	            		break;
	            	case 'locality':
	            		$this->db->where_in('mt_order_duplicate_farhan.locality', $values);
	            		break;
	            }
	        
			
			$this->db->order_by('order_id','desc');

			$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
			$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

			$this->db->select('mt_order_duplicate_farhan.order_id,
								mt_order_duplicate_farhan.decode_item,
							   mt_merchant.restaurant_name,
							   mt_client.first_name,
							   mt_client.last_name,
							   mt_client.contact_phone,
							   mt_order_duplicate_farhan.payment_type,
							   mt_order_duplicate_farhan.reminder,
							   mt_order_duplicate_farhan.purchase_value,
							   mt_order_duplicate_farhan.total_w_tax,
							   mt_order_duplicate_farhan.delivery_charge,
							   mt_order_duplicate_farhan.status,
							   mt_order_duplicate_farhan.delivery_agent,
							   mt_order_duplicate_farhan.locality,
							   mt_order_duplicate_farhan.date_created,
							   mt_order_duplicate_farhan.delivery_time,
							   mt_order_duplicate_farhan.delivery_time2,
							   mt_order_duplicate_farhan.comment,
							   mt_order_duplicate_farhan.workerType,
							   mt_order_duplicate_farhan.app_status

							  ');
			$query = $this->db->get('mt_order_duplicate_farhan');
		    return $query->result_array();
		
			
		}



		public function getOrder2($from,$to,$merchant,$status,$locality,$payment_type,$agent)
		{	

	            if($to == '')
				{

					$this->db->where('delivery_date',$from);
					
				}else{

					 $this->db->where('delivery_date >=', $from);
	           		 $this->db->where('delivery_date <=', $to);
				}

			$this->db->where_in('delivery_agent', $agent);
			$this->db->where_in("mt_order_duplicate_farhan.locality",$locality);
			$this->db->where_in("mt_order_duplicate_farhan.payment_type",$payment_type);
			$this->db->where_in("mt_order_duplicate_farhan.status",$status);
			$this->db->where_in("mt_order_duplicate_farhan.merchant_id",$merchant);


			$this->db->order_by('order_id','desc');

			$this->db->join('mt_merchant', 'mt_merchant.merchant_id = mt_order_duplicate_farhan.merchant_id');
			$this->db->join('mt_client', 'mt_client.client_id = mt_order_duplicate_farhan.client_id');

			$this->db->select('mt_order_duplicate_farhan.order_id,
								mt_order_duplicate_farhan.decode_item,
							   mt_merchant.restaurant_name,
							   mt_client.first_name,
							   mt_client.last_name,
							   mt_client.contact_phone,
							   mt_order_duplicate_farhan.payment_type,
							   mt_order_duplicate_farhan.reminder,
							   mt_order_duplicate_farhan.purchase_value,
							   mt_order_duplicate_farhan.total_w_tax,
							   mt_order_duplicate_farhan.delivery_charge,
							   mt_order_duplicate_farhan.status,
							   mt_order_duplicate_farhan.delivery_agent,
							   mt_order_duplicate_farhan.locality,
							   mt_order_duplicate_farhan.date_created,
							   mt_order_duplicate_farhan.delivery_time,
							   mt_order_duplicate_farhan.delivery_time2,
							   mt_order_duplicate_farhan.comment,
							   mt_order_duplicate_farhan.workerType,
							   mt_order_duplicate_farhan.app_status

							  ');
			$query = $this->db->get('mt_order_duplicate_farhan');
		    return $query->result_array();
		
			
		}

}