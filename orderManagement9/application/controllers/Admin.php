<?php 	

class Admin	extends CI_Controller	
{
	public function __construct() {
	    parent::__construct();

		$this->load->model('ModelAdmin');
		$this->load->library('form_validation');         
	  }

	public function index()
	{	
		
		$this->load->view('admin/view_login');
		
	}

	public function login()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['login'])) {
   			
   			
   			 $this->form_validation->set_rules('username', 'Username', 'required');
   			 $this->form_validation->set_rules('password', 'Password', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                      
                        $this->load->view('admin/view_login');
                     
                }
                else
                {
                       $data = array(

                       	"username" => $_POST['username'],
                       	"password" => $_POST['password']

                       );


                       $user = $this->ModelAdmin->login($data);

                       if(!empty($user))
                       {
                       		 $this->session->set_userdata('user', $user);
                       		 redirect(base_url().'admin/dashboard');

                       }else{

                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Invalid Login.</div>');
                       		redirect(base_url().'admin');
                       }
                }
			
		




		}


	}

	public function dashboard()
	{	

		$data['title']  = 'Dashboard';
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_dashboard.php");
		$this->load->view("admin/view_footer.php");

	}

	public function userAdd()
	{
		$data['title']  = 'Add User';
		$data['panels']  = $this->ModelAdmin->getPanels();
		$data['roles']  = $this->ModelAdmin->getRoles();
		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_add_user.php");
		$this->load->view("admin/view_footer.php");

	}


	public function storeUser()
		{
			if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['userAdd'])) {
	   			
	   			$this->form_validation->set_rules('panel_id', 'Panel', 'required');
	   			$this->form_validation->set_rules('role_id', 'Role', 'required');
	   			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[mt_farhan_users.username]');
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
                $this->form_validation->set_rules('confirmPassword', 'Password Confirmation', 'trim|required|matches[password]');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[mt_farhan_users.email]');
                $this->form_validation->set_rules('mobile', 'Mobile', 'required');

	                if ($this->form_validation->run() == FALSE)
	                {
	                      
	                        $data['title']  = 'Add User';
	                        $data['panels']  = $this->ModelAdmin->getPanels();
	                        $data['roles']  = $this->ModelAdmin->getRoles();
	                       
	                        $this->load->view("admin/view_header.php",$data);
	                        $this->load->view("admin/view_add_user.php");
	                        $this->load->view("admin/view_footer.php");
	                     
	                }
	                else
	                {
	                       $data = array(

	                       	"panel_id"  => $_POST['panel_id'],
	                       	"role_id"  => $_POST['role_id'],
	                       	"username" => $_POST['username'],
	                       	"email" => $_POST['email'],
	                       	"password" => $this->encryption->encrypt($_POST['password']),
	                       	"mobile" => $_POST['mobile'],

	                       );


	                       $user = $this->ModelAdmin->userAdd($data);

	                       if($user)
	                       {
	                       		$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> User Added Successfully.</div>');
	                       		redirect(base_url().'admin/userAdd');

	                       }else{

	                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
	                       		redirect(base_url().'admin/userAdd');
	                       }
	                }
				
			




			}


		}


		public function userList()
		{
			$data['title']  = 'Users List';
			
			$data['users']  = $this->ModelAdmin->getUsers();
			$this->load->view("admin/view_header.php",$data);
			$this->load->view("admin/view_list_users.php",$data);
			$this->load->view("admin/view_footer.php");

		}


		public function userEdit($id='')
		{
			$data['title']  = 'Edit User';
			$data['panels']  = $this->ModelAdmin->getPanels();
			$data['roles']  = $this->ModelAdmin->getRoles();
			$data['user']  = $this->ModelAdmin->getUserById($id);

			$this->load->view("admin/view_header.php",$data);
			$this->load->view("admin/view_edit_user.php");
			$this->load->view("admin/view_footer.php");

		}


			public function updateUser($id='')
				{
					if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['userEdit'])) {
			   			
			   			$this->form_validation->set_rules('panel_id', 'Panel', 'required');
			   			$this->form_validation->set_rules('role_id', 'Role', 'required');
			   			$this->form_validation->set_rules('username', 'Username', 'required');
		                $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		                $this->form_validation->set_rules('confirmPassword', 'Password Confirmation', 'trim|required|matches[password]');
		                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		                $this->form_validation->set_rules('mobile', 'Mobile', 'required');

			                if ($this->form_validation->run() == FALSE)
			                {
			                      
			                        $data['title']  = 'Edit User';
			                        $data['panels']  = $this->ModelAdmin->getPanels();
			                        $data['roles']  = $this->ModelAdmin->getRoles();
			                       	$data['user']  = $this->ModelAdmin->getUserById($id);
			                        $this->load->view("admin/view_header.php",$data);
			                        $this->load->view("admin/view_edit_user.php");
			                        $this->load->view("admin/view_footer.php");
			                     
			                }
			                else
			                {	
			                	
			                       $data = array(

			                       	"panel_id"  => $_POST['panel_id'],
			                       	"role_id"  => $_POST['role_id'],
			                       	"username" => $_POST['username'],
			                       	"email" => $_POST['email'],
			                       	"password" => $this->encryption->encrypt($_POST['password']),
			                       	"mobile" => $_POST['mobile'],

			                       );
			               

			                      $user = $this->ModelAdmin->updateUser($data,$id);

			                       if($user)
			                       {
			                       		$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> User Updated Successfully.</div>');
			                       		redirect(base_url().'admin/userList');

			                       }else{

			                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
			                       		redirect(base_url().'admin/userList');
			                       }
			                }
						
					




					}


				}

	public function userDelete($id)
	{

		$user = $this->ModelAdmin->userDelete($id);
		if($user)
		{
				$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> User Deleted Successfully.</div>');
				redirect(base_url().'admin/userList');

		}else{

				$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
				redirect(base_url().'admin/userList');
		}
	}


	public function roleAdd()
	{
		$data['title']  = 'Add Role';

		$this->load->view("admin/view_header.php",$data);
		$this->load->view("admin/view_add_role.php");
		$this->load->view("admin/view_footer.php");

	}

		public function storeRole()
			{
				if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['roleAdd'])) {
		   			
		
		   			$this->form_validation->set_rules('role', 'Role Name', 'required|is_unique[mt_farhan_roles.role]');


		                if ($this->form_validation->run() == FALSE)
		                {
		                      
		                     		 $data['title']  = 'Add Role';

		                      		$this->load->view("admin/view_header.php",$data);
		                      		$this->load->view("admin/view_add_role.php");
		                      		$this->load->view("admin/view_footer.php");
		                     
		                }
		                else
		                {
		                       $data = array(

		                       	"role"  => $_POST['role'],
		                      

		                       );


		                       $role = $this->ModelAdmin->roleAdd($data);

		                       if($role)
		                       {
		                       		$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Role Added Successfully.</div>');
		                       		redirect(base_url().'admin/roleAdd');

		                       }else{

		                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
		                       		redirect(base_url().'admin/roleAdd');
		                       }
		                }
					
				




				}


			}


			public function roleList()
			{
				$data['title']  = 'Roles List';
				
				$data['roles']  = $this->ModelAdmin->getRoles();
				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_list_roles.php",$data);
				$this->load->view("admin/view_footer.php");

			}

			public function roleEdit($id='')
			{
				$data['title']  = 'Edit Role';
				
				$data['role']  = $this->ModelAdmin->getRoleById($id);

				$this->load->view("admin/view_header.php",$data);
				$this->load->view("admin/view_edit_role.php");
				$this->load->view("admin/view_footer.php");

			}


				public function updateRole($id='')
					{
						if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['roleEdit'])) {
				   			
				   			
				   			$this->form_validation->set_rules('role', 'Role', 'required');
			            
				                if ($this->form_validation->run() == FALSE)
				                {
				                      
				                       $data['title']  = 'Edit Role';
				                       				
				                       	$data['role']  = $this->ModelAdmin->getRoleById($id);

				                       	$this->load->view("admin/view_header.php",$data);
				                       	$this->load->view("admin/view_edit_role.php");
				                       	$this->load->view("admin/view_footer.php");
				                     
				                }
				                else
				                {	
				                	
				                       $data = array(

				                      
				                       	"role" => $_POST['role'],
				                  
				                       );
				               

				                      $role = $this->ModelAdmin->updateRole($data,$id);

				                       if($role)
				                       {
				                       		$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Role Updated Successfully.</div>');
				                       		redirect(base_url().'admin/roleList');

				                       }else{

				                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
				                       		redirect(base_url().'admin/roleList');
				                       }
				                }
							
						




						}


					}


					public function roleDelete($id)
					{

						$user = $this->ModelAdmin->roleDelete($id);
						if($user)
						{
								$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Role Deleted Successfully.</div>');
								redirect(base_url().'admin/roleList');

						}else{

								$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
								redirect(base_url().'admin/roleList');
						}
					}


					public function whitelistingIpsAdd()
					{

						$data['title']  = 'Whitelisting IPs';
						
						$data['whitelistingIps'] = $this->ModelAdmin->getWhitelistingIps();

						$this->load->view("admin/view_header.php",$data);
						$this->load->view("admin/view_whitelisting_ips.php",$data);
						$this->load->view("admin/view_footer.php");

					}


					public function logout()
					{
					    $user_data = $this->session->all_userdata();
					        foreach ($user_data as $key => $value) {
					            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
					                $this->session->unset_userdata($key);
					            }
					        }
					    $this->session->sess_destroy();
					    redirect('admin');
					}


					public function storeWhitelisitng()
						{
							if ($_SERVER['REQUEST_METHOD'] === 'POST') {
					   			
											if($_POST['ips'] !=  '')
											{

												$data = array(

													"ips"  => implode(",",$_POST['ips'])
												

												);
											}else{


												$data = array(

													"ips" => ""
												);
											}
					  	
					                      


					                       $role = $this->ModelAdmin->updateWhitelisting($data);

					                       if($role)
					                       {
					                       		echo '<div class="alert alert-success"><strong>Success!</strong> Whitelisting IPs Added Successfully.</div>';
					                       		

					                       }else{

					                       		echo '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>';
					                       }
					                }
								
							




							}

							public function panelAdd()
							{
								$data['title']  = 'Add Panel';

								$this->load->view("admin/view_header.php",$data);
								$this->load->view("admin/view_add_panel.php");
								$this->load->view("admin/view_footer.php");

							}
						
							public function storePanel()
								{
									if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['panelAdd'])) {
							   			
							
							   			$this->form_validation->set_rules('panel', 'Panel Name', 'required|is_unique[mt_farhan_panels.panel]');


							                if ($this->form_validation->run() == FALSE)
							                {
							                      
							                     		 $data['title']  = 'Add Panel';

							                      		$this->load->view("admin/view_header.php",$data);
							                      		$this->load->view("admin/view_add_panel.php");
							                      		$this->load->view("admin/view_footer.php");
							                     
							                }
							                else
							                {
							                       $data = array(

							                       	"panel"  => $_POST['panel'],
							                      

							                       );


							                       $panel = $this->ModelAdmin->panelAdd($data);

							                       if($panel)
							                       {
							                       		$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Panel Added Successfully.</div>');
							                       		redirect(base_url().'admin/panelAdd');

							                       }else{

							                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
							                       		redirect(base_url().'admin/panelAdd');
							                       }
							                }
										
									




									}


								}

								public function panelList()
								{
									$data['title']  = 'Panels List';
									
									$data['panels']  = $this->ModelAdmin->getPanels();
									$this->load->view("admin/view_header.php",$data);
									$this->load->view("admin/view_list_panels.php",$data);
									$this->load->view("admin/view_footer.php");

								}


							public function panelEdit($id='')
							{
								$data['title']  = 'Edit Panel';
								
								$data['panel']  = $this->ModelAdmin->getPanelById($id);

								$this->load->view("admin/view_header.php",$data);
								$this->load->view("admin/view_edit_panel.php");
								$this->load->view("admin/view_footer.php");

							}

							public function updatePanel($id='')
								{
									if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['panelEdit'])) {
							   			
							   			
							   			$this->form_validation->set_rules('panel', 'Panel', 'required');
							           
							                if ($this->form_validation->run() == FALSE)
							                {
							                      
							                     $data['title']  = 'Edit Panel';
												
												$data['panel']  = $this->ModelAdmin->getPanelById($id);

												$this->load->view("admin/view_header.php",$data);
												$this->load->view("admin/view_edit_panel.php");
												$this->load->view("admin/view_footer.php");

							                     
							                }
							                else
							                {	
							                	
							                       $data = array(

							                      
							                       	"panel" => $_POST['panel'],
							                  
							                       );
							               

							                      $panel = $this->ModelAdmin->updatePanel($data,$id);

							                       if($panel)
							                       {
							                       		$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Panel Updated Successfully.</div>');
							                       		redirect(base_url().'admin/panelList');

							                       }else{

							                       		$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
							                       		redirect(base_url().'admin/panelList');
							                       }
							                }
										
									




									}


								}


								public function panelDelete($id)
								{

									$panel = $this->ModelAdmin->panelDelete($id);
									if($panel)
									{
											$this->session->set_flashdata('credential', '<div class="alert alert-success"><strong>Success!</strong> Panel Deleted Successfully.</div>');
											redirect(base_url().'admin/panelList');

									}else{

											$this->session->set_flashdata('credential', '<div class="alert alert-danger"><strong>Failed!</strong> Something went wrong.</div>');
											redirect(base_url().'admin/panelList');
									}
								}

}

 ?>