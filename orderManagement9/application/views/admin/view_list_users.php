<div class="card-body">
           <?php echo $this->session->flashdata('credential');?>
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Panel</th>
                  <th>Role</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Mobile</th>
       
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                <?php foreach($users as $user) { ?>
                <tr>
                  <td><?php echo $user->panel; ?></td>
                  <td><?php echo $user->role; ?></td>
                  <td><?php echo $user->username; ?></td>
                  <td><?php echo $user->email; ?></td>
                  <td><?php echo $user->mobile; ?></td>
    
                  <td>
                    <a href="<?php echo base_url(); ?>admin/userEdit/<?php echo $user->id ?>" class="btn btn-primary btn-sm">Edit</a>
                    <a href="<?php echo base_url(); ?>admin/userDelete/<?php echo $user->id ?>" onclick="return confirm('Do you want to delete?');" class="btn btn-danger btn-sm">Delete</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>