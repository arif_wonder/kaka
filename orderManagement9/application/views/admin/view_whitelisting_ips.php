
    <div class="container-fluid">

      <div class="row">
        <div class="col-lg-6">
          <!-- Example Bar Chart Card-->
    <div class="card card-register">
      <div class="card-header">Add Whitelistin IPs</div>
      <div class="card-body">
        <div id="message"></div>
        <form method="post" id="whitelisting">

          <div class="form-group">
            <?php echo form_error('ip','<div class="text-danger">', '</div>'); ?>
            <input class="form-control" id="ip" type="text" value="<?php echo $whitelistingIps->ips; ?>" name="role" placeholder="Enter Whitelisting IP">
          </div>

          <button type="submit" name="whitelistingAdd" class="btn btn-primary btn-block">Add</a>
        </form>

      </div>
    </div>
   


      </div>
  
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
