<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

$date = date('Y-m-d');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Order Management System</title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/admin.ico">

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
		
		<!-- Common CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/design-1/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/design-1/fonts/icomoon/icomoon.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/design-1/css/main.css" />

		<!-- Data Tables -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bs4.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bs4-custom.css" />
<style type="text/css">
.brad0{border-radius:0 !important }
</style>
</head>
<body>
<header class="main-heading">
	<div class="container-fluid">
	<div class="row">
		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
			<div class="page-icon"><i class="icon-layers"></i></div>
			<div class="page-title"><h5>Online Speedy Kaka</h5><h6 class="sub-heading">Welecome to Agents Management</h6></div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="right-actions">
					<a href="<?=base_url()?>order"  class="btn btn-primary float-right mx-1" data-toggle="tooltip" data-placement="top" title="" data-original-title="HOme">
						<i class="icon-home"></i>
					</a>
					<a href="#addNewAgentModal" data-toggle="modal" onclick="emptyModal()" data-target="#addNewAgentModal" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add New Agent">
						<i class="icon-profile-male"></i>
					</a>
				</div>
			</div>
		</div>	
	</div>
</header>
<div class="main-content">
						
						<!-- Row start -->
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header">Available Agents </div>
									<div class="card-body">
										<table id="basicExample" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="1%">#</th>
													<th>Name</th>
													<th width="1%">Status</th>
													<th>Mobile</th>
													<th>Username</th>
													<th>Password</th>
													<!--<th>Latitude</th>-->
													<!--<th>Longitude</th>-->
													
													<th>All Order</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>

												<?php $i =0; foreach ($agents as $agent): $i++;?>
												<?php $agentName = $agent['name']; ?>
													<tr>
														<td><?=$i?></td>
														<td><?=$agent['name']?></td>
														<td>
															<?php 
															if ($agent['status'] == 'true') echo "Active";else echo "Disable";
															?>
														</td>
														<td><?=$agent['mobile']?></td>
														<td><?=$agent['username']?></td>
														<td><?=$agent['password']?></td>
													
														
														<td>
															<button class="btn btn-sm btn-outline-primary" onclick="showOrders('<?=$agentName?>')" >
																<?=$CI->ModelOrder->getAgentOrderAll($agent['name']);?>
															</button>
															
														</td>
														<td style="padding: 2px;text-align: center;" width="15%">
														<button data-toggle="tooltip" data-placement="top" title="" data-original-title="Show History"  onclick="showHistory(<?=$agent['id']?>)" class="btn btn-outline-success btn-sm">
																<span class="icon-briefcase4" ></span>
															</button>
															<button class="btn btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show on map" onclick="showMap(<?=$agent['lati']?>,<?=$agent['longi']?>)">
																<span class="icon-location2" ></span>
															</button>
															<button data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Record"  onclick="showProfile(<?=$agent['id']?>)" class="btn btn-outline-success btn-sm">
																<span class="icon-enlarge" ></span>
															</button>
															<button class="btn btn-outline-danger btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Record" onclick="delAgent(<?=$agent['id']?>)">
																<span class="icon-trash2" ></span>
															</button>
															
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div id="showOrders"></div>
</div>

<footer class="main-footer fixed-btm">
				Copyright Online Speedy Kaka 2018.
</footer>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpiLVtEkNjAx6CaNiHvoIPirUf1moyNAk&callback=initMap">
 </script> 
<script type="text/javascript">

	function showHistory(agent) {
		$("#showHistoryModal").modal();
		$("#showHistoryModalBody").html("<h2 class='text-center'><span class='icon-spinner3'></span></h2>");
		$.post("<?php echo base_url(); ?>order/showAgentHistory",{agent}, 
				function(data){

					$("#showHistoryModalBody").html(data);
								 	
					//$('#myModal').modal('hide');

			});
	}
	function showOrders(agent) {
		$("#showOrdersModal").modal();
		$("#showOrdersModalBody").html("<h2 class='text-center'><span class='icon-spinner3'></span></h2>");
		$.post("<?php echo base_url(); ?>order/showAgentOrders",{agent}, 
				function(data){

					$("#showOrdersModalBody").html(data);
								 	
					//$('#myModal').modal('hide');

			});
	}
	function delAgent(agent_id)
	{
		if (confirm("Sure to delete?")) 
		{
			$.post("<?php echo base_url(); ?>order/delAgent",{agent_id}, 
				function(data){
					window.location.href=window.location.pathname;
			});
		}
	}
	function showProfile(agent_id)
		{
// 			$(".modal-title").html('Edit Agent Profile');
			$("#exampleModal2").modal();
			$("#showAgent").html('<center><h4>Loading...</h4></center>');
							
			$.post("<?php echo base_url(); ?>order/getAgentsData",{agent_id}, 
				function(data){

					$("#showAgent").html(data);
								 	
					//$('#myModal').modal('hide');

			});
		}
	function showMap(lat,long)
	{ 
		$("#map").modal();
			$(".modal-title").html('Agent Location');

		initMap(lat,long);  
	}
  function initMap(lati,longi) {
        var uluru = {lat: lati, lng: longi};
        var map = new google.maps.Map(document.getElementById('map2'), {
          zoom: 17,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
  
</script>
<!-- modals area -->
<div class="modal fade " id="showOrdersModal" tabindex="-1" role="dialog" aria-labelledby="map12" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content brad0">
			<div class="modal-header brad0">
				<h5 class="modal-title" id="map12">Agent Orders</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
				<div class="modal-body" id="showOrdersModalBody">
					
				</div>					
		</div>
	</div>
</div>
<div class="modal fade " id="showHistoryModal" tabindex="-1" role="dialog" aria-labelledby="map12" aria-hidden="true" >
	<div class="modal-dialog modal-lg" style="min-width: 1100px" role="document">
		<div class="modal-content brad0"  >
			<div class="modal-header brad0">
				<h5 class="modal-title" id="map12">Agent History</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
				<div class="modal-body" id="showHistoryModalBody">
					
				</div>					
		</div>
	</div>
</div>
<div class="modal fade" id="map" tabindex="-1" role="dialog" aria-labelledby="map12" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content brad0">
			<div class="modal-header brad0">
				<h5 class="modal-title" id="map12">Agent Location</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
				<div class="modal-body" id="map2" style="height: 444px">
					
				</div>					
		</div>
	</div>
</div>


<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content brad0">
			<div class="modal-header brad0">
				<h5 class="modal-title" id="exampleModalLabel2">Edit Agent Profile</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
				<div class="modal-body" id="showAgent">
					
				</div>
				
		</div>
	</div>
</div>

<div class="modal fade" id="addNewAgentModal" tabindex="-1" role="dialog" aria-labelledby="addNewAgentModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content brad0">
			<div class="modal-header brad0">
				<h5 class="modal-title" id="addNewAgentModalLabel">Add New Agent</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
			</div>
				<div class="modal-body" >
					
		<div class="form-group row">
			<div class="col-md-12">
			
				<label class="col-form-label" >Name &nbsp;<span id="error" style="color: red;font-size: 8pt"></span> </label>
				<input type="text" class="form-control input-sm" id="name" >
			</div>
				
							
		</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Username</label>
								<input type="text" id="username" class="input-sm form-control"></div>
							<div class="col-md-6">
								<label class="col-form-label">Password</label>
								<input type="password"  id="password" class="input-sm form-control">
							</div>
						</div>	
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Latitude</label>
								<input type="text"  id="lati" class="input-sm form-control"></div>
							<div class="col-md-6">
								<label class="col-form-label">Longitude</label>
								<input type="text"  id="longi" class="input-sm form-control">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Mobile</label>
								<input type="number"  id="mobile" class="input-sm form-control">
							</div>
							<div class="col-md-6">
			<label class="col-form-label">Status</label>
				<select id="status" class="form-control input-sm">
						<option value="true"  >Active</option>
						<option value="false"  >Disable</option>
				</select>
			</div>
						</div>
							<button type="button" class="btn btn-block btn-outline-success btn-sm mx-auto myBtn" onclick="updateProfile2()">Add Agent</button>	
														
	
				</div>
				
		</div>
	</div>
</div>

<script type="text/javascript">
	function emptyModal()
	{
		$(".modal-title").html("Add New Agent");
		$("#name,#username,#password,#lati,#longi").val("");
	}
	function updateProfile2()
	{
	
		var name = $("#name").val().trim();
		var username= $("#username").val().trim();
		var password= $("#password").val().trim();
		var status = $("#status").val();
		var lati = $("#lati").val().trim();
		var longi = $("#longi").val().trim();
		var mobile = $("#mobile").val();
		 if( name.length == 0) {$("#error").html("Name field is required");}
		 else{
		 	
		 	$.post("<?php echo base_url(); ?>order/addNewAgent",{name,username,password,status,lati,longi,mobile}, 
			function(data){$('#myModal').modal('hide');window.location.href=window.location.pathname; });
		 }
		
		
	}
</script>

<!-- end modal area -->

		<script src="<?php echo base_url(); ?>assets/design-1/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/js/tether.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/unifyMenu/unifyMenu.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/onoffcanvas/onoffcanvas.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/js/moment.js"></script>

		<!-- Data Tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bootstrap.min.js"></script>
		
		<!-- Custom Data tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/custom/custom-datatables.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/custom/fixedHeader.js"></script>

		<!-- Common JS -->
		<script src="<?php echo base_url(); ?>assets/design-1/js/common.js"></script>

</body>
</html>