	<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');
	
?>
							<div id="success"></div>

						

							<?php if($val == 'availabel_agents') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<?php if ($id) {
										?>
										<button onclick="removeAgent(<?=$id?>)" class='btn btn-sm btn-danger btn-block'>
											Remove : <?php echo $agentName; ?>
											</button>	
										<?php
									} ?>
										
										<br>
									

									<div class="form-group">
										<?php 
											$avail_agentsArray  = array();
											$avail_agent = $this->ModelOrder->getAvailableAgents();
											foreach ($avail_agent as $agentss) {
												$avail_agentsArray[] = $agentss['name'];
											}
											
										 ?>
										<?php $agents = $this->ModelOrder->getAgents(); ?>
									<select class="form-control" id="agentID">
										<option selected disabled value="0"> Add New</option>
										<?php 
											foreach ($agents as $agent): 	
										?>
										<?php if(!in_array($agent['name'], $avail_agentsArray)) { ?>
										<option value="<?=$agent['id']?>" ><?php echo $agent['name']; ?></option>
	
										<?php } endforeach ?>
									</select>
									<!-- <input type="text" id="agentName" class="form-control" value="<?php echo trim($agentName); ?>"> -->
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success" onclick="addNew()">Add New</button>
									</div>
									</div>
									</div>
							<?php } ?>

							
							<script type="text/javascript">
							$(".modal input, .modal textarea").focus();

			
							function removeAgent(id) {
								 $.post("<?php echo base_url(); ?>order/removeAgent",{id}, 
								 	function(data){
							  		$('#myModal').modal('hide'); 


							    });
							}
							function addNew() {
								var id = $("#agentID").val();
								 $.post("<?php echo base_url(); ?>order/addAgent",{id}, 
								 	function(data){
							  		$('#myModal').modal('hide'); 
							    });
							}
							function updateDeliveryAgent(id)
							{
								 var agentName = $("#agentName").val();

							
								 $.post("<?php echo base_url(); ?>order/updateDeliveryAgent",{id:id,agentName:agentName}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 

							    });
							}

							
							</script>