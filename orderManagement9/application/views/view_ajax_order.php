	<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

$sequence_number =sizeof($getOrders);
$i = 1;

?>
<input type="hidden" value="<?php echo $sequence_number ?>" id='newOrder'>
				  
					<?php foreach($getOrders as $getOrder) { ?>
					<tr data-time="<?php echo $getOrder['reminder']; ?>" data-currenttime="<?php echo date("g.i");  ?>" data-status="<?php echo $getOrder['status'] ?>" data-rowid="allocateColor<?php echo $getOrder['order_id']; ?>" id="allocateColor<?php echo $getOrder['order_id']; ?>" style="<?php echo allocateColor($getOrder['status']); ?>" <?php if(allocateColor($getOrder['status'])){ $this->session->set_userdata("order_id",$getOrder['order_id']); } ?>>
						<?php 
							$cellClr = "";
							if ($getOrder['workerType'] == 'A')
							{
								$cellClr = "background:yellow;";
							}
							elseif ($getOrder['workerType'] == 'B') {
								$cellClr = "background:blue;";
							}
							else
							{
								if ($getOrder['order_id'] % 2 == 0)
									$cellClr = "background:yellow;";
								else
								$cellClr = "background:blue;";
									
							}

								

						 ?>
						<td style="<?php echo $cellClr ?>"><?php echo $sequence_number--; ?></td>
						<td><?php echo $getOrder['order_id']; ?></td>
						<td><?php echo strtoupper($getOrder['restaurant_name']); ?></td>
						<td><?php echo strtoupper($getOrder['first_name']." ".$getOrder['last_name']); ?></td>
						<td><?php echo $getOrder['contact_phone'];?></td>
						<td>
							
							<button type="button" onclick="viewOrderAddress(<?php echo $getOrder['order_id']; ?>)" id="viewOrderAddress" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xs">view</button>
							 <!-- ADDED BY FARHAN 29/04/2018 -->
							<?php 
								if(isset($getOrder['email_address'])){

								$email = explode("@",$getOrder['email_address']);
								if(count($email) > 1){
									if($email[1] == 'www.speedypixelgame.com'){
								 ?>

								<button style="margin-top: 4px;" type="button" onclick="guestEmail(<?php echo $getOrder['order_id']; ?>)" id="viewOrderAddress" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-xs">email</button>
								<?php 
										} 
								}
								$sent_email = $getOrder['sent_email'];
								if($sent_email == 1){ ?>

									<button style="margin-top: 4px;" type="button" onclick="guestEmail(<?php echo $getOrder['order_id']; ?>)" id="viewOrderAddress" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs">E-sent</button>

								<?php }
								}

								?>
 							<!-- END BY FARHAN 29/04/2018 -->
						</td>
						 
						<td><?php echo strtoupper($getOrder['payment_type']); ?></td>
				
						<td contenteditable="true" id="reminder<?php echo $getOrder['order_id']; ?>" onclick="reminder(<?php echo $getOrder['order_id']; ?>)"><?php echo $getOrder['reminder']; ?></td>
						<td contenteditable="true" id="purchase_value<?php echo $getOrder['order_id']; ?>" onclick="purchaseValue(<?php echo $getOrder['order_id']; ?>)"><?php echo round($getOrder['purchase_value']); ?></td>
						<td contenteditable="true" id="total_w_tax<?php echo $getOrder['order_id']; ?>" onclick="total_w_tax(<?php echo $getOrder['order_id']; ?>)" ><?php echo round($getOrder['total_w_tax']); ?></td>
						
						<td contenteditable="true" id="delivery_charge<?php echo $getOrder['order_id']; ?>" onclick="delivery_charge(<?php echo $getOrder['order_id']; ?>)" ><?php echo round($getOrder['delivery_charge']); ?></td>
						
						<td>
							<?php $checkPaymentReference  = $CI->ModelOrder->checkPaymentReference($getOrder['order_id']); ?>

							<p id="status<?php echo $getOrder['order_id'];?>"><?php echo isset($checkPaymentReference) ?  $getOrder['status'].'(Paid)' : $getOrder['status']; ?></p>
					
						<button type="button" class="btn btn-xs btn-primary" onclick="editStatus('<?php echo $getOrder['order_id']; ?>','<?php echo $getOrder['status']; ?>')" data-toggle="modal" data-target="#myModal">edit</button>
						</td>
						
						<td id='workerType<?php echo $getOrder['order_id']; ?>'>
							<?php 
							if (empty($getOrder['workerType']))
							{
								if ($getOrder['order_id'] % 2 == 0)
								{
							?>
								<button class='btn btn-xs btn-primary' onclick="assignWork('<?php echo $getOrder['order_id'] ?>','B')">To B</button>
							<?php
								}
								else
								{
							?>
								<button class='btn btn-xs btn-primary' onclick="assignWork('<?php echo $getOrder['order_id'] ?>','A')">To A</button>
							<?php
								}
							?>
							
							
							<?php 
							}
							elseif ($getOrder['workerType'] == 'A')
							{
							?>
							<button class='btn btn-xs btn-primary' onclick="assignWork('<?php echo $getOrder['order_id'] ?>','B')">To B</button>

							<?php  
							}elseif ($getOrder['workerType'] == 'B') {
							?>
							<button class='btn btn-xs btn-primary' onclick="assignWork('<?php echo $getOrder['order_id'] ?>','A')">To A</button>

							<?php 
							} 
							
							?>
							
							

						</td>
						<td contenteditable="true" id="delivery_agent<?php echo $getOrder['order_id']; ?>" onclick="deliveryAgent(<?php echo $getOrder['order_id']; ?>)"><?php echo strtoupper($getOrder['delivery_agent']); ?></td>

						<td contenteditable="true" id="locality<?php echo $getOrder['order_id']; ?>" onclick="locality(<?php echo $getOrder['order_id']; ?>)"><?php echo strtoupper($getOrder['locality']); ?></td>

						<?php if(date('g:i') != $getOrder['reminder']) { ?>
<!-- 
						<td contenteditable="true" id="reminder<?php echo $getOrder['order_id']; ?>" onblur="reminder(<?php echo $getOrder['order_id']; ?>)"><?php echo $getOrder['reminder']; ?></td> -->
						<?php } ?>
						<?php 
							$time = strtotime($getOrder['date_created']);
							$myFormatedTime = date("g:i", $time);
						 ?>
						<td><?php echo $myFormatedTime; ?></td>
						<td contenteditable="true" id="delivery_time2<?php echo $getOrder['order_id']; ?>" onclick="deliveryTime2(<?php echo $getOrder['order_id']; ?>)"><?php echo !empty($getOrder['delivery_time']) ? $getOrder['delivery_time'] : $getOrder['delivery_time2']; ?></td>

						<td contenteditable="true" id="comment<?php echo $getOrder['order_id']; ?>" onclick="comment(<?php echo $getOrder['order_id']; ?>)">
							<?php echo "<p style='width: 100px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'>".strtoupper($getOrder['comment'])."<p>"; ?>
						</td>
						<td><?php echo $getOrder['app_status']; ?></td>
						<td>
							<button type="button" class="btn btn-primary btn-xs" onclick="sendSms(<?php echo $getOrder['order_id']; ?>)">sms</button>
						</td>
					   <?php $i++; ?>
					</tr>
					<?php } ?>
