	<?php 
$CI =& get_instance();
$CI->load->model('ModelOrder');

// echo "<pre>";
// print_r($orderById);
// die();


?>
<div id="success"></div>

<?php if ($type == "showAgentHistory"): ?>
	
			<!-- Data Tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bootstrap.min.js"></script>					<!-- Custom Data tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/custom/custom-datatables2.js"></script>
			<table id="basicExample3" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="1%">#</th>
													<th>Merchant_name</th>
													<th>Items</th>
													<th>Total</th>
													<th>Contact</th>
													<th width="1%">order_id</th>
													<th>Name</th>
													<th>Location</th>
													<th width="1%">Status</th>
													<th>Create Date</th>
													
												</tr>
											</thead>
											<tbody>

												<?php $i =0; foreach ($data as $row): $i++;?>
												
													<tr>
														<td><?=$i?></td>
														<td><?=$row['merchant_name']?></td>
														<td><?=$row['items']?></td>
														<td><?=$row['total']?></td>
														<td><?=$row['contact']?></td>
														<td><?=$row['order_id']?></td>
														<td><?=$row['name']?></td>
														<td><?=$row['location']?></td>
														<td><?=$row['status']?></td>
														<td><?=$row['create_date']?></td>
														
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
<?php endif ?>
<?php if ($type == "showAgentOrders"): ?>

		
			<!-- Data Tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/dataTables.bootstrap.min.js"></script>					<!-- Custom Data tables -->
		<script src="<?php echo base_url(); ?>assets/design-1/vendor/datatables/custom/custom-datatables2.js"></script>
		

		
								
										<table id="basicExample2" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="1%">#</th>
													<th>Order ID</th>
													<th>total_w_tax</th>
													<th>Status</th>
													<th>Delivery time</th>
													<th>Delivery date</th>
													
												</tr>
											</thead>
											<tbody>

												<?php $i =0; foreach ($data as $row): $i++;?>
												
													<tr>
														<td><?=$i?></td>
														<td><?=$row['order_id']?></td>
														<td><?=$row['total_w_tax']?></td>
														<td><?=$row['status']?></td>
														<td><?=$row['delivery_time2']?></td>
														<td><?=$row['delivery_date']?></td>
														
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
					
<?php endif ?>
<?php if ($type == "showProfile"): ?>
<!-- <pre>
	<?php print_r($agent_data) ;?>
</pre> -->

	<form>
		<div class="form-group row">
			<div class="col-md-12">
				<input type="hidden" id="agent_id" value="<?=$agent_data['id']?>">
				<label class="col-form-label">Name</label>
				<input type="text" class="form-control input-sm" id="name" value="<?=$agent_data['name']?>">
			</div>
				
							
		</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Username</label>
								<input type="text" id="username" value="<?=$agent_data['username']?>" class="input-sm form-control"></div>
							<div class="col-md-6">
								<label class="col-form-label">Password</label>
								<input type="password" value="<?=$agent_data['password']?>" id="password" class="input-sm form-control">
							</div>
						</div>	
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Latitude</label>
								<input type="text" value="<?=$agent_data['lati']?>" id="lati" class="input-sm form-control"></div>
							<div class="col-md-6">
								<label class="col-form-label">Longitude</label>
								<input type="text" value="<?=$agent_data['longi']?>" id="longi" class="input-sm form-control">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="col-form-label">Mobile</label>
								<input type="number"  value="<?=$agent_data['mobile']?>" id="mobile" class="input-sm form-control">
							</div>
							<div class="col-md-6">
			<label class="col-form-label">Status</label>
				<select id="status" class="form-control input-sm">
						<option value="true" <?php if($agent_data['status'] == 'true') echo " selected" ?> >Active</option>
						<option value="false" <?php if($agent_data['status'] == 'false') echo "selected" ?> >Disable</option>
				</select>
			</div>
						</div>
							<button type="button" class="btn btn-block btn-outline-success btn-sm mx-auto myBtn" onclick="updateProfile()">Update Profile</button>	
														
	</form>
<?php endif ?>
<script type="text/javascript">
	function updateProfile()
	{
		var agent_id = $("#agent_id").val();
		var name = $("#name").val().trim();
		var username= $("#username").val().trim();
		var password= $("#password").val().trim();
		var status = $("#status").val();
		var lati = $("#lati").val().trim();
		var longi = $("#longi").val().trim();
		var mobile = $("#mobile").val().trim();

		$.post("<?php echo base_url(); ?>order/updateAgentProfile",{agent_id,name,username,password,status,lati,longi,mobile}, 
			function(data){$('#myModal').modal('hide');window.location.href=window.location.pathname; });
		
	}
</script>
							<?php if($type == 'sms') { ?>
							<div class="row">
								<div class="col-md-8">
						         	 <div class="form-group text-center">
						         	<textarea rows="15" cols="15" class="form-control" id="smsText"><?php 
										$merchant_name = $orderById->restaurant_name;
										echo trim($merchant_name)."\r\n"; 
										
						         		$totalOrders = $orders2 = explode(",",$orderById->decode_item);
						         		foreach($totalOrders as $totalOrder)
						         		{
						         			echo trim($totalOrder).","."\r\n";
						         		}

						         		echo "\r\n"."Order Id - $order_id"."\r\n".$orderById->first_name." ".$orderById->last_name."\r\n";
						         		$name = $orderById->first_name." ".$orderById->last_name;

						        		$location =  trim($CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->street." ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->location_name)."\r\n"."\n";
						        		echo $location;
						         		$contact = $CI->ModelOrder->getClientById($orderById->client_id)[0]['contact_phone']."\r\n";

						         		echo str_replace("+91","",$contact);
						         		$total = $orderById->total_w_tax;
						         		echo "Total - Rs. ".round($total);

						         		$orders2 = implode(',', $orders2);

						         	?></textarea>	
						<input type="hidden" id="merchant_name" value="<?=$merchant_name?>">
						<input type="hidden" id="items" value="<?=$orders2?>">
						<input type="hidden" id="total" value="<?=$total?>">
						<input type="hidden" id="contact" value="<?=$contact?>">
						<input type="hidden" id="order_id" value="<?=$order_id?>">
						<input type="hidden" id="name" value="<?=$name?>">
						<input type="hidden" id="location" value="<?=$location?>">
						<input type="hidden" id="payment_type" value="<?=$orderById->payment_type?>">
						
						         	</div>
						         	</div>
						         	<div class="col-md-4">
							         	<div class="form-group" style="display:none">
											<select id="mobile" class="form-control" name="mobile">
											<?php 
											if ($worker_flag == "A") 
												$getDeliveryBoys = $this->ModelOrder->getAvailableAgentsAA();
											elseif($worker_flag == "B") 
												$getDeliveryBoys = $this->ModelOrder->getAvailableAgentsBB();
											else 
												$getDeliveryBoys = $this->ModelOrder->getAvailableAgents();

											?>

												<?php foreach(sort($getDeliveryBoys) as $getDeliveryBoy) { ?>
												<option value="<?php echo $getDeliveryBoy['mobile']; ?>"><?php echo $getDeliveryBoy['name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group text-center" style="display:none">
											<button type="button" class="btn btn-primary" onclick="sendSmsApi(<?php echo $order_id; ?>)" id="sendButton">Send</button>
										</div>
									<?php
									 $app_status = $this->ModelOrder->getStatus2($order_id); 
									 if($app_status['app_status'] != 'seen')
									 {
									 	?>
									 	<?php if ($app_status['app_status'] == 'unseen') 
									 		$app_status_flag = "old";
									 		else
									 		$app_status_flag = "new";	
									 	 ?>
									 	 <input type="hidden" id="app_status_flag" value="<?=$app_status_flag;?>">
									 	<div class="form-group">
											<select id="agent" class="form-control" name="agent">
											<?php 
												// $getDeliveryBoys = $CI->ModelOrder->getAvailableAgents(); ?>

												<?php foreach($getDeliveryBoys as $getDeliveryBoy) { ?>
												<option value="<?php echo $getDeliveryBoy['agent_id']; ?>"><?php echo $getDeliveryBoy['name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group text-center">
											<button type="button" class="btn btn-primary" onclick="sentToApp()" id="sendButton">Send to App</button>
										</div>
									 	<?php
									 }
									 else
									 {
									     ?><div class='alert alert-sm alert-success'>Order received and seen</div>
									     <button type="button" class="btn btn-danger" onclick="unAssign()">Re-Assign</button>
									     <?php
									 }
									?>
										
									</div>
								
						
							</div>
							<?php } ?>



							<?php if($type == 'comment') { ?>
								
							  	<div class="row">
									<div class="col-md-6 col-md-offset-3">	
										<div class="form-group">
										<textarea id="comment" class="form-control" ><?php echo trim($comment); ?></textarea>
										<!-- <input type="text" id="comment" class="form-control" value="<?php echo $comment; ?>"> -->
									</div>
									</div>
								
								
									<div class="col-md-6 col-md-offset-3">	
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="commentUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
								</div>
							
							<?php } ?>

							<?php if($type == 'delivery_time2') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="delivery_time2" class="form-control" value="<?php echo trim($delivery_time2); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryTimeUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'reminder') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<input type="text" id="reminder" class="form-control" value="<?php echo trim($reminder); ?>">
									</div>
									<div class="form-group text-center">
										<button type="button" class="btn btn-success myBtn" onclick="reminderUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'total_w_tax') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="total_w_tax" class="form-control" value="<?php echo trim($total_w_tax); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="totalWTaxUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>
							
							<?php if($type == 'delivery_charge') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="delivery_charge" class="form-control" value="<?php echo trim($delivery_charge); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryChargeUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'delivery_agent') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<select id="delivery_agent" class="form-control" name="delivery_agent">
							<?php 


		$password = $this->session->userdata('password');

		
		if ($password == "admin123") {
			$agents = $CI->ModelOrder->getAvailableAgents();
		}
		elseif ($password == "worker12pass") {
			
			$agents = $CI->ModelOrder->getAvailableAgentsA();
		}elseif ($password == "workerbpass") {
			$agents = $CI->ModelOrder->getAvailableAgentsB();
		}
							?>
							


									<?php foreach($agents as $agent) { ?>
												<option value="<?php echo $agent['name']; ?>" <?php if($agent['name'] == $delivery_agent){ echo 'selected';} ?>><?php echo $agent['name']; ?></option>
									<?php } ?>
									</select>
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="deliveryAgentUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'locality') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<select id="locality" class="form-control" name="locality">
									<?php $localities = $CI->ModelOrder->getLocality(); ?>	

									<option></option>
									<?php foreach($localities as $locality) { ?>
												<option value="<?php echo $locality['locality_name']; ?>" <?php if($locality['locality_name'] == $selected_locality){ echo 'selected';} ?>><?php echo $locality['locality_name']; ?></option>
									<?php } ?>
									</select>
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="localityUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

							<?php if($type == 'purchase_value') { ?>
									<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
									<input type="text" id="purchase_value" class="form-control" value="<?php echo trim($purchase_value); ?>">
									</div>
									<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="purchaseValueUpdate(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
									</div>
							<?php } ?>

<!-- ADDED BY FARHAN -->
<style type="text/css">
	#itemTable {  
    border-collapse: collapse;
}
#itemTable th, #itemTable td {
    padding: 3px;
}
.highlight{

	background: #00ff00;
}
</style>
<!-- END BY FARHAN -->

							<?php if($type == 'item') { ?>
							<div class="row">
								<div class="col-md-12">
									<!-- ADDED BY FARHAN -->
									<table id="itemTable" class="table">
										<tr>
											<th>Name:</th>
											<td><?php echo $CI->ModelOrder->getClientById($orderById->client_id)[0]['first_name']." ".$CI->ModelOrder->getClientById($orderById->client_id)[0]['last_name']; ?></td>
										</tr>
										<tr>
											<th>Merchant Name:</th>
											<td><?php echo $orderById->restaurant_name; ?></td>
										</tr>
										<tr>
											<?php 
												$merchantAddress = $CI->ModelOrder->getMerchantById2($orderById->merchant_id);
											 ?>
											<th>Merchant Address:</th>
											<td><?php echo $merchantAddress->street." ".$merchantAddress->city." ".$merchantAddress->state." ".$merchantAddress->post_code; ?></td>
										</tr>
										<tr>
											<th>Search Address:</th>
											<td><?php echo $orderById->search_address; ?></td>
										</tr>
										<tr class="highlight">
											<th>Deliver to:</th>
											<td><?php echo '<span contenteditable="true" onblur="updateClientStreet('.$orderById->order_id.')" id="clientStreet'.$orderById->order_id.'">'.$orderById->street.'</span> '.$orderById->city.' '.$orderById->state.' '.$orderById->post_code; ?></td>
										</tr>
										<tr class="highlight">
											<th>Landmark:</th>
											<td><?php echo $orderById->location_name; ?></td>
										</tr>
										<tr class="highlight">
											<th>Contact Number:</th>
											<td><?php echo $orderById->contact_phone; ?></td>
										</tr>
										<tr class="highlight">
											<th>Delivery Instruction:</th>
											<td><?php echo $orderById->delivery_instruction; ?></td>
										</tr>
										<tr>
											<th>Payment Type:</th>
											<td><?php echo $orderById->payment_type; ?></td>
										</tr>
										<tr>
											<th>TRN Date:</th>
											<td><?php echo $orderById->delivery_date." ".$orderById->delivery_time; ?></td>
										</tr>
										<tr>
											<th>Delivery Date:</th>
											<td><?php echo $orderById->delivery_date; ?></td>
										</tr>
										<?php 
											$items = json_decode($orderById->json_details);
											foreach($items as $item) {
										?>
										<tr>
											<th width="50%"><?php echo $item->qty." ".$CI->ModelOrder->getItemById($item->item_id); ?></th>
											<td><?php echo "₹ ".$item->price*$item->qty; ?></td>
										</tr>
										<?php } ?>
										<tr>
											<th>Sub Total:</th>
											<td><?php echo "₹ ".round($orderById->sub_total,2); ?></td>
										</tr>
										<tr>
											<th>Delivery Fee:</th>
											<td><?php echo "₹ ".round($orderById->delivery_charge,2); ?></td>
										</tr>
										<tr>
											<th>Tax <?php echo ($orderById->tax*100)."%" ?>:</th>
											<td><?php echo "₹ ".round($orderById->taxable_total,2); ?></td>
										</tr>
										<tr>
											<th>Total:</th>
											<td><?php echo "₹ ".round($orderById->total_w_tax,2); ?></td>
										</tr>
									</table>
									<!-- END BY FARHAN -->
								</div>
					<!-- 			<div class="col-md-8">
						         	 <div class="form-group text-center">
							         	<textarea rows="15" class="form-control" id="decode_item"><?php 
							         		$totalOrders = explode(",",$orderById->decode_item);
							         		foreach($totalOrders as $totalOrder)
							         		{
							         			echo trim($totalOrder).","."\r\n";
							         		}
							         	?></textarea>
						         	</div>
						         	<div class="form-group text-center">
									<button type="button" class="btn btn-success myBtn" onclick="editOrderItem(<?php echo $order_id; ?>)">Submit</button>
									</div>
								</div>
								<div class="col-md-4">
									<?php 
											
											echo "<p><b>Address:</b></p> ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->street."</br></br><b> Landmark: </b> ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->location_name."</br></br> ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->city." ".$CI->ModelOrder->getClientAddressById($order_id,$orderById->client_id)->state."</p><b>Delivery Instruction:</b><p>".$orderById->delivery_instruction."</p>";


									?>
								</div> -->
							</div>
							<?php } ?>

							<?php if($type == 'status') { ?>
							<div class="row">
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<input type="hidden" id="delivery_agent_color" value="<?php echo trim($orderById->delivery_agent) ?>">
										<select id="status" class="form-control" name="status">
											<option value="Pending" <?php if($status == 'Pending'){ echo 'selected';} ?>>Pending</option>
											<option value="Confirmed" <?php if($status == 'Confirmed'){ echo 'selected';} ?>>Confirmed</option>
											<option value="Processing" <?php if($status == 'Processing'){ echo 'selected';} ?>>Processing</option>
											<option value="Delivered" <?php if($status == 'Delivered'){ echo 'selected';} ?>>Delivered</option>
											<option value="Cancelled" <?php if($status == 'Cancelled'){ echo 'selected';} ?>>Cancelled</option>
											<option value="On Hold" <?php if($status == 'On Hold'){ echo 'selected';} ?>>On Hold</option>
										</select>
									</div>
									</div>
									
									
									<div class="col-md-6 col-md-offset-3">
									<div class="form-group">
										<button type="button" class="btn btn-success" onclick="orderStatus(<?php echo $order_id; ?>)">Submit</button>
									</div>
									</div>
							</div>
							<?php } ?>

							<!-- ADDED BY FARHAN 29/04/2018 -->
							<?php if ($type == "guestCheckout"): ?>
				
								<form>
									
									<div class="form-group">
										<input type="hidden" id="client_id<?php echo $order_id?>" value="<?php echo $orderById->client_id; ?>">
										<label>Subject</label>
										<input type="text" value="Thank you for your order!"  id="guestSubject<?php echo $order_id?>" class="form-control" required="">
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="email"   id="guestEmail<?php echo $order_id?>" class="form-control" placeholder="Enter Email" required="">
									</div>
									<div class="form-group">
										<label>Message</label>
										<textarea required="" id="guestMessage<?php echo $order_id?>" class="guestMessage"><div style="text-align: center;"><img src="https://www.onlinekaka.com/upload/1476254450-1475737169-kaka-logo-(1).png" width="200"></div><p>Dear, <?php echo $orderById->first_name." ".$orderById->last_name; ?></p><p> Thank you for placing your order at OnlineKaka. We hope you enjoy your food! Your order number is <?php echo $order_id ?>. We have included your order receipt and delivery details below:</p><p><b>Order Details</b></p><table style="text-align: left">
										<tr>
											<th>Customer Name:</th>
											<td><?php echo $CI->ModelOrder->getClientById($orderById->client_id)[0]['first_name']." ".$CI->ModelOrder->getClientById($orderById->client_id)[0]['last_name']; ?></td>
										</tr>
										<tr>
											<th>Merchant Name:</th>
											<td><?php echo $orderById->restaurant_name; ?></td>
										</tr>
										<tr>
											<th>Telephone:</th>
											<td><?php echo $orderById->restaurant_phone; ?></td>
										</tr>
										<tr>
											<?php 
												$merchantAddress = $CI->ModelOrder->getMerchantById2($orderById->merchant_id);
											 ?>
											<th>Merchant Address:</th>
											<td><?php echo $merchantAddress->street." ".$merchantAddress->city." ".$merchantAddress->state." ".$merchantAddress->post_code; ?></td>
										</tr>
										<tr>
											<th>TRN Type:</th>
											<td><?php echo $orderById->trans_type; ?></td>
										</tr>
										<tr>
											<th>Payment Type:</th>
											<td><?php echo $orderById->payment_type; ?></td>
										</tr>
										<tr>
											<th>Reference#:</th>
											<td><?php echo $orderById->order_id; ?></td>
										</tr>
										<tr>
											<th>TRN Date:</th>
											<td><?php echo $orderById->delivery_date." ".$orderById->delivery_time; ?></td>
										</tr>
										<tr>
											<th>Delivery Date:</th>
											<td><?php echo $orderById->delivery_date; ?></td>
										</tr>
										<tr>
											<th>Deliver to:</th>
											<td><?php echo '<span contenteditable="true" onblur="updateClientStreet('.$orderById->order_id.')" id="clientStreet'.$orderById->order_id.'">'.$orderById->street.'</span> '.$orderById->city.' '.$orderById->state.' '.$orderById->post_code; ?></td>
										</tr>
										<tr>
											<th>Landmark:</th>
											<td><?php echo $orderById->location_name; ?></td>
										</tr>
										<tr>
											<th>Delivery Instruction:</th>
											<td><?php echo $orderById->delivery_instruction; ?></td>
										</tr>
										<tr>
											<th>Contact Number:</th>
											<td><?php echo $orderById->contact_phone; ?></td>
										</tr>
										
										<?php 
											$items = json_decode($orderById->json_details);
											foreach($items as $item) {
										?>
										<tr>
											<th width="50%"><?php echo $item->qty." ".$CI->ModelOrder->getItemById($item->item_id); ?></th>
											<td><?php echo "₹ ".$item->price*$item->qty; ?></td>
										</tr>
										<?php } ?>
										<tr>
											<th>Sub Total:</th>
											<td><?php echo "₹ ".round($orderById->sub_total,2); ?></td>
										</tr>
										<tr>
											<th>Delivery Fee:</th>
											<td><?php echo "₹ ".round($orderById->delivery_charge,2); ?></td>
										</tr>
										<tr>
											<th>Tax <?php echo ($orderById->tax*100)."%" ?>:</th>
											<td><?php echo "₹ ".round($orderById->taxable_total,2); ?></td>
										</tr>
										<tr>
											<th>Total:</th>
											<td><?php echo "₹ ".round($orderById->total_w_tax,2); ?></td>
										</tr>
									</table><p> Best Regards<br>Onlinekaka</p></textarea>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-success" onclick="sendGuestEmail('<?php echo $order_id; ?>')">Send</button>
									</div>													
								</form>
							<?php endif ?>
							<!-- END BY FARHAN 29/04/2018 -->


							<script type="text/javascript">
						$(".modal input, .modal textarea").focus();

							function orderStatus(order_id)
							{
							    
								 var status = $("#status").val();
                                 
								 var agentName = $("#delivery_agent_color").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxStatusUpdate",{order_id:order_id,status:status,agentName:agentName}, 
								 	function(data){

								 		
								 		//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		  	$('#myModal').modal('hide');
							  		  	var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}
						function unAssign(){
						    
        						var  order_id 	= $("#order_id").val();
        						
        					

								 $.post("<?php echo base_url(); ?>order/unAssign",{order_id}, 
								 	function(data){ 
                                    $("#msg").html("<h2>Successfully undo</h2>");
							  		$('#myModal').modal('hide');  
							    });
							
						}
							function sentToApp() 
							{
								
						var  merchant_name= $("#merchant_name").val();
						var  items		= $("#items").val();
						var  total 		= $("#total").val();
						var  contact 	= $("#contact").val();
						var  order_id 	= $("#order_id").val();
						var  name 		= $("#name").val();
						var  location 	= $("#location").val();
						var  agent_id 	= $("#agent").val();
						var payment_type = $("#payment_type").val();
						var app_status_flag = $("#app_status_flag").val();

								 $.post("<?php echo base_url(); ?>order/sendToApp",{merchant_name,items,total,contact,order_id,name,location,agent_id,app_status_flag,payment_type}, 
								 	function(data){ 
                                    //alert(data);
							  		$('#myModal').modal('hide');  
							    });
							}
							function purchaseValueUpdate(order_id)
							{
								 var purchase_value = $("#purchase_value").val();

								// alert(purchase_value);

							
								 $.post("<?php echo base_url(); ?>order/ajaxPurchaseValueUpdate",{order_id:order_id,purchase_value:purchase_value}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');  
 
							    });
							}

							function deliveryAgentUpdate(order_id)
							{
								 var delivery_agent = $("#delivery_agent").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryAgentUpdate",{order_id:order_id,delivery_agent:delivery_agent}, 
								 	function(data){

								 	if(data == 'false')
								 	{
								 		alert('Agent Name is not Available in the list');
								 	}
								 	else
								 	{

								 		$('#myModal').modal('hide');
								 		var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);  
								 	}

								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		

							    });
							}

							function localityUpdate(order_id)
							{
								 var locality = $("#locality").val();

								//alert(delivery_agent);
								 $.post("<?php echo base_url(); ?>order/ajaxLocalityUpdate",{order_id:order_id,locality:locality}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');  
							  		var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}

							function totalWTaxUpdate(order_id)
							{
								 var total_w_tax = $("#total_w_tax").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxTotalWTaxValueUpdate",{order_id:order_id,total_w_tax:total_w_tax}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 
							  		var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}
							
								function deliveryChargeUpdate(order_id)
							{
								 var delivery_charge = $("#delivery_charge").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryChargeUpdate",{order_id:order_id,delivery_charge:delivery_charge}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide'); 
							  		var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}

							function deliveryTimeUpdate(order_id)
							{
								 var delivery_time2 = $("#delivery_time2").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxDeliveryTime2Update",{order_id:order_id,delivery_time2:delivery_time2}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		 $('#myModal').modal('hide');
							  		 var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}

							function reminderUpdate(order_id)
							{
								 var reminder = $("#reminder").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxReminderUpdate",{order_id:order_id,reminder:reminder}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		 $('#myModal').modal('hide');
							  		 var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}

							function commentUpdate(order_id)
							{
								 var comment = $("#comment").val();

							
								 $.post("<?php echo base_url(); ?>order/ajaxCommentUpdate",{order_id:order_id,comment:comment}, 
								 	function(data){


								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');
							  		var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
							}

							function editOrderItem(order_id)
							{
								 var decode_item = $("#decode_item").val().trim();
							
								 $.post("<?php echo base_url(); ?>order/ajaxDecodeItemUpdate",{order_id:order_id,decode_item:decode_item}, 
								 	function(data){

								 	
								 	//$("#success").html('<div class="alert alert alert-success">Updated</div>');
							  		$('#myModal').modal('hide');
							  		var from = $("#from").val();var to2 = $("#to").val();
					var order_id ;
					refresh(order_id,from,to2);

							    });
								
							} 

							function sendSmsApi(order_id)
							{
								var smsText = $("#smsText").val().trim();
								var mobile = $("#mobile").val().trim();

								var agentName = $("#mobile option:selected").text();
								
								 $.post("<?php echo base_url(); ?>order/smsApi",{order_id:order_id,smsText:smsText,mobile:mobile,agentName:agentName}, 
								 	function(data){

								 	
								 	if(data == 'false')
								 	{
								 		alert('Agent Name is not Available in the list');
								 	}
								 	else
								 	{

								 		$('#myModal').modal('hide'); 
								 		var from = $("#from").val();var to2 = $("#to").val();
										var order_id ;
										refresh(order_id,from,to2); 
								 	}
								 
							  		
 
							    });
							}

							$("#comment,#delivery_time2,#delivery_charge,#total_w_tax,#locality,#delivery_agent,#purchase_value").keyup(function(event){
							    if(event.keyCode == 13){
							        $(".myBtn").click();
							    }
							});

							var selectized = $('#locality,#delivery_agent').selectize({

								create: true,
								sortField: 'text'

							});
							//selectized[0].selectize.focus();

							
							 $('#reminder').timepicker({ 'timeFormat': 'g.i' });

							 // ADDED BY FARHAN	

							 function updateClientStreet(order_id)
							 {
							 	var clientStreet = $("#clientStreet"+order_id).text();

							 	
								 $.post("<?php echo base_url(); ?>order/updateClientStreet",{order_id:order_id,clientStreet:clientStreet}, 
								 	function(data){

								 		// $('#myModal').modal('hide');

								 	});

							 }
								// ADDED BY FARHAN 29/04/2018
								function sendGuestEmail(order_id)
								{

								 var client_id = $("#client_id"+order_id).val();
								 var guestEmail = $("#guestEmail"+order_id).val();
								 if(guestEmail == ''){

								 	alert("Please Enter Email");

								 }else{

									 var guestSubject = $("#guestSubject"+order_id).val();
									 var guestMessage = $("#guestMessage"+order_id).val();
									
									 $.post("<?php echo base_url(); ?>order/sendGuestEmail",{order_id:order_id,guestEmail:guestEmail,guestSubject:guestSubject,guestMessage:guestMessage,client_id:client_id}, 
									 	function(data){
									 		$('#myModal').modal('hide');

									 		desktopNotification("Email Sent Successfully.");

									 	});
								 	
								 }
								}
								

							 	

				
						
							 	$('.guestMessage').richText({
									  // text formatting
									  bold: true,
									  italic: true,
									  underline: true,

									  // text alignment
									  leftAlign: true,
									  centerAlign: true,
									  rightAlign: true,

									  // lists
									  ol: false,
									  ul: false,

									  // title
									  heading: false,

									  // fonts
									  fonts: false,
									  fontList: ["Arial", 
									    "Arial Black", 
									    "Comic Sans MS", 
									    "Courier New", 
									    "Geneva", 
									    "Georgia", 
									    "Helvetica", 
									    "Impact", 
									    "Lucida Console", 
									    "Tahoma", 
									    "Times New Roman",
									    "Verdana"
									    ],
									  fontColor: false,
									  fontSize: true,

									  // uploads
									  imageUpload: false,
									  fileUpload: false,

									  // media
									  Embed: false,

									  // link
									  urls: false,

									  // tables
									  table: false,

									  // code
									  removeStyles: false,
									  code: false,

						

									  // dev settings
									  useSingleQuotes: false,
									  height: 0,
									  heightPercentage: 100,
									  id: "",
									  class: "",
									  useParagraph: false
									  
									});
							 	// END BY FARHAN 29/04/2018
							 // END BY FARHAN
							</script>