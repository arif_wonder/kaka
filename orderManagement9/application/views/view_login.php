<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #c31f25;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #ef3b3a;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}
.container .info h1 {
  margin: 0 0 15px;
  padding: 0;
  font-size: 36px;
  font-weight: 300;
  color: #1a1a1a;
}
.container .info span {
  color: #4d4d4d;
  font-size: 12px;
}
.container .info span a {
  color: #000000;
  text-decoration: none;
}
.container .info span .fa {
  color: #EF3B3A;
}
body {
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #ef3b3a, #ef3b3a);
  background: -moz-linear-gradient(right, #ef3b3a, #ef3b3a);
  background: -o-linear-gradient(right, #ef3b3a, #ef3b3a);
  background: linear-gradient(to left, #cc0100, #4c4c4c);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
#logo{margin-top: -20px;margin-left: -2px;margin-bottom: 25px;}
	</style>

</head>
<body>
	<div class="login-page">
  <center><div id="logo" >
  <img src="https://www.onlinekaka.com/upload/1476254450-1475737169-kaka-logo-(1).png" width="200">
  </div></center>
  <div class="form" >
  <?php
  if($this->session->flashdata('failure')){ ?>
   <div style="background-color:#ef3b3a; color: #fff; padding: 10px;font-size: 15px; "><?php echo $this->session->flashdata('failure'); ?></div><br>
  <?php }  ?>
    <form class="login-form" method="post" name="login" action="<?php echo base_url(); ?>login/login_save">
      <?php echo form_error('username','<div style="color:red;">', '</div>'); ?>
      <input type="text" placeholder="username" name="username" />
      <?php echo form_error('password','<div style="color:red;">', '</div>'); ?>
      <input type="password" placeholder="password" name="password" />
      <button type="submit" name="login">Login</button>
   
    </form>
  </div>
</div>
</body>
</html>