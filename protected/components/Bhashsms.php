<?php
class Bhashsms
{
	public $host='http://103.255.100.34/api/send_transactional_sms.php?';	
	public $user;
	public $password;	
	public $is_curl=false;
	public $to;
	public $message;		
	public $sender='';
	public $sms_type=0;
	public $priority='dnd';
	public $msg_token = 'JAM7a9';
	
	public function sendSMS()
	{	
		if (empty($this->user)){
			throw new Exception(t("User is empty"));
		}
		
		if (empty($this->password)){
			throw new Exception(t("Password is empty"));
		}
		
		if (empty($this->sender)){
			throw new Exception(t("Sender is empty"));
		}
		
		if (empty($this->to)){
			throw new Exception(t("To is required"));
		}
		
		if (empty($this->message)){
			throw new Exception(t("Message is required"));
		}

		$params="username={user}&msg_token={msg_token}&sender_id={sender}&mobile={phone}&message={text}";		
				
		$params=$this->smarty('user',$this->user,$params);
		$params=$this->smarty('msg_token',$this->msg_token,$params);
		$params=$this->smarty('sender',urlencode($this->sender),$params);
		$params=$this->smarty('phone',urlencode($this->to),$params);		
		$params=$this->smarty('text',urlencode($this->message),$params);		
		$params=$this->smarty('priority',$this->priority,$params);		
		$params=$this->smarty(0,$this->sms_type,$params);		
				
		//echo $this->host;
		//echo $params;
		//die();
						
		if ($this->is_curl==FALSE){						
			$resp=file_get_contents($this->host.$params);															
			$resp=trim($resp);			
			if(!empty($resp)){				
				if (strlen($resp)<=11){			
					return $resp;
				} else throw new Exception($resp);
			} else {
				throw new Exception(t("empty response from api"));
			}
		} else {			
			$resp=$this->Curl($this->host,$params);			
			$resp=trim($resp);
			if(!empty($resp)){				
				if (strlen($resp)<=11){			
					return $resp;
				} else throw new Exception($resp);
			} else {
				throw new Exception(t("empty response from api"));
			}
		}
	}			
	
	public function smarty($search='',$value='',$subject='')
    {	
	   return str_replace("{".$search."}",$value,$subject);
    }
    
    public function Curl($uri="",$post="")
	{
		 $error_no='';
		 $ch = curl_init($uri);
		 curl_setopt($ch, CURLOPT_POST, 1);		 
		 curl_setopt($ch, CURLOPT_POSTFIELDS, $post);		 
		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 $resutl=curl_exec ($ch);		
		 		 		 		 
		 if ($error_no==0) {
		 	//return $resutl;
		 	return 'Message Sent';
		 } else return false;			 
		 curl_close ($ch);		 				 		 		 		 		 		
	}
	
} /*END CLASS*/