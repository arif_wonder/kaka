
<div class="container enter-address-wrap">

<div class="section-label">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Enter your address below")?></span>
      <b></b>
    </a>     
</div>  

<form id="frm-modal-enter-address" class="frm-modal-enter-address" method="POST" onsubmit="return false;" >
<?php echo CHtml::hiddenField('action','setAddress');?> 
<?php echo CHtml::hiddenField('web_session_id',
isset($this->data['web_session_id'])?$this->data['web_session_id']:''
);?>

<div class="row">
  <div class="col-md-12 ">
    <?php echo CHtml::textField('client_address',
	 isset($_SESSION['kr_search_address'])?$_SESSION['kr_search_address']:''
	 ,array(
	 'class'=>"grey-inputs",
	 'data-validation'=>"required"
	 ))?>

  </div> 
</div> <!--row-->

<div class="row food-item-actions top10">
  <div class="col-md-9 "></div>
  <div class="col-md-3 ">
     <input type="submit" class="green-button inline" value="<?php echo t("Submit")?>" id="frm-enter-address-button">
  </div>
</div>
 </form>

</div> <!--container-->

<script type="text/javascript">

// var IsplaceChange = false;  // ADDED BY FARHAN 27 MAY 2018
//   // ADDED BY FARHAN 27 MAY 2018

//   var input = document.getElementById("client_address");
//   var options = {
//    types: ['geocode'],
//    componentRestrictions: {country: "in"}
//   };
//   var autocomplete = new google.maps.places.Autocomplete(input,options);

//   google.maps.event.addListener(autocomplete, 'place_changed', function () {
//       var place = autocomplete.getPlace();

//       IsplaceChange = true;
//   });

//   $("#client_address").keydown(function () {
//       IsplaceChange = false;
//   });

//   $("#frm-modal-enter-address").submit(function () {

//       var search = $("#client_address").val();
//       if(search == '')
//         {
//                 swal("Address field is required");
//                 return false;
//         }

//       var address = search.split(", ");
//       var lastEl = address[address.length-1];
//       if(lastEl == 'India')
//       {   
//           IsplaceChange = true;
//       }
//       if (IsplaceChange == false) {
//           $("#client_address").val('');
//           swal("Please select your location from dropdown list");
//           return false;
//       }
    

//   });

  // END BY FARHAN 27 MAY 2018

$.validate({ 	
	language : jsLanguageValidator,
	language : jsLanguageValidator,
    form : '#frm-modal-enter-address',   
    onError : function() {      
    },
    onSuccess : function() {     
      form_submit('frm-modal-enter-address');
      return false;
    }  
});


jQuery(document).ready(function() {
	var google_auto_address= $("#google_auto_address").val();	
	if ( google_auto_address =="yes") {		
	} else {
		$("#client_address").geocomplete({
		    country: $("#admin_country_set").val()
		});	
	}
  
  // ADDED BY FARHAN 6 AUG 2018

//   $("#client_address").focus(function(){
//     $("#client_address").val('');
//   });

//   $("#client_address").blur(function(){

//     var s = $("#hidden_client_address").val();
//     $("#client_address").val(s);

// });

  // END BY FARHAN
});


</script>
<?php
die();