<?php 

if(isset($_POST['agent_busy_button'])) {

    $params = array(

        "farhan_busy_status" => trim($_POST['farhan_busy_status']),
        "farhan_busy_text" => trim($_POST['farhan_busy_text'])
    );

   updateAgentBusyStatus($params); 
    

$myBaseUrl = Yii::app()->request->baseUrl;

header('Location: '.$myBaseUrl.'/admin/AgentsBusy');

}
?>

<section id="container" >

    <!--main content start-->
    <section id="main-content" style="margin-left: 0px;">
        <section class="wrapper">

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">

                <?php 
                    
                    $notAllowed = FunctionsV3::get_agents_busy(); 

                 ?>
                <form class="uk-form uk-form-horizontal" id="agent_busy_form" action="" method="post">
                        <ul class="uk-switcher uk-margin " id="tab-content">

                        <li class="uk-active">
                                <fieldset>
                                    
                                  <div class="uk-form-row">
                                    <label><h3>Change Status and Text</h3></label>
                                    <select class="uk-form-width-large" required="" name="farhan_busy_status">
                                        <option value="1" <?php if($notAllowed['farhan_busy_status'] == 1){ echo "selected"; } ?>>Available</option>
                                        <option value="0" <?php if($notAllowed['farhan_busy_status'] == 0){ echo "selected"; } ?>>Busy</option>
                                    </select>
                                    
                                </div>
                                </fieldset>
                                 <fieldset>
                                    
                                  <div class="uk-form-row">
                                    <textarea class="uk-form-width-large" rows="6" name="farhan_busy_text" required=""><?php echo $notAllowed['farhan_busy_text']; ?></textarea>
                                    
                                </div>
                                </fieldset>
                        </li>
                    </ul>
                    <button type="submit" class="uk-button" name="agent_busy_button">Submit</button>
                </form>

            </div>

        </div>






    </section>

</section><!-- /MAIN CONTENT -->

<!--main content end-->

</section>
