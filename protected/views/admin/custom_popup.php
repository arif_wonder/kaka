<?php 

if(isset($_POST['farhan_popup'])) {

    $params = array(

        "status" => trim($_POST['farhan_status']),
        "title" => trim($_POST['farhan_title']),
        "description" => trim($_POST['farhan_description']),
        "time_period" => trim($_POST['farhan_time_period']),
        "button_text" => trim($_POST['farhan_button_text'])
        
    );

   updatePopUp($params); 
    

    $myBaseUrl = Yii::app()->request->baseUrl;

header('Location: '.$myBaseUrl.'/admin/popUp');


}
?>
<style type="text/css">
    .jqte{
        margin: 0px;
    }
    .jqte_editor img {
    float: none;
}
</style>
<section id="container" >

    <!--main content start-->
    <section id="main-content" style="margin-left: 0px;">
        <section class="wrapper">

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">

                <?php 
                    
                    $getPopUp = getPopUp();

                 ?>
                <form class="uk-form uk-form-horizontal" id="allowed_address_confirm_form" action="" method="post">
                        <ul class="uk-switcher uk-margin " id="tab-content">

                        <li class="uk-active">
                                <fieldset>
                                    
                                <div class="uk-form-row">
                                    <label><b>Status</b></label><br>

                                    <select class="uk-form-width-large" required="" name="farhan_status">
                                        <option value="1" <?php if($getPopUp['status'] == 1){ echo "selected"; } ?>>Enabled</option>
                                        <option value="0" <?php if($getPopUp['status'] == 0){ echo "selected"; } ?>>Disabled</option>
                                    </select>
                                    
                                </div>
                                <div class="uk-form-row">
                                    <label><b>Title</b></label><br>
                                    <input class="uk-form-width-large" type="text" required="" name="farhan_title" value="<?php echo $getPopUp['title']; ?>" placeholder="Enter Title" />        
                                </div>
                                <div class="uk-form-row">
                                    <label><b>Description</b></label><br>
                                    <textarea class="big-textarea" rows="6" name="farhan_description" required=""><?php echo $getPopUp['description']; ?></textarea>
                                </div>
                                <div class="uk-form-row">
                                    <label><b>Time Period</b></label><br>

                                    <select class="uk-form-width-large" required="" name="farhan_time_period">
                                        <option value="0" <?php if($getPopUp['time_period'] == 0){ echo "selected"; } ?>>Half an Hour</option>
                                        <option value="1" <?php if($getPopUp['time_period'] == 1){ echo "selected"; } ?>>One Hour</option>
                                        <option value="2" <?php if($getPopUp['time_period'] == 2){ echo "selected"; } ?>>1 Day</option>
                                        <option value="3" <?php if($getPopUp['time_period'] == 3){ echo "selected"; } ?>>3 Days</option>
                                        <option value="4" <?php if($getPopUp['time_period'] == 5){ echo "selected"; } ?>>7 Days</option>
                                        
                                    </select>
                                    
                                </div>
                                <div class="uk-form-row">
                                    <label><b>Button Text</b></label><br>
                                    <input class="uk-form-width-large" type="text" required="" name="farhan_button_text" value="<?php echo $getPopUp['button_text']; ?>" placeholder="Enter Button Text" />        
                                </div>

                                </fieldset>
                        </li>
                    </ul>
                    <button type="submit" class="uk-button" name="farhan_popup">Submit</button>
                </form>

            </div>

        </div>






    </section>

</section><!-- /MAIN CONTENT -->

<!--main content end-->

</section>
