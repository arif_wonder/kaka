<?php 
if(isset($_POST['not_allowed_search_button'])) {


    $address = ucwords(trim($_POST['not_allowed_address']));

    insertNotAllowedAddress($address); 
    

    $myBaseUrl = Yii::app()->request->baseUrl;

header('Location: '.$myBaseUrl.'/admin/NotAllowedAddress');


} 


if(isset($_POST['allowed_address_confirm_button'])) {

    $params = array(

        "farhan_busy_status" => trim($_POST['farhan_busy_status']),
        "farhan_busy_text" => trim($_POST['farhan_busy_text'])
    );

   updateAllowedAddressStatus($params); 
    

    $myBaseUrl = Yii::app()->request->baseUrl;

header('Location: '.$myBaseUrl.'/admin/NotAllowedAddress');


}
?>

<section id="container" >

    <!--main content start-->
    <section id="main-content" style="margin-left: 0px;">
        <section class="wrapper">

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">

                    <form class="uk-form uk-form-horizontal" id="not_allowed_address_form" action="" method="post">

                        <ul class="uk-switcher uk-margin " id="tab-content">
                        <li class="uk-active">
                            <fieldset>

                                  <div class="uk-form-row">
                                    <label><h3>Add Not Allowed Address</h3></label>
                                    <input class="uk-form-width-large" type="text" required="" name="not_allowed_address" id="not_allowed_search_address" placeholder="Enter not allowed address" />        
                                </div>
                            </fieldset>
                        </li>
                    </ul>
                    <button type="submit" class="uk-button" name="not_allowed_search_button">Submit</button>
                </form>
                <?php 
                    
                    $notAllowed = FunctionsV3::get_not_allowed_status(); 

                 ?>
                <form class="uk-form uk-form-horizontal" id="allowed_address_confirm_form" action="" method="post">
                        <ul class="uk-switcher uk-margin " id="tab-content">

                        <li class="uk-active">
                                <fieldset>
                                    
                                  <div class="uk-form-row">
                                    <label><h3>Change Status and Text</h3></label>
                                    <select class="uk-form-width-large" required="" name="farhan_busy_status">
                                        <option value="1" <?php if($notAllowed['farhan_busy_status'] == 1){ echo "selected"; } ?>>Enabled</option>
                                        <option value="0" <?php if($notAllowed['farhan_busy_status'] == 0){ echo "selected"; } ?>>Disabled</option>
                                    </select>
                                    
                                </div>
                                </fieldset>
                                 <fieldset>
                                    
                                  <div class="uk-form-row">
                                    <textarea class="uk-form-width-large" rows="6" name="farhan_busy_text" required=""><?php echo $notAllowed['farhan_busy_text']; ?></textarea>
                                    
                                </div>
                                </fieldset>
                        </li>
                    </ul>
                    <button type="submit" class="uk-button" name="allowed_address_confirm_button">Submit</button>
                </form>
                <h3>List Of Not Allowed Addresses</h3>
                <table id="" class="uk-table uk-table-hover uk-table-striped uk-table-condensed">  
               <thead>
                <tr>
                    <th>SR No.</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
               </thead>
    
            <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php $notAllowedAddresses = FunctionsV3::get_not_allowed_address(); 
                    foreach($notAllowedAddresses as $key => $notAllowedAddress){ ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $notAllowedAddress['not_allowed_address']; ?></td>
                    <td><a href="<?php echo  Yii::app()->request->baseUrl; ?>/admin/NotAllowedAddressDelete?id=<?php echo $notAllowedAddress['id']; ?>" onclick="return confirm('Are you sure you want to delete this?');">Delete</a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
            </div>

        </div>






    </section>

</section><!-- /MAIN CONTENT -->

<!--main content end-->

</section>
