<?php
$enabled=Yii::app()->functions->getOptionAdmin('admin_payu_bolt_enabled');
?>

<div id="error-message-wrapper"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','adminBoltPayUMoney')?>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Enabled Bolt by PayUMoney")?>?</label>
  <?php 
  echo CHtml::checkBox('admin_payu_bolt_enabled',
  $enabled=="yes"?true:false
  ,array(
    'value'=>"yes",
    'class'=>"icheck"
  ))
  ?> 
</div>


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Merchant Key")?></label>
  <?php 
  echo CHtml::textField('admin_payu_bolt_merchant_key',
  Yii::app()->functions->getOptionAdmin('admin_payu_bolt_merchant_key')
  ,array(
    'class'=>"uk-form-width-large"
  ))
  ?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","SALT")?></label>
  <?php 
  echo CHtml::textField('admin_payu_bolt_merchant_salt',
  Yii::app()->functions->getOptionAdmin('admin_payu_bolt_merchant_salt')
  ,array(
    'class'=>"uk-form-width-large"
  ))
  ?>
</div>



<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>