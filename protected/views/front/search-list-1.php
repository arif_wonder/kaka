<div id="search-listview" class="col-md-6 border infinite-item <?php echo $delivery_fee!=true?'free-wrap':'non-free'; ?>">

    <div class="inner">
        <?php if ( $val['is_sponsored']==2){?>
           <!-- CHANGES BY FARHAN 3 OCT 2018 -->
           <?php if(!empty($val['sponsored_text'])) { ?>
        <div class="ribbon"><span style="background:<?php if(strtolower($val['sponsored_text']) == 'trending'){ echo '#5B3256'; } ?>"><?php echo $val['sponsored_text']; ?></span></div>

          <?php }else{ ?>

           <div class="ribbon"><span>Sponsored</span></div>

         <?php } ?>


        <?php }?>

        
        <?php if ($offer=FunctionsV3::getOffersByMerchant($merchant_id)):?>
        <div class="ribbon-offer"><span><?php echo $offer;?></span></div>
        <?php endif;?>

        <?php 
            $myArea = explode(", ",$_GET['s']);
            $notAllowedAddress = FunctionsV3::get_not_allowed_address();
            $myArea2 = array_column($notAllowedAddress, 'not_allowed_address');
          
            $commonResult =array_intersect($myArea,$myArea2);
              
            $get_not_allowed_status = FunctionsV3::get_not_allowed_status();
         ?>
        <?php  if(FunctionsV3::get_agents_busy()['farhan_busy_status'] == 1 && ($get_not_allowed_status['farhan_busy_status'] == 0 || sizeof($commonResult) == 0)) {?>

        <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>" >
        <img class="logo-medium"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
        </a>
        <?php }else if(sizeof($commonResult) > 0 && $get_not_allowed_status['farhan_busy_status'] == 1){ 
          $farhan_busy_text = $get_not_allowed_status['farhan_busy_text'];
        
        ?>

          <a href="javascript:void(0)" onclick="swal('<?php echo $farhan_busy_text; ?>')" >
        <img class="logo-medium"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
        </a>
        <?php }else{    ?>

         <a href="javascript:void(0)" onclick="swal('<?php echo FunctionsV3::get_agents_busy()['farhan_busy_text']; ?>')">
        <img class="logo-medium"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
        </a>

        <?php } ?>
        <!-- ADDED BY FARHAN 8 SEPT 2018 -->
        <?php
        $offerText = Yii::app()->functions->getOption("farhan_offer_text",$merchant_id);
        if($offerText == ''){
        	$myOffer1 ='';
        	$myoffer2 ='';
        }
        else
        {

        	$myOffer1 = "style='margin-bottom:7px;'";
        	$myOffer2 = "style='margin-top:9px;'";
        }
        ?>
        <!-- END BY FARHAN -->
        <h2 class="concat-text" <?php echo $myOffer1; ?>><?php echo clearString($val['restaurant_name'])?></h2>
        <p class="merchant-address concat-text"><?php echo $val['merchant_address']?></p>
         <span class="label label-primary"><?php echo $offerText; ?></span> <!-- ADDED BY FARHAN -->
        <div class="mytable" <?php echo $myOffer2; ?>>
          <div class="mycol a" style="vertical-align: middle;">
             <div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>   
             <p><?php //echo $ratings['votes']." ".t("Reviews")?></p> <!-- Change by farhan 8 aug 2018 -->
          </div>
          <div class="mycol b">
             <?php echo FunctionsV3::prettyPrice($val['minimum_order'])?>
             <p><?php echo t("Minimum Order")?></p>
          </div>
        </div> <!--mytable-->

        <div class="top25"></div>

        <?php echo FunctionsV3::merchantOpenTag($merchant_id)?>
        <br>
        
         <!--ADDED BY BILAL --> 
      <?php  $checkout=FunctionsV3::isMerchantcanCheckout($merchant_id); ?>
        <?php if ( $checkout['holiday']==1):?>
                 <?php echo CHtml::hiddenField('is_holiday',$checkout['msg'],array('class'=>'is_holiday'));?>
                 <p class="text-danger"><?php echo $checkout['msg']?></p>
              <?php else :?>
                 <p class="text-danger"><?php echo $checkout['msg']?></p>
                 
              <?php endif;?>
              
        <!--END ADDED BY BILAL -->
        <p style="min-height: 10px;">
        <?php echo FunctionsV3::getFreeDeliveryTag($merchant_id)?>
        <?php if($merchant_id == 85){ ?>                        
        <span class="label label-default">Free Delivery On Orders Over ₹ 99</span>
        <?php } ?>
        <p class="top15 cuisine concat-text">
        <?php echo FunctionsV3::displayCuisine($val['cuisine']);?>
        </p>                
         <p>
         
            <?php 


        
        $myArea2 = array("Alambagh","Kanpur Road","Krishna Nagar","IIM Road","Shaheed Path","Ashiyana","Krishna Nagar","Aashiana");
        
        $commonResult =array_intersect($myArea,$myArea2);

         if($distance > $merchant_delivery_distance || sizeof($commonResult) > 0)
        {
            echo "<span class='label label-danger'>THIS RESTAURANT DOES NOT DELIVER IN YOUR AREA</span>";
        }

        ?>

        </p>                         
       
       
        <!-- EDIT BY BILAL -->
        <?php 
       // if($distance > $merchant_delivery_distance)
       // {
       //     echo "<span class='label label-danger'>THIS RESTAURANT DOES NOT DELIVER IN YOUR AREA</span>";
       // }
       //echo Functions3::getShippingTime();
        ?>

     <!-- COMMENT OUT BY BILAL                            
        <p>
        <?php 
        if ($distance){
          echo t("Distance").": ".number_format($distance,1)." $distance_type";
        } else echo  t("Distance").": ".t("not available");
        ?>
        </p>
      -->  
        <?php if($val['service']!=3):?>
          <!-- CHANGES BY FARHAN 9 AUG 2018 -->
        <p>
          <?php
            if ($distance){
             $distance_time = FunctionsV3::getShippingTime($merchant_id,$distance);
             if(empty($distance_time)){
                  echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id);
                }
            else{
                echo t("Delivery Est")?>: <?php echo $distance_time;
              }
            }else{

              echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id);

            }

          ?>  
            
        </p>
        <!-- END BY FARHAN -->
        <?php endif;?>
       
        <!-- EDIT BY FARHAN -->
        <!-- <p> -->
        <?php 
        // if($val['service']!=3){
        //   if (!empty($merchant_delivery_distance)){
        //    echo t("Delivery Distance").": ".$distance."<br>".$merchant_delivery_distance." $distance_type_orig";
        //   } else echo  t("Delivery Distance").": ".t("not available");
        // }
        ?>
        <!-- </p> -->
        <!-- END BY FARHAN                       -->
     
  <!--   COMMENT OUT BY BILAL  <p>
        <?php 
        if($val['service']!=3){
          if ($delivery_fee){
               echo t("Delivery Fee").": ".FunctionsV3::prettyPrice($delivery_fee);
          } else echo  t("Delivery Fee").": ".t("Free Delivery");
        }
        ?>
    </p> -->
        <!-- EDIT BY FARHAN -->
        <?php //echo FunctionsV3::displayServicesList($val['service'])?>  
        <br>        
         <!-- ADDED BY FARHAN -->
         <?php 
          $notAllowedAddress = FunctionsV3::get_not_allowed_address();
            $myArea2 = array_column($notAllowedAddress, 'not_allowed_address');
          
            $commonResult =array_intersect($myArea,$myArea2);
              
            $get_not_allowed_status = FunctionsV3::get_not_allowed_status();
          ?>
         <?php  if(FunctionsV3::get_agents_busy()['farhan_busy_status'] == 1 && ($get_not_allowed_status['farhan_busy_status'] == 0 || sizeof($commonResult) == 0)) {?>
        <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>" 
        class="orange-button rounded3 medium">
        <?php echo t("Order Now")?>
        </a>
        <?php }else if(sizeof($commonResult) > 0 && $get_not_allowed_status['farhan_busy_status'] == 1){ 
          $farhan_busy_text = $get_not_allowed_status['farhan_busy_text'];
          ?>

         <a href="javascript:void(0)" onclick="swal('<?php echo $farhan_busy_text; ?>')"
        class="orange-button rounded3 medium">
        <?php echo t("Order Now")?>
        </a>


        <?php }else{ ?>

           <a href="javascript:void(0)" onclick="swal('<?php echo FunctionsV3::get_agents_busy()['farhan_busy_text']; ?>')"
        class="orange-button rounded3 medium">
        <?php echo t("Order Now")?>
        </a>

        <?php } ?>
       
       <!-- END BY FARHAN  -->
        
        
    </div> <!--inner-->       
 </div> <!--col--> 