
<div id="search-listgrid" class="infinite-item <?php echo $delivery_fee!=true?'free-wrap':'non-free'; ?>">
    <div class="inner list-view">
    
		<?php if ( $val['is_sponsored']==2){?>
           <!-- CHANGES BY FARHAN 3 OCT 2018 -->
         <?php if(!empty($val['sponsored_text'])) { ?>
        <div class="ribbon"><span style="background:<?php if(strtolower($val['sponsored_text']) == 'Trending'){ echo '#5B3256'; } ?>"><?php echo $val['sponsored_text']; ?></span></div>

          <?php }else{ ?>

           <div class="ribbon"><span>Sponsored</span></div>

         <?php } ?>


        <?php }?>
    
    <?php if ($offer=FunctionsV3::getOffersByMerchant($merchant_id)):?>
    <div class="ribbon-offer"><span><?php echo $offer;?></span></div>
    <?php endif;?>
    <?php 
            $myArea = explode(", ",$_GET['s']);
            $notAllowedAddress = FunctionsV3::get_not_allowed_address();
            $myArea2 = array_column($notAllowedAddress, 'not_allowed_address');
          
            $commonResult =array_intersect($myArea,$myArea2);
              
            $get_not_allowed_status = FunctionsV3::get_not_allowed_status();
         ?>

    <div class="row">
	    <div class="col-md-2 border ">
	   	     <!-- ADD FARHAN CODE -->
	     <?php  if(FunctionsV3::get_agents_busy()['farhan_busy_status'] == 1 && ($get_not_allowed_status['farhan_busy_status'] == 0 || sizeof($commonResult) == 0)) {?>

	     <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>">
	     	<img class="logo-small"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
	     </a>
	     <?php }else if(sizeof($commonResult) > 0 && $get_not_allowed_status['farhan_busy_status'] == 1){ 
	            $farhan_busy_text = $get_not_allowed_status['farhan_busy_text'];
	          
	       ?>

	      <a href="javascript:void(0)" onclick="swal('<?php echo $farhan_busy_text; ?>')" >
	     <img class="logo-small"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
		 </a>
	     <?php }else{ ?>
	   
	    <a href="javascript:void(0)" onclick="swal('<?php echo FunctionsV3::get_agents_busy()['farhan_busy_text']; ?>')">
	      <img class="logo-small"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
	     </a>
	      <?php } ?>
	       <!-- END FARHAN CODE -->
	     <?php echo FunctionsV3::displayServicesList($val['service'])?>    
	     <?php FunctionsV3::displayCashAvailable($merchant_id)?>      
	    </div> <!--col-->
	    
	    <div class="col-md-7 border">
	     
	       <div class="mytable">
	         <div class="mycol">
	            <div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>   
	         </div>
	         <div class="mycol">
	            <p><?php //echo $ratings['votes']." ".t("Reviews")?></p>
	         </div>
	         <div class="mycol">
	            <?php echo FunctionsV3::merchantOpenTag($merchant_id)?>                
	         </div>
	         
	         <div class="mycol">
	          <p><?php echo t("Minimum Order").": ".FunctionsV3::prettyPrice($val['minimum_order'])?></p>
	         </div>
	         
	       </div> <!--mytable-->
	       
	       <h2><?php echo clearString($val['restaurant_name'])?></h2>
	       <p class="merchant-address concat-text"><?php echo $val['merchant_address']?></p>       	   	    
	       <p class="cuisine">
           <?php echo FunctionsV3::displayCuisine($val['cuisine']);?>
           </p>                
                                                       
           <p>
	        <?php 
	        if ($distance){
	        	echo t("Distance").": ".number_format($distance,1)." $distance_type";
	        } else echo  t("Distance").": ".t("not available");
	        ?>
	        </p>
	        
	        <?php if($val['service']!=3):?>
	        <!-- CHANGES BY FARHAN 9 SEP 2018 -->
	        <p>
	        <?php 
	        if ($distance){
	         $distance_time = FunctionsV3::getShippingTime($merchant_id,$distance);
	         if(empty($distance_time)){
	              echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id);
	            }
	        else{
	            echo t("Delivery Est")?>: <?php echo $distance_time;
	          }
	        }else{

	          echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id);

	        }
	        ?></p>
	        <!-- END BY FARHAN -->
	        <?php endif;?>
	        
	        <p>
	        <?php 
	        if($val['service']!=3){
		        if (!empty($merchant_delivery_distance)){
		        	echo t("Delivery Distance").": ".$merchant_delivery_distance." $distance_type_orig";
		        } else echo  t("Delivery Distance").": ".t("not available");
	        }
	        ?>
	        </p>
	                                
	        <p>
	        <?php 
	        if($val['service']!=3){
		        if ($delivery_fee){
		             echo t("Delivery Fee").": ".FunctionsV3::prettyPrice($delivery_fee);
		        } else echo  t("Delivery Fee").": ".t("Free Delivery");
	        }
	        ?>
	        </p>
	        <?php 
	        	 $offerText = Yii::app()->functions->getOption("farhan_offer_text",$merchant_id);
	        if(!empty($offerText)){
	         ?>
	        <p class="badge" style="margin-top: 5px;"><?php echo $offerText; ?></p> <!-- ADDED BY FARHAN -->
	        <?php } ?>	
	        <p class="top15"><?php echo FunctionsV3::getFreeDeliveryTag($merchant_id)?></p>
	    
	    </div> <!--col-->
	    
	    <div class="col-md-3 relative border">
	    

	     <!-- ADDED BY FARHAN -->
         <?php 
          $notAllowedAddress = FunctionsV3::get_not_allowed_address();
            $myArea2 = array_column($notAllowedAddress, 'not_allowed_address');
          
            $commonResult =array_intersect($myArea,$myArea2);
              
            $get_not_allowed_status = FunctionsV3::get_not_allowed_status();
          ?>
         <?php  if(FunctionsV3::get_agents_busy()['farhan_busy_status'] == 1 && ($get_not_allowed_status['farhan_busy_status'] == 0 || sizeof($commonResult) == 0)) {?>

	      <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>" 
          class="orange-button rounded3 medium">
           <?php echo t("Order Now")?>
        </a>

       <?php }else if(sizeof($commonResult) > 0 && $get_not_allowed_status['farhan_busy_status'] == 1){ 
          $farhan_busy_text = $get_not_allowed_status['farhan_busy_text'];
          ?>
            <a href="javascript:void(0)" onclick="swal('<?php echo $farhan_busy_text; ?>')"
        class="orange-button rounded3 medium">
        <?php echo t("Order Now")?></a>

          <?php }else{ ?>
         <!-- END FARHAN CODE -->
            <a href="javascript:void(0)" onclick="swal('<?php echo FunctionsV3::get_agents_busy()['farhan_busy_text']; ?>')"
        class="orange-button rounded3 medium">
        <?php echo t("Order Now")?>
        </a>

        <?php } ?>
         
	    
	    </div>
    </div> <!--row-->
    
    </div> <!--inner-->
</div>  <!--infinite-item-->   