<?php 
/*add global variables to footer*/
ScriptManager::registerGlobalVariables();
?>

<script src="//www.google.com/recaptcha/api.js?onload=onloadMyCallback&render=explicit" async defer ></script>

<?php if(Yii::app()->controller->action->id =="menu") {?>
<footer style="position:fixed; left:0px;bottom:0px; z-index:3; height:50px;width:100%;background:#354b60;" class="hidden-lg">
	 
 <div class="col-xs-1 text-center cart-mobile-handle border relative" style="width:100%;"	>
  <a href="javascript:;"><span style="font-size:18px; text-decoration:none;">My Cart </span><i class="ion-ios-cart"> </i></a>
  </div><!--cart-mobile-handle-->
 
</footer>
<?php } 

$getPopUp = getPopUp();
?>
<input type="hidden" id="cookieTime" value="<?php echo $getPopUp['time_period']; ?>">
<input type="hidden" id="farhanStatus" value="<?php echo $getPopUp['status']; ?>">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content  text-center">
        <div class="modal-header" style="background: #c41f25; color: #fff">
          <button type="button" class="close" data-dismiss="modal" style="opacity: 1; color: #fff;text-shadow: none;">&times;</button>
          <h4 class="modal-title"><?php echo $getPopUp['title']; ?></h4>
        </div>
        <div class="modal-body" style="font-weight: bold;">
        	 <?php echo $getPopUp['description']; ?>
        </div>
        <div class="modal-footer" style="text-align: center">
          <button type="button" class="btn btn-danger" style="background-color: #c41f25;border-color: #c41f25;" data-dismiss="modal"><?php echo $getPopUp['button_text']; ?></button>
        </div>
      </div>
      
    </div>
  </div>
 


 
</body>
</html>