<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<head>

<!-- IE6-8 support of HTML5 elements --> 
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link rel="shortcut icon" href="<?php echo  Yii::app()->request->baseUrl; ?>/favicon3.ico?ver=1.1" />
<?php 
/*add the analytic codes */
Widgets::analyticsCode();
?>

<style type="text/css">

.topSearch {
    
    position: fixed;
    top: 0;
    width: 100%;
    height: 40px;
    border-top: 1px solid #000;
    background: #5cb85c;
    z-index: 3;
}
/* Added By Gulzar 2018-7-24 */
@media only screen and (max-width : 1100px) 
{

	.mobile-banner-wrap { display: none !important; }
	.top-menu-wrapper img.logo {
	    border: 0px solid green;
	    max-width: 80px;
	}

	.top-menu-wrapper .container {    padding-top: 5px;
    padding-bottom: 5px;}

    .cart-item-count {    font-size: 14px;
        background-color: #e43304;
        padding: 2px 6px;
        text-align: center;
        border-radius: 10px;
        position: absolute;
        right: 30%;
        top: 14px;
        font-weight: bold;
        display: block;
        text-align: center;
        width: 30px;
    }
}
/*end*/
</style>
</head>
<body>
	<div class="topSearch" style="text-align: center;font-size: 15px;display: none;" id="mobile-filter-handle">
		<a style="margin-top: 3px; background: #5cb85c;border: 1px solid #5cb85c;" href="javascript:;" class="orange-button block center rounded mr10px">
                  Search <i class="fa fa-search"></i>
                </a>
	</div>