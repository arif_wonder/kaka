<?php
$this->renderPartial('/front/banner-contact',array(
   'h1'=>t("Contact Us"),
   'sub_text'=>$address." ".$country,
   'contact_phone'=>$contact_phone,
   'contact_email'=>$contact_email
));

$fields=yii::app()->functions->getOptionAdmin('contact_field');
if (!empty($fields)){
	$fields=json_decode($fields);
}
?>

<div class="sections section-grey2 section-contact relative">
  <div id="contact-map"></div>
  
  <div class="container-map">
     <div class="inner">
        <div class="row">
           <div class="col-md-7 dim">
             <h2><?php echo t("Contact")." $website_title";?> </h2>

             <p>
             <?php echo t("We are always happy to hear from our clients and visitors, you may contact us anytime")?>
             </p>
             
             <p><?php echo $contact_content?></p>
             <!-- ADDED BY FARHAN -->
             <div style="text-align:center">
                <h2> Want help? Start live chat below </h2>

               <a href="javascript:void(0)"" onclick='$("#zsiq_agtpic").click();' class="orange-button medium inline rounded">Live Chat</a>               
             </div>
             <!-- END BY FARHAN -->
           </div> <!--col-->
           <div class="col-md-5 black">
              
                           <div class="top30"></div>
           
             <form class="uk-form uk-form-horizontal forms" id="forms" onsubmit="return false;">   
             <?php echo CHtml::hiddenField('action','contacUsSubmit')?>
             <?php echo CHtml::hiddenField('currentController','store')?>
             <?php if (is_array($fields) && count($fields)>=1):?>
             <?php foreach ($fields as $val):?>
             <?php  
        $placeholder='';
        $validate_default="required";
        switch ($val) {
          case "name":
            $placeholder="Name";
            break;
          case "email":  
              $placeholder="Email address";
              $validate_default="email";
            break;
          case "phone":  
              $placeholder="Phone";
            break;
          case "country":  
              $placeholder="Country";
            break;
          case "message":  
              $placeholder="Message";
            break;      
          default:
            break;
        }
       ?>                 
       <?php if ( $val=="message"):?>
             <div class="row top10">
             <div class="col-md-12">
               <?php echo CHtml::textArea($val,'',array(
                'placeholder'=>t($placeholder),
                'class'=>'grey-fields full-width'
               ))?>
             </div>
             </div>
             <?php else :?>
             <div class="row top10">
             <div class="col-md-12">
               <?php echo CHtml::textField($val,'',array(
                'placeholder'=>t($placeholder),
                'class'=>'grey-fields full-width',
                'data-validation'=>$validate_default
               ))?>
             </div>
             </div>
             <?php endif;?>
             
             <?php endforeach;?>
             
             <div class="row top10">
             <div class="col-md-12 text-center">
                <input type="submit" value="<?php echo t("Submit")?>" class="orange-button medium inline rounded">
             </div>
             </div>
             <?php endif;?>
             </form>

             
             
           </div> <!--col-->
        </div> <!--row-->
     </div> <!--inner-->
  </div> <!--container-->

</div> <!--sections-->
<!-- ADDED BY FARHAN -->
<script type="text/javascript">


    var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || 
    {widgetcode:"4d01e64318f2050faf3c688e985b73d409e17b89f81528915c4be7ff49d4493f", values:{},ready:function(){}};
    var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
    s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
    

</script>
<!-- END BY FARHAN -->