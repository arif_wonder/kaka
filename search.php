<?php
require_once 'db.php';
// Output HTML formats
$html = '<tr>';
$html .= '<td class="small">client_id</td>';
$html .= '<td class="small">first_name</td>';
$html .= '<td class="small">email_address</td>';
$html .= '<td class="small">contact_phone</td>';
$html .= '<td class="small">address</td>';
$html .= '<td class="small">date_created</td>';
$html .= '</tr>';

// Get the Search
$search_string = preg_replace("/[^A-Za-z0-9]/", " ", $_POST['query']);
$search_string = $test_db->real_escape_string($search_string);

// Check if length is more than 1 character
if (strlen($search_string) >= 10 && $search_string !== ' ') {
	//Insert Time Stamp
	//$time = "UPDATE query_data SET timestamp=now() WHERE name='" .$search_string. "'";
	//Count how many times a query occurs
	//$query_count = "UPDATE query_data SET querycount = querycount +1 WHERE name='" .$search_string. "'";
	// Query
	$query = 'SELECT * FROM mt_client WHERE contact_phone LIKE "%'.$search_string.'%"';

	//Timestamp entry of search for later display
	//$time_entry = $test_db->query($time);
	//Count how many times a query occurs
	//$query_count = $test_db->query($query_count);
	// Do the search
	$result = $test_db->query($query);
	while($results = $result->fetch_array()) {
		$result_array[] = $results;
	}
	
	// Check for results
	if (isset($result_array)) {
		foreach ($result_array as $result) {
		// Output strings and highlight the matches
		 $client_id = $result['client_id'];
		 $first_name = $result['first_name']." ".$result['last_name'];
		 $email_address = $result['email_address'];
		 $contact_phone = $result['contact_phone'];
		 $address = $result['street']." ".$result['city']." ".$result['state']." ".$result['zipcode'];
		 $date_created = $result['date_created'];
		// Replace the items into above HTML
		$o = str_replace('client_id', $client_id, $html);
		$o = str_replace('first_name', $first_name, $o);
		$o = str_replace('email_address', $email_address, $o);
		$o = str_replace('contact_phone', $contact_phone, $o);
		$o = str_replace('address', $address, $o);
		$o = str_replace('date_created', $date_created, $o);
		// Output it
		echo($o);
			}
		}else{
		// Replace for no results
		$o = str_replace('nameString', '<span class="label label-danger">No Names Found</span>', $html);
		$o = str_replace('compString', '', $o);
		$o = str_replace('zipString', '', $o);
		$o = str_replace('cityString', '', $o);
		// Output
		echo($o);
	}
}
?>