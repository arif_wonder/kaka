<?php
class Callback extends CI_Controller {
	public $data 	= 	array();
    public function __construct()
	   {
		parent::__construct();
		$this->load->model('callback_model');
		$this->load->model('common');
		$this->load->library('form_validation');
		$this->load->library('general_functions');
		$this->load->helper('url'); 
     }
	
	public function index()
	{
			
	}
	
	public function sendOTP()
	{
		$mobile = $this->input->post("mobile");
		if($mobile == '')
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Mobile no is empty")));
		}
		
		if(!$this->general_functions->check_phone_number($mobile))
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Please enter 10 digits mobile no")));
		}
		$customer_info = $this->callback_model->customer_info("contact_phone",$mobile);
		if(count($customer_info) == 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Customer not registered")));
		}
		$client_id = $customer_info->client_id;
		$otp = $this->general_functions->generateCode(4);
		$otp_msg = sprintf($this->lang->line('otp_msg'),$otp);
		$this->general_functions->sendSMS($mobile,$otp_msg);
		$this->common->update_record(TBL."client",array("client_id"=>$client_id),array("mobile_verification_code"=>$otp,"mobile_verification_date"=>date('Y-m-d H:i:s', strtotime('60 minute'))));
		return $this->general_functions->print_response(json_encode(array("status"=>true,"otp"=>$otp,"message"=>"Your otp send")));
	}
	
	
	
	public function authOTP()
	{
		$mobile = $this->input->post("mobile");
		$otp = $this->input->post("otp");
		$message = array();
		if($mobile == '')
		{
			$message["mobile"] = "Mobile no is empty";
		}
		
		if(!$this->general_functions->check_phone_number($mobile))
		{
			$message["mobile"] = "Please enter 10 digits mobile no";
		}
		if($otp == '')
		{
			$message["otp"] = "OTP is empty";
		}
		if(count($message) > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>$message)));
		}
		$query = $this->db->where(array("contact_phone"=>$mobile,"mobile_verification_code"=>$otp))->where("mobile_verification_date >=",date("Y-m-d H:i:s"))->get(TBL."client");
		if($query->num_rows() == 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Your otp has expired")));
		}
		$customer_info = $this->callback_model->customer_info("contact_phone",$mobile);
		return $this->general_functions->print_response(json_encode(array("status"=>true,"customer_info"=>$customer_info)));
	}
	
	public function authRegisterOTP()
	{
		$mobile = $this->input->post("mobile");
		$otp = $this->input->post("otp");
		$message = array();
		if($mobile == '')
		{
			$message["mobile"] = "Mobile no is empty";
		}
		
		if(!$this->general_functions->check_phone_number($mobile))
		{
			$message["mobile"] = "Please enter 10 digits mobile no";
		}
		if($otp == '')
		{
			$message["otp"] = "OTP is empty";
		}
		if(count($message) > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>$message)));
		}
		$query = $this->db->where(array("contact_phone"=>$mobile,"mobile_verification_code"=>$otp))->where("mobile_verification_date >=",date("Y-m-d H:i:s"))->get(TBL."client_tmp");
		if($query->num_rows() == 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Your otp has expired")));
		}
		$customer_info = $this->common->select_scaler_where(TBL."client_tmp",array("contact_phone"=>$mobile));
		$customer_info = (array)$customer_info;
		$customer_info['client_id'] = '';
		$this->callback_model->create_customer($customer_info);
		$customer_info = $this->callback_model->customer_info("contact_phone",$mobile);
		return $this->general_functions->print_response(json_encode(array("status"=>true,"customer_info"=>$customer_info)));
	}
	
	public function checkCustomer()
	{
		$mobile = $this->input->post("mobile");
		if($mobile == '')
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Mobile no is empty")));
		}
		
		if(!$this->general_functions->check_phone_number($mobile))
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Please enter 10 digits mobile no")));
		}
		$customer_info = $this->callback_model->customer_info("contact_phone",$mobile);
		if(count($customer_info) == 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Customer not registered")));
		}
		
		return $this->general_functions->print_response(json_encode(array("status"=>true,"message"=>"Customer valid")));
	}
	
	
	public function login()
	{
		$mobile = $this->input->post("mobile");
		$password = $this->input->post("password");
		$message = array();
		if($mobile == '')
		{
			$message["mobile"] = "Mobile no is empty";
		}
		
		if(!$this->general_functions->check_phone_number($mobile))
		{
			$message["mobile"] = "Please enter 10 digits mobile no";
		}
		if($password == '')
		{
			$message["password"] = "Password is empty";
		}
		if(count($message) > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>$message)));
		}
		$query = $this->db->where(array("contact_phone"=>$mobile,"password"=>md5($password)))->get(TBL."client");
		
		if($query->num_rows() == 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Invalid Password")));
		}
		$customer_info = $this->callback_model->customer_info("contact_phone",$mobile);
		return $this->general_functions->print_response(json_encode(array("status"=>true,"customer_info"=>$customer_info)));
	}
	
	public function register()
	{
		$mobile = $this->input->post("mobile");
		$email = $this->input->post("email");
		$name = $this->input->post("name");
		$password = $this->input->post("password");
		$message = array();
		if($name == '')
		{
			$message["name"] = "Name is empty";
		}
		if($mobile == '')
		{
			$message["mobile"] = "Mobile no is empty";
		}
		if(!$this->general_functions->check_phone_number($mobile))
		{
			$message["mobile"] = "Please enter 10 digits mobile no";
		}
		if($email == '')
		{
			$message["email"] = "Email is empty";
		}
		if($password == '')
		{
			$message["password"] = "Password is empty";
		}
		if(!$this->general_functions->check_email_address($email))
		{
			$message["email"] = "Please enter valid email address";
		}
		if(count($message) > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>$message)));
		}
		$query = $this->db->where("contact_phone",$mobile)->or_where("email_address",$email)->get(TBL."client");
		
		if($query->num_rows() > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Email address already exist")));
		}
		$otp = $this->general_functions->generateCode(4);
		$otp_msg = sprintf($this->lang->line('otp_msg'),$otp);
		$this->general_functions->sendSMS($mobile,$otp_msg);
		$name_arr = explode(" ",$name);
		$first_name = $name_arr[0];
		$last_name = $name_arr[1];
		$customer_data = array("first_name"=>$first_name,"last_name"=>$last_name,"email_address"=>$email,"contact_phone"=>$mobile,"social_strategy"=>"app","password"=>md5($password),"date_created"=>date("Y-m-d H:i:s"),"date_modified"=>date("Y-m-d H:i:s"),"ip_address"=>$this->general_functions->get_IP_address(),"status"=>"active","mobile_verification_code"=>$otp,"mobile_verification_date"=>date('Y-m-d H:i:s', strtotime('60 minute')));
		$customer_info = $this->common->insert_record(TBL."client_tmp",$customer_data);
		return $this->general_functions->print_response(json_encode(array("status"=>true,"otp"=>$otp,"customer_info"=>$customer_data)));
	}
	
	
	public function resetPassword()
	{
		$mobile = $this->input->post("mobile");
		$password = $this->input->post("password");
		$otp = $this->input->post("otp");
		$message = array();
		if($mobile == '')
		{
			$message["mobile"] = "Mobile no is empty";
		}
		
		if(!$this->general_functions->check_phone_number($mobile))
		{
			$message["mobile"] = "Please enter 10 digits mobile no";
		}
		if($password == '')
		{
			$message["password"] = "Password is empty";
		}
		if($otp == '')
		{
			$message["otp"] = "OTP is empty";
		}
		if(count($message) > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>$message)));
		}
		$query = $this->db->where(array("contact_phone"=>$mobile,"mobile_verification_code"=>$otp))->where("mobile_verification_date >=",date("Y-m-d H:i:s"))->get(TBL."client");
		if($query->num_rows() == 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"You otp has expired")));
		}
		$this->common->update_record(TBL."client",array("contact_phone"=>$mobile),array("password"=>md5($password)));
		
		return $this->general_functions->print_response(json_encode(array("status"=>true,"message"=>"Password reset successfully")));
	}
	
	public function nearByMerchant()
	{
		$page = $this->input->get('page');
		$q = $this->input->get('q');
		$lat = $this->input->get('lat');
		$lon = $this->input->get('lon');
		$order_by = $this->input->get('order_by');
		if(empty($page))
		{
			$page = 1;
		}
		$limit = ($page-1) * 10;
		
		$filter_delivery_time = $this->input->get('filter_delivery_time');
		$filter_cuisine = $this->input->get('filter_cuisine');
		$filter_minimum_order = $this->input->get('filter_minimum_order');
		$filter_rating = $this->input->get('filter_rating');
		$search = array();
		$search_filter = array();
		$search['q'] = $q;
		$search['lat'] = $lat;
		$search['lon'] = $lon;
		$search['order_by'] = $order_by;
		$search_filter['filter_minimum_order'] = $filter_minimum_order;
		$search_filter['filter_delivery_time'] = $filter_delivery_time;
		$search_filter['filter_cuisine'] = $filter_cuisine;
		$search_filter['filter_rating'] = $filter_rating;
		$total_mearchants = $this->callback_model->get_total_mearchants($search);
		$merchant_list = $this->callback_model->get_mearchants($search,$search_filter,$limit);
		$merchant_info = array();
		$merchant_info['merchant_info'] = $merchant_list;
		$merchant_info['page'] = $page;
		$merchant_info['total'] = $total_mearchants;
		$this->general_functions->print_response(json_encode($merchant_info));
	}
	
	
	
	public function filterMerchant()
	{
		$q = $this->input->get('q');
		$lat = $this->input->get('lat');
		$lon = $this->input->get('lon');
		
		$search = array();
		$search_filter = array();
		$search['q'] = $q;
		$search['lat'] = $lat;
		$search['lon'] = $lon;
		
		$filter_list = $this->callback_model->get_filter_mearchants($search);
		
		$this->general_functions->print_response(json_encode($filter_list));
	}
	
	
	
	public function featuredMerchant()
	{
		$page = $this->input->get('page');
		if(empty($page))
		{
			$page = 1;
		}
		$limit = ($page-1) * 10;
		
		$search = array();
		$search_filter = array();
		$search['is_featured'] = 1;
		$total_mearchants = $this->callback_model->get_total_mearchants($search);
		$merchant_list = $this->callback_model->get_mearchants($search,$search_filter,$limit);
		$merchant_info = array();
		$merchant_info['merchant_info'] = $merchant_list;
		$merchant_info['page'] = $page;
		$merchant_info['total'] = $total_mearchants;
		$this->general_functions->print_response(json_encode($merchant_info));
	}
	

	
	public function merchantDetail()
	{
		$merchant_id = $this->input->get('merchant_id');
		if($merchant_id == '')
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Invalid restaurant id")));
		}
		if(!is_numeric($merchant_id))
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Invalid restaurant id")));
		}
		$merchant_info = $this->callback_model->get_merchant_info($merchant_id);
		$this->general_functions->print_response(json_encode($merchant_info));
	}
	
	public function saveAddress()
	{
		$lat = $this->input->post('lat');
		$lon = $this->input->post('lon');
		$street = $this->input->post('street');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$zipcode = $this->input->post('zipcode');
		$country = $this->input->post('country');
		$device_id = $this->input->post('device_id');
		$address_type = $this->input->post('address_type');
		$message = array();
		if($device_id == '')
		{
			$message["device_id"] = "Device Id is empty";
		}
		if($lat == '')
		{
			$message["lat"] = "Latitude is empty";
		}
		if($lon == '')
		{
			$message["lon"] = "Longitude is empty";
		}
		if($address_type == '')
		{
			$message["address_type"] = "Address type is empty";
		}
		
		if(count($message) > 0)
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>$message)));
		}
		$address_info = array("address_type"=>$address_type,"lat"=>$lat,"lon"=>$lon,"street"=>$street,"city"=>$city,"state"=>$state,"zipcode"=>$zipcode,"country"=>$country,"device_id"=>$device_id);
		$num_address = $this->common->num_rows_where(TBL."client_address",array("device_id"=>$device_id));
		if($num_address == 0)
		{
		$address_id = $this->common->insert_record(TBL."client_address",$address_info);
		}
		else
		{
			$this->common->update_record(TBL."client_address",array("device_id"=>$device_id),$address_info);
		}
		return $this->general_functions->print_response(json_encode(array("status"=>true,"address_info"=>$address_info)));
	}
	
	public function getAddress()
	{
		$device_id = $this->input->get('device_id');
		if($device_id == '')
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Device id not found")));
		}
		$address_info = $this->db->get_where(TBL."client_address",array("device_id"=>$device_id))->result_array();
		$address_info = $address_info[0];
		if($address_info['id'] == '')
		{
			return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Address not found")));
		}
		return $this->general_functions->print_response(json_encode(array("status"=>true,"address_info"=>$address_info)));
	}
}
?>
