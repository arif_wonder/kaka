<?php
class GenerateToken extends CI_Controller {
	public $data 	= 	array();
    public function __construct()
   {
	parent::__construct();
	$this->load->library('general_functions');
   }
	 
	 public function index()
	 {
		 $result = array();
		 $device_id = $this->input->post('device_id');
		 if($device_id == '')
		 {
			 return $this->general_functions->print_response(json_encode(array("status"=>false,"message"=>"Device id not found")));
		 }
		 $query = $this->db->get_where(TBL."api_token",array("device_id"=>$device_id));
		 $result = $query->row();
		 $token = hash('sha256', time().SERVER_ID.$device_id);
		$expiry = date('Y-m-d H:i:s', strtotime('1 hour'));
		 if($query->num_rows() > 0)
		 {
			$this->db->where(array("device_id"=>$device_id))->update(TBL."api_token", array("token"=>$token,"created_at"=>date("Y-m-d H:i:s"),"expiry"=>$expiry));
			return $this->general_functions->print_response(json_encode(array("status"=>true,"token"=>$token,"message"=>"Token generated")));
		 }
		 else
		 {
			 $this->db->insert(TBL."api_token", array("device_id"=>$device_id,"token"=>$token,"created_at"=>date("Y-m-d H:i:s"),"expiry"=>$expiry));
			return $this->general_functions->print_response(json_encode(array("status"=>true,"api_token"=>$token,"message"=>"Token generated")));
		 }
	 }
	
}
?>
