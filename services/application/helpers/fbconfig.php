<?php
session_start();
require_once 'autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;
// init app with app id and secret
FacebookSession::setDefaultApplication( '878743142243964','7801537deaa3fa962d28239ff4fedec8' );
// login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper(base_url('fbLogin') );
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
// see if we have a session
if ( isset( $session ) ) { 
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me?locale=en_US&fields=name,email' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
     	$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
 	    $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
	    $femail = $graphObject->getProperty('email');    // To Get Facebook email ID
		$full_name = explode(' ',$fbfullname);
		$first_name = $full_name[0];
		$last_name = $full_name[1];
//		echo "<pre>";print_r($graphObject);exit;
		
		$prof_image = "http://graph.facebook.com/".$fbid."/picture?width=135&height=135";
		
		 $query = $CI->db->query("SELECT COUNT(customer_id) AS 'num_user' FROM tbl_customer WHERE email = '".$femail."'");
		 $row = $query->result();
		
		if($row[0]->num_user)
		{
			$user_query = $CI->db->query("SELECT * FROM tbl_customer WHERE email = '".$femail."'");
		 	$user_info = $user_query->result();
			$image = $user_info[0]->image;
			$customer_id = $user_info[0]->customer_id;

	
			
			if($image)
			{
				
				unlink(DIR_WS_CUSTOMER_IMAGE.$image);
				unlink(DIR_WS_CUSTOMER_THUMB.$image);
				unlink(DIR_WS_CUSTOMER_SMALL.$image);
			}
			
			$img_file=file_get_contents($prof_image);
$new_image = time().".jpeg";
$file_loc=DIR_WS_CUSTOMER_IMAGE."/".$new_image;

$file_handler=fopen($file_loc,'w');

fwrite($file_handler,$img_file);
copy($file_loc,DIR_WS_CUSTOMER_THUMB.$new_image);
copy($file_loc,DIR_WS_CUSTOMER_SMALL.$new_image);
   

fclose($file_handler);

$CI->db->query("UPDATE tbl_customer SET first_name = '".$first_name."', last_name = '".$last_name."', image = '".$new_image."' WHERE email = '".$femail."'");
			
			$user_data = array('customer_id' => $customer_id, 'name' => $first_name, 'email' => $femail);
		    $CI->session->set_userdata($user_data);

		}
		else
		{
		$img_file=file_get_contents($prof_image);
$new_image = time().".jpeg";
$file_loc=DIR_WS_CUSTOMER_IMAGE."/".$new_image;

$file_handler=fopen($file_loc,'w');

fwrite($file_handler,$img_file);
copy($file_loc,DIR_WS_CUSTOMER_THUMB.$new_image);
copy($file_loc,DIR_WS_CUSTOMER_SMALL.$new_image);
			$CI->db->query("INSERT INTO tbl_customer SET first_name = '".$first_name."', last_name = '".$last_name."', image = '".$new_image."', email = '".$femail."', fblogin = 1, date_added = NOW()");
			$customer_id = $CI->db->insert_id();

			$CI->db->query("INSERT INTO tbl_customer_address SET customer_id = '".$customer_id."', first_name = '".$first_name."', last_name = '".$last_name."', email = '".$femail."', `default`= 1, date_added = NOW()");
			
			$user_data = array('customer_id' => $customer_id, 'name' => $first_name, 'email' => $femail);
			$CI->session->set_userdata($user_data);
		}
		redirect(base_url("order/checkout"));
} 
?>