<?php
require_once 'data/apiClient.php';
require_once 'data/contrib/apiOauth2Service.php';
session_start();
$client = new apiClient();
$client->setApplicationName("Login with Google");

$oauth2 = new apiOauth2Service($client);

if (isset($_GET['code'])) {
  $client->authenticate();
  $_SESSION['token'] = $client->getAccessToken();
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  //header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) 
{
 	$client->setAccessToken($_SESSION['token']);
}

if (isset($_REQUEST['logout'])) 
{
  unset($_SESSION['token']);
  unset($_SESSION['sessionBrought']);
  $client->revokeToken();
}

if ($client->getAccessToken()) 
{
  $validData = $oauth2->userinfo->get();

$gid = $validData['id'];              
$email = $validData['email']; 
$name = $validData['name']; 
$full_name = explode(' ',$name);
$first_name = $full_name[0];
$last_name = $full_name[1];
$prof_image = $validData['picture']; 

 
		
		 $query = $CI->db->query("SELECT COUNT(customer_id) AS 'num_user' FROM tbl_customer WHERE email = '".$email."'");
		 $row = $query->result();
		
		if($row[0]->num_user)
		{
			$user_query = $CI->db->query("SELECT * FROM tbl_customer WHERE email = '".$email."'");
		 	$user_info = $user_query->result();
			$image = $user_info[0]->image;
			$customer_id = $user_info[0]->customer_id;
			
			
			
	
			
			if($image)
			{
				
				unlink(DIR_WS_CUSTOMER_IMAGE.$image);
				unlink(DIR_WS_CUSTOMER_THUMB.$image);
				unlink(DIR_WS_CUSTOMER_SMALL.$image);
			}
			
			$img_file=file_get_contents($prof_image);
$new_image = time().".jpeg";
$file_loc=DIR_WS_CUSTOMER_IMAGE."/".$new_image;

$file_handler=fopen($file_loc,'w');

fwrite($file_handler,$img_file);
copy($file_loc,DIR_WS_CUSTOMER_THUMB.$new_image);
copy($file_loc,DIR_WS_CUSTOMER_SMALL.$new_image);
   

fclose($file_handler);

$CI->db->query("UPDATE tbl_customer SET first_name = '".$first_name."', last_name = '".$last_name."', image = '".$new_image."' WHERE email = '".$email."'");
$user_data = array('customer_id' => $customer_id, 'name' => $first_name, 'email' => $email);
$CI->session->set_userdata($user_data);
		}
		else
		{
		$img_file=file_get_contents($prof_image);
$new_image = time().".jpeg";
$file_loc=DIR_WS_CUSTOMER_IMAGE."/".$new_image;

$file_handler=fopen($file_loc,'w');

fwrite($file_handler,$img_file);
copy($file_loc,DIR_WS_CUSTOMER_THUMB.$new_image);
copy($file_loc,DIR_WS_CUSTOMER_SMALL.$new_image);
   

fclose($file_handler);
			$CI->db->query("INSERT INTO tbl_customer SET first_name = '".$first_name."', last_name = '".$last_name."', image = '".$new_image."', email = '".$email."', glogin = 1, date_added = NOW()");
			$customer_id = $CI->db->insert_id();
			
			$CI->db->query("INSERT INTO tbl_customer_address SET customer_id = '".$customer_id."', first_name = '".$first_name."', last_name = '".$last_name."', email = '".$email."', `default`= 1, date_added = NOW()");
			$user_data = array('customer_id' => $customer_id, 'name' => $first_name, 'email' => $email);
			$CI->session->set_userdata($user_data);
		}
		$page_uri = $_SESSION['page_uri'];
		//session_unset($_SESSION['page_uri']);
 		//redirect($page_uri);
		redirect(base_url("order/checkout"));
  
 

} 




	
?>