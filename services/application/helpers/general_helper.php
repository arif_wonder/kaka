<?php
function get_battery_attributes($battery_id)
{
	$CI = & get_instance();
	$CI->load->model('everydaychoice');
	$result = $CI->everydaychoice->get_battery_attributes($battery_id);
	return $result;
}

function get_battery_price($battery_id)
{
	$CI = & get_instance();
	$CI->load->model('everydaychoice');
	$result = $CI->everydaychoice->get_battery_price($battery_id);
	return $result;
}


function getBlogCategory($parent_id="",$id="",$no_parent="",$featured="",$limit="")
	 {
	  $CI =& get_instance();  
	  $where = "";
	  if(is_numeric($parent_id))
	   {
	     $where .= " AND `tbl_blogs_categories`.`parent_id` = '".$parent_id."'";
	   }
	   
	   if(is_numeric($no_parent))
	   {
	     $where .= " AND `tbl_blogs_categories`.`parent_id` != '".$parent_id."'";
	   }
	  
	  if($id != "")
	   {
	     $where .= " AND `tbl_blogs_categories`.`id` = '".$id."'";
	   } 
	   
	   if($featured != "")
	   {
	     $where .= " AND `tbl_blogs_categories`.`featured` = '".$featured."'";
	   }
	   
	   //$where .= "order by  `tbl_blogs_categories`.`name` asc `tbl_blogs_categories`.`date_added` DESC  ";
	  // $where .= " 'order_by `tbl_blogs_categories`.`date_added` DESC  `tbl_blogs_categories`.`name` asc'";
	   if($limit != "")
	   {
	     $where .= " limit 0, ".$limit."";
	   }  
	   
	     $sql = "select `tbl_blogs_categories`.* from `tbl_blogs_categories` where `tbl_blogs_categories`.`status` = '1' ".$where." order by`tbl_blogs_categories`.`date_added` DESC,`tbl_blogs_categories`.`id`";
	  
	  $query = $CI->db->query($sql);
	  $data = "";
	  if($query->num_rows>0)
	  {
		$data = $query->result();
	  }
	  return $data;
	 }
	 
	 
	  function getBlogSubCategory($parent_id="",$id="",$no_parent="",$featured="",$limit="")
	 {
	  $CI =& get_instance();  
	  $where = "";
	  if(is_numeric($parent_id))
	   {
	     $where .= " AND `tbl_blogs_categories`.`parent_id` != '".$parent_id."'";
	   }
	   
	   if(is_numeric($no_parent))
	   {
	     $where .= " AND `tbl_blogs_categories`.`parent_id` != '".$parent_id."'";
	   }
	  
	  if($id != "")
	   {
	     $where .= " AND `tbl_blogs_categories`.`id` = '".$id."'";
	   } 
	   
	   if($featured != "")
	   {
	     $where .= " AND `tbl_blogs_categories`.`featured` = '".$featured."'";
	   }
	   
	   $where .= "order by  `tbl_blogs_categories`.`name` asc";
	   
	   if($limit != "")
	   {
	     $where .= " limit 0, ".$limit."";
	   }  
	   
	    $sql = "select `tbl_blogs_categories`.* from `tbl_blogs_categories` where `tbl_blogs_categories`.`status` = '1' ".$where . "";
	  
	  $query = $CI->db->query($sql);
	  $data = "";
	  if($query->num_rows>0)
	  {
		$data = $query->result();
	  }
	  return $data;
	 }
	 
	 
	  function geTagType($tag_id="",$tag="")
	 {
	  $CI =& get_instance(); 
	  $where = "";
	  if($tag_id!="")
	  {
	    $where .= " AND `tbl_blog_tag`.`blog_tag_id` = '".$tag_id."'";
	  }
	  
	  if($tag!="")
	  {
	    $where .= " AND `tbl_blog_tag`.`tag_id` = '".$tag."'";
	  }
	  
	  $sql = "select tbl_master_tag.name from `tbl_blog_tag` left join tbl_master_tag on tbl_master_tag.id = tbl_blog_tag.tag_id where 1 ".$where; 
	 
	 
	 
	  $query = $CI->db->query($sql);
	  $data = array();
	  if($query->num_rows>0)
	  {
		 foreach ($query->result_array() as $row) 
		 {
		        array_push($data,$row['name']);
		  }
		  //print_r($data);die;
		 return $data;
	  }
	  //return $data;
	 }
	 
	 function getTagname($id="",$name="")
	 {
	  $CI =& get_instance();
	  $limit = "";
	  $where = "";
	
	  if($id!="")
	  {
	    $where .= " AND `id` = '".$id."'";
	  }
	  
	  if($name!="")
	  {
	    $where .= " AND `name` LIKE '".$name."'";
		$limit .= " limit 0,10";
	  }
	  $sql = "select * from `tbl_master_tag` where status=1 ".$where." order by name asc ".$limit;
	  $query = $CI->db->query($sql);
	  $data = "";
	  if($query->num_rows>0)
	  {
		$data = $query->result();
	  }
	  return $data;
	 }
	 
	 function getBlogCategoryParent($categories_id)
	 {
		  $CI =& get_instance();
		  $sql = "select parent_id from `tbl_blogs_categories` where `id` = '".$categories_id."'";
		  $query = $CI->db->query($sql);
		  $data = "";
		  if($query->num_rows>0)
		  {
			$data = $query->result();
		  }
		  return $data;
	 }
	 
	 function geTagTypes($tag_id="",$tag="")
	 {
	  $CI =& get_instance(); 
	  $where = "";
	  if($tag_id!="")
	  {
	    $where .= " AND `tbl_blog_tag`.`blog_tag_id` = '".$tag_id."'";
	  }
	  
	  if($tag!="")
	  {
	    $where .= " AND `tbl_blog_tag`.`tag_id` = '".$tag."'";
	  }
	  
	   $sql = "select tbl_master_tag.name,tbl_blog_tag.tag_id from `tbl_blog_tag` left join tbl_master_tag on tbl_master_tag.id = tbl_blog_tag.tag_id where 1 ".$where; 
	 
	 
	 
	  $query = $CI->db->query($sql);
	  if($query->num_rows>0)
	  {
		 return $query->result();
	  }
	  //return $data;
	 }
	 
	 function sendSMS($numbers,$message)
	 {
		$url = "http://sms.jayinegroup.com/pushsms.php";
		$params = array("username"=>"vinodsharma","password"=>"16655","sender"=>"evecho","numbers"=>$numbers,"message"=>urlencode($message));
		$postData = '';
	   foreach($params as $k => $v) 
	   { 
		  $postData .= $k . '='.$v.'&'; 
	   }
		$postData = rtrim($postData, '&');
		$output = file_get_contents($url."?".$postData);
		/*$ch = curl_init();  
		curl_setopt($ch,CURLOPT_URL,$url."?".$postData);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_HEADER, false); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$output=curl_exec($ch);
		curl_close($ch);*/
		return $output;
	 }
?>