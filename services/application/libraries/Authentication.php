<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentication{

    /**
     * sentry
     *
     * allows/disallows users from a certain page
     * management of user sessions
     */
	private $CI;
	function __construct()
    {
        $this->CI =& get_instance();
	}
	 
    function sentry($per_s_name="", $section="") {
        //new CI instance
        $this->CI =& get_instance();
        //disable caching
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $this->CI->load->helper('url');
        //$this->CI->load->library('session');
        $this->CI->load->model('login_model');
        //get session data on db
        /*$result = $this->CI->login_model->check_session(
                        $this->CI->session->userdata('my_session_id'),
                        $this->CI->session->userdata('users_id')
        );*/
		$result =1;
        //don't do anything if record is found
        if (count($result) > 0) {
			
			$perm_status=1;
			$users_type_id = $this->CI->session->userdata('users_type_id');
			//Check user type to there section
			if(($users_type_id!=ADMIN_USERTYPE && $section=='manage') or ($users_type_id!=RESTAURANT_USERTYPE && $section=='restaurant') or ($users_type_id!=CUSTOMER_USERTYPE && $section=='users'))
			{
				$perm_status=0;
			}
			
			if(!empty($per_s_name))
			{
			$permid = $this->get_premission_id($per_s_name);
			//echo $permid;
			//exit;
			if(!empty($permid))
			{
				//echo $this->CI->session->userdata('users_id')." - ".$permid;
				$perm_status = $this->Permissions($this->CI->session->userdata('users_id'),$permid);
			}
			}
			//exit;
			if($perm_status==0)
			{
				$this->CI->session->set_flashdata('message', "You are not authorized to this section.");
				header('Location:' . base_url() . 'message_page');
			}
			
            //exit;
        } else {
            //logout expired session/intruders
			//echo $this->CI->session->userdata('my_session_id')." ".$this->CI->session->userdata('users_id');
			//exit;
			
            $this->CI->session->sess_destroy();
            if(empty($section))
			{
				header('Location:' . base_url() . 'index.php');
			}
			else
			{
				if($section=='manage')
				{
					header('Location:' . base_url() . 'login/manage');
				}
				if($section=='users')
				{
					header('Location:' . base_url() . 'login/user');
				}
				if($section=='restaurant')
				{
					header('Location:' . base_url() . 'login/restaurant');
				}
			}
			
			
        }
    }
	
	function get_premission_id($s_name="")
	{
	$sql= "select id from tbl_permission where s_name='$s_name'";
			
			$query = $this->CI->db->query($sql);
			foreach($query->result() as $row)
			{
				return	 $row->id;
				
			}

		return "";
	}

	function user_type_premission($tID,$pID)
	{
	$sql= "select status from tbl_users_type_permission where users_type_id='$tID' and permission_id='$pID'   ";
			
			$query = $this->CI->db->query($sql);
			foreach($query->result() as $row)
			{
				return	 $row->status;
				
			}

		return "";
	}


	function user_premission($uid,$perid)
	{
		$sql= "status from tbl_users_permission where users_id='$uid' and permission_id='$perid' ";
		
		$query = $this->CI->db->query($sql);
		foreach($query->result() as $row)
		{
			return	 $row->status;
				
		}

		return "";
	}

	function Permissions($uid,$perid)
	{
		$tmptype_id="";
		$tmpvalue="";
		$sql="select status from tbl_users_permission where users_id='$uid' and permission_id='$perid'";

		$query = $this->CI->db->query($sql);

		if($query->num_rows() > 0)
		{ 	
			$row = $query->row();
			$tmpvalue = $row->status;
			if($tmpvalue!=0)
			{
				$sql_group="select U.users_type_id from  tbl_users U where U.users_id='$uid' order by U.users_type_id asc limit 1";
				$query_group = $this->CI->db->query($sql_group);
				foreach($query_group->result() as $row_gid)
					{
						$tmptype_id= $row_gid->users_type_id;
					}
					
				$sql_role="select status from tbl_users_type_permission where users_type_id='$tmptype_id' and permission_id='$perid' ";
				
				$query_role = $this->CI->db->query($sql_role);
				
				
				if($query_role->num_rows() >0)
				{
					foreach($query_role->result() as $row_role)
						{
							return $row_role->status;
						}
				}else
					{
						return 0;	
					}
				
			}
			else
			{
				return 0;
			}
		}else
			{
		
				$sql_group="select U.users_type_id from tbl_users U where U.users_id='$uid' order by U.users_type_id asc limit 1";
				$query_group = $this->CI->db->query($sql_group);
				if($query_group->num_rows() > 0)
				{ 	
					$row_gid = $query_group->row();
					$tmptype_id = $row_gid->users_type_id;
		
				}
				
				$sql_role="select status from tbl_users_type_permission where users_type_id='".$tmptype_id."' and permission_id='".$perid."'";
				$query_role = $this->CI->db->query($sql_role);
				if($query_role->num_rows() >0)
				{
				
					$row_role  = $query_role->row();
					$val = $row_role->status;
					if($val == 0)
					{
					
						return 0;
					}else if($val ==1)
						{
							return 1;
						}	
				}else
					{
						return 0;							
					
					}
			
		
			}	

	}

}

?>