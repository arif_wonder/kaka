<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class General_functions {
    var $CI;
    var $status_arr = array("0"=>"In Active", "1"=>"Active");
	var $gender_arr = array("m"=>"Male", "f"=>"Female");
	var $notify_arr = array("0"=>"No", "1"=>"Yes");
	var $data = array(); 
	public function __construct()
    {
		$this->CI = & get_instance();
		$this->CI->load->model('common');
		$this->CI->load->library('session');
		$this->CI->load->library('cart');
    }
	
	function get_days_in_months($month, $year)
	{
		$month_arr = array( '01'=>31,'02'=>28,'03'=>31,'04'=>30,'05'=>31,'06'=>30,'07'=>31,'08'=>31,'09'=>30,'10'=>31,'11'=>30,'12'=>31);
		
		if($this->is_lear_year($year)) // chk for leap year
		{
			$month_arr['02'] = 29;	
		}else
			{
				$month_arr['02'] = 28;
			}
		return 	$month_arr[$month];
		
	}
	
  function is_lear_year($year)
   {
		
		if($year%400 ==0 || ($year%100 != 0 && $year%4 == 0))
			return true;
		else
			return false;
		
     // return $year%400 ==0 || ($year%100 != 0 && $year%4 == 0);
   }
   
   
	
	function WeekToDate ($week, $year)
	{
		$Jan1 = mktime (1, 1, 1, 1, 1, $year);
		$iYearFirstWeekNum = (int) strftime("%W",mktime (1, 1, 1, 1, 1, $year));
		if ($iYearFirstWeekNum == 1)
		{
		$week = $week - 1;
		}
		$weekdayJan1 = date ('w', $Jan1);
		$FirstMonday = strtotime(((4-$weekdayJan1)%7-3) . ' days', $Jan1);
		$CurrentMondayTS = strtotime(($week) . ' weeks', $FirstMonday);
		return ($CurrentMondayTS);
	}
		
	
	
function create_list($sql_query, $selected_id="", $deselect_id=0)
{
	$this_list = '';
	if(!empty($sql_query))
	{
		
		$query = $this->CI->db->query($sql_query);
		if ($query->num_rows() > 0)
		{
			while (list($row[0], $row[1]) = $this->CI->db->call_function('fetch_row', $this->CI->db->result_id))
			{
				if(!empty($selected_id) && $selected_id==$row[0])
				{
					$this_list .= "<OPTION value=\"" . $row[0] . "\" SELECTED>" . ucwords($row[1]) . "</OPTION>";
				}
				else if($deselect_id != 0 && $row[0] == $deselect_id)
				{
					$this_list .= "<OPTION value=\"" . $row[0] . "\"  DISABLED='DISABLED' >" . ucwords($row[1]) . "</OPTION>";
				}else
				{
					$this_list .= "<OPTION value=\"" . $row[0] . "\" >" . ucwords($row[1]) . "</OPTION>";
				}	
			}
		}
		
	}
	return $this_list;
}
function create_multiplelist($sql_query, $selected_ids)
{
	$this_list = '';
	if(!empty($sql_query))
	{
		$query = $this->CI->db->query($sql_query);
		if ($query->num_rows() > 0)
		{
			while (list($row[0], $row[1]) = $this->CI->db->call_function('fetch_row', $this->CI->db->result_id))
			{
				if(isset($selected_ids) && in_array($row[0],$selected_ids))
				{
					$this_list .= "<OPTION value=\"" . $row[0] . "\" SELECTED>" . ucwords($row[1]) . "</OPTION>";
				}
				else
				{
					$this_list .= "<OPTION value=\"" . $row[0] . "\" >" . ucwords($row[1]) . "</OPTION>";
				}	
			}
		}
		
	}
	return $this_list;
}
function create_multiple_checkbox_list($sql_query, $name, $selected_ids)
{
	$this_list = '';
	if(!empty($sql_query))
	{
		$query = $this->CI->db->query($sql_query);
		if ($query->num_rows() > 0)
		{
			while (list($row[0], $row[1]) = $this->CI->db->call_function('fetch_row', $this->CI->db->result_id))
			{
				if(isset($selected_ids) && in_array($row[0],$selected_ids))
				{
					$this_list .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"" . $name . "[]\" value=\"" . $row[0] . "\" checked />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . ucwords($row[1]);
				}
				else
				{
					$this_list .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"" . $name . "[]\" value=\"" . $row[0] . "\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . ucwords($row[1]);
				}	
			}
		}
		
	}
	return $this_list;
}
function get_ddmmyy_from_yymmdd($date)
{
	list($year, $month, $day) = preg_split("/[\/.-]/", $date);
	return $day."/".$month."/".$year;
}
function date_to_display($datetime)
{
	$data_arr = explode(" ",$datetime);
	$date = $data_arr[0]; 
	//$time = $data_arr[1]; 
	list($year, $month, $day) = preg_split("/[\/.-]/", $date);
	return $day."/".$month."/".$year;
}
function datetime_to_display($datetime)
{
	$data_arr = explode(" ",$datetime);
	$date = $data_arr[0]; 
	$time = $data_arr[1]; 
	
	list($year, $month, $day) = preg_split("/[\/.-]/", $date);
	return $day."/".$month."/".$year." ".$time;
}
function date_to_store($date)
{
	list($day, $month, $year) = preg_split("/[\/.-]/", $date);
	return $year."/".$month."/".$day;
}
function date_diff( $date1)
{
  	$keep_date=array();
  	$chk_negative=substr( $date1,0,1);
	$datediff= abs( $date1);
	$fullday=  floor($datediff/(60*24));
	$fullhours= floor(($datediff-($fullday*60*24))/(60));
	$minuts=floor(($datediff-($fullday*60*24)-($fullhours*60))); 
	
	if($fullday > 0){$keep_date[1]=$fullday."day";}else{$keep_date[1]="";} 
	if($fullhours >0){$keep_date[2]=$fullhours."hrs"; }else{$keep_date[2]="";} 
	if($minuts > 0){ $keep_date[3]=$minuts."min"; }else{$keep_date[3]="";} 
	
	$keep_date[0]="";
	if( $chk_negative=='-')
	{
	 $keep_date[0]= "-";
	}
	
	$tmpdate="";
	if($keep_date[1]!="" and $keep_date[2]!="" and $keep_date[3]!="" ) 
	{
		$tmpdate= $keep_date[1]." ".$keep_date[2];
	}
	
	elseif($keep_date[1]!="" and $keep_date[3]!="" ) 
	{
		$tmpdate= $keep_date[1]." ".$keep_date[3];
	}
	
	elseif($keep_date[1]!="" and $keep_date[2]!="" ) 
	{
		$tmpdate= $keep_date[1]." ".$keep_date[2];
	}
	
	
	
	elseif($keep_date[2]!="" and $keep_date[3]!="")
	{
		$tmpdate= $keep_date[2]." ".$keep_date[3];
	} 
	
	elseif($keep_date[1]!=""  ) 
	{
		$tmpdate= $keep_date[1];
	}
	
	elseif($keep_date[2]!=""  ) 
	{
		$tmpdate= $keep_date[2];
	}
	
	elseif($keep_date[3]!="")
	{
		$tmpdate= $keep_date[3];
	}
	
	return $keep_date[0].$tmpdate;
}
 function m2h($mins) {
    if ($mins < 0) {
        $min = Abs($mins);
    } else {
        $min = $mins;
    }
    $H = Floor($min / 60);
    $M = ($min - ($H * 60)) / 100;
    $hours = $H + $M;
    if ($mins < 0) {
        $hours = $hours * (-1);
    }
 
    $expl = explode(".", $hours);
    $H = $expl[0];
    if (empty($expl[1])) {
        $expl[1] = 00;
    }
 
    $M = $expl[1];
    if (strlen($M) < 2) {
        $M = $M . 0;
    }
 
	$H = $H <10?'0'.$H:$H;
    $hours = $H . "h" ." ". $M."m";
 
    return $hours;
}
function convert_date_to_today( $date="" )
{
	if( strtoupper(trim($date))==strtoupper(trim(date('d-M'))))
	{
	return "Today";
	
	}
	elseif( strtoupper(trim($date))==strtoupper(trim( date("d-M", strtotime("yesterday")))))
	{
	return "yesterday";
	}
	else
	{
	return $date;
	}
}
##############################################################################
function expire_indication( $date1)
{
  	$keep_date=array();
  	$chk_negative=substr( $date1,0,1);
	$datediff= abs( $date1) ;
	 $fullday=  floor($datediff/(60*24));
	 $fullhours= floor(($datediff-($fullday*60*24))/(60));
	$minuts=floor(($datediff-($fullday*60*24)-($fullhours*60))); 
	if ($chk_negative=='-')
	{
	return 1; #RED
	}
	elseif( $fullday==0 and( $fullhours<8 or $minuts<60 ))
	{
	return 2; #blink
	}
	else
	{
	return 0;
	
	}
	##return $keep_date[0].$tmpdate;
}
###################################################
public function make_thumb($image_path,$thumb_path,$thumb_width=150,$thumb_height=150)
{
        $this->CI->load->library('image_lib');
		$config['image_library'] = 'gd2';
        $config['source_image']    = $image_path;
        $config['new_image'] = $thumb_path;
        $config['maintain_ratio'] = TRUE;
        $config['width']     = $thumb_width;
        $config['height']    = $thumb_height;
        $this->CI->image_lib->initialize($config); 
        if ( ! $this->CI->image_lib->resize())
        {
            echo $this->CI->image_lib->display_errors();
        }
}
public function generateCode($characters) {
		/* list all possible characters, similar looking characters and vowels have been removed */
		$possible = '0123456789';
		$code = '';
		$i = 0;
		while ($i < $characters) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		return strtoupper($code);
}
function get_IP_address()
{
    return $_SERVER['REMOTE_ADDR'];
}
public function send_emailer($from_email="", $from_name="", $to_email="", $subject="",$message="", $cc_email="", $bcc_email="",$protocol="smtp",$smtp_host="ssl://smtp.gpi.com",$smtp_port="587",$smtp_user="",$smtp_pass="") 
{
	
	
	if($protocol=='smtp')
	{
	$config = Array(
			  'protocol' => $protocol,
			  'smtp_host' => $smtp_host,
			  'smtp_port' => $smtp_port,
			  'smtp_user' => $smtp_user, // change it to yours
			  'smtp_pass' => $smtp_pass, // change it to yours
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
		);
		
	
	
	}
	else
	{
		$config = Array(
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
		);
	}
	$this->CI->load->library('email',$config);
	$this->CI->email->set_mailtype("html");
	$this->CI->email->from($from_email, $from_name);
	$this->CI->email->to($to_email);
	
	if(!empty($cc_email))
	{
	$this->CI->email->cc($cc_email);
	}
	
	if(!empty($bcc_email))
	{
	$this->CI->email->bcc($bcc_email);
	}
	$this->CI->email->subject($subject);
	$this->CI->email->message($message);
	$this->CI->email->send();
  // print_r($this->CI->email->print_debugger());die;
	return $this->CI->email->print_debugger();
}
public function send_emailer_attachment($from_email="", $from_name="", $to_email="", $subject="", $message="", $attachment="") 
{
		$config = Array(
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
		);
	$this->CI->load->library('email',$config);
	$this->CI->email->set_mailtype("html");
	$this->CI->email->from($from_email, $from_name);
	$this->CI->email->to($to_email);
	
	if(!empty($cc_email))
	{
	$this->CI->email->cc($cc_email);
	}
	
	if(!empty($bcc_email))
	{
	$this->CI->email->bcc($bcc_email);
	}
	$this->CI->email->subject($subject);
	$this->CI->email->message($message);
	$this->CI->email->attach($attachment);
	$this->CI->email->send();
  // print_r($this->CI->email->print_debugger());die;
	return $this->CI->email->print_debugger();
}
##########################################################################################################
# IMAGE FUNCTIONS																						 #
# You do not need to alter these functions																 #
##########################################################################################################
public function resizeImage($image,$width,$height,$scale) {
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
	
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$image); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$image,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$image);  
			break;
    }
	
	chmod($image, 0777);
	return $image;
}
//You do not need to alter these functions
public function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}
//You do not need to alter these functions
public function getHeight($image) {
	$size = getimagesize($image);
	$height = $size[1];
	return $height;
}
//You do not need to alter these functions
public function getWidth($image) {
	$size = getimagesize($image);
	$width = $size[0];
	return $width;
}
function resize($image_name , $thumbs_nails_path='', $width, $height) {	
      $CI =& get_instance();
      /////thumbnail path//////////////////
			$base_name = basename(str_replace("\\", "/", $image_name));
		    $thumb_image_path = $thumbs_nails_path . $base_name;
			
			///////////Get New Dimension////////////////
			list($image_width, $image_height, $type) = getimagesize($image_name);
			
			if($width >= $image_width){
			$width = $image_width;
			}
			if($height >= $image_height){
			$height = $image_height;
			}
			 
			$thumb_scale = min($width/$image_width, $height/$image_height);
			$width = floor($thumb_scale*$image_width);
			$height = floor($thumb_scale*$image_height);
			
			
			///////////Check File Extension////////////////////
			if($type == 1){
			$main_image = imagecreatefromgif($image_name);
			}
			if($type == 2){
			$main_image = imagecreatefromjpeg($image_name);
			}
			if($type == 3){
			$main_image = imagecreatefrompng($image_name);
			}
			 
			$thumb_image = imagecreatetruecolor($width, $height);
			
			imagecopyresampled($thumb_image, $main_image , 0, 0, 0, 0, $width, $height, $image_width, $image_height);
			
			if($type == 1){
			imagegif($thumb_image, $thumb_image_path, 100);
			}
			if($type == 2){ 
			imagejpeg($thumb_image, $thumb_image_path, 100);
			}
			if($type == 3){
			imagepng($thumb_image, $thumb_image_path);
			}
			imagedestroy($thumb_image);
			imagedestroy($main_image);
			@chmod($thumb_image_path, 0777);
			}
			
public function getCurrentDay($current_date="")
{
   if($current_date == "")
    {
      $current_date = date('l',time());
    }
   if($current_date == 'Sunday')
    {
	  $val = '0';
 	}
	if('Monday' == $current_date)
    {
	  $val = '1';
 	}
	if('Tuesday' == $current_date)
    {
	  $val = '2';
 	}
	if('Wednesday' == $current_date)
    {
	  $val = '3';
 	}
	if('Thursday' == $current_date)
    {
	  $val = '4';
 	}
	if('Friday' == $current_date)
    {
	  $val = '5';
 	}
	if('Saturday' == $current_date)
    {
	  $val = '6';
 	}
	return $val;
}	
public function slugify($text,$table)
{ 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
  // trim
  $text = trim($text, '-');
  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  // lowercase
  $text = strtolower($text);
  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);
  	return $text;
  
}	
	function is_in_array($array, $key, $key_value){
      $within_array = 'no';
      foreach( $array as $k=>$v ){
        if( is_array($v) ){
            $within_array = $this->is_in_array($v, $key, $key_value);
            if( $within_array == 'yes' ){
                return true;
            }
        } else {
                if( $v == $key_value && $k == $key ){
                        $within_array = 'yes';
                         return true;
                }
        }
      }
      return  false;
}
function search_in_array($array, $key, $key_value){
      $within_array = 'no';
	  $counter = 0;
      foreach( $array as $k=>$v ){
        if( is_array($v) ){
            $within_array = $this->is_in_array($v, $key, $key_value);
            if( $within_array == 'yes' ){
                return $counter;
            }
        } else {
                if( $v == $key_value && $k == $key ){
                        $within_array = 'yes';
                         return $counter;
                }
        }
		$counter++;
      }
      return  false;
}
function check_email_address($email) {
	$regex = "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/";
	if ( preg_match( $regex, $email ) )
	 { 
	 	return true;
	 }
	else { 
		return false;
	} 
}
function check_phone_number($phone)
{
if(strlen($phone) != 10)
{
	return false;
}
return true;
}
public function convert_number_to_words($number) {
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    if (!is_numeric($number)) {
        return false;
    }
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }
    if ($number < 0) {
        return $negative . $this->convert_number_to_words(abs($number));
    }
    $string = $fraction = null;
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . $this->convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= $this->convert_number_to_words($remainder);
            }
            break;
    }
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    return $string;
}
public function sendSMS($mobileno,$msgtext)
{
	$params="user=20082245&pwd=x66p4p&senderid=ONKAKA&mobileno=$mobileno&msgtext=$msgtext&smstype=0";	
	$uri = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
	$error_no='';
		 $ch = curl_init($uri);
		 curl_setopt($ch, CURLOPT_POST, 1);		 
		 curl_setopt($ch, CURLOPT_POSTFIELDS, $params);		 
		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 $resutl=curl_exec ($ch);		
		 		 		 		 
		 if ($error_no==0) {
		 	 return $resutl;
		 } else return false;			 
		 curl_close ($ch);	
}
public function print_response($json)
{
	header('Content-Type: application/json');
	print_r($json);
}
public function getOption($option_name,$merchant_id)
{
	$option_info = $this->CI->db->query("SELECT * FROM ".TBL."option WHERE merchant_id = $merchant_id AND option_name = '$option_name'")->row();
	return stripslashes($option_info->option_value);
}

public function get_opening_hours($merchant_id)
{
	$stores_open_day=$this->getOption("stores_open_day",$merchant_id);
		$stores_open_starts=$this->getOption("stores_open_starts",$merchant_id);
		$stores_open_ends=$this->getOption("stores_open_ends",$merchant_id);
		$stores_open_custom_text=$this->getOption("stores_open_custom_text",$merchant_id);
		
		$stores_open_day=!empty($stores_open_day)?(array)json_decode($stores_open_day):false;
		$stores_open_starts=!empty($stores_open_starts)?(array)json_decode($stores_open_starts):false;
		$stores_open_ends=!empty($stores_open_ends)?(array)json_decode($stores_open_ends):false;
		$stores_open_custom_text=!empty($stores_open_custom_text)?(array)json_decode($stores_open_custom_text):false;
		//echo "<pre>";print_r($stores_open_day);exit;
		
		$stores_open_pm_start=$this->getOption("stores_open_pm_start",$merchant_id);
		$stores_open_pm_start=!empty($stores_open_pm_start)?(array)json_decode($stores_open_pm_start):false;
		
		$stores_open_pm_ends=$this->getOption("stores_open_pm_ends",$merchant_id);
		$stores_open_pm_ends=!empty($stores_open_pm_ends)?(array)json_decode($stores_open_pm_ends):false;		
												
		$open_starts='';
		$open_ends='';
		$open_text='';
		$data='';
				
		if (is_array($stores_open_day) && count($stores_open_day)>=1){
			foreach ($stores_open_day as $val_open) {	
				if (array_key_exists($val_open,(array)$stores_open_starts)){
					$open_starts=$this->timeFormat($stores_open_starts[$val_open],true);
				}							
				if (array_key_exists($val_open,(array)$stores_open_ends)){
					$open_ends=$this->timeFormat($stores_open_ends[$val_open],true);
				}							
				if (array_key_exists($val_open,(array)$stores_open_custom_text)){
					$open_text=$stores_open_custom_text[$val_open];
				}					
				
				$pm_starts=''; $pm_ends=''; $pm_opens='';
				if (array_key_exists($val_open,(array)$stores_open_pm_start)){
					$pm_starts=$this->timeFormat($stores_open_pm_start[$val_open],true);
				}											
				if (array_key_exists($val_open,(array)$stores_open_pm_ends)){
					$pm_ends=$this->timeFormat($stores_open_pm_ends[$val_open],true);
				}												
				
				$full_time='';
				if (!empty($open_starts) && !empty($open_ends)){					
					$full_time=$open_starts." - ".$open_ends."&nbsp;&nbsp;";
				}			
				if (!empty($pm_starts) && !empty($pm_ends)){
					if ( !empty($full_time)){
						$full_time.=" / ";
					}				
					$full_time.="$pm_starts - $pm_ends";
				}												
								
				$data[]=array(
				  'day'=>$val_open,
				  'hours'=>$full_time,
				  'open_text'=>$open_text
				);
				
				$open_starts='';
		        $open_ends='';
		        $open_text='';
			}
			return $data;
		}			
		return false;
}
public function timeFormat($time='',$is_display=false)
	{
		if(empty($time)){
			return false;
		}
		$time_format = "12";
		switch ($time_format){
			case "12":
				if ( $is_display==true){
					return date("g:i A", strtotime($time));
				} else return date("G:i", strtotime($time));
				break;
			default:
				if ( $is_display==true){
					return date("G:i", strtotime($time));
				} else return date("G:i", strtotime($time));
				break;	
		}
		return $time;
	}
}
##end of class
?>