<?php
class Callback_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
		$this->validateToken();
	}
	
	public function validateToken()
	{
		$request_header = $this->input->request_headers();
		$authorization = $request_header['Authorization'];
		$authorization = explode(" ",$authorization);
		$token = $authorization[1];
		if($token == '')
		 {
		     header('Content-Type: application/json');
			 print_r(json_encode(array("status"=>false,"message"=>"Token not found")));
			 exit();
		 }
		$query = $this->db->where(array("token"=>$token))->where("expiry >=",date("Y-m-d H:i:s"))->get(TBL."api_token");
		if($query->num_rows() == 0)
		{
		    header('Content-Type: application/json');
			print_r(json_encode(array("status"=>false,"message"=>"Token has expired")));
			exit();
		}
	}
	
	public function customer_info($field,$value)
	{
		if($field == "contact_phone")
		{
			$this->db->like($field, $value, 'before'); 
		}
		else
		{
			$this->db->where($field, $value);
		}
		$customer_info = $this->db->get(TBL."client")->row();
		return $customer_info;
	}
	
	public function create_customer($customer_data)
	{
		$contact_phone = $customer_data["contact_phone"];
		if($contact_phone == '')
		{
			return print_r(json_encode(array("status"=>false,"message"=>"Mobile no is empty")));
		}
		if(!$this->general_functions->check_phone_number($contact_phone))
		{
			return print_r(json_encode(array("status"=>false,"message"=>"Please enter 10 digits mobile no")));
		}
		$this->db->insert(TBL."client",$customer_data);
		
		$client_id = $this->db->insert_id();
		$customer_info = $this->customer_info("client_id",$client_id);
		return $customer_info;
	}
	
	public function get_mearchants($search,$search_filter,$limit)
	{
		if($search['lat'] != '' && $search['lon'] != '')
		{
			$lat = $search['lat'];
			$lon = $search['lon'];
			$dist_sql = ",111.045 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(m.latitude)) * COS(RADIANS(m.lontitude) - RADIANS($lon)) + SIN(RADIANS($lat)) * SIN(RADIANS(m.latitude)))) AS distance";
		}
		$this->db->select("m.merchant_id,m.restaurant_name,m.street,cuisine,delivery_charges,minimum_order,delivery_charges,ratings,latitude,lontitude,o.option_value as 'delivery_time'$dist_sql");
		$this->db->from(TBL."view_merchant m");
		$this->db->join(TBL."item i","m.merchant_id = i.merchant_id");
		$this->db->join(TBL."option o","m.merchant_id = o.merchant_id");
		$this->db->where("m.status","active");
		$this->db->where(array("o.option_name"=>"merchant_delivery_estimation"));
		if($search['q'] != "")
		{
			$q = $search['q'];
			$this->db->where("(m.restaurant_name LIKE '%".$q."%' OR m.street LIKE '%".$q."%')");
		}
		if(is_array($search_filter['filter_cuisine']) && $search_filter['filter_cuisine'][0] != '')
		{
			$cuisine_where = "(";
			foreach($search_filter['filter_cuisine'] as $filter_cuisines)
			{
				$cuisine_where .= "m.cuisine LIKE '%\"".$filter_cuisines."\"%' OR ";
			}
			$cuisine_where = substr($cuisine_where,0,strlen($cuisine_where)-3);
			$cuisine_where .= ")";
			$this->db->where($cuisine_where);
		}
		if(is_array($search_filter['filter_delivery_time']) && $search_filter['filter_delivery_time'][0] != '')
		{
			$merchant_delivery_estimation_where = "(";
			foreach($search_filter['filter_delivery_time'] as $merchant_delivery)
			{
				$merchant_delivery_estimation_where .= "(o.option_name = 'merchant_delivery_estimation' AND o.option_value = '".$merchant_delivery."') OR ";
			}
			$merchant_delivery_estimation_where = substr($merchant_delivery_estimation_where,0,strlen($merchant_delivery_estimation_where)-3);
			$merchant_delivery_estimation_where .= ")";
			$this->db->where($merchant_delivery_estimation_where);
		}
		if(is_array($search_filter['filter_minimum_order']) && $search_filter['filter_minimum_order'][0] != '')
		{
			$minimum_order_where = "(";
			foreach($search_filter['filter_minimum_order'] as $minimum_orders)
			{
				$minimum_order_where .= "m.minimum_order_where = '".$minimum_orders."' OR ";
			}
			$minimum_order_where = substr($minimum_order_where,0,strlen($minimum_order_where)-3);
			$minimum_order_where .= ")";
			$this->db->where($minimum_order_where);
		}
		if(is_array($search_filter['filter_rating']) && $search_filter['filter_rating'][0] != '')
		{
			$ratings_where = "(";
			foreach($search_filter['filter_rating'] as $filter_ratings)
			{
				$ratings_where .= "m.ratings = '".$filter_ratings."' OR ";
			}
			$ratings_where = substr($ratings_where,0,strlen($ratings_where)-3);
			$ratings_where .= ")";
			$this->db->where($ratings_where);
		}
		if($search['is_featured'] == 1)
		{
			$this->db->where(array("m.is_featured"=>1));
		}

		$this->db->group_by("m.merchant_id");
		$this->db->limit(10, $limit);
		if($search['order_by'] == '')
		{
		if($search['lat'] != '' && $search['lat'] != '')
		{
			$this->db->order_by('distance','ASC');
		}
		}
		else
		{
			if($search['order_by'] == 'ratings')
			{
				$this->db->order_by('m.ratings','DESC');
			}
			if($search['order_by'] == 'minimum_order')
			{
				$this->db->order_by('m.minimum_order','ASC');
			}
			if($search['order_by'] == 'delivery_time')
			{
				$this->db->order_by('o.option_value','ASC');
			}
			
			
			
		}
		$result = $this->db->get()->result_array();
		$filter = array();
		$merchant_info = array();
		foreach($result as $r)
		{
			$merchant_photo = $this->db->where(array("merchant_id"=>$r['merchant_id'],"option_name"=>"merchant_photo"))->get(TBL."option")->row();
			$merchant_photo = $merchant_photo->option_value;
			$r['pic'] = "https://www.onlinekaka.com/upload/".$merchant_photo;
			$cuisine = $r['cuisine'];
			$cuisine = substr($cuisine,1,(strlen($cuisine)-2));
			$cuisine_info = explode(",",$cuisine);
			$cuisine_list = array();
			foreach($cuisine_info as $c)
			{
				$c = substr($c,1,(strlen($c)-2));
				$cuisine_detail = $this->db->where(array("cuisine_id"=>$c))->get(TBL."cuisine")->result_array();
				$cuisine_detail = $cuisine_detail[0];
				$cuisine_list[] = $cuisine_detail['cuisine_name'];
				$cuisine_ids[] = $c;
			}
			$r['cuisine_info'] = $cuisine_list;
			$merchant_info[] = $r;
			
		}
		$merchant_infos['list'] = $merchant_info;
		
		return $merchant_infos;
	}
	
	public function get_total_mearchants($search)
	{
		if($search['lat'] != '' && $search['lon'] != '')
		{
			$lat = $search['lat'];
			$lon = $search['lon'];
			$dist_sql = ",111.045 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(m.latitude)) * COS(RADIANS(m.lontitude) - RADIANS($lon)) + SIN(RADIANS($lat)) * SIN(RADIANS(m.latitude)))) AS distance";
		}
		$this->db->select("m.merchant_id,m.restaurant_name,m.street,cuisine,delivery_charges,minimum_order,delivery_charges,ratings,latitude,lontitude$dist_sql");
		$this->db->from(TBL."view_merchant m");
		$this->db->join(TBL."item i","m.merchant_id = i.merchant_id");
		$this->db->join(TBL."option o","m.merchant_id = o.merchant_id");
		$this->db->where("m.status","active");
		$this->db->where(array("o.option_name"=>"merchant_delivery_estimation"));
		if($search['q'] != "")
		{
			$q = $search['q'];
			$this->db->where("(m.restaurant_name LIKE '%".$q."%' OR m.street LIKE '%".$q."%')");
		}
		if($search['is_featured'] == 1)
		{
			$this->db->where(array("m.is_featured"=>1));
		}

		$this->db->group_by("m.merchant_id");
		if($search['lat'] != '' && $search['lat'] != '')
		{
			$this->db->order_by('distance','ASC');
		}
		else
		{
			$this->db->order_by('merchant_id','ASC');
		}
		$num_rows = $this->db->get()->num_rows();
		return $num_rows;
	}
	
	
	public function get_filter_mearchants($search)
	{
		if($search['lat'] != '' && $search['lon'] != '')
		{
			$lat = $search['lat'];
			$lon = $search['lon'];
			$dist_sql = ",111.045 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(m.latitude)) * COS(RADIANS(m.lontitude) - RADIANS($lon)) + SIN(RADIANS($lat)) * SIN(RADIANS(m.latitude)))) AS distance";
		}
		$this->db->select("m.merchant_id,m.restaurant_name,m.street,cuisine,delivery_charges,minimum_order,delivery_charges,ratings,latitude,lontitude,o.option_value as 'delivery_time'$dist_sql");
		$this->db->from(TBL."view_merchant m");
		$this->db->join(TBL."item i","m.merchant_id = i.merchant_id");
		$this->db->join(TBL."option o","m.merchant_id = o.merchant_id");
		$this->db->where("m.status","active");
		$this->db->where(array("o.option_name"=>"merchant_delivery_estimation"));
		if($search['q'] != "")
		{
			$q = $search['q'];
			$this->db->where("(m.restaurant_name LIKE '%".$q."%' OR m.street LIKE '%".$q."%')");
		}
		
		
		if($search['is_featured'] == 1)
		{
			$this->db->where(array("m.is_featured"=>1));
		}

		$this->db->group_by("m.merchant_id");
		
		$result = $this->db->get()->result_array();
		$filter = array();
		$merchant_info = array();
		foreach($result as $r)
		{
			$merchant_photo = $this->db->where(array("merchant_id"=>$r['merchant_id'],"option_name"=>"merchant_photo"))->get(TBL."option")->row();
			$merchant_photo = $merchant_photo->option_value;
			$r['pic'] = "https://www.onlinekaka.com/upload/".$merchant_photo;
			$cuisine = $r['cuisine'];
			$cuisine = substr($cuisine,1,(strlen($cuisine)-2));
			$cuisine_info = explode(",",$cuisine);
			$cuisine_list = array();
			foreach($cuisine_info as $c)
			{
				$c = substr($c,1,(strlen($c)-2));
				$cuisine_detail = $this->db->where(array("cuisine_id"=>$c))->get(TBL."cuisine")->result_array();
				$cuisine_detail = $cuisine_detail[0];
				$cuisine_list[] = $cuisine_detail['cuisine_name'];
				$cuisine_ids[] = $c;
			}
			$r['cuisine_info'] = $cuisine_list;
			$merchant_info[] = $r;
			$merchant_delivery_estimations[] = $r['delivery_time'];
			$minimum_orders[] = $r['minimum_order'];
			$ratings[] = $r['ratings'];
		}
		$cuisine_ids = array_unique($cuisine_ids);
		$minimum_orders = array_unique($minimum_orders);
		$merchant_delivery_estimations = array_unique($merchant_delivery_estimations);
		$ratings = array_unique($ratings);
		

		
		$counter = 0;
		foreach($cuisine_ids as $cuisine_id)
		{
			$cuisine_info = $this->common->select_scaler_where(TBL."cuisine",array("cuisine_id"=>$cuisine_id));
			$filter['cuisine'][$counter]['cuisine_id'] = $cuisine_info->cuisine_id;
			$filter['cuisine'][$counter]['cuisine_name'] = $cuisine_info->cuisine_name;
			$counter++;
		}
		$filter['delivery_time'] = $merchant_delivery_estimations;
		$filter['minimum_order'] = $minimum_orders;
		$filter['ratings'] = $ratings;
		
		return $filter;
	}
	
	public function get_merchant_info($merchant_id)
	{
		
		$this->db->select("m.*");
		$this->db->from(TBL."view_merchant m");
		$this->db->join(TBL."item i","m.merchant_id = i.merchant_id");
		$this->db->where(array("m.merchant_id"=>$merchant_id,"m.status"=>"active"));
		$result = $this->db->get()->result_array();
		$result = $result[0];
		$merchant_info = array();
		$merchant_photo = $this->db->where(array("merchant_id"=>$merchant_id,"option_name"=>"merchant_photo"))->get(TBL."option")->row();
		$merchant_photo = $merchant_photo->option_value;
		$result['pic'] = "https://www.onlinekaka.com/upload/".$merchant_photo;
		$cuisine = $result['cuisine'];
		$cuisine = substr($cuisine,1,(strlen($cuisine)-2));
		$cuisine_info = explode(",",$cuisine);
		$cuisine_list = array();
		foreach($cuisine_info as $c)
		{
			$c = substr($c,1,(strlen($c)-2));
			$cuisine_detail = $this->db->where(array("cuisine_id"=>$c))->get(TBL."cuisine")->result_array();
			$cuisine_list[] = $cuisine_detail[0];
		}
		$result['cuisine_info'] = $cuisine_list;
		$item_category_list = $this->db->where(array("merchant_id"=>$merchant_id))->group_by("category")->get(TBL."item")->result_array();
		$category_list = array();
		foreach($item_category_list as $item_category)
		{
			$category = trim($item_category['category']);
			$category = substr($category,2,(strlen($category)-4));
			$category_list[] = $category;
		}
		$item_list = array();
		$counter = 0;
		foreach($category_list as $c)
		{
		$category_info = $this->common->select_scaler_where(TBL."category",array("cat_id"=>$c));
			$item_list[$counter]['category_info'] = $category_info;
			$c = '["'.$c.'"]';
			$item_info = $this->db->where(array("merchant_id"=>$merchant_id,"category"=>$c))->get(TBL."item")->result_array();
			$item_list[$counter]['item_info'] = $item_info;
			$counter++;
		}
		$result['item_list'] = $item_list;
		
		$result['merchant_delivery_estimation'] = $this->general_functions->getOption('merchant_delivery_estimation',$merchant_id);
		
		$result['opening_hours'] = $this->general_functions->get_opening_hours($merchant_id);
		
		$merchant_review = $this->db->where(array("merchant_id"=>$merchant_id,"status"=>"publish"))->order_by("date_created")->get(TBL."review")->result_array();
		$result['review_list'] = $merchant_review;
		$merchant_info[] = $result;
		
		return $merchant_info;
	}
   
 }